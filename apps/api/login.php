<?php

include "conn.php";

$username = $_REQUEST['username'];
$password = $_REQUEST['password'];
// DEBUG
//$username = 'cusven1';
//$password = 'demo';

$output = array();

// USER

// $query_login = "SELECT * from View_User WHERE Email = '$username' AND Password = '$password'";
$query_login = "
SELECT u.ID, u.RoleID, u.LastLogin, u.Email, u.Type, u.Password, u.IDCustomer, 
    CASE
        WHEN u.IDVendor IS NULL OR u.IDVendor = ''
            THEN c.PairedVendorID
        WHEN u.IDVendor IS NOT NULL
            THEN u.IDVendor
    END as IDVendor 
FROM Ms_User u
    LEFT JOIN Ms_Customer AS c ON c.IDCustomer = u.IDCustomer
    WHERE u.RoleID = 5";

$result_login = sqlsrv_query($dbhandle, $query_login, array(), array( "Scrollable" => 'static' ));

$num_row = sqlsrv_num_rows($result_login);

if (sqlsrv_num_rows($result_login) == 1) {
	while ($row = sqlsrv_fetch_array($result_login))
	{
		$login_sukses = true;
	    $output["status"] = "sukses";
		$output["ID"] = $row["ID"];
		$output["Email"] = $row["Email"];
		$output["IDCustomer"] = $row["IDCustomer"];
		$output["IDVendor"] = $row["IDVendor"];
		$id_customer = $row["IDCustomer"];
		$id_vendor = $row["IDVendor"];
	}	
} else {
	$login_sukses = false;
    $output["status"] = "gagal";
}

// CUSTOMER NAME

if ($login_sukses) {
	$query_customer = "SELECT Customer from Ms_Customer WHERE IDCustomer = '$id_customer'";
	$result_customer = sqlsrv_query($dbhandle, $query_customer, array(), array( "Scrollable" => 'static' ));
	$num_row = sqlsrv_num_rows($result_customer);
	if (sqlsrv_num_rows($result_customer) == 1) {
		while ($row = sqlsrv_fetch_array($result_customer))
		{
		    $output["Customer"] = $row["Customer"];
		}
	} else {
		$output["Customer"] = "";
	}
}

// VENDOR NAME

if ($login_sukses) {
	$query_vendor = "SELECT Vendor from Ms_Vendor WHERE IDVendor = '$id_vendor'";
	$result_vendor = sqlsrv_query($dbhandle, $query_vendor, array(), array( "Scrollable" => 'static' ));
	$num_row = sqlsrv_num_rows($result_vendor);
	if (sqlsrv_num_rows($result_vendor) == 1) {
		while ($row = sqlsrv_fetch_array($result_vendor))
		{
		    $output["Vendor"] = $row["Vendor"];
		}
	} else {
		$output["Vendor"] = "";
	}
}

// PESAN

$query_pesan = "SELECT * FROM Pesan WHERE (IDRekanan = '$id_customer' OR IDRekanan = '$id_vendor') OR IsBoardcast = 1";

$result_pesan = sqlsrv_query($dbhandle, $query_pesan, array(), array( "Scrollable" => 'static' ));

$tmp = array();
$output_pesan = array();
while($row = sqlsrv_fetch_array($result_pesan)){
          $tmp["IDRekanan"] = $row["IDRekanan"];
          $tmp["Judul"] = $row["Judul"];
          $tmp["Pesan"] = $row["Pesan"];
          $tmp["StartDate"] = $row["StartDate"];
          $tmp["EndDate"] = $row["EndDate"];
          $tmp["Keterangan"] = $row["Keterangan"];
          array_push($output_pesan, $tmp);
}

// PANDUAN

$query_panduan = "SELECT * FROM Panduan";

$result_panduan = sqlsrv_query($dbhandle, $query_panduan, array(), array( "Scrollable" => 'static' ));

$tmp = array();
$output_panduan = array();
while($row = sqlsrv_fetch_array($result_panduan)){
          $tmp["Judul"] = $row["Judul"];
          $tmp["Deskripsi"] = $row["Deskripsi"];
          array_push($output_panduan, $tmp);
}

// NEWS

$query_news = "SELECT * FROM News";

$result_news = sqlsrv_query($dbhandle, $query_news, array(), array( "Scrollable" => 'static' ));

$tmp = array();
$output_news = array();
while($row = sqlsrv_fetch_array($result_news)){
          $tmp["ID"] = $row["ID"];
          $tmp["Judul"] = $row["Judul"];
          $tmp["Berita"] = $row["Berita"];
          $tmp["ImageURL"] = $row["ImageURL"];
          array_push($output_news, $tmp);
}


// JSON OUTPUT 

if ($login_sukses) {
	$output['pesan'] = $output_pesan; 
	$output['panduan'] = $output_panduan; 
	$output['news'] = $output_news; 
}

header('Content-Type: application/json');
echo json_encode($output);

?>
