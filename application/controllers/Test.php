<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Test extends CI_Controller
{
    private $statuses;

    public function index()
    {
        echo $this->getStatusApproval('8470166394');
    }

    public function getStatusApproval($billingDocument)
    {
        $this->db->reset_query();

        if ($this->statuses == null) {
            $this->statuses = $this->db->where_in('ID', [21,22,23,24])->get('Status')->result();

            $temp = [];
            foreach ($this->statuses as $status) {
                $temp[$status->ID] = $status->Status;
            }
            $this->statuses = $temp;
        }

        return $this->statuses[$billingDocument->Status];
    }
}
