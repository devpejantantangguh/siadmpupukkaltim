<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends CI_Controller
{
    public $updater_role = 4; //User Unit Kerja
    public $updater_role2 = 5; //Manage Unit Kerja
    public $updater_role3; //Superintendent
    function __construct()
    {
        parent::__construct();
        Auth::guard();
        $this->load->model('m_vl');
        $this->load->model('invoice/m_input');
        $this->load->model('m_invoice');
        $this->load->model('m_po');
        $this->updater_role3 = roleID('Superintendent');

    }

    public function index()
    {
        $this->blade->view('invoice.invoice.index', array(
            'table'     => 'Invoice',
            'role'              => (int) Auth::roleID(),
            'updater_role'      => $this->updater_role,
            'statuses'  => $this->m_vl->get_status(1)
        ));
    }
    public function all()
    {
        $this->blade->view('invoice.invoice.all', array(
            'table'     => 'Invoice',
            'statuses'  => $this->m_vl->get_status(1)
        ));
    }

    public function get($status = null, $optional = null)
    {
        //dd(Auth::UnitKerja()->UnitKerja);
        //dd(Auth::user()->RoleName);
        $roleID = (int) Auth::roleID();
        $area = Auth::unitKerja()->Area;
        $matriks= Auth::matriksUnitKerja(true);

        if($optional == 'po'){
            $lists = $this->db->where('Kategori',1)->where('Keterangan',NULL)->get('VL_JenisTransaksi')->result();
            $jt = [];
            foreach($lists as $list) {
                $jt[] = $list->ID;
            }
            $this->db->where_in('JenisTransaksi', $jt);
        }

        if(strpos($status,'-') !== false) {
            $xstatus = explode('-', $status);
            $this->db->where_in('Status', $xstatus);
        }elseif(empty($status)) {

        }else {
            $this->db->where('Status', $status);
        }


        if($this->updater_role == $roleID OR $this->updater_role2 == $roleID OR $this->updater_role3 == $roleID){
            $this->db->where_in('i.CostCenter', $matriks);
        }
        $search = isset($_GET['filter']) ? $_GET['filter'] : '';
        if(strpos($search, '["ID"') !== false) {
            $search = '';
        }
        $search = !empty($search) ? json_decode($search) : '';
        $limit = isset($_GET['take']) ? $_GET['take'] : 10;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;
        if(!empty($search)) {
            Filter::dxDataGridFilters($search, $this->db);
        }
        if ($area=='Pusat') {
            Filter::areaFilter('i', $this->db);
        }else{
            if($this->updater_role == $roleID){
                Filter::areaFilter('i', $this->db,'custom');
            }else{
                Filter::areaFilter('i', $this->db);
            }
        }
        

        if(isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        } else {
            $this->db->order_by('i.ID', 'DESC');
        }

        $tempdb = clone $this->db; //clone for count all rows
        $count= $tempdb->from('Vw_AllInvoice i')->count_all_results();
        $items = $this->db
                    ->get('Vw_AllInvoice i', $limit, $offset)->result();

        foreach ($items as $item) {
            $item->ID = encode_url($item->ID);
            //$key->ID = $this->encrypt->encode($key->ID);
        }
        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $items
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }


    public function getOLD($status = null, $optional = null)
    {
        //dd(Auth::UnitKerja()->UnitKerja);
        //dd(Auth::user()->RoleName);
        $roleID = (int) Auth::roleID();
        $matriks= Auth::matriksUnitKerja(true);

        if($optional == 'po'){
            $lists = $this->db->where('Kategori',1)->where('Keterangan',NULL)->get('VL_JenisTransaksi')->result();
            $jt = [];
            foreach($lists as $list) {
                $jt[] = $list->ID;
            }
            $this->db->where_in('JenisTransaksi', $jt);
        }

        if(strpos($status,'-') !== false) {
            $xstatus = explode('-', $status);
            $this->db->where_in('Status', $xstatus);
        }elseif(empty($status)) {

        }else {
            $this->db->where('Status', $status);
        }


        if($this->updater_role == $roleID OR $this->updater_role2 == $roleID OR $this->updater_role3 == $roleID){
            $this->db->where_in('i.CostCenter', $matriks);
        }
        $search = isset($_GET['filter']) ? $_GET['filter'] : '';
        if(strpos($search, '["ID"') !== false) {
            $search = '';
        }
        $search = !empty($search) ? json_decode($search) : '';
        $limit = isset($_GET['take']) ? $_GET['take'] : 10;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;
        if(!empty($search)) {
            Filter::dxDataGridFilters($search, $this->db);
        }
        $area = Auth::unitKerja()->Area;
        if ($area=='Pusat') {
            Filter::areaFilter('i', $this->db);
        }else{
            Filter::areaFilter('i', $this->db,'custom');
        }

        if(isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        } else {
            $this->db->order_by('i.ID', 'DESC');
        }

        $tempdb = clone $this->db; //clone for count all rows
        $count= $tempdb->from('Invoice i')->count_all_results();
        $items = $this->db
                    ->select('i.*,Ms_UnitKerja.UnitKerja,ms_karyawan.Nama')
                    ->join('Ms_UnitKerja','i.CostCenter = Ms_UnitKerja.CostCenter')
                    ->join('ms_user','i.CreatedBy = ms_user.ID','LEFT')
                    ->join('ms_karyawan','ms_user.IDKaryawan = ms_karyawan.ID','LEFT')
                    ->get('Invoice i', $limit, $offset)->result();

        foreach ($items as $item) {

            $item->Catatan = $this->m_invoice->getLastKeterangan($item->ID);
            $item->ID = encode_url($item->ID);
            //$key->ID = $this->encrypt->encode($key->ID);
        }
        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $items
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function get_all($status = null, $optional = null)
    {
        $search = isset($_GET['filter']) ? json_decode($_GET['filter']) : '';
        $limit = isset($_GET['take']) ? $_GET['take'] : 10;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;
        if(!empty($search)) {
            Filter::dxDataGridFilters($search, $this->db);
        }

        Filter::areaFilter('i', $this->db,'custom');
        if(isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        } else {
            $this->db->order_by('i.ID', 'DESC');
        }

        $tempdb = clone $this->db; //clone for count all rows
        $count= $tempdb->from('Vw_AllInvoice i')->count_all_results();
        $items = $this->db
                    ->get('Vw_AllInvoice i', $limit, $offset)->result();

        foreach ($items as $item) {
            $item->Keterangan = $this->m_invoice->getLastKeterangan($item->ID);
            $item->ID = encode_url($item->ID);
        }
        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $items
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function create()
    {
        $this->blade->view('invoice/invoice/create',[
            'list_transaksi'    => $this->m_vl->get_jenis_transaksi('po'),
            'list_kurs'         => $this->m_vl->get_kurs(),
        ]);
    }

    public function store($id = false)
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            notif('error', 'Anda tidak berhak mengubah data.');
            redirect('invoice/invoice/');
            die();
        }
        /* No Invoice dan No Faktur Pajak Harus Unique */
        $this->load->library('form_validation');
        $this->form_validation->set_rules('NoInvoice','Nomor Invoice','required|is_unique[Invoice.NoInvoice]');
        $this->form_validation->set_message('is_unique', '%s sudah pernah digunakan');
        //Update 18 Dec, kecuali 0000000000000000
        if($this->input->post('NoFakturPajak') != "0000000000000000"){
            $this->form_validation->set_rules('NoFakturPajak','Nomor Faktur Pajak','required|is_unique[Invoice.NoFakturPajak]');
        }

        // No Invoice dan NoFakturPajak Harus Unique dan Jika Status = Canceled dan LifeCycle = 1 maka ignore
        $vInvoice = $this->db->where('NoInvoice', $this->input->post('NoInvoice'))->where('Status !=',9)->where('LifeCycle !=', 1)->get('Invoice')->num_rows();
        if($vInvoice) notif('error_invoice','No Invoice sudah pernah digunakan');

        $vFakturPajak = $this->db->where('NoFakturPajak', $this->input->post('NoFakturPajak'))->where('Status !=',9)->where('LifeCycle !=', 1)->get('Invoice')->num_rows();
        # bypass jika NoFakturPajak 0000000000000000
        if($this->input->post('NoFakturPajak') == '0000000000000000')  {
            $vFakturPajak = 0;
        } else {
            if($vFakturPajak) notif('error_faktur_pajak','No Faktur Pajak sudah pernah digunakan');
        }

        if($vInvoice != 0 || $vFakturPajak != 0) {
            return $this->blade->view('invoice/invoice/create',[
                'list_transaksi'    => $this->m_vl->get_jenis_transaksi('po'),
                'list_kurs'         => $this->m_vl->get_kurs(),
            ]);
        }

        $this->db->trans_start();

        $data = $_POST;
        $data['CostCenter'] = $data['CostCenter'];
        $data['LifeCycle'] = false;
        $data['Piutang'] = false;
        $data['Status'] = 1;
        list($d,$m,$y) = explode('-', $data['TglInvoice']);
        $data['TglInvoice'] = date('Y-m-d', mktime(0,0,0,$m,$d,$y));
        list($d,$m,$y) = explode('-', $data['TglFakturPajak']);
        $data['TglFakturPajak'] = date('Y-m-d', mktime(0,0,0,$m,$d,$y));
        $data['NilaiInvoice'] = str_replace(',','.',str_replace('.','', $data['NilaiInvoice']));
        $data['Tahun'] = date('Y');
        $data['IsPriority'] = false;
        $data['NamaVendor'] = $this->m_input->getNamaVendor($data['IDVendor']);
        $data['Keterangan'] = $this->input->post('Keterangan');
        $data['CreatedOn'] = date('Y-m-d H:i:s');
        $data['CreatedBy'] = Auth::user()->ID;
        if(!$id){
            $id = $this->m_input->createInvoice($data);
            //$this->m_invoice->createHistory($id,actionID('Create'), '');
            $this->m_invoice->newHistory($id,actionID('Create'),statusID('DRAFT'), '');
            $this->m_invoice->setIDArea($id);
            $this->db->trans_complete();

            notif('success', 'Data berhasil ditambahkan.');
        }else{
            $this->db->where('ID',$id)->update('Invoice',$data);
            //$this->m_invoice->createHistory($id,actionID('Draft'), '');
            $this->m_invoice->newHistory($id,actionID('Draft'),statusID('DRAFT'), '');
            $this->db->trans_complete();

            notif('success', 'Data berhasil diubah.');
        }

        redirect('invoice/invoice/edit/'.encode_url($id));
    }

    public function edit($id = false)
    {
        if(!$id){
            show_404();
            die();
        }

        if(isset($_GET['setAsDraft'])) {
            //$this->m_invoice->createHistory(decode_url($id),1, '');
            $this->m_invoice->newHistory(decode_url($id),1,statusID('DRAFT'), '');
            $this->db->where('ID', decode_url($id))->update('Invoice',['Status' => 1]);
        }

        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            notif('error', 'Anda tidak berhak mengubah data.');
            redirect('invoice/invoice/');
            die();
        }

        $encode_id = $id;
        $id = decode_url($id);
        $attachments = $this->db->where('IDParent', $id)->get('Attachment')->result();
        $data = $this->db->where('ID',$id)->get('Invoice')->row();

        if(!$data){
            show_404();
            die();
        }

        if($data->Status != 1 && $data->Status != 2){
            redirect('invoice/invoice/view/'.encode_url($id));
            die();
        }

        $view = $data->Status == 9 ? 'cancel' : 'edit';

        # mengembalikan ke format indo supaya input mask berjalan smooth
        $data->NilaiInvoice = str_replace('.',',', $data->NilaiInvoice);
        $this->blade->view('invoice.invoice.'.$view,[
            'table'             => 'Invoice',
            'id'                => $id,
            'status'            => $data->Status,
            'attachments'       => $attachments,
            'data'              => $data,
            'list_transaksi'    => $this->m_vl->get_jenis_transaksi('po'),
            'list_kurs'         => $this->m_vl->get_kurs(),
            'histories'         => $this->m_invoice->getHistory($id),
            'flash_message'     => $this->session->flashdata('error'),
        ]);
    }

    public function cancel()
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            notif('error', 'Anda tidak berhak mengubah data.');
            die();
        }
        $id = ($this->input->post('id'));
        if($id){
        $encode_id = $id;
        $id = decode_url($id);
        $this->db->where('IDParent', $id)->delete('Attachment');
        $this->db->where('IDInvoice', $id)->delete('RelatedInvoice');
        $this->db->where('ID',$id)->update('Invoice',array('LifeCycle' => true,'Status' =>9));
        //$this->m_invoice->createHistory($id,27, '');
        $this->m_invoice->newHistory($id,27,statusID('CANCELED'), '');
        notif('success', 'Data berhasil dicancel.');
        }

    }

    public function view($id = false)
    {
        $encode_id = $id;
        $id = decode_url($id);
        $data = $this->m_po->get_detail_invoice($id);

        # mengembalikan ke format indo supaya input mask berjalan smooth
        $data->NilaiInvoice = str_replace('.',',', $data->NilaiInvoice);
        $data->NilaiVerifikasi = str_replace('.',',', $data->NilaiVerifikasi);

        if(!$data){
            show_404();
            die();
        }
        $list_transaksi = $this->m_vl->get_jenis_transaksi('po');
        $documents = $this->m_po->get_kelengkapan_dokumen($id);
        $current_note = $this->m_po->get_current_note($id);
        $last_note = $this->m_po->get_last_note($id);
        $table = 'Invoice';
        $list_kurs = $this->m_vl->get_kurs();
        $list_payment_term = $this->m_vl->get_payment_term();
        $attachments = $this->db->where('IDParent', $id)->get('Attachment')->result();
        //$Keterangan = $this->m_invoice->getLastKeterangan($id);
        $histories = $this->m_invoice->getHistory($id);

        $this->blade->view('invoice.invoice.view', compact('data','table','documents','id','list_transaksi','current_note','last_note','list_kurs','list_payment_term','attachments','histories'));
    }

    public function update_detail($status = false)
    {
        # No Invoice Tidak Boleh Sama, Kecuali Status Canceled (9)
        if($this->input->post('NoInvoice_OLD') != $this->input->post('NoInvoice')){
            $hasNoInvoice = $this->db->where('NoInvoice',$this->input->post('NoInvoice'))->where('Status !=', 9)->get('Invoice');
            if($hasNoInvoice->num_rows() > 0) {
                echo "Nomor Invoice sudah digunakan.";
                return;
            }
        }

        # No Faktur Pajak Tidak Boleh Sama, Kecuali Status Canceled (9)
        if($this->input->post('NoFakturPajak_OLD') != $this->input->post('NoFakturPajak')){
            $hasNoFakturPajak = $this->db->where('NoFakturPajak',$this->input->post('NoFakturPajak'))->where('Status !=',9)->get('Invoice');
            if($hasNoFakturPajak->num_rows() > 0) {
                echo "No Faktur Pajak sudah digunakan.";
                return;
            }
        }

        list($d,$m,$y) = explode('-', $this->input->post('TglInvoice'));
        $tglInvoice = date('Y-m-d', mktime(0,0,0,$m,$d,$y));

        list($d,$m,$y) = explode('-', $this->input->post('TglFakturPajak'));
        $TglFakturPajak = date('Y-m-d', mktime(0,0,0,$m,$d,$y));

        # convert format indo menjadi format sql 900.000,99 => 900000.99
        $nilaiInvoice = str_replace(',','.',str_replace('.','',$this->input->post('NilaiInvoice')));

        $id = $this->input->post('id');
        $this->db->where('id', $id)->update('Invoice',[
            'NoInvoice' => $this->input->post('NoInvoice'),
            'CostCenter' => $this->input->post('CostCenter'),
            'TglInvoice' => $tglInvoice,
            'NilaiInvoice' => $nilaiInvoice,
            'KursInvoice' => $this->input->post('KursInvoice'),
            'NoFakturPajak' => $this->input->post('NoFakturPajak'),
            'TglFakturPajak' => $TglFakturPajak,
            'Keterangan' => $this->input->post('Keterangan')
        ]);
        if($status != false){
            $this->db->where('ID',$id)->update('Invoice',['Status' => $status]);
        }

        echo 'success';
    }

    public function setDraft($id = false)
    {
        if(!$id){
            show_404();
            die();
        }
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            notif('error', 'Anda tidak berhak mengubah data.');
            redirect('invoice/invoice/');
            die();
        }

        $encode_id = encode_url($id);
        $status = statusID('DRAFT');
        $action = actionID('Draft');//draft


        $this->db->trans_start();
        $data['Status'] = $status;
        $res = $this->db->where('ID',$id)->update('Invoice',$data);

        $this->db->trans_complete();
        if($res){

            //$this->m_invoice->createHistory($id,$action, '');
            $this->m_invoice->newHistory($id,$action,$status, '');

            notif('success', 'Invoice berhasil disimpan sebagai Draft');
            redirect('invoice/invoice/');
        }else{
            notif('error', 'Data gagal diubah');
            redirect('invoice/invoice/edit/'.$encode_id);
        }
    }

    public function update($id = false, $is_manual = 0)
    {
        if(!$id){
            show_404();
            die();
        }
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            notif('error', 'Anda tidak berhak mengubah data.');
            redirect('invoice/invoice/');
            die();
        }

        $this->db->select('r.NoPO')
                ->where('i.ID', $id)
                ->where('r.PO_Manual', 0)
                ->join('RelatedInvoice r', 'i.ID = r.IDInvoice')
                ->join('Ms_User', 'i.CreatedBy = Ms_User.ID')
                ->join('Ms_Karyawan', 'Ms_User.IDKaryawan = Ms_Karyawan.ID');
        $listpo = $this->db->group_by('r.NoPO')
                            ->get('Invoice i')->result();
        $jaminan = [];

        foreach ($listpo as $key) {
             $simona = $this->load->database('simona', TRUE);
             $simona->where('i.NO_PO', $key->NoPO);
             $res = $simona->get('tr_performance_bond i')->result_array();

             if(count($res) < 1){
                notif('error', 'Jaminan PO tidak lengkap');
                redirect('invoice/invoice/edit/'.encode_url($id));
                die();
            }
        }

        $encode_id = encode_url($id);
        $this->db->trans_start();
        $data['Status'] =  statusID('CONFIRMED');
        $res = $this->db->where('ID',$id)->update('Invoice',$data);

        $this->db->trans_complete();
        if($res){

            //$this->m_invoice->createHistory($id,actionID('Confirm'), '');
            $this->m_invoice->newHistory($id,actionID('Confirm'),statusID('CONFIRMED'), '');

            notif('success', 'Invoice berhasil di-confirm');
            redirect('invoice/invoice/');
        }else{
            notif('error', 'Data gagal diubah');
            redirect('invoice/invoice/edit/'.$encode_id);
        }
    }

    public function upload($id)
    {
        if(!$id){
            show_404();
            die();
        }
        $encode_id = encode_url($id);
        $result = $this->m_input->uploadAttachment($id);

        if($result == false) {
            notif('error', 'Upload gagal.');
            redirect('invoice/invoice/edit/'. $encode_id.'?status=error_upload');
        } else {
            $uid = (int) Auth::user()->ID;

            $this->m_input->createAttachment($id, $result);
            $this->db->where('ID',$id)->update('Invoice',[
                'UpdatedBy' => $uid,
                'UpdatedOn' => date('Y-m-d H:i:s')
            ]);
        }
        notif('success', 'Upload berhasil');
        redirect('invoice/invoice/edit/'. $encode_id);
    }
}
