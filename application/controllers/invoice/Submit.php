<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Submit extends CI_Controller
{

    public $updater_role; //Superintendet
    function __construct() {
        parent::__construct();
        Auth::guard();
        $this->load->model('m_vl');
        $this->load->model('m_invoice');
        $this->load->library('email');
        $this->updater_role = roleID('Superintendent');
    }

    public function index() {
        echo $this->blade->view('invoice.submit.index', array(
            'table'         => 'Invoice',
            'list_status'   => $this->m_vl->get_status(1),
        ));
    }

    public function submit()
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            echo $this->updater_role.'Anda tidak berhak mengubah data.';
            die();
        }
        $row_aff = 0;
        $count = 0;
        $uid = Auth::user()->ID;
        $confirmed = statusID('CONFIRMED');
        $waiting = statusID('WAITING APPROVAL');
        $submit = actionID('Submit');
        $ids = $this->input->post('ids');
        $ids = $this->m_invoice->filter_ids_bystatus($ids, $confirmed);
        foreach ($ids as $key => $id) {
            $ids[$key] = decode_url($id);
            //$this->m_invoice->createHistory(decode_url($id), $submit, '');
            $this->m_invoice->newHistory(decode_url($id), $submit,$waiting, '');
        }
        if(!empty($ids)){
            $this->db->where_in('ID',$ids);
            $this->db->where('Status',$confirmed)
                    ->update('Invoice',array('Status' => $waiting,'SubmittedBy' => $uid,'SubmittedOn' => date('Y-m-d H:i:s')));

            $row_aff = $this->db->affected_rows();
            $count = count($ids);
        }


        echo $row_aff . ' dari ' . $count . ' invoice berhasil disubmit';
    }

    public function sentEmailSubmit()
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            echo 'Anda tidak berhak mengubah data.';
            die();
        }
        $ids = $this->input->post('ids');
        foreach ($ids as $key => $id) {
            $ids[$key] = decode_url($id);

            if($this->m_invoice->isKirimEmail(decode_url($id))){
                $res = $this->m_invoice->getEmailManager(decode_url($id));

                foreach ($res as $value) {
                    $from_email = "no-reply@mitraku.pupukkaltim.com"; 
                    /*Send Email when reject*/
                    $invoice = $this->m_invoice->getDetail(decode_url($id));
                     //$to_email = "sony.candra@pupukkaltim.com"; 
                     $to_email = $value->Email;
                     $data = array(
                        'NamaVendor' => $invoice->NamaVendor,
                        'NoInvoice' => $invoice->NoInvoice,
                        'NilaiInvoice' => 'Rp. '.number_format($invoice->NilaiInvoice,2,',','.'),
                        'Keterangan' => '',
                     );
                    $mesg = $this->load->view('templates/email-submit',$data, true);
                     //Load email library 
                     $this->load->library('email'); 
               
                     $this->email->from($from_email, 'Mitraku'); 
                     $this->email->to($to_email);
                     $this->email->subject('Pending Approval Pengajuan Pembayaran Invoice Vendor'); 
                     $this->email->message($mesg); 
               
                     //Send mail 
                     if($this->email->send());
                     $msg = 'success';
                 }
            }else{
                $msg = 'no-sent';
            }
        }
        echo $msg;
    }
    public function setDraft()
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            echo 'Anda tidak berhak mengubah data.';
            die();
        }
        $row_aff = 0;
        $count = 0;
        $draft = statusID('DRAFT');
        $revisi = actionID('Revisi');
        $ids = $this->input->post('ids');
        foreach ($ids as $key => $id) {
            $ids[$key] = decode_url($id);
            //$this->m_invoice->createHistory(decode_url($id), $revisi, '');
            $this->m_invoice->newHistory(decode_url($id), $revisi, $draft, '');
        }
        if(!empty($ids)){
            $this->db->where_in('ID',$ids)
                    ->update('Invoice',array('Status' => $draft));

            $row_aff = $this->db->affected_rows();
            $count = count($ids);
        }


        echo $row_aff . ' dari ' . $count . ' invoice berhasil direvisi';
    }
    public function sentEmail()
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            echo 'Anda tidak berhak mengubah data.';
            die();
        }
        $row_aff = 0;
        $count = 0;
        $draft = statusID('DRAFT');
        $revisi = actionID('Revisi');
        $ids = $this->input->post('ids');
        foreach ($ids as $key => $id) {
            $ids[$key] = decode_url($id);
            $from_email = "no-reply@mitraku.pupukkaltim.com"; 
            /*Send Email when reject*/
            $invoice = $this->m_invoice->getDetail(decode_url($id));
             //$to_email = "sony.candra@pupukkaltim.com"; 
            $to_email = $this->m_invoice->getEmailCreator($id);
             $data = array(
                'NamaVendor' => $invoice->NamaVendor,
                'NoInvoice' => $invoice->NoInvoice,
                'NilaiInvoice' => 'Rp. '.number_format($invoice->NilaiInvoice,2,',','.'),
                'Keterangan' => '',
             );
            $mesg = $this->load->view('templates/email',$data, true);
             //Load email library 
             $this->load->library('email'); 
       
             $this->email->from($from_email, 'Mitraku'); 
             $this->email->to($to_email);
             $this->email->subject('Revisi pengajuan pembayaran Invoice No. '.$invoice->NoInvoice); 
             $this->email->message($mesg); 
       
             //Send mail 
             if($this->email->send());
        }
        echo 'Email terkirim.';
    }

    public function revisi()
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            echo('Anda tidak berhak mengubah data.');

            die();
        }
        $ids = $this->input->post('ids');

        $this->db->where_in('ID',$ids)
                ->where('Status',1)
                ->update('Invoice',array('Status' => 1));

        $row_aff = $this->db->affected_rows();
        $count = count($ids);
        echo $row_aff . ' dari ' . $count . ' Dokumen Berhasil di Update Status menjadi Draft';
    }

    public function view($invoice_id)
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            notif('error', 'Anda tidak berhak mengubah data.');
            redirect('invoice/submit/');
            die();
        }
        $encode_id = $invoice_id;
        $invoice_id = decode_url($invoice_id);
        $data = $this->db->where('ID', $invoice_id)->get('Invoice')->row();
        $data->NilaiInvoice = str_replace('.', ',', $data->NilaiInvoice);
        if(!$data){
            show_404();
            die();
        }
        $attachments = $this->db->where('IDParent', $invoice_id)->get('Attachment')->result();

        $this->blade->view('invoice.submit.view',[
            'id'              => $invoice_id,
            'data'              => $data,
            'status'            => $data->Status,
            'list_transaksi'    => $this->m_vl->get_jenis_transaksi('po'),
            'list_kurs'         => $this->m_vl->get_kurs(),
            'attachments'       => $attachments,
            'table'             => 'DaftarPO',
            'histories'         => $this->m_invoice->getHistory($invoice_id)
        ]);
    }

    public function reject_update($id)
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            notif('error', 'Anda tidak berhak mengubah data.');
            redirect('invoice/submit/');
            die();
        }
        if(!(int)$id){
            $encode_id = $id;
            $id = decode_url($id);
        }

        $this->db->trans_start();
        $confirmed = statusID('CONFIRMED');
        $draft = statusID('DRAFT');
        $revisi = actionID('Revisi');
        $this->db->where('ID',$id)
            ->where('Status',$confirmed) // update status confirmed
            ->update('Invoice', array('Status' => $draft));
        $isSuccess = $this->db->affected_rows();

        $Keterangan = $this->input->post('Keterangan');
        $this->db->trans_complete();

        if($isSuccess) {
            //$this->m_invoice->createHistory($id, $revisi, $Keterangan);
            $this->m_invoice->newHistory($id, $revisi, $draft, $Keterangan);

            notif('success', 'Invoice berhasil direvisi');

            $from_email = "no-reply@mitraku.pupukkaltim.com"; 
            /*Send Email when reject*/
            $invoice = $this->m_invoice->getDetail($id);
             //$to_email = "sony.candra@pupukkaltim.com"; 
             $to_email = $this->m_invoice->getEmailCreator($id);
             $data = array(
                'NamaVendor' => $invoice->NamaVendor,
                'NoInvoice' => $invoice->NoInvoice,
                'NilaiInvoice' => 'Rp. '.number_format($invoice->NilaiInvoice,2,',','.'),
                'Keterangan' => $Keterangan,
             );
            $mesg = $this->load->view('templates/email',$data, true);
             //Load email library 
             $this->load->library('email'); 
       
             $this->email->from($from_email, 'Mitraku'); 
             $this->email->to($to_email);
             $this->email->subject('Revisi pengajuan pembayaran Invoice No. '.$invoice->NoInvoice); 
             $this->email->message($mesg); 
       
             //Send mail 
             if($this->email->send());
            redirect('invoice/submit/index?status=reject_ok');
        }else{
            notif('error', 'Invoice gagal direvisi');
            redirect('invoice/submit/index?status=reject_failed');
        }
    }

    public function update($id)
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            notif('error', 'Anda tidak berhak mengubah data.');
            redirect('invoice/submit/');
            die();
        }
        $uid = Auth::user()->ID;
        $confirmed = statusID('CONFIRMED');
        $waiting = statusID('WAITING APPROVAL');
        $submit = actionID('Submit');
        $this->db->where('ID',$id)
            ->where('Status',$confirmed) // update status hanya yang draft
            ->update('Invoice', array('Status' => $waiting,'SubmittedBy' => $uid,'SubmittedOn' => date('Y-m-d H:i:s')));
        $isSuccess = $this->db->affected_rows();

        if($isSuccess > 0) {
            //$this->m_invoice->createHistory($id, $submit, '');
            $this->m_invoice->createHistory($id, $submit, $waiting, '');
            notif('success', 'Submit berhasil');

                /*Start Email*/
                if($this->m_invoice->isKirimEmail(($id))){
                    $res = $this->m_invoice->getEmailManager(($id));

                    foreach ($res as $value) {
                        $from_email = "no-reply@mitraku.pupukkaltim.com"; 
                        /*Send Email when reject*/
                        $invoice = $this->m_invoice->getDetail(($id));
                         //$to_email = "sony.candra@pupukkaltim.com"; 
                         $to_email = $value->Email;
                         $data = array(
                            'NamaVendor' => $invoice->NamaVendor,
                            'NoInvoice' => $invoice->NoInvoice,
                            'NilaiInvoice' => 'Rp. '.number_format($invoice->NilaiInvoice,2,',','.'),
                            'Keterangan' => '',
                         );
                        $mesg = $this->load->view('templates/email-submit',$data, true);
                         //Load email library 
                         $this->load->library('email'); 
                   
                         $this->email->from($from_email, 'Mitraku'); 
                         $this->email->to($to_email);
                         $this->email->subject('Pending Approval Pengajuan Pembayaran Invoice Vendor'); 
                         $this->email->message($mesg); 
                   
                         //Send mail 
                         if($this->email->send());
                         $msg = 'success';
                     }
                }else{
                    $msg = 'no-sent';
                }
            /*End Email*/
            redirect("invoice/submit?status=success&id=".encode_url($id));
        }else{
            notif('error', 'submit gagal');
            redirect("invoice/submit?status=error&id=".encode_url($id));
        }

    }

}
