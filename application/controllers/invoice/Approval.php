<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approval extends CI_Controller
{

    public $updater_role = 5; //Manager Unit Kerja
    public $updater_role_2 = 2; //Manager Keuangan
    function __construct() {
        parent::__construct();
        Auth::guard();
        $this->load->model('m_vl');
        $this->load->model('m_invoice');
    }

    public function index() {
        echo $this->blade->view('invoice.approval.index', array(
            'table'         => 'Invoice',
            'list_status'   => $this->m_vl->get_status(1),
        ));
    }

    public function approve()
    {
        $waiting_approval = statusID('WAITING APPROVAL');
        $approved = statusID('APPROVED');
        $approve_atasan = actionID('Approve Atasan');
        $roleID = (int) Auth::roleID();
        if($this->updater_role == $roleID || $this->updater_role_2 == $roleID ){
            
        }else{
            echo 'Anda tidak berhak mengubah data.';
            die();
        }
        $row_aff = 0;
        $count = 0;
        $ids = $this->input->post('ids');
        $ids = $this->m_invoice->filter_ids_bystatus($ids, $waiting_approval);
        foreach ($ids as $key => $id) {
            $ids[$key] = decode_url($id);
            //$this->m_invoice->createHistory(decode_url($id), $approve_atasan, '');
            $this->m_invoice->newHistory(decode_url($id), $approve_atasan, $approved, '');
        }
        if(!empty($ids)){
            $this->db->where_in('ID',$ids);
            $this->db->where('Status',$waiting_approval)
                    ->update('Invoice',array('Status' => $approved));

            $row_aff = $this->db->affected_rows();
            $count = count($ids);
        }


        echo 'Invoice berhasil diapprove';
    }
    public function setDraft()
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role == $roleID || $this->updater_role_2 == $roleID ){
            
        }else{
            echo 'Anda tidak berhak mengubah data.';
            die();
        }
        $row_aff = 0;
        $count = 0;
        $ids = $this->input->post('ids');
        foreach ($ids as $key => $id) {
            $ids[$key] = decode_url($id);
            //$this->m_invoice->createHistory(decode_url($id), 8, '');
            $this->m_invoice->newHistory(decode_url($id), 8,statusID('DRAFT'), '');
        }
        if(!empty($ids)){
            $this->db->where_in('ID',$ids)
                    ->update('Invoice',array('Status' => 1));

            $row_aff = $this->db->affected_rows();
            $count = count($ids);
        }


        echo $row_aff . ' dari ' . $count . ' invoice berhasil direvisi';
    }
    public function sentEmail()
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role == $roleID || $this->updater_role_2 == $roleID ){
            
        }else{
            echo 'Anda tidak berhak mengubah data.';
            die();
        }
        $row_aff = 0;
        $count = 0;
        $draft = statusID('DRAFT');
        $revisi = actionID('Revisi');
        $ids = $this->input->post('ids');
        foreach ($ids as $key => $id) {
            $ids[$key] = decode_url($id);
            $from_email = "no-reply@mitraku.pupukkaltim.com"; 
            /*Send Email when reject*/
            $invoice = $this->m_invoice->getDetail(decode_url($id));
             //$to_email = "sony.candra@pupukkaltim.com"; 

             $to_email = $this->m_invoice->getEmailCreator(decode_url($id));
             $data = array(
                'NamaVendor' => $invoice->NamaVendor,
                'NoInvoice' => $invoice->NoInvoice,
                'NilaiInvoice' => 'Rp. '.number_format($invoice->NilaiInvoice,2,',','.'),
                'Keterangan' => '',
             );
            $mesg = $this->load->view('templates/email',$data, true);
             //Load email library 
             $this->load->library('email'); 
       
             $this->email->from($from_email, 'Mitraku'); 
             $this->email->to($to_email);
             $this->email->subject('Revisi pengajuan pembayaran Invoice No. '.$invoice->NoInvoice); 
             $this->email->message($mesg); 
       
             //Send mail 
             if($this->email->send());
        }
        echo 'Email terkirim.';
    }

    public function revisi()
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role == $roleID || $this->updater_role_2 == $roleID ){
            
        }else{
            echo 'Anda tidak berhak mengubah data.';
            die();
        }
        $ids = $this->input->post('ids');

        $this->db->where_in('ID',$ids)
                ->where('Status',1)
                ->update('Invoice',array('Status' => 1));

        $row_aff = $this->db->affected_rows();
        $count = count($ids);
        echo $row_aff . ' dari ' . $count . ' Dokumen Berhasil di Update Status menjadi Draft';
    }

    public function view($invoice_id)
    {
        $roleID = (int) Auth::roleID();
        
        if($this->updater_role == $roleID || $this->updater_role_2 == $roleID ){
            
        }else{
            notif('error', 'Anda tidak berhak mengubah data.');
            redirect('invoice/approval/');
            die();
        }
        $encode_id = $invoice_id;
        $invoice_id = decode_url($invoice_id);
        $data = $this->db->where('ID', $invoice_id)->get('Invoice')->row();
        $data->NilaiInvoice = str_replace('.', ',', $data->NilaiInvoice);
        if(!$data){
            show_404();
            die();
        }
        $attachments = $this->db->where('IDParent', $invoice_id)->get('Attachment')->result();

        $this->blade->view('invoice.approval.view',[
            'id'              => $invoice_id,
            'data'              => $data,
            'list_transaksi'    => $this->m_vl->get_jenis_transaksi('po'),
            'list_kurs'         => $this->m_vl->get_kurs(),
            'attachments'       => $attachments,
            'table'             => 'DaftarPO',
            'histories'         => $this->m_invoice->getHistory($invoice_id)
        ]);
    }

    public function reject_update($id)
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role == $roleID || $this->updater_role_2 == $roleID ){
            
        }else{
            notif('error', 'Anda tidak berhak mengubah data.');
            redirect('invoice/approval/');
            die();
        }
        if(!(int)$id){
            $encode_id = $id;
            $id = decode_url($id);
        }

        $this->db->trans_start();

        $this->db->where('ID',$id)
            ->where('Status',2) // update status waiting approval
            ->update('Invoice', array('Status' => 1));
        $isSuccess = $this->db->affected_rows();

        $Keterangan = $this->input->post('Keterangan');
        $this->db->trans_complete();
        if($isSuccess) {
            //$this->m_invoice->createHistory($id, 8, $Keterangan);
            $this->m_invoice->newHistory($id, 8,statusID('DRAFT'), $Keterangan);
            $from_email = "no-reply@mitraku.pupukkaltim.com"; 
            /*Send Email when reject*/
            $invoice = $this->m_invoice->getDetail($id);
             //$to_email = "sony.candra@pupukkaltim.com"; 
            $to_email = $this->m_invoice->getEmailCreator($id);
             $data = array(
                'NamaVendor' => $invoice->NamaVendor,
                'NoInvoice' => $invoice->NoInvoice,
                'NilaiInvoice' => 'Rp. '.number_format($invoice->NilaiInvoice,2,',','.'),
                'Keterangan' => $Keterangan,
             );
            $mesg = $this->load->view('templates/email',$data, true);
             //Load email library 
             $this->load->library('email'); 
       
             $this->email->from($from_email, 'Mitraku'); 
             $this->email->to($to_email);
             $this->email->subject('Revisi pengajuan pembayaran Invoice No. '.$invoice->NoInvoice); 
             $this->email->message($mesg); 
       
             //Send mail 
             if($this->email->send());
            notif('success', 'Invoice berhasil direvisi');
            redirect('invoice/approval/index?status=reject_ok');
        }else{
            notif('error', 'Invoice gagal direvisi, revisi hanya untuk status Waiting Approval');
            redirect('invoice/approval/index?status=reject_failed');
        }
    }

    public function update($id)
    {
        $roleID = (int) Auth::roleID();
        
        if($this->updater_role == $roleID || $this->updater_role_2 == $roleID ){
            
        }else{
            notif('error', 'Anda tidak berhak mengubah data.');
            redirect('invoice/approval/');
            die();
        }
        $this->db->where('ID',$id)
            ->where('Status',2) // update status hanya yang draft
            ->update('Invoice', array('Status' => 3));
        $isSuccess = $this->db->affected_rows();

        if($isSuccess > 0) {
            //$this->m_invoice->createHistory($id, 3, '');
            $this->m_invoice->newHistory($id, 3, statusID('APPROVED'), '');
            notif('success', 'Approval berhasil');
            redirect("invoice/approval?status=success&id=".encode_url($id));
        }else{
            notif('error', 'Approval gagal');
            redirect("invoice/approval?status=error&id=".encode_url($id));
        }

    }

}
