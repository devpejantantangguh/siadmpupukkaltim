<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Input extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('invoice/m_input','m_input');
        $this->load->model('m_vl');
        Auth::guard();
    }

    public function get($id_invoice = 0)
    {
        /*$encode_id = $id_invoice;
        $id_invoice = decode_url($id_invoice);*/
        if($id_invoice == 0) {
            $items = [];
            $count = 0;
        } else {

            $search = isset($_GET['filter']) ? json_decode($_GET['filter']) : '';
            $limit = isset($_GET['take']) ? $_GET['take'] : 10;
            $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;
            if(!empty($search)) {
                Filter::dxDataGridFilters($search, $this->db);
            }
            Filter::areaFilter('i', $this->db);

            if(isset($_GET['orderby'])) {
                $this->db->order_by($_GET['orderby']);
            } else {
                $this->db->order_by('r.ID', 'DESC');
            }
        $this->db->select('r.*, i.NoInvoice, Ms_Karyawan.Nama')
                ->where('i.ID', $id_invoice)
                ->join('RelatedInvoice r', 'i.ID = r.IDInvoice')
                ->join('Ms_User', 'i.CreatedBy = Ms_User.ID')
                ->join('Ms_Karyawan', 'Ms_User.IDKaryawan = Ms_Karyawan.ID');
        $tempdb = clone $this->db; //clone for count all rows
        $count= $tempdb->from('Invoice i')->count_all_results();
            $items = $this->db->get('Invoice i', $limit, $offset)
                                ->result();
        }

        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $items
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function getJaminan($id_invoice = 0)
    {
        if($id_invoice == 0) {
            $items = [];
            $jaminan = [];
            $count = 0;
        } else {
            $this->db->select('r.NoPO')
                ->where('i.ID', $id_invoice)
                ->join('RelatedInvoice r', 'i.ID = r.IDInvoice')
                ->join('Ms_User', 'i.CreatedBy = Ms_User.ID')
                ->join('Ms_Karyawan', 'Ms_User.IDKaryawan = Ms_Karyawan.ID');
            $listpo = $this->db->group_by('r.NoPO')
                                ->get('Invoice i')->result();
            $jaminan = [];
            foreach ($listpo as $key) {
                 $simona = $this->load->database('simona', TRUE);
                 $simona->where('i.NO_PO', $key->NoPO);
                 $res = $simona->get('tr_performance_bond i')->row();
                 if($res){
                    $jaminan[] = $res;
                 }

            }
        }
        
        $data = json_decode(json_encode(array(
            'totalCount'    => count($jaminan),
            'data'          => $jaminan
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    // search vendor
    public function search()
    {
        $params = $this->input->get('q');

        $lists = $this->m_input->searchVendor($params);
        echo json_encode($lists);
    }

    public function get_po($id_invoice)
    {
        header('Content-Type: application/json');
        $simona = $this->load->database('simona', TRUE);

        // get invoice type = barang / jasa
        $row = $this->db->where('ID', $id_invoice)->get('Invoice')->row();

        if($row->JenisTransaksi == 1) { // invoice barang
            $sql = "SELECT PO_NO as NoPO FROM po_goods
                        JOIN ms_vendor on ms_vendor.ID_VENDOR = po_goods.VENDOR_ID
                        WHERE ms_vendor.KODE_VENDOR_SAP = ?";

            $query = $simona->query($sql, [$row->IDVendor]);

            $data = [];

            if($query->num_rows() > 0) {
                foreach($query->result() as $row) {
                    $data[] = [
                        'id' => $row->NoPO,
                        'text' => $row->NoPO
                    ];
                }
            }

            echo json_encode($data);

        }elseif($row->JenisTransaksi == 2) { // invoice jasa
            $sql = "SELECT PO_NO as NoPO FROM po_svc
                        JOIN ms_vendor on ms_vendor.ID_VENDOR = po_svc.VENDOR_ID
                        WHERE ms_vendor.KODE_VENDOR_SAP = ?;";

            $query = $simona->query($sql, [$row->IDVendor]);

            $data = [];

            if($query->num_rows() > 0) {
                foreach($query->result() as $row) {
                    $data[] = [
                        'id' => $row->NoPO,
                        'text' => $row->NoPO
                    ];
                }
            }

            echo json_encode($data);
        } elseif($row->JenisTransaksi == 3) { // invoice jasa distribusi

        }
    }

    public function get_sagr($id_invoice, $id_po)
    {
        header('Content-Type: application/json');
        $row = $this->db->where('ID', $id_invoice)->get('Invoice')->row();

        if($row->JenisTransaksi == 1) { // sagr goods
            $this->m_input->get_sagr_barang($id_po);
        } elseif($row->JenisTransaksi == 2) { // sagr jasa
            $this->m_input->get_sagr_jasa($id_po);
        } elseif($row->JenisTransaksi == 3) { // sagr jasa distribusi

        }
    }

    public function get_detail_po($id_invoice, $nomor_po)
    {
        $row = $this->db->where('ID', $id_invoice)->get('Invoice')->row();

        if($row->JenisTransaksi == 1) {
            $this->m_input->get_detail_po_barang($nomor_po);
        } else if($row->JenisTransaksi == 2) {
            $this->m_input->get_detail_po_jasa($nomor_po);
        } else if($row->JenisTransaksi == 3) {
            // $this->m_input->get_detail_po_distribusi($nomor_po);
        }
    }

    public function get_detail_sagr($id_invoice, $no_sagr = null)
    {
        if($no_sagr =='null'){
            die();
        }
        $row = $this->db->where('ID', $id_invoice)->get('Invoice')->row();

        if($row->JenisTransaksi == 1) {
            $this->m_input->get_detail_sagr_barang($no_sagr);
        } elseif($row->JenisTransaksi == 2) {
            $this->m_input->get_detail_sagr_jasa($no_sagr);
        } elseif ($row->JenisTransaksi == 3) {

        }
    }

    public function update($id)
    {
        $submit = $this->input->post('submit');

        if($submit == 'draft') {
            $status = 1;
        } elseif($submit == 'approved') {
            $status = 2;
        }
        $this->db->where('ID',$id)->update('Invoice',['status' => $status]);
        notif('success', 'Data berhasil diubah.');
        redirect('invoice/lists/index');
    }

    public function add_related_invoice($id)
    {
        //header('Content-Type: application/json');
        /*cek dulu, nomer PO dan GRS tidak boleh sama */
        $data = $this->input->post();
        $nopo = $data['NoPO'];
        $sagr = $data['NoSAGR'];
        if(strlen($nopo) != 10){
            echo "Nomor PO Harus 10 digit";
            return;
        }
        if(strlen($sagr) != 10){
            echo "Nomor No SAGR Harus 10 digit";
            return;
        }
        if(empty($data['TglSAGR'])){
            echo "Tgl SAGR Wajib diisi";
            return;
        }

        /*
        //Update 18 November, boleh sama.
        $hasPO = $this->db->where('IDInvoice',$id)->where('NoPO', $data['NoPO'])->get('RelatedInvoice');
        if($hasPO->num_rows() > 0) {
            echo "Nomor PO Tidak Boleh Sama";
            return;
        }
        */

        if( isset($_POST['NoSAGR']) ) {
            if($_POST['NoSAGR'] != '0000000000'){
                $hasSAGR = $this->db->where('IDInvoice', $id)->where('NoSAGR', $_POST['NoSAGR'])->get('RelatedInvoice');
                if($hasSAGR->num_rows() > 0) {
                    echo "Nomor SAGR Tidak Boleh Sama";
                    return;
                }
            }
        }


        $this->db->trans_start();

        $data['IDInvoice'] = $id;
        $data['NilaiKontrak'] = empty($data['NilaiKontrak']) ? 0 : str_replace('.','',$data['NilaiKontrak']);
        $data['NilaiPO'] = str_replace(['.',','],'',$data['NilaiPO']);
        $data['NilaiPO'] = !is_numeric($data['NilaiPO']) ? 0 : $data['NilaiPO'];

        if(isset($data['TglSAGR']) && !empty($data['TglSAGR'])) {
            list($d,$m,$y) = explode('-', $data['TglSAGR']);
            $data['TglSAGR'] = date('Y-m-d', mktime(0,0,0,$m,$d,$y));
        }
        if(isset($data['TanggalBA']) && !empty($data['TanggalBA'])) {
            list($d,$m,$y) = explode('-', $data['TanggalBA']);
            $data['TanggalBA'] = date('Y-m-d', mktime(0,0,0,$m,$d,$y));
        }

        $this->db->insert('RelatedInvoice', $data);
        $uid = (int) Auth::user()->ID;
        $this->db->where('ID',$id)->update('Invoice',[
            'UpdatedBy' => $uid,
            'UpdatedOn' => date('Y-m-d H:i:s')
        ]);

        $this->db->trans_complete();
        echo 'ok';
    }
    public function update_related_invoice($id, $id_related_invoice)
    {
        //header('Content-Type: application/json');
        /*cek dulu, nomer PO dan GRS tidak boleh sama */
        $data = $this->input->post();
        $nopo = $data['NoPO'];
        $sagr = $data['NoSAGR'];
        if(strlen($nopo) != 10){
            echo "Nomor PO Harus 10 digit";
            return;
        }
        if(strlen($sagr) != 10){
            echo "Nomor No SAGR Harus 10 digit";
            return;
        }
        if(empty($data['TglSAGR'])){
            echo "Tgl SAGR Wajib diisi";
            return;
        }

        /*
        //Update 18 November, boleh sama.
        $hasPO = $this->db->where('IDInvoice',$id)->where('NoPO', $data['NoPO'])->get('RelatedInvoice');
        if($hasPO->num_rows() > 0) {
            echo "Nomor PO Tidak Boleh Sama";
            return;
        }
        */

        if( isset($_POST['NoSAGR']) ) {
            if($_POST['NoSAGR'] != '0000000000'){
                $hasSAGR = $this->db->where('IDInvoice !=', $id)->where('NoSAGR', $_POST['NoSAGR'])->get('RelatedInvoice');
                if($hasSAGR->num_rows() > 0) {
                    echo "Nomor SAGR Tidak Boleh Sama";
                    return;
                }
            }
        }


        $this->db->trans_start();

        $data['IDInvoice'] = $id;
        $data['NilaiKontrak'] = empty($data['NilaiKontrak']) ? 0 : str_replace('.','',$data['NilaiKontrak']);
        $data['NilaiPO'] = str_replace(['.',','],'',$data['NilaiPO']);
        $data['NilaiPO'] = !is_numeric($data['NilaiPO']) ? 0 : $data['NilaiPO'];

        if(isset($data['TglSAGR']) && !empty($data['TglSAGR'])) {
            list($d,$m,$y) = explode('-', $data['TglSAGR']);
            $data['TglSAGR'] = date('Y-m-d', mktime(0,0,0,$m,$d,$y));
        }
        if(isset($data['TanggalBA']) && !empty($data['TanggalBA'])) {
            list($d,$m,$y) = explode('-', $data['TanggalBA']);
            $data['TanggalBA'] = date('Y-m-d', mktime(0,0,0,$m,$d,$y));
        }

        $this->db->where('ID',$id_related_invoice)->update('RelatedInvoice', $data);
        $this->db->trans_complete();
        echo 'ok';
    }

    public function delete_related_invoice()
    {
        $ids = $this->input->post('ids');
        $id_invoice = $this->input->post('id_invoice');
        $uid = (int) Auth::user()->ID;
        $this->db->where_in('ID', $ids)->delete('RelatedInvoice');
        $this->db->where('ID',$id_invoice)->update('Invoice',[
            'UpdatedBy' => $uid,
            'UpdatedOn' => date('Y-m-d H:i:s')
        ]);
    }
    public function get_related_invoice()
    {
        header('Content-Type: application/json');
        $this->db->select('r.*, i.NoInvoice, Ms_Karyawan.Nama')
                ->where('r.ID', $this->input->post('id'))
                ->join('RelatedInvoice r', 'i.ID = r.IDInvoice')
                ->join('Ms_User', 'i.CreatedBy = Ms_User.ID')
                ->join('Ms_Karyawan', 'Ms_User.IDKaryawan = Ms_Karyawan.ID');
            $items = $this->db->get('Invoice i')
                                ->row();
            list($y,$m,$d) = explode('-', $items->TglSAGR);
            $items->TglSAGR = date('d-m-Y', mktime(0,0,0,$m,$d,$y));

            list($y,$m,$d) = explode('-', $items->TanggalBA);
            $items->TanggalBA = date('d-m-Y', mktime(0,0,0,$m,$d,$y));
        echo json_encode($items);
    }

    public function remove_attachment($id,$attachment_id)
    {
        $this->db->where('ID', $attachment_id)->delete('Attachment');
        $uid = Auth::user()->ID;
        $this->db->where('ID',$id)->update('Invoice',[
            'UpdatedBy' => $uid,
            'UpdatedOn' => date('Y-m-d H:i:s')
        ]);
        notif('success', 'Data berhasil diubah.');
        redirect($_SERVER['HTTP_REFERER']);
    }


}
