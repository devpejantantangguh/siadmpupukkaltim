<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if(Auth::guest()) {
            show_error('Anda belum Login!',405);
        }
    }

    private function config($name)
    {
        $config = array(
            'user_settings/users' => array(
                'ID' => 'required',
                'RoleID' => 'required',
                'Email' => 'required|valid_email',
                'Type'  => 'trim'
            ),
            'master/country' => array(
                'ID'        => 'required',
                'Country'   => 'required',
                'Keterangan' => 'trim'
            ),
            'master/propinsi' => array(
                'ID'         => 'required',
                'Propinsi'   => 'required',
                'Keterangan' => 'trim'
            ),
            'master/kota' => array(
                'ID'         => 'required',
                'IDPropinsi' => 'required',
                'Kota'       => 'required',
                'Keterangan' => 'trim'
            ),
            'master/unitkerja' => array(
                'CostCenter'        => 'required',
                'UnitKerja'         => 'required',
                'KepalaUnitKerja'   => 'trim',
                'Keterangan'        => 'trim'
            ),
            'master/material' => array(
                'IDMaterial'        => 'required',
                'Material'          => 'required',
                'Keterangan'        => 'trim'
            ),
            'master/karyawan' => array(
                'ID'            => 'required',
                'Nama'          => 'required',
                'CostCenter'    => 'required',
                'NPKLama'       => 'trim',
                'Keterangan'    => 'trim',
            ),
            'master/vendor' => array(
                'IDVendor'      => 'required',
                'Vendor'        => 'required',
                'IsWAPU'        => 'required',
                'Aktif'         => 'required',
                'Email'         => 'required|valid_email',
                'Alamat1'       => 'required',
                'Alamat2'       => 'trim',
                'Kota'          => 'required',
                'Propinsi'      => 'required',
                'Country'       => 'required',
                'Phone'         => 'required',
                'KodePos'       => 'required',
                'Keterangan'    => 'trim',
            ),
            'master/customer' => array(
                'IDCustomer'        => 'required',
                'PairedVendorID'    => 'required',
                'NPWP'              => 'required',
                'Customer'          => 'required',
                'IsWAPU'            => 'required',
                'Alamat1'           => 'required',
                'Alamat2'           => 'trim',
                'Kota'              => 'required',
                'Propinsi'          => 'required',
                'Country'           => 'required',
                'Email'             => 'required|valid_email',
                'Phone'             => 'required',
                'KodePos'           => 'required',
                'Keterangan'        => 'trim',
            ),
            'globalSetting' => array(
                'Setting'   => 'required',
                'Value'     => 'required'
            )
        );
        return $config[$name];
    }

    public function get($table)
    {
        $table = ucwords($table);
        $search = isset($_GET['filter']) ? json_decode($_GET['filter']) : '';
        $limit = isset($_GET['take']) ? $_GET['take'] : 100;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;

        if(!empty($search)) {
            $this->db->like($search[0], $search[2]);
        }
        if(isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        } else {

            $pairs['Ms_User'] = array('orderby' => 'ID');
            $pairs['Ms_Country'] = array('orderby' => 'ID');
            $pairs['Ms_Propinsi'] = array('orderby' => 'ID');
            $pairs['Ms_Kota'] = array('orderby' => 'ID');
            $pairs['Ms_UnitKerja'] = array('orderby' => 'CostCenter');
            $pairs['Ms_Karyawan'] = array('orderby' => 'ID');
            $pairs['Ms_Material'] = array('orderby' => 'IDMaterial');
            $pairs['Ms_Vendor'] = array('orderby' => 'IDVendor');
            $pairs['Ms_Customer'] = array('orderby' => 'IDCustomer');
            $pairs['GlobalSettings'] = array('orderby' => 'Setting');
            $pairs['Temp_BillDocDetail'] = array('orderby' => 'IDBillDocHeader');
            $pairs['BillingDocument'] = array('orderby' => 'ID');
            $pairs['DokumenPajak'] = array('orderby' => 'IDParent');
            $pairs['Ms_Role'] = array('orderby' => 'ID');

            $this->db->order_by($pairs[$table]['orderby'], 'DESC');
        }

        $items = $this->db->get($table, $limit, $offset)->result();
        $count = $this->db->count_all($table);

        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $items
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function pushValidate($table, $rules, $update = false, $where = null)
    {
        header('Content-Type: application/json');
        $this->load->library('form_validation');

        $final_rules = array();
        foreach($rules as $field => $rule) {
            if($update && isset($_POST[$field])) {
                $final_rules[$field] = $rule;
            } elseif( ! $update) {
                $final_rules[$field] = $rule;
            }    
        }

        // print_r([$final_rules, $_POST]);die();

        foreach($final_rules as $field => $rule) {
            $this->form_validation->set_rules($field, $field, $rule);
        }

        if($this->form_validation->run() !== false || empty($final_rules)) {
            $data = array();

            foreach($rules as $field => $rule) {
                if(isset($_POST[$field])) {
                    $data[$field] = $this->input->post($field);
                    if(is_numeric($data[$field])){
                        $data[$field] = intval($data[$field]);
                    }
                }
            }

            $hasRecord = ['Ms_Material','Ms_Customer','Ms_Vendor'];
            if(in_array($table, $hasRecord)) {
                if($update) {
                    $data['UpdatedOn'] = date('Y-m-d H:i:s');
                    $data['UpdatedBy'] = Auth::user()->ID;
                }else{
                    $data['CreatedOn'] = date('Y-m-d H:i:s');
                    $data['CreatedBy'] = Auth::user()->ID;
                }
            }

            if($update) {
                $this->db->where($where)->update($table, $data);
            }else{
                $this->db->insert($table, $data);
            }
            
            if($this->db->affected_rows() > 0) {
                return json_encode(array('status' => 'success'));
            } else {
                $this->output->set_status_header(401);
                return json_encode(array('status' => 'error','message' => 'Database Error'));
            }
        } 
        $this->output->set_status_header(401);
        return json_encode(array('status' => 'error','message' => $this->form_validation->error_array()));;
    }

    public function store($table)
    {
        $table = ucwords($table);

        $pairs['Ms_User'] = array('validate' => 'user_settings/users');
        $pairs['Ms_Country'] = array('validate' => 'master/country');
        $pairs['Ms_Propinsi'] = array('validate' => 'master/propinsi');
        $pairs['Ms_Kota'] = array('validate' => 'master/kota');
        $pairs['Ms_Material'] = array('validate' => 'master/material');
        $pairs['Ms_Vendor'] = array('validate' => 'master/vendor');
        $pairs['Ms_Customer'] = array('validate' => 'master/customer');
        $pairs['Ms_Karyawan'] = array('validate' => 'master/karyawan');
        $pairs['Ms_UnitKerja'] = array('validate' => 'master/unitkerja');
        $pairs['GlobalSettings'] = array('validate' => 'globalSetting');

        echo $this->pushValidate($table, $this->config($pairs[$table]['validate']));
    }

    public function update($table, $id)
    {
        $table = ucwords($table);
        
        $pairs['Ms_User'] = array('validate' => 'user_settings/users', 'where' => array('ID' => $id));
        $pairs['Ms_Country'] = array('validate' => 'master/country', 'where' => array('ID' => $id));
        $pairs['Ms_Propinsi'] = array('validate' => 'master/propinsi', 'where' => array('ID' => $id));
        $pairs['Ms_Kota'] = array('validate' => 'master/kota', 'where' => array('ID' => $id));
        $pairs['Ms_UnitKerja'] = array('validate' => 'master/unitkerja', 'where' => array('CostCenter' => $id));
        $pairs['Ms_Material'] = array('validate' => 'master/material', 'where' => array('IDMaterial' => $id));
        $pairs['Ms_Vendor'] = array('validate' => 'master/vendor', 'where' => array('IDVendor' => $id));
        $pairs['Ms_Customer'] = array('validate' => 'master/customer', 'where' => array('IDCustomer' => $id));
        $pairs['Ms_Karyawan'] = array('validate' => 'master/karyawan', 'where' => array('ID' => $id));
        $pairs['GlobalSettings'] = array('validate' => 'globalSetting', 'where' => array('Setting' => $id));

        echo $this->pushValidate($table, $this->config($pairs[$table]['validate']), true, $pairs[$table]['where']);
    }

    public function destroy($table, $id)
    {
        $table = ucwords($table);
        
        $pairs['Ms_User'] = array('where' => array('ID' => $id));
        $pairs['Ms_Country'] = array('where' => array('ID' => $id));
        $pairs['Ms_Propinsi'] = array('where' => array('ID' => $id));
        $pairs['Ms_Kota'] = array('where' => array('ID' => $id));
        $pairs['Ms_Material'] = array('where' => array('IDMaterial' => $id));
        $pairs['Ms_UnitKerja'] = array('where' => array('CostCenter' => $id));
        $pairs['Ms_Vendor'] = array('where' => array('IDVendor' => $id));
        $pairs['Ms_Customer'] = array('where' => array('IDCustomer' => $id));
        $pairs['Ms_Karyawan'] = array('where' => array('ID' => $id));
        $pairs['GlobalSettings'] = array('where' => array('Setting' => $id));

        $this->db->delete($table, $pairs[$table]['where']);
    }
}
