<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Simona extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_simona');
    }

    public function get_po($nomor_po)
    {
        $rows = $this->m_simona->get_purcashe_order();

        header('Content-Type: application/json');

        foreach ($rows as $row) {
            if ($row['NoPO'] == $nomor_po) {
                echo json_encode($row);
                die();
            }
        }
    }

    public function get_jaminan($nomor_po)
    {
        $rows = $this->m_simona->get_jaminan();

        header('Content-Type: application/json');

        foreach ($rows as $row) {
            if ($row['NomorPO'] == $nomor_po) {
                echo json_encode($row);
                die();
            }
        }
    }
}
