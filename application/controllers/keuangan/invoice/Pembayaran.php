<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;
include __DIR__.'/Invoice_Controller.php';
class Pembayaran extends Invoice_Controller {
    public $updater_role = [12,1013]; //Sie Pembayaran
    public $status_current = 5;        
    public $status_current2 = 6;
    public $status_updated = 8;
    public $status_reject = 4;
    public $action_bayar = 7;
    public $action_reject = 9;
    public function __construct()
    {
        parent::__construct();
        Auth::guard();

        $this->load->model('m_po');
        $this->load->model('m_vl');
        $this->load->model('m_invoice');
    }
    public function index()
    {
        //$this->blade->view('keuangan.invoice_tracking.verifikasi.index');
        $data['table'] = 'Invoice';
        $data['listStatus'] = $this->_getStatus(1);
        $this->blade->view('keuangan.invoice_tracking.pembayaran.index',$data);
    }
    public function get()
    {
        $table = ucwords('Invoice');

        $search = isset($_GET['filter']) ? json_decode($_GET['filter']) : '';
        $limit = isset($_GET['take']) ? $_GET['take'] : 10;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;
        if(!empty($search)) {
            Filter::dxDataGridFilters($search, $this->db);
        }
        $this->db->select('Invoice.*');
        Filter::areaFilter('Invoice', $this->db);

        if(isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        } else {
            $this->db->order_by('Invoice.TglTransmittal', 'DESC');
        }
        
        $this->db->like('NoTransmittal','AK');
        $this->db->where('Invoice.Status', $this->status_current); //verified
        $this->db->or_where('Invoice.Status', $this->status_current2); //verified
        //$this->db->where('Invoice.Status <=', '7');


        $tempdb = clone $this->db; //clone for count all rows
        $count= $tempdb->from('Vw_AllInvoice Invoice')->count_all_results();
        $items = $this->db->get('Vw_AllInvoice Invoice', $limit, $offset)->result();
        $i= 1;
        foreach ($items as $item) {
            $item->No = $i++;
            $item->ID = encode_url($item->ID);
            $item->ReceivedBy = getNames($item->ReceivedBy);
        }
        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $items
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function open($id = 0)
    {
        $roleID = (int) Auth::roleID();
        if(!in_array($roleID,$this->updater_role)){
            notif('error', 'Anda tidak berhak mengubah data.');
            redirect('keuangan/invoice/pembayaran/');
            die();
        }
        $encode_id = $id;
        $id = decode_url($id);
        $data = $this->m_po->get_detail_invoice($id);
        if(!$data){
            show_404();
            die();
        }
        $list_transaksi = $this->m_vl->get_jenis_transaksi('po');
        $documents = $this->m_po->get_kelengkapan_dokumen($id);
        $current_note = $this->m_po->get_current_note($id);
        $last_note = $this->m_po->get_last_note($id);
        $table = 'Invoice';
        $list_kurs = $this->m_vl->get_kurs();
        $list_payment_term = $this->m_vl->get_payment_term();
        $attachments = $this->db->where('IDParent', $id)->get('Attachment')->result();
        $Keterangan = $this->m_invoice->getLastKeterangan($id);
        $histories = $this->m_invoice->getHistory($id);

        $this->blade->view('keuangan.invoice_tracking.pembayaran.open', compact('data','table','documents','id','list_transaksi','current_note','last_note','list_kurs','list_payment_term','attachments','Keterangan','histories'));
    }  

    public function update($id)
    {
        $roleID = (int) Auth::roleID();
        if(!in_array($roleID,$this->updater_role)){
            notif('error', 'Anda tidak berhak mengubah data.');
            redirect('keuangan/invoice/pembayaran/');
            die();
        }

        if($_POST['submit'] == 'paid') {
            $Keterangan = $this->input->post('note');
            //$this->createHistory($id, $this->action_bayar);
            $this->m_invoice->newHistory($id, $this->action_bayar,$this->status_updated, $Keterangan);
            $this->db->where('ID',$id)
                ->where('Status',$this->status_current) // update status yang verified
                ->or_where('Status',$this->status_current2) // update status yang applied
                ->update('Invoice', array('Status' => $this->status_updated)); //paid
            $isSuccess = $this->db->affected_rows();
            if($isSuccess > 0) {
                notif('success', 'Data berhasil diubah.');
                redirect("keuangan/invoice/pembayaran/?update=success");    
            }else{
                notif('error', 'Data gagal diubah.');
                redirect("keuangan/invoice/pembayaran/open/".encode_url($id)."}?update=fail");
            }
        }else{
            $Keterangan = $this->input->post('note');
            //$this->createHistory($id, $this->status_reject);
            $this->m_invoice->newHistory($id, $this->action_reject,$this->status_reject, $Keterangan);
            $this->db->where('ID',$id)
                ->where('Status',$this->status_current) // update status yang verified
                ->or_where('Status',$this->status_current2) // update status yang applied
                ->update('Invoice', array('Status' => $this->status_reject)); //paid
            $this->m_invoice->updateIDArea($id,'reject');
            notif('success', 'Data berhasil diubah.');
            redirect("keuangan/invoice/pembayaran/open/{$id}?update=reject");
        }
    }
    public function paygen(){
        $data['table'] = 'Paygen';
        $this->blade->view('keuangan.invoice_tracking.pembayaran.paygen', $data);
    }

        public function getpaygen()
    {
        $table = ucwords('Temp_Paygen');

        $limit = isset($_GET['take']) ? $_GET['take'] : 10;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;

        //$this->db->join('Invoice', 'Invoice.NoDokumen = Temp_Paygen.NoDokumen','LEFT');
        $query = $this->db->get('Temp_Paygen', $limit, $offset);
        $res = array();
        $items = array();
        $i = 1;
        foreach ($query->result_array() as $items){
            $items['No'] = $i++;
            array_push($res, $items);
        }
        //$items = $query->result();
        $count = $this->db->count_all($table);

        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $res
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function paygenstore()
    {
        $roleID = (int) Auth::roleID();
        if(!in_array($roleID,$this->updater_role)){
            notif('error', 'Anda tidak berhak mengubah data.');
            redirect('keuangan/invoice/pembayaran/');
            die();
        }

        $this->db->trans_start(); 
        $this->db->where('1=1', null, false)->delete('Temp_Paygen');

        $config['upload_path']          = './storage/paygen/';
        $config['allowed_types']        = 'xls|xlsx|csv'; // xlsm|xltx|xltm
        $config['max_size']             = 10000;
        $config['overwrite']            = true;
        $config['max_filename']         = 255;
        // $config['encrypt_name']         = true;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('fileexcel')) {

            notif('error', 'Data gagal diupload. '.$this->upload->display_errors());
        } else {
            $file = $this->upload->data('full_path');
            $spreadsheet = IOFactory::load($file);
            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            $matchcount = 0;
            $deletecount = 0;
            foreach($sheetData as $key => $row) {
                if($key < 2) continue;
                $data = [];
                $data['NoDokumen'] = (string) $row['A'];
                $data['IDVendor'] = (string) $row['B'];
                $data['Tahun'] = (string) $row['C'];
                $data['TglClearing'] = date('Y-m-d', strtotime($row['D']));
                $data['NoClearing'] = (string) $row['E'];
                $data['RekeningTujuan'] = (string) $row['F'];
                $data['NomorRekening'] = (string) $row['G'];
                $data['Amount'] = (float) $row['H'];
                $data['CreatedBy'] = Auth::user()->ID;
                $data['CreatedOn'] = date('Y-m-d H:i:s');
                 
                if(!empty($data['NoDokumen'])){
                    $ismatch = $this->ismatch($data['NoDokumen']);
                    if($ismatch==1){
                        $data['IsMatch'] = true;
                        $matchcount++;
                        $this->db->insert('Temp_Paygen', $data);
                    }else{

                        $data['IsMatch'] = false;
                        $deletecount++;
                    }
                }
                
            }

            $this->db->trans_complete();
            
            if($deletecount>0){
                notif('warning', 'Ditemukan '.$matchcount.' data cocok, '.$deletecount.' data tidak cocok.');
            }else{
                notif('success', 'Ditemukan '.$matchcount.' data cocok');
            }
            
        }

            redirect('keuangan/invoice/pembayaran/paygen');
    }

    public function paygenpost()
    {
        $roleID = (int) Auth::roleID();
        if(!in_array($roleID,$this->updater_role)){
            echo('Anda tidak berhak mengubah data.');
            die();
        }

        $postdata = $this->input->post();
        $NoDokumen = $postdata['NoDokumen'];
                $this->db->select('*, Invoice.ID as IDInvoice');
                $this->db->where('Temp_Paygen.NoDokumen', $NoDokumen);

                $this->db->join('Invoice', 'Invoice.NoDokumenSAP = Temp_Paygen.NoDokumen AND Invoice.Tahun = Temp_Paygen.Tahun');
                $row = $this->db->get('Temp_Paygen')->result_array();
                foreach ($row as $value) {
                    if($value['IsMatch'] == true){
                        $id = $value['IDInvoice'];
                        $newvalue['NoDokumen'] = $NoDokumen;
                        $newvalue['IDVendor'] = $value['IDVendor'];
                        $tahun = $value['Tahun'];
                        $newvalue['Tahun'] = $tahun;
                        $newvalue['TglClearing'] = $value['TglClearing'];
                        $newvalue['NoClearing'] = $value['NoClearing'];
                        $newvalue['RekeningTujuan'] = $value['RekeningTujuan'];
                        $newvalue['NomorRekening'] = $value['NomorRekening'];
                        $newvalue['CreatedBy'] = $value['CreatedBy'];
                        $newvalue['CreatedOn'] = $value['CreatedOn'];
                        $newvalue['Amount'] = $value['Amount'];

                        //1
                        $Keterangan = $this->input->post('note');
                        //$this->createHistory($id, $this->action_bayar);
                        $this->m_invoice->newHistory($id, $this->action_bayar,$this->status_updated, $Keterangan);
                        $this->db->group_start();
                        $this->db->where('NoDokumenSAP', $NoDokumen)

                            ->where('Tahun', $tahun)
                        ->group_end()
                            ->group_start()
                            ->where('Status',$this->status_current) // update status yang verified
                            ->or_where('Status',$this->status_current2) // update status yang applied
                            ->group_end()
                            ->update('Invoice', array('Status' => $this->status_updated));
                        $isSuccess = $this->db->affected_rows();
                         //2
                        $this->db->where('NoDokumen',$NoDokumen);
                       $q = $this->db->get('PayGen');

                       if ( $q->num_rows() > 0 ) 
                       {
                          $this->db->where('NoDokumen',$NoDokumen);
                          $this->db->update('PayGen',$newvalue);
                       } else {
                          
                          $this->db->insert('PayGen',$newvalue);
                       }
                        //3
                        $this->db->where('NoDokumen', $NoDokumen);
                        $this->db->delete('Temp_Paygen'); 
                    }
 
                }
        if(!$row) {
            echo 'Data kosong';
        }else{
            echo 'Data berhasil diupdate';    
        }
    }
    public function submit()
    {

        $roleID = (int) Auth::roleID();
        if(!in_array($roleID,$this->updater_role)){
            echo('Anda tidak berhak mengubah data.');
            die();
        }

        $postdata = $this->input->post();
        $ID = (int) $postdata['ID'];
        $action = $postdata['action'];
        if($action=='pay'){
            $actionID = 6;
            $status = 6;
        }else{
            $actionID = 11;
            $status = 1;
        }
        $data = array(
            'NoInvoice'=>$postdata['NoInvoice'],
            'NoPO'=>$postdata['NoPO'],
            'NamaVendor'=>$postdata['NamaVendor'],
            'NilaiPO'=> (int) $postdata['NilaiPO'],
            'NilaiInvoice'=> (int) $postdata['NilaiInvoice'],
            'KursInvoice'=>$postdata['KursInvoice'],
            'NomorKontrak'=>$postdata['NomorKontrak'],
            'NilaiKontrak'=> (int) $postdata['NilaiKontrak'],
            'NomorBA'=>$postdata['NomorBA'],
            'TanggalBA'=>$postdata['TanggalBA'],
            'NoSAGR'=>$postdata['NoSAGR'],
            'NoFakturPajak'=>$postdata['NoFakturPajak'],
            'TglFakturPajak'=>$postdata['TglFakturPajak'],
            'PaymentTerm'=>$postdata['PaymentTerm'],
            'PPN'=>'',
            'Status'=>$status
        );
        $this->db->set($data);
        $this->db->where('ID', $ID);
        $this->db->update('Invoice'); // gives UPDATE `mytable` SET `field` = 'field+1' WHERE `id` = 2

        $Keterangan = $this->input->post('note');
        //$this->createHistory($ID, $status);
        $this->m_invoice->newHistory($ID, $actionID,$status, $Keterangan);
        if(isset($_POST['matriks'])) {
            $this->m_po->updateMatriks($ID, $_POST['matriks']);
        }
        $this->update_priority($ID);

        echo 'Berhasil mengubah data';
    }    
    public function receive()
    {
        $roleID = (int) Auth::roleID();
        if(!in_array($roleID,$this->updater_role)){
            echo json_encode(array('status' => 404, 'msg' => 'Anda tidak berhak mengubah data.'));
            die();
        }
        $ids = $this->input->post('ids');
        $ids = array_unique($ids);
        $NoTransmittal = [];
        foreach ($ids as $key => $id) {
            if(!(int)$id){
                $ReceivedBy = infoInvoice(decode_url($id),'ReceivedBy');
                if($ReceivedBy == '' || $ReceivedBy == null){
                    $ids[$key] = decode_url($id);
                    $NoTransmittal[$key] = infoInvoice(decode_url($id),'NoTransmittal');
                    $encode_id = $id;
                    $id = decode_url($id);
                }
                
            }
        }

        $status = $this->status_updated; //paid
        $uid = Auth::user()->ID;
        $data = array(
            'ReceivedBy'=>$uid,
            'ReceivedOn'=>date('Y-m-d H:i:s')
        );
        if(!empty($NoTransmittal)){

        
            $this->db->set($data);
            $this->db->where_in('NoTransmittal', $NoTransmittal);
            $row = $this->db->update('Invoice'); 
            
            if(!$row) {
                echo json_encode(array('status' => 404, 'msg' => 'Gagal mengubah data.'));
            }else{
                $row_aff = $this->db->affected_rows();
                $count = count($ids);

                $msg = $row_aff . ' dokumen berhasil diupdate';
                echo json_encode(array('status' => 200, 'msg' => $msg));    
            }
        }else{
            $msg = 'Tidak ada perubahan dokumen.';
            echo json_encode(array('status' => 200, 'msg' => $msg)); 
        }

        
    }
    public function process()
    {
        $roleID = (int) Auth::roleID();
        if(!in_array($roleID,$this->updater_role)){
            echo json_encode(array('status' => 404, 'msg' => 'Anda tidak berhak mengubah data.'));
            die();
        }
        $ids = $this->input->post('ids');
        $ids = array_unique($ids);
        foreach ($ids as $key => $id) {
            if(!(int)$id){
                $ids[$key] = decode_url($id);
                $encode_id = $id;
                $id = decode_url($id);
                
                //$this->m_invoice->createHistory($id, $this->action_bayar, '');
                $this->m_invoice->newHistory($id, $this->action_bayar,$this->status_updated, '');
                
            }
        }

        $status = $this->status_updated; //paid
        $data = array(
            'Status'=>$status
        );
        $this->db->set($data);
        $this->db->where_in('ID', $ids);
        $row = $this->db->update('Invoice'); // gives UPDATE `mytable` SET `field` = 'field+1' WHERE `id` = 2
        
        if(!$row) {
            echo json_encode(array('status' => 404, 'msg' => 'Gagal mengubah data.'));
        }else{
            $row_aff = $this->db->affected_rows();
            $count = count($ids);

            $msg = $row_aff . ' dokumen berhasil diupdate';
            echo json_encode(array('status' => 200, 'msg' => $msg));    
        }

        
    }
    public function getPiutang()
    {
        header('Content-Type: application/json');

        $ARDocNo = $this->input->post('ARDocNo');

        $row = $this->db->where('ARDocNo',$ARDocNo)->get('ListPiutang')->row();
        if(!$row) {
            echo json_encode(array('status' => '404', 'data' => array()));
        }else{
            echo json_encode(array('status' => 200, 'data' => $row));    
        }
    }

    private function createHistory($id, $status)
    {
        $data = [
            'IDInvoice' => $id,
            'Action'    => $status,
            'UserID'    => Auth::user()->ID,
            'Timestamp' => date('Y-m-d H:i:s'), 
            'Keterangan'    => $this->input->post('note'),
        ];
        $this->db->insert('HistoryInvoice', $data);
    }

        private function update_priority($id)
    {
        $update_id = isset($_POST['IsPriority']) ? true : false;

        $this->db->where('ID',$id)
            ->update('Invoice',array('IsPriority' => $update_id));
    }

    public function reject($id)
    {
        $this->blade->view('keuangan/invoice_tracking/verifikasi/reject',compact('id'));
    }

    public function reject_update($id)
    {
        $roleID = (int) Auth::roleID();
        if(!in_array($roleID,$this->updater_role)){
            notif('error', 'Anda tidak berhak mengubah data.');
            redirect('keuangan/invoice/pembayaran/');
            die();
        }
        $this->db->trans_start();
        $data = $this->db->where('ID',$id)->get('Invoice')->row()->NoTransmittal;
        $New = $data;
        $NoTransmittal = explode('AK',$data);
        if(count($NoTransmittal)>0){
            $New = $NoTransmittal[0];
        }
        $isSuccess = $this->db->where('ID',$id)
            ->where('Status',$this->status_current) // update status dari verified
            ->or_where('Status',$this->status_current2) // update status dari applied
            ->update('Invoice', array('Status' => $this->status_reject,
                    'NoDokumenSAP' => '',
                    'ReceivedBy' => null,
                    'ReceivedOn' => null,
                    'NoTransmittal' => $New,
                    'StatusTransmittal' => false)); //back to akuntansi (checked)
        
        $Keterangan = $this->input->post('Keterangan');
        $this->m_invoice->updateIDArea($id,'reject');
        $this->db->trans_complete();
        
        if($isSuccess) {
            //$this->m_invoice->createHistory($id, $this->action_reject, $Keterangan);
            $this->m_invoice->newHistory($id, $this->action_reject,$this->status_reject, $Keterangan);
            notif('success', 'Data berhasil direject.');
            redirect('keuangan/invoice/pembayaran?status=reject_ok');
        }else{
            notif('error', 'Data gagal direject.');
            redirect('keuangan/invoice/pembayaran?status=reject_failed');
        }
    }
    private function ismatch($NoDokumen){
        $this->db->where('NoDokumenSAP', $NoDokumen);
        $this->db->where('Status', $this->status_current);
        $row = $this->db->get('Invoice')->num_rows();
        return $row;
    }
}
