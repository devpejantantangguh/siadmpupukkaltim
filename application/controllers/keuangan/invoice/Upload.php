<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;

class Upload extends CI_Controller 
{
function __construct()
{
    parent::__construct();
    Auth::guard();
}

    public function index()
    {
        $this->blade->view('keuangan.invoice_tracking.upload.index',array(
            'table' => 'ListPiutang'
        ));
    }

    public function store()
    {
        $this->db->trans_start(); 
        $this->db->where('1=1', null, false)->delete('Temp_ListPiutang');

        $config['upload_path']          = './storage/list_piutang/';
        $config['allowed_types']        = 'xls|xlsx|csv'; // xlsm|xltx|xltm
        $config['max_size']             = 10000;
        $config['overwrite']            = true;
        $config['max_filename']         = 255;
        // $config['encrypt_name']         = true;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('fileexcel')) {
            die($this->upload->display_errors());
        } else {
            $file = $this->upload->data('full_path');
            $spreadsheet = IOFactory::load($file);
            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

            foreach($sheetData as $key => $row) {
                if($key < 2) continue;
                $data = [];
                $data['IDCustomer'] = $row['A'];
                $data['ARDocNo'] = $row['B'];
                $data['NoBillingDoc'] = $row['C'];
                $data['NoSO'] = $row['D'];

                list($d,$m,$y) = explode('/', $row['E']);
                $duedate = date('Y-m-d', mktime(0,0,0,$m,$d,$y));

                $data['DocumentDate'] = $duedate;
                $data['PaymentMethod'] = $row['F'];
                $data['Deskripsi'] = $row['G'];
                $data['AmountDC'] = $row['H'];
                $data['KursDC'] = $row['I'];
                $data['AmountLC'] = $row['J'];
                $data['KursLC'] = $row['K'];

                $this->db->insert('Temp_ListPiutang', $data);
            }

            $this->db->trans_complete();

            redirect('keuangan/invoice/upload');
        }
    }

    public function post()
    {
        try {
            $this->db->trans_start();
            $this->db->where('0 = 0')->delete('ListPiutang');
            $ids = $this->input->post('ids');
            foreach($ids as $id) {
                $this->db->where('ARDocNo', $id);
                $rows = $this->db->get('Temp_ListPiutang')->result_array();
                
                foreach($rows as $row) {
                    $this->db->where('ARDocNo', $id)->delete('Temp_ListPiutang');
                    $row['CreatedBy'] = Auth::user()->ID;
                    $row['CreatedOn'] = date('Y-m-d H:i:s');
                    unset($row['ID']);
                    $this->db->insert('ListPiutang', $row);

                }
            }

            $this->db->trans_complete();

            
            echo json_encode(array('status' => 200, 'msg' => 'Berhasil mengubah data.'));
        } catch (Exception $e) {
            echo json_encode([
                'status'    => 404,
                'message'   => $e->getMessage()
            ]);
        }
    }

    public function postAll()
    {
        try {
            $this->db->trans_start();
            $this->db->where('0 = 0')->delete('ListPiutang');
            
            $rows = $this->db->get('Temp_ListPiutang')->result_array();
            
            foreach($rows as $row) {
                
                $row['CreatedBy'] = Auth::user()->ID;
                $row['CreatedOn'] = date('Y-m-d H:i:s');
                unset($row['ID']);
                $this->db->insert('ListPiutang', $row);

            }
            $this->db->where('0 = 0')->delete('Temp_ListPiutang');

            $this->db->trans_complete();

            echo json_encode(array('status' => 200, 'msg' => 'Berhasil mengubah data.'));
        } catch (Exception $e) {
            echo json_encode([
                'status'    => 404,
                'msg'   => $e->getMessage()
            ]);
        }
    }

    public function get_temp()
    {

        $search = isset($_GET['filter']) ? json_decode($_GET['filter']) : '';
        $limit = isset($_GET['take']) ? $_GET['take'] : 10;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;
        if(!empty($search)) {
            if (in_array('and', $search)) {
                $query = $search;
                if (!empty($query)) {
                    foreach ($query as $key => $src) {                        
                        if (is_array($src)) {
                            if ($src[0] == 'IDCustomer') {
                                $search[$key][0] = 'Temp_ListPiutang.IDCustomer';
                            }
                        }
                    }
                }
            }else{
                if($search[0] == 'IDCustomer') {
                    $search[0] = 'Temp_ListPiutang.IDCustomer';  
                }
            }
            Filter::dxDataGridFilters($search, $this->db);
        }
        if(isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        } 
        $this->db->select('Temp_ListPiutang.*, Ms_Customer.PairedVendorName');
        $this->db->join('Ms_Customer','Temp_ListPiutang.IDCustomer = Ms_Customer.IDCustomer', 'LEFT');
        $items = $this->db->get('Temp_ListPiutang', $limit, $offset)->result();
        $tempdb = clone $this->db; //clone for count all rows
        $count= $tempdb->from('Temp_ListPiutang')->count_all_results();

        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $items
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function get()
    {

        $search = isset($_GET['filter']) ? json_decode($_GET['filter']) : '';
        $limit = isset($_GET['take']) ? $_GET['take'] : 10;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;

        if(isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        } else {
            $this->db->order_by('ID', 'DESC');
        }

        if(!empty($search)) {
            if (in_array('and', $search)) {
                $query = $search;
                if (!empty($query)) {
                    foreach ($query as $key => $src) {                        
                        if (is_array($src)) {
                            if ($src[0] == 'IDCustomer') {
                                $search[$key][0] = 'ListPiutang.IDCustomer';
                            }
                        }
                    }
                }
            }else{
                if($search[0] == 'IDCustomer') {
                    $search[0] = 'ListPiutang.IDCustomer';
                }
            }
            Filter::dxDataGridFilters($search, $this->db);
        }
        $this->db->select('ListPiutang.*, Ms_Customer.PairedVendorName');
        $this->db->join('Ms_Customer','ListPiutang.IDCustomer = Ms_Customer.IDCustomer', 'LEFT');
        
        $tempdb = clone $this->db; //clone for count all rows
        
        $items = $this->db->get('ListPiutang', $limit, $offset)->result();
        $count= $tempdb->from('ListPiutang')->count_all_results();

        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $items
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }

}
