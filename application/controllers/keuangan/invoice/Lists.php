<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lists extends CI_Controller {

    public function index()
    {
        $this->blade->view('keuangan.invoice_tracking.lists.index', array(
            'table' => 'ListPiutang'
        ));
    }

}
