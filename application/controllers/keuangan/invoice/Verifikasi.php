<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include __DIR__.'/Invoice_Controller.php';
class Verifikasi extends Invoice_Controller {
    public $updater_role = 11; //Sie Piutang Keuangan

    public $status_current = 10; //ar checked   
    public $status_updated = 5; //verified
    public $status_updated2 = 6; //applied
    public $status_reject = 4;

    public $action_verify = 10; //verifikasi ar
    public $action_piutang = 6;
    public $action_reject = 9;

    public function __construct()
    {
        parent::__construct();
        Auth::guard();

        $this->load->model('m_po');
        $this->load->model('m_vl');
        $this->load->model('m_invoice');
    }

    public function index()
    {
        //$this->blade->view('keuangan.invoice_tracking.verifikasi.index');
        $data['table'] = 'Invoice';
        $data['listStatus'] = $this->_getStatus(1);
        $this->blade->view('keuangan.invoice_tracking.verifikasi.index',$data);
    }
    public function get()
    {
        
        $table = ucwords('Invoice');

        $search = isset($_GET['filter']) ? json_decode($_GET['filter']) : '';
        $limit = isset($_GET['take']) ? $_GET['take'] : 10;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;
        if(!empty($search)) {
            Filter::dxDataGridFilters($search, $this->db);
        }
        $this->db->select('Invoice.*');
        Filter::areaFilter('Invoice', $this->db);

        if(isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        } else {
            $this->db->order_by('Invoice.ID', 'DESC');
        }
        $this->db->where('Invoice.Status', '10'); // AR cheched

        $tempdb = clone $this->db; //clone for count all rows
        $count= $tempdb->from('Vw_AllInvoice Invoice')->count_all_results();
        $query = $this->db->get('Vw_AllInvoice Invoice', $limit, $offset);
        $res = array();
        $items = array();
        $i = 1;
        foreach ($query->result_array() as $items){
            $items['No'] = $i++;
            $items['ID'] = encode_url($items['ID']);

            $items['ReceivedBy'] = getNames($items['ReceivedBy']);
            array_push($res, $items);
        }

        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $res
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function open($id = 0)
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            notif('error', 'Anda tidak berhak mengubah data.');
            redirect('keuangan/invoice/verifikasi/');
            die();
        }
        $encode_id = $id;
        $id = decode_url($id);
        
        $data = $this->m_po->get_detail_invoice($id);
        if(!$data){
            show_404();
            die();
        }
        $list_transaksi = $this->m_vl->get_jenis_transaksi('po');
        $documents = $this->m_po->get_kelengkapan_dokumen($id);
        $current_note = $this->m_po->get_current_note($id);
        $last_note = $this->m_po->get_last_note($id);
        $table = 'Invoice';
        $list_kurs = $this->m_vl->get_kurs();
        $list_payment_term = $this->m_vl->get_payment_term();
        $attachments = $this->db->where('IDParent', $id)->get('Attachment')->result();
        $list_piutang = $this->get_piutang($data->IDVendor);
        $count_applied_amount = $this->countAppliedAmount($id);
        $Keterangan = $this->m_invoice->getLastKeterangan($id);
        $histories = $this->m_invoice->getHistory($id);
        
        $this->blade->view('keuangan.invoice_tracking.verifikasi.open', compact('data','table','documents','id','list_transaksi','current_note','last_note','list_kurs','list_payment_term','attachments','list_piutang','count_applied_amount','Keterangan','histories'));
    }
    public function getAppliedDocument($id_invoice = 0)
    {
        if($id_invoice == 0) {
            $items = [];
            $count = 0;
        } else {
            $limit = isset($_GET['take']) ? $_GET['take'] : 10;
            $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;

            if(isset($_GET['orderby'])) {
                $this->db->order_by($_GET['orderby']);
            } else {
                $this->db->order_by('r.ID', 'DESC');
            }

            $items = $this->db->select('r.*, i.ID')
                                ->where('i.ID', $id_invoice)
                                ->join('AppliedDocument r', 'i.ID = r.IDInvoice')
                                ->get('Invoice i', $limit, $offset)
                                ->result();
            $count = $this->db->where('ID', $id_invoice)->count_all_results('AppliedDocument');
        }

        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $items
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function add_applied_document($id)
    {
        $postdata = $this->input->post();
        $data['ARDocNo'] = $postdata['ARDocNo'];
        $data['IDCustomer'] = $postdata['IDCustomer'];
        $data['Kurs'] = $postdata['Kurs'];
        $data['KursApplied'] = $postdata['Kurs'];
        $data['ARAmount'] = (float) str_replace(',', '.', str_replace('.', '', $postdata['ARAmount']));
        $data['AppliedAmount'] = (float) str_replace(',', '.', str_replace('.', '', $postdata['AppliedAmount']));
        $data['IDInvoice'] = (int) $id;
        $data['AppliedBy'] = (int) Auth::user()->ID;
        $data['AppliedOn'] = date('Y-m-d H:i:s');
        $this->db->insert('AppliedDocument', $data);

        $this->db->select_sum('AppliedAmount');
        $this->db->from('AppliedDocument');
        $this->db->where('IDInvoice', $id);
        $query = $this->db->get();
        echo (float) $query->row()->AppliedAmount;
    }
    public function delete_applied_document($id)
    {
        $ids = $this->input->post('ids');
        $this->db->where_in('ARDocNo', $ids)->delete('AppliedDocument');

        $this->db->select_sum('AppliedAmount');
        $this->db->from('AppliedDocument');
        $this->db->where('IDInvoice', $id);
        $query = $this->db->get();
        echo (int) $query->row()->AppliedAmount;
    }

    public function update($id)
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            notif('error', 'Anda tidak berhak mengubah data.');
            redirect('keuangan/invoice/verifikasi/');
            die();
        }
        if($_POST['submit'] == 'applied') {
            $countApplied = $this->countApplied($id);
            if($countApplied>0){
                $update_id = $this->status_updated2; //aplied
        //$this->createHistory($id, $this->$action_piutang, $this->input->post('note'));
$Keterangan = $this->input->post('note');
$this->m_invoice->newHistory($id, $this->$action_piutang,$update_id, $Keterangan);
            }else{
                $update_id = $this->status_updated; //verified
                //$this->createHistory($id, $this->action_verify, $this->input->post('note'));

$Keterangan = $this->input->post('note');
$this->m_invoice->newHistory($id, $this->$action_verify,$update_id, $Keterangan);
            }
            $this->db->where('ID',$id)
                ->where('Status',$this->status_current) // update status hanya yang AR Checked
                ->update('Invoice', array('Status' => $update_id));
            $isSuccess = $this->db->affected_rows();
            $this->update_nilaiverifikasi($id);
            $this->m_invoice->updateIDArea($id,'update');

            

            if($isSuccess > 0) {
                notif('success', 'Data berhasil diubah.');
                redirect("keuangan/invoice/verifikasi/?update=success&status=".$update_id);    
            }else{
                notif('error', 'Data gagal diubah.');
                redirect("keuangan/invoice/verifikasi/open/{$id}?update=fail");
            }
        }
    }

    public function revisi()
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            echo('Anda tidak berhak mengubah data.');
            die();
        }
        $ids = $this->input->post('ids');

        $this->db->where_in('ID',$ids)
            ->group_start()
                ->where('Status',4)
                ->or_where('Status',5)
            ->group_end()
            ->update('Invoice',array('Status' => 1));

        $row_aff = $this->db->affected_rows();
        $count = count($ids);

        echo $row_aff . ' dari ' . $count . ' Dokumen Berhasil di Update Status menjadi Draft';
    } 
    public function submit()
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            echo('Anda tidak berhak mengubah data.');
            die();
        }

        $postdata = $this->input->post();
        $ID = (int) $postdata['ID'];
        $action = $postdata['action'];
        if($action=='verify'){
            $actionID = 5;
            $status = 5;
        }else{
            $actionID = 1;
            $status = 11;
        }
        $data = array(
            'NoInvoice'=>$postdata['NoInvoice'],
            'NoPO'=>$postdata['NoPO'],
            'NamaVendor'=>$postdata['NamaVendor'],
            'NilaiPO'=> (int) $postdata['NilaiPO'],
            'NilaiInvoice'=> (int) $postdata['NilaiInvoice'],
            'KursInvoice'=>$postdata['KursInvoice'],
            'NomorKontrak'=>$postdata['NomorKontrak'],
            'NilaiKontrak'=> (int) $postdata['NilaiKontrak'],
            'NomorBA'=>$postdata['NomorBA'],
            'TanggalBA'=>$postdata['TanggalBA'],
            'NoSAGR'=>$postdata['NoSAGR'],
            'NoFakturPajak'=>$postdata['NoFakturPajak'],
            'TglFakturPajak'=>$postdata['TglFakturPajak'],
            'PaymentTerm'=>$postdata['PaymentTerm'],
            'PPN'=>'',
            'Status'=>$status
        );
        $this->db->set($data);
        $this->db->where('ID', $ID);
        $this->db->update('Invoice'); // gives UPDATE `mytable` SET `field` = 'field+1' WHERE `id` = 2
$this->m_invoice->setIDArea($ID,'update');
//$this->createHistory($ID, $status);
$this->m_invoice->newHistory($ID, $actionID,$status, '');
        if(isset($_POST['matriks'])) {
            $this->m_po->updateMatriks($ID, $_POST['matriks']);
        }
        $this->update_priority($ID);
        echo 'Berhasil mengubah data';
    }    
    public function receive()
    {

        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            echo json_encode(array('status' => 404, 'msg' => 'Anda tidak berhak mengubah data.'));
            die();
        }
        $ids = $this->input->post('ids');
        $ids = array_unique($ids);
        
        $NoTransmittal = [];
        foreach ($ids as $key => $id) {
            if(!(int)$id){
                $ids[$key] = decode_url($id);
                $NoTransmittal[$key] = infoInvoice(decode_url($id),'NoTransmittal');
                $encode_id = $id;
                $id = decode_url($id);
                
                $update_id = $this->status_updated; //verified
                
            }
        }
        $uid = Auth::user()->ID;
        $row = $this->db->where_in('NoTransmittal', $NoTransmittal)
                        ->update('Invoice', array('ReceivedBy' => $uid,'ReceivedOn' => date('Y-m-d H:i:s')));
        if(!$row) {
            echo json_encode(array('status' => 404, 'msg' => 'Gagal mengubah data.'));
        }else{
            $row_aff = $this->db->affected_rows();
            $count = count($ids);

            $msg = $row_aff . ' dokumen berhasil diupdate';
            echo json_encode(array('status' => 200, 'msg' => $msg));    
        }
        
    }  
    public function process()
    {

        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            echo json_encode(array('status' => 404, 'msg' => 'Anda tidak berhak mengubah data.'));
            die();
        }
        $ids = $this->input->post('ids');
        $ids = array_unique($ids);
        foreach ($ids as $key => $id) {
            if(!(int)$id){
                $ids[$key] = decode_url($id);
                $encode_id = $id;
                $id = decode_url($id);
                
                $update_id = $this->status_updated; //verified
$Keterangan = $this->input->post('note');
//$this->createHistory($id, $this->action_verify, '');
$this->m_invoice->newHistory($id, $this->action_verify,$update_id, $Keterangan);
                
            }
        }

        $row = $this->db->where_in('ID',$ids)
                        ->where('Status',$this->status_current) // update status hanya yang AR Checked
                        ->update('Invoice', array('Status' => $update_id));
        if(!$row) {
            echo json_encode(array('status' => 404, 'msg' => 'Gagal mengubah data.'));
        }else{
            $row_aff = $this->db->affected_rows();
            $count = count($ids);

            $msg = $row_aff . ' dokumen berhasil diupdate';
            echo json_encode(array('status' => 200, 'msg' => $msg));    
        }
        
    }
    public function getPiutang()
    {
        header('Content-Type: application/json');

        $ARDocNo = $this->input->post('ARDocNo');

        $row = $this->db->where('ARDocNo',$ARDocNo)->get('ListPiutang')->row();
        if(!$row) {
            echo json_encode(array('status' => '404', 'data' => array()));
        }else{
            echo json_encode(array('status' => 200, 'data' => $row));    
        }
    }
    private function createHistory($id, $status, $note = '')
    {
        if($note == ''){
            $this->input->post('note');
        }
        $data = [
            'IDInvoice' => $id,
            'Action'    => $status,
            'UserID'    => Auth::user()->ID,
            'Timestamp' => date('Y-m-d H:i:s'), 
            'Keterangan'    => $note,
        ];
        $this->db->insert('HistoryInvoice', $data);
    }

        private function update_priority($id)
    {
        $update_id = isset($_POST['IsPriority']) ? true : false;

        $this->db->where('ID',$id)
            ->update('Invoice',array('IsPriority' => $update_id));
    }
        private function get_piutang($IDVendor)
    {
        $this->db->where('Ms_Customer.PairedVendorID', $IDVendor);
        $this->db->join('Ms_Customer', 'ListPiutang.IDCustomer = Ms_Customer.IDCustomer');
        $row = $this->db->get('ListPiutang')->result();

        $lists = [];
        foreach($row as $bil) {
            $lists[$bil->ARDocNo] = $bil->ARDocNo;
        }

        return $lists;
    
    }
    public function ajax()
    {
        header('Content-Type: application/json');
        $id = $this->input->post('id');
        $this->db->join('Ms_Customer','Ms_Customer.IDCustomer = ListPiutang.IDCustomer');
        $query = $this->db->where('ARDocNo', $id)->get('ListPiutang')->row_array();
        echo json_encode($query);
    }

    public function get_ARDocNo($id_vendor)
    {
        header('Content-Type: application/json');        
        $this->db->where('Ms_Customer.PairedVendorID', $id_vendor);
        $this->db->join('Ms_Customer', 'ListPiutang.IDCustomer = Ms_Customer.IDCustomer');
        $query = $this->db->get('ListPiutang');
            $data = [];

            if($query->num_rows() > 0) {
                foreach($query->result() as $row) {
                    $data[] = [
                        'id' => $row->ARDocNo, 
                        'text' => $row->ARDocNo
                    ];
                }
            }
            echo json_encode($data);
    }
    private function countApplied($id)
    {
        $this->db->where('IDInvoice', $id);
        $row = $this->db->get('AppliedDocument')->num_rows();
        return $row;
    }
    public function countAppliedAmount($id)
    {
        $count = 0;

        $this->db->where('IDInvoice', $id);
        $row = $this->db->get('AppliedDocument');
        
        if($row->num_rows() > 0) {
            foreach($row->result() as $row) {
                $count = $count + $row->AppliedAmount;
            }
        }
        return ($count);
    }


    public function reject($id)
    {
        $this->blade->view('keuangan/invoice_tracking/verifikasi/reject',compact('id'));
    }

    public function reject_update($id)
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            echo('Anda tidak berhak mengubah data.');
            die();
        }
        $this->db->trans_start();
        $data = $this->db->where('ID',$id)->get('Invoice')->row()->NoTransmittal;
        $New = $data;
        $NoTransmittal = explode('AK',$data);
        if(count($NoTransmittal)>0){
            $New = $NoTransmittal[0];
        }
        $isSuccess = $this->db->where('ID',$id)
            ->where('Status',10) // update status dari AR Checked
            ->update('Invoice', array('Status' => 4,
                    'NoDokumenSAP' => '',
                    'ReceivedBy' => null,
                    'ReceivedOn' => null,
                    'NoTransmittal' => $New,
                    'StatusTransmittal' => false)); //back to akuntansi (checked)
        $Keterangan = $this->input->post('Keterangan');
        $this->db->trans_complete();
        
        if($isSuccess) {
            //$this->m_invoice->createHistory($id, $this->action_reject, $Keterangan);
            $this->m_invoice->newHistory($id, $this->action_reject, $this->status_reject, $Keterangan);
            notif('success', 'Data berhasil direject.');
            redirect('keuangan/invoice/verifikasi?status=reject_ok');
        }else{
            notif('error', 'Data gagal direject.');
            redirect('keuangan/invoice/verifikasi?status=reject_failed');
        }
    }
    private function update_nilaiverifikasi($id_invoice)
    {
            $items = $this->db->select('(AppliedAmount)')
                                ->where('IDInvoice', $id_invoice)
                                ->get('AppliedDocument')
                                ->result();
            $sum = 0;
            foreach ($items as $value) {
                $sum = (float) $sum + ((float) $value->AppliedAmount);
            }
            $this->db->set("NilaiVerifikasi", "NilaiVerifikasi - $sum", FALSE); 
            $this->db->where("ID", $id_invoice);
            $this->db->update("Invoice");
    }

}


