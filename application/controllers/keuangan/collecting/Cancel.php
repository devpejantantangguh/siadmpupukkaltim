<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Curl\Curl;

class Cancel extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		Auth::guard();
	}

	public function index()
	{
		$this->blade->view('keuangan.collecting.cancel.index');
	}

	/**
	 * Upload Excel Billing Document
	 * @return redirect kembali ke index
	 */
	public function store()
	{

		$config['upload_path'] = './storage/billing_doc/';
		$config['allowed_types'] = 'xls|xlsx|csv'; // xlsm|xltx|xltm
		$config['max_size'] = 10 * 1024;
		$config['overwrite'] = true;
		$config['max_filename'] = 255;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('fileexcel')) {
			show_error($this->upload->display_errors());
		} else {
			$path = $this->upload->data('full_path');

			if (isset($_POST['remove_last'])) {
				$this->db->where(1, 1)->delete('Temp_CancelBillDoc');
			}

			$this->readExcel($path);

			redirect('keuangan/collecting/cancel');
		}
	}

	public function readExcel($path)
	{
		try {
			set_time_limit(0);

			$this->db->trans_start();

			$user_id = Auth::user()->ID;

			$reader = ReaderFactory::create(Type::XLSX);
			$reader->open($path);

			foreach ($reader->getSheetIterator() as $key => $sheet) {
				if ($key != 1) break;
				foreach ($sheet->getRowIterator() as $i => $row) {
					if ($i == 1) continue;
					if (empty($row[0])) continue;

					$this->db->insert('Temp_CancelBillDoc', [
						'NoBillingDoc' => $row[0],
						'CreatedBy' => $user_id,
						'CreatedOn' => date('Y-m-d H:i:s'),
						'Keterangan' => $row[1]
					]);
				}
			}

			$this->db->trans_complete();

		} catch (Exception $e) {
			Log::error($e);
		}
	}

	public function postall()
	{
		$posts = $this->db->get('Temp_CancelBillDoc')->result();

		$ids = [];
		foreach($posts as $id) {
			$ids[] = $id->ID;
		}

		$this->cancelProcess($ids);
	}

	private function cancelProcess($ids)
	{
		$success = 0;
		$failed = 0;

    	foreach($ids as $id) {
    		$row = $this->db->where('ID', $id)->get('Temp_CancelBillDoc')->row();

    		$billdoc = $this->db->where('NoBillingDoc', $row->NoBillingDoc)->get('BillingDocument')->row();

    		// ganti status billing menjadi canceled
			if(!$billdoc || $billdoc->Status != 26) {
				$status = $this->db->where('NoBillingDoc', $row->NoBillingDoc)->update('BillingDocument', [
					'Status' => 26
				]);
				if ($status) {
					$success++;
					$this->db->where('ID', $id)->delete('Temp_CancelBillDoc');

					$this->db->insert('HistoryBillDoc',[
						'NoBillDoc' => $row->NoBillingDoc,
						'Action' => 26,
						'UserID' => Auth::user()->ID,
						'Timestamp' => now(),
						'Keterangan' => $row->Keterangan
					]);
				} else {
					$failed++;
				}
			} else {
				$failed++;
			}
		}

    	if ($failed == 0) {
			echo "{$success} Success";
		} else {
			echo "{$success} Success deleted and {$failed} error";
		}
	}

	public function post()
	{
		$ids = $this->input->post('ids');

		$this->cancelProcess($ids);
	}

	public function get()
	{
		$limit = isset($_GET['take']) ? $_GET['take'] : 100;
		$offset = isset($_GET['skip']) ? $_GET['skip'] : 0;

		if (isset($_GET['orderby'])) {
			$this->db->order_by($_GET['orderby']);
		} else {
			$this->db->order_by('ID', 'DESC');
		}

		$count = clone $this->db;
		$query = $this->db->get('Temp_CancelBillDoc', $limit, $offset);


		$items = $query->result();
		$count = $count->get('Temp_CancelBillDoc')->num_rows();

		$data = [
			'totalCount' => $count,
			'data' => $items
		];

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function remove()
	{
		$ids = $this->input->post('ids');

		foreach ($ids as $id) {
			$this->db->where('ID', $id)->delete('Temp_CancelBillDoc');
		}
	}

	public function edit($id)
	{
		$nobillingdoc = $this->db->where('ID', $id)->get('Temp_CancelBillDoc')->row()->NoBillingDoc;
		$this->blade->view('keuangan.collecting.cancel.edit', compact('id', 'nobillingdoc'));
	}

	public function update($id)
	{
		$this->db->where('ID', $id)->update('Temp_CancelBillDoc', [
			'NoBillingDoc' => $this->input->post('NoBillingDoc')
		]);

		return redirect('keuangan/collecting/cancel/index');
	}
}
