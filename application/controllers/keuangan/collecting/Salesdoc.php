<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

class Salesdoc extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		Auth::guard();
	}

	public function index()
	{
		$this->blade->view('keuangan.collecting.salesdoc.index', [
			'table' => 'Sales Document'
		]);
	}

	public function store()
	{
		$config['upload_path'] = './storage/billing_doc/';
		$config['allowed_types'] = 'xls|xlsx|csv'; // xlsm|xltx|xltm
		$config['max_size'] = 10 * 1024;
		$config['overwrite'] = true;
		$config['max_filename'] = 255;
		$config['encrypt_name'] = true;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('fileexcel')) {
			show_error($this->upload->display_errors());
		} else {
			$path = $this->upload->data('full_path');

			if (isset($_POST['remove_last'])) {
				$this->db->where(1, 1)->delete('Temp_SalesdocDetail');
			}

			$this->readExcel($path);

			redirect('keuangan/collecting/salesdoc');
		}
	}

	public function edit($id)
	{
		if ($this->input->post()) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('NoBillingDoc', 'No Billing Document', 'required');
			$this->form_validation->set_rules('NoSO', 'No SO', 'required');
			$this->form_validation->set_rules('Incoterm', 'Incoterm', 'required');
			$this->form_validation->set_rules('Incoterm2', 'Incoterm2', 'required');
			$this->form_validation->set_rules('HargaSatuan', 'HargaSatuan', 'required');
			$this->form_validation->set_rules('PaymentMethod', 'PaymentMethod', 'required');
			$this->form_validation->set_rules('NoFakturPajak', 'NoFakturPajak', 'required');
			$this->form_validation->set_rules('TglFakturPajak', 'TglFakturPajak', 'required');
			$this->form_validation->set_rules('DPP', 'DPP', 'required');
			$this->form_validation->set_rules('TaxAmount', 'TaxAmount', 'required');

			if ($this->form_validation->run() == false) {
				$query = $this->db->where('ID', $id)->get('Temp_SalesdocDetail');
				if ($query->num_rows() == 0) {
					die('Whoops! ID Not Found');
				}
				$row = $query->row();

				return $this->blade->view('keuangan.collecting.salesdoc.edit', compact('row'));
			} else {
				$data = $this->input->post();
				$data['IsValid'] = 1;
				$this->db->where('ID', $id)->update('Temp_SalesdocDetail', $data);

				$query = $this->db->where('ID', $id)->get('Temp_SalesdocDetail');
				if ($query->num_rows() == 0) {
					die('Whoops! ID Not Found');
				}
				$row = $query->row();
				$success = true;

				return $this->blade->view('keuangan.collecting.salesdoc.edit', compact('row', 'success'));
			}
		} else {
			$query = $this->db->where('ID', $id)->get('Temp_SalesdocDetail');
			if ($query->num_rows() == 0) {
				die('Whoops! ID Not Found');
			}
			$row = $query->row();
			return $this->blade->view('keuangan.collecting.salesdoc.edit', compact('row'));
		}
	}

	public function readExcel($path)
	{
		try {
			set_time_limit(0);
			$user_id = Auth::user()->ID;

			$reader = ReaderFactory::create(Type::XLSX);
			$reader->open($path);

			$this->db->trans_start();

			$this->db->insert('Temp_SalesdocHeader', [
				'CreatedBy' => $user_id,
				'CreatedOn' => date('Y-m-d H:i:s')
			]);

			$insert_id = $this->db->insert_id();

			foreach ($reader->getSheetIterator() as $key => $sheet) {
				if ($key != 1) {
					break;
				}
				foreach ($sheet->getRowIterator() as $i => $row) {
					if ($i == 1) {
						continue;
					}
					if (empty($row[1]) || empty($row[2])) {
						continue;
					}

					$insert = $this->insertRow($insert_id, $row, $user_id, $i);
				}
			}

			$this->db->trans_complete();

			$reader->close();
		} catch (Exception $e) {
			Log::error($e);
		}
	}


	public function checkArray($key, $arr)
	{
		if (array_key_exists($key, $arr)) {
			return $arr[$key];
		}
		return null;
	}

	public function insertRow($parent_id, $row, $user_id, $i)
	{
		$data = [
			'IsValid' => $this->validateData($row),
			//$requiredCols = [0,1,2,4,5,9,10,11,12,13,16,17,18];
			'IDMaterial' => $this->format($this->checkArray(0, $row), 10),
			'NoBillingDoc' => $this->format($this->checkArray(1, $row), 10),
			'NoSO' => $this->format($this->checkArray(2, $row), 10),
			'AlatAngkut' => $this->format($this->checkArray(3, $row), 15),
			'Incoterm' => $this->format($this->checkArray(4, $row), 20),
			'Incoterm2' => $this->format($this->checkArray(5, $row), 30),

			'Tujuan' => $this->format($this->checkArray(7, $row), 50),
			'TempatMuat' => $this->format($this->checkArray(8, $row), 50),
			'HargaSatuan' => $this->format($this->checkArray(9, $row), 18, 'int'),
			'PaymentMethod' => $this->format($this->checkArray(10, $row), 1),
			'NoFakturPajak' => $this->format($this->checkArray(11, $row), 25),
			'TglFakturPajak' => $this->format($this->checkArray(12, $row)),
			'DPP' => $this->format($this->checkArray(13, $row), 18),
			'TaxAmount' => $this->format($this->checkArray(14, $row), 18, 'float'),
			'NoPEB' => $this->format($this->checkArray(15, $row), 20),
			'TglPEB' => $this->format($this->checkArray(16, $row)),
			'NetValueBeforePPH2' => $this->format($this->checkArray(17, $row), 100, 'float'),
			'BillT' => $this->format($this->checkArray(18, $row), 10),
			'FPOption' => $this->format($this->checkArray(19, $row), 2),
		];

		// jika tanggal pelayaran 0, maka tidak di masukkan ke dalam database
		if (!empty($this->checkArray(6, $row)) || $this->checkArray(6, $row) != 0) {
			$data['TglPelayaran'] = $this->format($this->checkArray(6, $row));
		}

		try {
			$this->db->insert('Temp_SalesdocDetail', $data);
		} catch (\Throwable $th) {
			show_error("Terdapat kesalahan di baris ke-{$i}. " . $th->getMessage());
		}
	}

	public function format($field, $max_length = 100, $type = 'string')
	{
		if (is_object($field)) {
			return $field->format('Y-m-d');
		}
		if (strlen($field) > $max_length) {
			return substr($field, 0, $max_length);
		}
		if ($type == 'string') {
			return $field;
		} elseif ($type == 'int') {
			return (int)$field;
		} elseif ($type == 'float') {
			return (float)$field;
		}
	}

	public function validateData($row)
	{
		$requiredCols = [0, 1, 2, 9, 10, 11, 12, 13, 16, 17, 18];

		foreach ($requiredCols as $key => $col) {
			$field = $this->format($this->checkArray($col, $row));
			// DPP bisa di buat 0 jika orang luar (tanpa tax)
			if ($col == 13 && $field == 0) {
				continue;
			}
			if (empty($field)) {
				return 0;
			}
		}

		return 1;
	}

	public function dxDataGridFilters($filters)
	{
		if (strpos($filters, '"and"')) {
			$filter = json_decode($filters);
			if (!empty($filters)) {
				foreach ($filters as $filter) {
					if (is_array($filter)) {
						$this->doFilter($filter);
					}
				}
			}
		} else {
			$filter = json_decode($filters);
			if (is_array($filter)) {
				$this->doFilter($filter);
			}
		}
	}

	public function doFilter($filter)
	{
		if ($filter[1] == 'contains') {
			$this->db->like($filter[0], $filter[2]);
		} elseif ($filter[1] == 'notcontains') {
			$this->db->not_like($filter[0], $filter[2]);
		} elseif ($filter[1] == 'startswith') {
			$this->db->like($filter[0], $filter[2], 'after');
		} elseif ($filter[1] == 'endswith') {
			$this->db->like($filter[0], $filter[2], 'before');
		} else {
			$this->db->where("{$filter[0]} {$filter[1]}", $filter[2]);
		}
	}

	public function get()
	{
		$limit = isset($_GET['take']) ? $_GET['take'] : 100;
		$offset = isset($_GET['skip']) ? $_GET['skip'] : 0;

		if (isset($_GET['orderby'])) {
			$this->db->order_by($_GET['orderby']);
		} else {
			$this->db->order_by('ID', 'DESC');
		}

		if (isset($_GET['filter'])) {
			$this->dxDataGridFilters($_GET['filter']);
		}
		$tempdb = clone $this->db; //clone for count all rows
		$count = $tempdb->from('Temp_SalesdocDetail')->count_all_results();
		$query = $this->db
			->get('Temp_SalesdocDetail', $limit, $offset);
		$res = array();
		$items = array();
		foreach ($query->result_array() as $items) {
			$items['TglPelayaran'] = ($items['TglPelayaran'] == '0000-00-00' or $items['TglPelayaran'] == '1900-01-01') ? '' : $items['TglPelayaran'];
			array_push($res, $items);
		}
		$data = json_decode(json_encode(array(
			'totalCount' => $count,
			'data' => $res,
		)));

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	private function isUploaded($noBilling, $materialID)
	{
		$isExist = $this->db->where('NoBillingDoc', $noBilling)->where('IDMaterial', $materialID)->get('SalesOrder')->num_rows() > 0;

		if($isExist) {
			$this->db->where('IDMaterial', $materialID)->where('NoBillingDoc', $noBilling)->update('Temp_SalesDocDetail',[
				'IsValid' => 2
			]);
		}

		return $isExist;
	}

	public function post()
	{
		$ids = $this->input->post('ids');

		$status['success'] = 0;
		$status['failed'] = 0;
		foreach ($ids as $id) {
			$this->db->trans_start();
			$row = $this->db->where('ID', $id)->get('Temp_SalesdocDetail')->row();

			if ($row->IsValid == 1) {
//				$this->db->where('NoSO', $row->NoSO);
//				$this->db->where('NoBillingDoc', $row->NoBillingDoc);
//				$count = $this->db->from('SalesOrder')->count_all_results();
				$uploaded = $this->isUploaded($row->NoBillingDoc, $row->IDMaterial);
				if (!$uploaded) {
					$this->db->insert('SalesOrder', [
						'NoSO' => $row->NoSO,
						'NoBillingDoc' => $row->NoBillingDoc,
						'Incoterm' => $row->Incoterm,
						'Incoterm2' => $row->Incoterm2,
						'AlatAngkut' => $row->AlatAngkut,
						'TglPelayaran' => $row->TglPelayaran,
						'Tujuan' => $row->Tujuan,
						'TempatMuat' => $row->TempatMuat,
						'NoPEB' => $row->NoPEB,
						'TglPEB' => $row->TglPEB,
						'HargaSatuan' => $row->HargaSatuan,
						'NoFakturPajak' => $row->NoFakturPajak,
						'DPP' => $row->DPP,
						'TaxAmount' => $row->TaxAmount,
						'PaymentMethod' => $row->PaymentMethod,
						'TglFakturPajak' => $row->TglFakturPajak,
						'IDMaterial' => $row->IDMaterial,
						'NetValueBeforePPH2' => $row->NetValueBeforePPH2,
						'BillT' => $row->BillT,
						'FPOption' => $row->FPOption
					]);
					$this->db->where('ID', $id)->delete('Temp_SalesdocDetail');
					$status['success']++;
				} else {
					$status['failed']++;
				}

			} else {
				$status['failed']++;
			}

			$this->db->trans_complete();
		}

		echo "{$status['success']} Document Posted and {$status['failed']} Document Failed to post";
	}

	public function remove()
	{
		$ids = $this->input->post('ids');

		foreach ($ids as $id) {
			$this->db->where('ID', $id)->delete('Temp_SalesdocDetail');
		}
	}

	public function postall()
	{
		$lists = $this->db->where('isValid', 1)->get('Temp_SalesdocDetail')->result();

		foreach ($lists as $salesdoc) {

			$uploaded = $this->isUploaded($salesdoc->NoBillingDoc, $salesdoc->IDMaterial);
			if(!$uploaded) {
				$this->db->insert('SalesOrder', [
					'NoSO' => $salesdoc->NoSO,
					'NoBillingDoc' => $salesdoc->NoBillingDoc,
					'Incoterm' => $salesdoc->Incoterm,
					'Incoterm2' => $salesdoc->Incoterm2,
					'AlatAngkut' => $salesdoc->AlatAngkut,
					'TglPelayaran' => $salesdoc->TglPelayaran,
					'Tujuan' => $salesdoc->Tujuan,
					'TempatMuat' => $salesdoc->TempatMuat,
					'NoPEB' => $salesdoc->NoPEB,
					'TglPEB' => $salesdoc->TglPEB,
					'HargaSatuan' => $salesdoc->HargaSatuan,
					'NoFakturPajak' => $salesdoc->NoFakturPajak,
					'DPP' => $salesdoc->DPP,
					'TaxAmount' => $salesdoc->TaxAmount,
					'PaymentMethod' => $salesdoc->PaymentMethod,
					'TglFakturPajak' => $salesdoc->TglFakturPajak,
					'IDMaterial' => $salesdoc->IDMaterial,
					'NetValueBeforePPH2' => $salesdoc->NetValueBeforePPH2,
					'BillT' => $salesdoc->BillT,
					'FPOption' => $salesdoc->FPOption
				]);
				$this->db->where('ID', $salesdoc->ID)->delete('Temp_SalesdocDetail');
			}
		}
		notif('success', 'Semua data yang valid berhasil di masukkan ke dalam Sales Dokumen');
		redirect('keuangan/collecting/salesdoc');
	}
}
