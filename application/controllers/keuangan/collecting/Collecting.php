<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Collecting_Controller extends CI_Controller {

    protected function _getCustomer($id = false)
    {
        if($id) {
            $this->db->where('IDCustomer',$id);
            $row = $this->db->get('Ms_Customer',1)->row();
            return ($row) ? $row->Customer : null;
        } else {
            $rows = $this->db->get('Ms_Customer')->result();

            $customers = [];
            foreach($rows as $row) {
                $customers[] = array(
                    'id'  => $row->IDCustomer,
                    'name'  => "$row->Customer ({$row->IDCustomer})"
                );
            }

            return $customers;
        }
    }

    protected function _getMaterials()
    {
        $rows = $this->db->get('Ms_Material')->result();

        $materials = [];
        foreach($rows as $row) {
            $materials[] = array(
                'id'  => $row->IDMaterial,
                'name'  => "$row->Material ({$row->IDMaterial})"
            );
        }

        return $materials;
    }

    protected function _getJenisTransaksi()
    {
        $rows = $this->db->get('VL_JenisTransaksi')->result();

        $listJenisTransaksi = [];
        foreach($rows as $row) {
            $listJenisTransaksi[] = array(
                'id'  => $row->ID,
                'name'  => $row->JenisTransaksi
            );
        }

        return $listJenisTransaksi;
    }

    protected function _getStatus()
    {
        $this->db->where('Kategori',2); // Kat. untuk Billing Doc
        $rows = $this->db->get('Status')->result();

        $listStatus = [];
        foreach($rows as $row) {
            $listStatus[] = array(
                'id'  => $row->ID,
                'name'  => $row->Status
            );
        }

        return $listStatus;
    }

    protected function _getSalesOrder($id = false)
    {
        if($id) {
            $this->db->where('NoSO',$id);
            $row = $this->db->get('SalesOrder',1)->row();
            return $row ? $row->ProductDetail : null;
        } else {
            $rows = $this->db->get('SalesOrder')->result();

            $salesOrders = [];
            foreach($rows as $row) {
                $salesOrders[] = array(
                    'id'  => $row->NoSO,
                    'name'  => "$row->ProductDetail (#{$row->NoSO})"
                );
            }

            return $salesOrders;
        }
    }

    protected function _fix($number) {
        return preg_replace('/[^0-9]+/','',$number);
    }

    protected function _validateDate($date, $format = 'Y-m-d') {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    protected function _date($date) {
        if($this->_validateDate($date)) {
            return date('Y-m-d', strtotime($date));    
        }
        return date('Y-m-d');
    }
    
}
