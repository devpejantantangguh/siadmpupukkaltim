<?php
defined('BASEPATH') or exit('No direct script access allowed');

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Curl\Curl;

include __DIR__ . '/Collecting.php';

class Upload extends Collecting_Controller
{
    public function __construct()
    {
        parent::__construct();
        Auth::guard();

        $this->load->model('keuangan/collecting/m_upload', 'm');
        $this->load->model('keuangan/collecting/m_billdoc', 'billdoc');
    }

    public function index()
    {
        $table = 'BillingDocument';
        $this->blade->view('keuangan.collecting.upload.index', compact('table'));
    }

    public function store()
    {
        $config['upload_path']          = './storage/billing_doc/';
        $config['allowed_types']        = 'xls|xlsx|csv'; // xlsm|xltx|xltm
        $config['max_size']             = 10 * 1024;
        $config['overwrite']            = true;
        $config['max_filename']         = 255;
        // $config['encrypt_name']         = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('fileexcel')) {
            show_error($this->upload->display_errors());
        } else {
            $path = $this->upload->data('full_path');

            if (isset($_POST['remove_last'])) {
                $this->db->where(1, 1)->delete('Temp_BillDocDetail');
            }

            $this->readExcel($path);

            redirect('keuangan/collecting/upload');
        }
    }

    public function remove()
    {
        $ids = $this->input->post('ids');
        if (!empty($ids)) {
            foreach ($ids as $id) {
                $this->m->remove($id);
            }
        }
        echo json_encode(array('status' => 'success'));
    }

    public function edit($id)
    {
        if ($this->input->post()) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('NoBillingDoc', 'No Billing Document', 'required');
            $this->form_validation->set_rules('Sektor', 'Sektor', 'required');
            $this->form_validation->set_rules('BillingDate', 'BillingDate', 'required');
            $this->form_validation->set_rules('NoSO', 'NoSO', 'required');
            $this->form_validation->set_rules('IDCustomer', 'IDCustomer', 'required');
            $this->form_validation->set_rules('Customer', 'Customer', 'required');
            $this->form_validation->set_rules('IDMaterial', 'IDMaterial', 'required');
            $this->form_validation->set_rules('Material', 'Material', 'required');
            $this->form_validation->set_rules('Kuantum', 'Kuantum', 'required');
            $this->form_validation->set_rules('UoM', 'UoM', 'required');
            $this->form_validation->set_rules('Kurs', 'Kurs', 'required');

            if ($this->form_validation->run() == false) {
                $row = $this->db->where('ID', $id)->get('Temp_BillDocDetail', 1)->row();
                if (!isset($row)) {
                    die("Whoops!");
                }

                return  $this->blade->view('keuangan.collecting.upload.edit', compact('row'));
            } else {
                $data = $this->input->post();
                $data['IsValid'] = 1;
                $this->db->where('ID', $id)->update('Temp_BillDocDetail', $data);

                $row = $this->db->where('ID', $id)->get('Temp_BillDocDetail', 1)->row();
                if (!isset($row)) {
                    die("Whoops!");
                }
                $success = true;

                return $this->blade->view('keuangan.collecting.upload.edit', compact('row', 'success'));
            }
        } else {
            $row = $this->db->where('ID', $id)->get('Temp_BillDocDetail', 1)->row();
            if (!isset($row)) {
                die("Whoops!");
            }
            $this->blade->view('keuangan.collecting.upload.edit', compact('row'));
        }
    }

    public function post()
    {
        $ids = $this->input->post('ids');

        $status = [
            'success'    => 0,
            'failed'    => 0
        ];
        foreach ($ids as $id) {
            $this->db->trans_start();

            $row = $this->db->where('ID', $id)->get('Temp_BillDocDetail')->row();
            $uploaded = $this->hasUploaded($row->NoBillingDoc, $row->IDMaterial);

            if (!$row || $uploaded) {
                $status['failed']++;
            } else {
                $this->db->where('ID', $id)->delete('Temp_BillDocDetail');
                $this->db->where('NoBillingDoc', $row->NoBillingDoc);
                $this->db->where('IDMaterial', $row->IDMaterial);
                $count = $this->db->from('BillingDocument')->count_all_results();
                if ($count == 0) {
                    $this->db->insert('BillingDocument', [
                        // 'ID'                => $row->ID,
                        'NoBillingDoc'      => $row->NoBillingDoc,
                        'Sektor'            => $row->Sektor,
                        'JenisTransaksi'    => 1,
                        'BillingDate'       => $row->BillingDate,
                        'NoSO'              => $row->NoSO,
                        'IDCustomer'        => $row->IDCustomer,
                        'Customer'          => $row->Customer,
                        'Status'            => 21,
                        'IDMaterial'        => $row->IDMaterial,
                        'Material'          => $row->Material,
                        'Kuantum'           => $row->Kuantum,
                        'Kurs'              => $row->Kurs,
                        'UoM'               => $row->UoM,
                        'LifeCycle'         => 0,
                        'IDPayer'           => $row->IDCustomer,
                        'Payer'             => $row->Customer,
                        'Keterangan'        => $row->Keterangan,
                        'NoBAST'            => $row->NoBAST,
                        'NoRegSurveyor'     => $row->NoRegSurveyor,
                        'NoSKBDN'           => $row->NoSKBDN,
                        'TglSKBDN'          => $row->TglSKBDN,
                        'Bank'              => $row->Bank,
                    ]);
                }
                $status['success']++;
            }

            $this->db->trans_complete();
        }

        echo "Post Success " . $status['success'] . ' and ' . $status['failed'];
    }

    public function readExcel($path)
    {
        try {
            set_time_limit(0);
            $user_id = Auth::user()->ID;

            $reader = ReaderFactory::create(Type::XLSX);
            $reader->open($path);

            $this->db->trans_start();

            $this->db->insert('Temp_BillDocHeader', array(
                'CreatedBy' => $user_id,
                'CreatedOn' => date('Y-m-d H:i:s')
            ));
            $insert_id = $this->db->insert_id();

            foreach ($reader->getSheetIterator() as $key => $sheet) {
                if ($key != 1) {
                    break;
                }
                foreach ($sheet->getRowIterator() as $i => $row) {
                    if ($i == 1) {
                        continue;
                    }
                    if (empty($row[1]) || empty($row[2])) {
                        continue;
                    }

                    $this->insertRow($insert_id, $row, $user_id, $i);
                }
            }

            $this->db->trans_complete();

            $reader->close();
        } catch (Exception $e) {
            Log::error($e);
        }
    }

    public function insertRow($insert_id, $row, $user_id, $i)
    {
        $data = [
            'IsValid'           => $this->validateData($row),
            'IDBillDocHeader'   => $insert_id,
            'NoBillingDoc'      => $this->format($row[0], 10),
            'Sektor'            => $this->format($row[1], 5),
            'BillingDate'       => $this->format($row[2]),
            'NoSO'              => $this->format($row[3], 10),
            'IDCustomer'        => $this->format($row[4], 10),
            'Customer'          => $this->format($row[5], 30),
            'IDMaterial'        => $this->format($row[6], 10),
            'Material'          => $this->format($row[7], 250),
            'Kuantum'           => $this->format($row[8], 18, 'float'),
            'UoM'               => $this->format($row[9], 7),
            'Kurs'              => $this->format($row[10], 3),
        ];

        // hanya tambahan details
        if (isset($row[11])) {
            $data = array_merge($data, [
                'Keterangan'        => $this->format($row[11], 255),
                'NoBAST'            => $this->format($row[12], 20),
                'NoRegSurveyor'     => $this->format($row[13], 20),
                'NoSKBDN'           => $this->format($row[14], 20),
                'TglSKBDN'          => $this->format($row[15]),
                'Bank'              => $this->format($row[16], 20),
            ]);
        }

        $this->db->insert('Temp_BillDocDetail', $data);
    }

    public function format($field, $max_length = 100, $type = 'string')
    {
        if (is_object($field)) {
            return $field->format('Y-m-d');
        }
        if (strlen($field) > $max_length) {
            return substr($field, 0, $max_length);
        }
        if ($type == 'string') {
            return $field;
        } elseif ($type == 'int') {
            return (int) $field;
        } elseif ($type == 'float') {
            return (float) $field;
        }
    }

    public function validateData($row)
    {
        $requiredCols = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

        foreach ($requiredCols as $key => $col) {
            $field = $this->format($row[$col]);
            if (empty($field)) {
                return 0;
            }
        }
        return 1;
    }

    public function dxDataGridFilters($filters)
    {
        if (strpos($filters, '"and"')) {
            $filter = json_decode($filters);
            if (!empty($filters)) {
                foreach ($filters as $filter) {
                    if (is_array($filter)) {
                        $this->doFilter($filter);
                    }
                }
            }
        } else {
            $filter = json_decode($filters);
            if (is_array($filter)) {
                $this->doFilter($filter);
            }
        }
    }

    public function doFilter($filter)
    {
        if ($filter[1] == 'contains') {
            $this->db->like($filter[0], $filter[2]);
        } elseif ($filter[1] == 'notcontains') {
            $this->db->not_like($filter[0], $filter[2]);
        } elseif ($filter[1] == 'startswith') {
            $this->db->like($filter[0], $filter[2], 'after');
        } elseif ($filter[1] == 'endswith') {
            $this->db->like($filter[0], $filter[2], 'before');
        } else {
            $this->db->where("{$filter[0]} {$filter[1]}", $filter[2]);
        }
    }
    
    private function hasUploaded($noBillingDoc, $materialId)
	{
		$row = $this->db->where('NoBillingDoc', $noBillingDoc)->where('IDMaterial', $materialId)->get('BillingDocument')->row();

		if($row) {
			if($row->Status == 26) {
				$this->db->where('NoBillingDoc', $noBillingDoc)->delete('BillingDocument');
				$this->db->where('NoBillingDoc', $noBillingDoc)->delete('SalesOrder');
				return false;
			}
			$this->db->where('IDMaterial', $materialId)->where('NoBillingDoc', $noBillingDoc)->update('Temp_BilldocDetail', [
				'IsValid' => 2
			]);
		}

		return $row != null;
	}

    public function postall()
    {
        $ids = $this->db->select('ID')->where('IsValid', 1)->get('Temp_BillDocDetail')->result();

        $status = [
            'success'    => 0,
            'failed'    => 0
        ];

        foreach ($ids as $row) {
            $id = $row->ID;
            $this->db->trans_start();

            $row = $this->db->where('ID', $id)->get('Temp_BillDocDetail')->row();
            $uploaded = $this->hasUploaded($row->NoBillingDoc, $row->IDMaterial);

            if (!$row || $uploaded) {
                $status['failed']++;
            } else {
                $this->db->where('ID', $id)->delete('Temp_BillDocDetail');

                $this->db->where('NoBillingDoc', $row->NoBillingDoc);
                $this->db->where('IDMaterial', $row->IDMaterial);
                $count = $this->db->from('BillingDocument')->count_all_results();
                if ($count == 0) {
                    $this->db->insert('BillingDocument', [
                        // 'ID'                => $row->ID,
                        'NoBillingDoc'      => $row->NoBillingDoc,
                        'Sektor'            => $row->Sektor,
                        'JenisTransaksi'    => 1,
                        'BillingDate'       => $row->BillingDate,
                        'NoSO'              => $row->NoSO,
                        'IDCustomer'        => $row->IDCustomer,
                        'Customer'          => $row->Customer,
                        'Status'            => 21,
                        'IDMaterial'        => $row->IDMaterial,
                        'Material'          => $row->Material,
                        'Kuantum'           => $row->Kuantum,
                        'Kurs'              => $row->Kurs,
                        'UoM'               => $row->UoM,
                        'LifeCycle'         => 0,
                        'IDPayer'           => $row->IDCustomer,
                        'Payer'             => $row->Customer,
                        'Keterangan'        => $row->Keterangan,
                        'NoBAST'            => $row->NoBAST,
                        'NoRegSurveyor'     => $row->NoRegSurveyor,
                        'NoSKBDN'           => $row->NoSKBDN,
                        'TglSKBDN'          => $row->TglSKBDN,
                        'Bank'              => $row->Bank,
                    ]);
                }
                $status['success']++;
            }

            $this->db->trans_complete();
        }

        notif('success', 'Semua data yang valid berhasil di masukkan ke Billing Document');
        redirect('keuangan/collecting/upload');
    }

    public function get()
    {
        ini_set('memory_limit', '256M'); // This also needs to be increased in some cases. Can be changed to a higher value as per need)
        ini_set('sqlsrv.ClientBufferMaxKBSize', '524288'); // Setting to 512M

        $limit = isset($_GET['take']) ? $_GET['take'] : 100;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;

        if (isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        } else {
            $this->db->order_by('IDBillDocHeader', 'DESC');
        }

        if (isset($_GET['filter'])) {
            $this->dxDataGridFilters($_GET['filter']);
        }

        $count = clone $this->db;
        $query = $this->db->get('Temp_BillDocDetail', $limit, $offset);


        $items = $query->result();
        $count = $count->get('Temp_BillDocDetail')->num_rows();

        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $items
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }
}
