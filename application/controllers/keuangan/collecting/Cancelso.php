<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use Curl\Curl;

class Cancelso extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        Auth::guard();
    }

    public function index()
    {
        $this->blade->view('keuangan.collecting.cancelso.index');
    }

    /**
     * Upload Excel Sales Order
     * @return redirect kembali ke index
     */
    public function store()
    {

        $config['upload_path']          = './storage/billing_doc/';
        $config['allowed_types']        = 'xls|xlsx|csv'; // xlsm|xltx|xltm
        $config['max_size']             = 10 * 1024;
        $config['overwrite']            = true;
        $config['max_filename']         = 255;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('fileexcel')) {
            show_error($this->upload->display_errors());
        } else {
            $path = $this->upload->data('full_path');

            if(isset($_POST['remove_last'])) {
                $this->db->where(1,1)->delete('Temp_CancelSalesOrder');
            }

            $this->readExcel($path);

            redirect('keuangan/collecting/cancelso');
        }
    }

    public function readExcel($path)
    {
        try {
            set_time_limit(0);

            $this->db->trans_start();

            $user_id = Auth::user()->ID;

            $reader = ReaderFactory::create(Type::XLSX);
            $reader->open($path);

            foreach($reader->getSheetIterator() as $key => $sheet) {
                if($key != 1) break;
                foreach($sheet->getRowIterator() as $i => $row) {
                    if($i == 1) continue;
                    if(empty($row[0])) continue;

                    $this->db->insert('Temp_CancelSalesOrder', [
                        'NoSO' => $row[0],
                        'CreatedBy' => $user_id,
                        'CreatedOn' => date('Y-m-d H:i:s')
                    ]);
                }
            }

            $this->db->trans_complete();

        } catch (Exception $e) {
            Log::error($e);
        }
    }

    public function postall()
    {
        $posts = $this->db->get('Temp_CancelSalesOrder')->result();

        $success = 0;
        $failed = 0;
        foreach($posts as $post) {
            $this->db->trans_start();

            $id = $post->NoSO;
            $success = $this->db->where('NoSO', $id)->delete('SalesOrder');
            if($success) {
                $this->db->where('NoSO', $id)->delete('Temp_CancelSalesOrder');
                $success++;
            } else {
                $failed++;
            }

            $this->db->trans_complete();
        }

        if($failed == 0) {
            echo "{$success} Success";
        } else {
            echo "{$success} Success deleted and {$failed} error";
        }

    }

    public function post()
    {
        $ids = $this->input->post('ids');

        $success = 0;
        $failed = 0;
        foreach($ids as $id) {
            $this->db->trans_start();

            $row = $this->db->where('ID', $id)->get('Temp_CancelSalesOrder')->row();

            $status = $this->db->where('NoSO', $row->NoSO)->delete('SalesOrder');
            if($status) {
                $success++;
                $this->db->where('ID', $id)->delete('Temp_CancelSalesOrder');
            } else {
                $failed++;
            }

            $this->db->trans_complete();
        }

        if($failed == 0) {
            echo "{$success} Success";
        } else {
            echo "{$success} Success deleted and {$failed} error";
        }
    }

    public function get()
    {
        $limit = isset($_GET['take']) ? $_GET['take'] : 100;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;

        if(isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        } else {
            $this->db->order_by('ID', 'DESC');
        }

        $count = clone $this->db;
        $query = $this->db->get('Temp_CancelSalesOrder', $limit, $offset);


        $items = $query->result();
        $count = $count->get('Temp_CancelSalesOrder')->num_rows();

        $data = [
            'totalCount'    => $count,
            'data'          => $items
        ];

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function remove()
    {
        $ids = $this->input->post('ids');

        foreach($ids as $id) {
            $this->db->where('ID', $id)->delete('Temp_CancelSalesOrder');
        }
    }

    public function edit($id)
    {
        $NoSO = $this->db->where('ID',$id)->get('Temp_CancelSalesOrder')->row()->NoSO;
        $this->blade->view('keuangan.collecting.cancelso.edit', compact('id', 'NoSO'));
    }

    public function update($id)
    {
        $this->db->where('ID',$id)->update('Temp_CancelSalesOrder', [
            'NoSO' => $this->input->post('NoSO')
        ]);

        return redirect('keuangan/collecting/cancelso/index');
    }
}
