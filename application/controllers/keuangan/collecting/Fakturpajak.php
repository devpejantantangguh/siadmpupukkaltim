<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FakturPajak extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('keuangan/collecting/M_fakturpajak','m');
        Auth::guard();
    }

    public function index()
    {
        $this->blade->view('keuangan.collecting.faktur_pajak.index', array(
            'table'     => 'FakturPajak',
        ));
    }

    public function get()
    {
        $limit = isset($_GET['take']) ? $_GET['take'] : 10;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;

        if(isset($_GET['orderby'])) {
            $this->db->order_by('ID', $_GET['orderby']);
        } else {
            $this->db->order_by('ID', 'DESC');
        }

//        $query = $this->db->select('f.ID, f.NPWP_PKT, f.NoFakturPajak, f.Timestamp, c.IDCustomer as IDRekanan,
//        							c.Customer, b.Material, c1.Customer as CreatedByName, c2.Customer as UpdatedByName')
//                ->join('Ms_Customer as c','c.NPWP = f.NPWP_Customer','left')
//                ->join('SalesOrder s','s.NoFakturPajak = f.NoFakturPajak','left')
//                ->join('BillingDocument as b','s.NoBillingDoc = b.NoBillingDoc AND s.IDMaterial = b.IDMaterial','left')
//                ->join('Ms_Customer as c1','c1.IDCustomer = f.CreatedBy','left')
//                ->join('Ms_Customer as c2','c2.IDCustomer = f.UpdatedBy','left');

        $query = $this->db->select('f.ID, f.NPWP_PKT, f.NoFakturPajak, b.TglFakturPajak as Timestamp, c.IDCustomer as IDRekanan, 
        							c.Customer, b.Material, s.NetValueBeforePPH2 as Total')
			->join('Ms_Customer as c','c.NPWP = f.NPWP_Customer','left')
			->join('vw_billdoc_v3 b', 'b.NoFakturPajak = f.NoFakturPajak','left')
			->join('SalesOrder s', 'b.NoBillingDoc = s.NoBillingDoc','left')
			->join('Ms_Customer as c1','c1.IDCustomer = f.CreatedBy','left')
			->join('Ms_Customer as c2','c2.IDCustomer = f.UpdatedBy','left');

        $total = clone $query;
		$query = $this->db->get('FakturPajak as f');

        $items = $query->result();
        $count = $total->get('FakturPajak as f')->num_rows();

        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $items
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function upload()
    {
        $config['upload_path']          = './storage/faktur_pajak/';
        // $config['allowed_types']        = 'doc|pdf|docx|csv|xlsx';
        $config['allowed_types']        = 'pdf';
        $config['max_size']             = 2 * 1024;
        $config['overwrite']            = true;
        $config['max_filename']         = 255;
        $config['remove_spaces']        = true;
        // $config['encrypt_name']         = true;

        $this->load->library('upload', $config);

        if( ! $this->upload->do_upload('file')) {
            header("HTTP/1.0 405 Method Not Allowed");
            die($this->upload->display_errors(null,null));
        } else {
            $upload_data = $this->upload->data();

            $detail = $this->m->detail_fakturpajak($upload_data['file_name']);

            $this->db->where('FileName',$upload_data['file_name'])->delete('FakturPajak');

            $data = array(
                // 'IDParent'  => $id_parent,
                'FileName'  => $upload_data['file_name'],
                'NPWP_PKT'  => $detail['NPWP_PKT'],
                'NoFakturPajak'  => $detail['NoFakturPajak'],
                'NPWP_Customer'  => $detail['NPWP_Customer'],
                'Timestamp'  => $detail['TglFakturPajak'],
                'CreatedBy'  => Auth::user()->ID,
                'CreatedOn'  => date('Y-m-d H:i:s'),
            );

            $this->db->insert('FakturPajak', $data);
        }
    }
}
