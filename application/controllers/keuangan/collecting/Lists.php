<?php
defined('BASEPATH') or exit('No direct script access allowed');

include __DIR__ . '/Collecting.php';

class Lists extends Collecting_Controller
{

	public function __construct()
	{
		parent::__construct();
		Auth::guard();
		$this->load->model('keuangan/collecting/m_approval', 'approval');
		$this->load->model('keuangan/collecting/m_lists', 'm_lists');
	}

	public function index()
	{
		$this->blade->view('keuangan.collecting.lists.index', array(
			'table' => 'BillingDocument'
		));
	}

	private function getJenisTransaksi()
	{
		$rows = $this->db->where('Kategori', 1)->get('VL_JenisTransaksi')->result();

		$listJenisTransaksi = [];
		foreach ($rows as $row) {
			$listJenisTransaksi[$row->ID] = $row->JenisTransaksi;
		}

		return $listJenisTransaksi;
	}

	public function test()
	{
		$this->db->insert('dokumenpajak', [
			'IDParent' => 1,
			'Kategori' => 2,
			'Tipe' => 1,
			'FilePath' => 'xxx',
			'CreatedOn' => date('Y-m-d')
		]);

		$this->db->insert('dokumenpajak', [
			'IDParent' => 1,
			'Kategori' => 2,
			'Tipe' => 2,
			'FilePath' => 'xxx',
			'CreatedOn' => date('Y-m-d')
		]);

		$this->db->insert('dokumenpajak', [
			'IDParent' => 2,
			'Kategori' => 2,
			'Tipe' => 1,
			'FilePath' => 'xxx',
			'CreatedOn' => date('Y-m-d')
		]);

		$this->db->insert('dokumenpajak', [
			'IDParent' => 2,
			'Kategori' => 2,
			'Tipe' => 2,
			'FilePath' => 'xxx',
			'CreatedOn' => date('Y-m-d')
		]);

		echo 'ok';
	}

	public function edit($id)
	{
		$jenisTransaksi = $this->getJenisTransaksi();

		if ($this->input->post()) {

			$this->load->library('form_validation');
			$this->form_validation->set_rules('NoBillingDoc', 'No Billing Document', 'required');
			$this->form_validation->set_rules('Sektor', 'Sektor', 'required');
			$this->form_validation->set_rules('BillingDate', 'BillingDate', 'required');
			$this->form_validation->set_rules('NoSO', 'NoSO', 'required');
			$this->form_validation->set_rules('IDCustomer', 'IDCustomer', 'required');
			$this->form_validation->set_rules('Customer', 'Customer', 'required');
			$this->form_validation->set_rules('IDMaterial', 'IDMaterial', 'required');
			$this->form_validation->set_rules('Material', 'Material', 'required');
			$this->form_validation->set_rules('Kuantum', 'Kuantum', 'required');
			$this->form_validation->set_rules('UoM', 'UoM', 'required');
			$this->form_validation->set_rules('Kurs', 'Kurs', 'required');

			if ($this->form_validation->run() == FALSE) {

				$data = $this->db->where('ID', $id)->get('BillingDocument', 1)->row();
				if (!isset($data)) die("Whoops!");
				$histories = $this->db->where('NoBillDoc', $data->NoBillingDoc)->get('HistoryBillDoc')->result();
				$this->blade->view('keuangan.collecting.lists.edit', compact('data', 'histories', 'jenisTransaksi'));
			} else {
				$request = $this->input->post();
				$this->db->where('ID', $id)->update('BillingDocument', $request);

				$data = $this->db->where('ID', $id)->get('BillingDocument', 1)->row();
				$this->db->insert('HistoryBillDoc', array(
					'NoBillDoc' => $data->NoBillingDoc,
					'Action' => 1,
					'UserID' => Auth::user()->ID,
					'Timestamp' => date('d-m-Y H:i:s'),
					'Keterangan' => 'Update Bill Doc'
				));

				$data = $this->db->where('ID', $id)->get('BillingDocument', 1)->row();
				if (!isset($data)) die("Whoops!");
				$histories = $this->db->where('NoBillDoc', $data->NoBillingDoc)->get('HistoryBillDoc')->result();
				$success = true;

				$this->blade->view('keuangan.collecting.lists.edit', compact('data', 'histories', 'jenisTransaksi', 'success'));
			}
		} else {
			$data = $this->db->where('ID', $id)->get('BillingDocument', 1)->row();
			if (!isset($data)) die("Whoops!");
			$histories = $this->db->where('NoBillDoc', $data->NoBillingDoc)->get('HistoryBillDoc')->result();
			$this->blade->view('keuangan.collecting.lists.edit', compact('data', 'histories', 'jenisTransaksi'));
		}

		/*
        'customers'             => $this->_getCustomer(),
        'materials'             => $this->_getMaterials(),
        */
	}

	public function update($id)
	{
		try {
			$this->db->trans_start();
			// $this->load->library('form_validation');
			// $this->form_validation->set_rules('NoBillingDoc','','required|numeric');

			$billDoc = $this->db->where('ID', $id)->get('BillingDocument', 1)->row();

			$this->db->insert('HistoryBillDoc', array(
				'NoBillDoc' => $billDoc->NoBillingDoc,
				'Action' => 1,
				'UserID' => Auth::user()->ID,
				'Timestamp' => date('d-m-Y H:i:s'),
				'Keterangan' => 'Update Bill Doc'
			));
			$this->db->reset_query();

			$rules = array(
				'NoBillingDoc' => 'required',
				'JenisTransaksi' => 'required',
				'Sektor' => 'required',
				'BillingDate' => 'required',
				'NoSO' => 'required',
				'NoGI' => 'required',
				'NoFakturPajak' => 'required',
				'IDCustomer' => 'required',
				// 'Customer' => 'required',
				'Status' => 'required',
				'IDMaterial' => 'required',
				'Material' => 'required',
				'Kuantum' => 'required',
				'UoM' => 'required',
				'Kurs' => 'required',
				'HargaSatuan' => 'required',
				'Jumlah' => 'required',
				'DPP' => 'required',
				'TaxAmount' => 'required',
				'Total' => 'required',
				'Keterangan' => 'trim',
				'IDPayer' => 'required',
				// 'Payer' => 'required',
				'IDSalesOrg' => 'required',
				// 'SalesOrg' => 'required',
			);

			foreach ($rules as $field => $rule) {
				$data[$field] = $this->input->post($field);
			}

			$data['Customer'] = $this->_getCustomer($data['IDCustomer']);
			$data['SalesOrg'] = $this->_getSalesOrder($data['IDSalesOrg']);
			$data['Payer'] = $this->_getCustomer($data['IDPayer']);
			$data['LastDownload'] = date('Y-m-d H:i:s');

			$this->db->where('ID', $id)
				->update('BillingDocument', $data);

			$this->db->trans_complete();

			header('Content-Type: application/json');
			echo json_encode(array('status' => 'success'));
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	public function get()
	{
		$this->m_lists->collection();
	}

	public function approve()
	{
		$ids = $this->input->post('ids');

		foreach ($ids as $id) {
			$this->approval->approve_document($id);
		}
	}

	public function approveall()
	{
		//approve semua yang belum pernah diproses oleh logged user id.
		$user_id = Auth::user()->ID;
		$this->db->order_by('BillingDocument.Status', 'ASC');
		$this->db->order_by('BillingDocument.NoBillingDoc', 'DESC');
		$this->db->where('UserID', null);
		$this->db->join('HistoryBillDoc', 'HistoryBillDoc.NoBillDoc = BillingDocument.NoBillingDoc', 'LEFT');
		$ids = $this->db->get('BillingDocument', '1')->result();
		//print_r($ids);
		foreach ($ids as $id) {
			$this->approval->approve_document($id->NoBillingDoc);
			echo 'go';
			//echo $id->NoBillingDoc;
		}
	}

	public function cancel()
	{
		$ids = $this->input->post('ids');

		foreach ($ids as $id) {
			$billing = $this->db->where('NoBillingDoc', $id)->get('BillingDocument')->row();
			if($billing->Status != 26) {
				$this->db->where('NoBillingDoc', $id)->update('BillingDocument', [
					'Status' => 26
				]);

				$this->db->insert('HistoryBillDoc',[
					'NoBillDoc' => $billing->NoBillingDoc,
					'Action' => 26,
					'UserID' => Auth::user()->ID,
					'Timestamp' => now(),
					'Keterangan' => $_REQUEST['keterangan'] ?? 'Dibatalkan'
				]);
			}

		}
	}

	public function dokumen_pajak($id, $type)
	{
		$query = $this->db->where('IDParent', $id)->where('Tipe', $type)->get('DokumenPajak');

		if ($query->num_rows() > 0) {
			$row = $query->row();

			if ($type == 1) {
				$status = 'SPP';
			} elseif ($type == 2) {
				$status = 'Bukti Potong';
			}

			$this->blade->view('keuangan/collecting/lists/dokumen_pajak', [
				'status' => $status,
				'data' => $row
			]);
		} else {
			show_404('Dokumen Pajak Tidak Ditemukan');
			die();
		}
	}

	public function document_update($id, $type)
	{
		if($_REQUEST['status'] == 'approve') {
			$this->db->update('DokumenPajak', [
				'Judul' => $_POST['Judul'],
				'ApprovedBy' => Auth::user()->ID,
				'ApprovedOn' => date('Y-m-d H:i:s')
			], [
				'IDParent' => $id,
				'Kategori' => 2,
				'Tipe' => $type
			]);
		} else {
			$this->db->update('DokumenPajak', [
				'Judul' => 'Reject ' . $_POST['Judul'],
				'Tipe' => $type == 1 ? 3 : 4
			], [
				'IDParent' => $id,
				'Kategori' => 2,
				'Tipe' => $type
			]);
		}

		redirect("keuangan/collecting/lists?submit=success&type={$type}");
	}

	public function document_popup()
	{
		$query = $this->db->where('IDParent', $_GET['id'])->where('Tipe', $_GET['type'])->get('DokumenPajak');

		if ($query->num_rows() > 0) {
			$row = $query->row();

			$this->blade->view('keuangan/collecting/lists/document_popup', [
				'data' => $row
			]);
		} else {
			die('Dokumen Pajak Tidak Ditemukan');
		}
	}

	public function approve_spp()
	{
		$ids = $_POST['ids'];
		$user_id = Auth::user()->ID;

		foreach ($ids as $id) {
			$this->db->where([
				'IDParent' => $id,
				'Kategori' => 2,
				'Tipe' => 1 // SPP ID Tipenya 1
			])->update('DokumenPajak', [
				'ApprovedBy' => $user_id,
				'ApprovedOn' => date('Y-m-d H:i:s')
			]);
		}

		echo 'done';
	}

	public function approve_bukpot()
	{
		$ids = $_POST['ids'];
		$user_id = Auth::user()->ID;

		foreach ($ids as $id) {
			$this->db->where([
				'IDParent' => $id,
				'Kategori' => 2,
				'Tipe' => 2 // Bukpot ID Tipenya 2
			])->update('DokumenPajak', [
				'ApprovedBy' => $user_id,
				'ApprovedOn' => date('Y-m-d H:i:s')
			]);
		}

		echo 'done';
	}

	public function materials()
	{
		$no_billing = $_GET['id'];

		$materials = $this
			->db
			->where('NoBillingDoc', $no_billing)
			->get('billingdocument')
			->result();

		foreach ($materials as $key => $material) {
			$sales_order = $this->db->select('NetValueBeforePPH2')
				->where('NoSO', $material->NoSO)
				->where('IDMaterial', $material->IDMaterial)
				->get('SalesOrder')
				->row();

			if($material->Kurs == 'IDR') {
				$materials[$key]->NetValue = 'Rp.' . number_format($sales_order->NetValueBeforePPH2,0,',','.');
			} elseif($material->Kurs == 'USD') {
				$materials[$key]->NetValue = '$' . number_format($sales_order->NetValueBeforePPH2);
			} else {
				$materials[$key]->NetValue = $material->Kurs . ' ' . number_format($sales_order->NetValueBeforePPH2);
			}

		}

		$this->blade->view('keuangan.collecting.lists.materials', compact('materials', 'no_billing'));
	}
}
