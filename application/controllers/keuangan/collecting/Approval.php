<?php
defined('BASEPATH') or exit('No direct script access allowed');

include __DIR__ . '/Collecting.php';

class Approval extends Collecting_Controller
{
	private $users_id = [];
	private $global_setting = null;

	const APR_ALREADY_APPROVE = 1;
	const APR_TOTAL_NOT_VALID = 2;
	const APR_ENSURE_DOCUMENT_UPLOADED = 3;
	const APR_SUCCESS = 4;

	public function __construct()
	{
		parent::__construct();
		Auth::guard();
		$this->load->model('keuangan/collecting/m_approval', 'm');
		$this->load->model('keuangan/collecting/m_lists', 'lists');

		$globals = $this->db->like('Setting', "Approval")->get('globalsettings')->result();

		$users_id = [];
		foreach ($globals as $global) {
			$users_id[] = $global->Value;
		}

		$this->users_id = $users_id;
	}

	public function index()
	{
		$message = false;
		if (!in_array(Auth::user()->ID, $this->users_id)) {
			$message = "You are not authorized to approve document";
		}

		$this->blade->view('keuangan.collecting.approval.index', array(
			'table' => 'BillingDocument',
			'message' => $message
		));
	}

	public function show($level)
	{
		$this->blade->view('keuangan.collecting.approval.index', array(
			'table' => 'BillingDocument',
			'level' => $level
		));
	}

	public function get($level = 21)
	{
		$this->lists->collection('approval');
	}

	public function approve()
	{
		if (!in_array(Auth::id(), $this->users_id)) {
			// not allow
			notif('error', "Anda tidak berhak melakukan Approval");
		} else {
			// allow to update
			$success = 0;
			$warning = 0;
			$error = 0;
			$approved = 0;
			$ids = $_REQUEST['ids'];
			foreach ($ids as $id) {
				$response = $this->approveProcess($id);

				if($response == self::APR_SUCCESS) {
					$success++;
				} elseif($response == self::APR_ENSURE_DOCUMENT_UPLOADED) {
					$warning++;
				} elseif($response == self::APR_TOTAL_NOT_VALID) {
					$error++;
				} elseif($response == self::APR_ALREADY_APPROVE) {
					$approved++;
				}
			}

			$messages = [];
			if($success) {
				$messages[] = $success . " Dokumen Berhasil di Approve";
			}
			if($warning) {
				$messages[] = "Warning : " . $warning . " Dokumen memerlukan upload dokumen";
			}
			if($error) {
				$messages[] = "Error: " . $error . " Dokumen tidak valid.";
			}
			if($approved) {
				$messages[] = "Warning : " . $approved . " Dokumen sudah di approve";
			}

			notif('results', $messages);
		}
	}

	public function approve_all()
	{
		if (!in_array(Auth::id(), $this->users_id)) {
			// not allow
			notif('error', "Anda tidak berhak melakukan Approval");
		} else {
			$global_setting = [];
			$settings = $this->db->get('globalsettings')->result();
			foreach ($settings as $setting) {
				$global_setting[$setting->Setting] = $setting->Value;
			}

			$user_id = Auth::id();

			if ($user_id == $global_setting['Approval1FP']) {
				$premission_status = 21;
			} elseif ($user_id == $global_setting['Approval2FP']) {
				$premission_status = 22;
			} elseif ($user_id = $global_setting['Approval3FP']) {
				$premission_status = 23;
			}

			$documents = $this->db->select('NoBillingDoc')
				->where('Status', $premission_status)
				->get('vw_billdoc_v3')
				->result();

			$success = 0;
			$warning = 0;
			$error = 0;
			$approved = 0;
			foreach ($documents as $document) {
				$response = $this->approveProcess($document->NoBillingDoc);

				if($response == self::APR_SUCCESS) {
					$success++;
				} elseif($response == self::APR_ENSURE_DOCUMENT_UPLOADED) {
					$warning++;
				} elseif($response == self::APR_TOTAL_NOT_VALID) {
					$error++;
				} elseif($response == self::APR_ALREADY_APPROVE) {
					$approved++;
				}
			}

			$messages = [];
			if($success) {
				$messages[] = $success . " Dokumen Berhasil di Approve";
			}
			if($warning) {
				$messages[] = "Warning : " . $warning . " Dokumen memerlukan upload dokumen";
			}
			if($error) {
				$messages[] = "Error: " . $error . " Dokumen tidak valid.";
			}
			if($approved) {
				$messages[] = "Warning : " . $approved . " Dokumen sudah di approve";
			}

//			notif('info', str_replace("\n", "<br/>", $messages));
			notif('results', $messages);
		}
	}

	private function alreadyApprove($billing_id)
	{
		return $this
				->db
				->where('UserID', Auth::user()->ID)
				->where('NoBillDoc', $billing_id)
				->get('HistoryBillDoc')
				->num_rows() > 0;
	}

	private function approveProcess($billing_id)
	{
//		$this->db->trans_start();
		$sales_order = $this->db->where('NoBillingDoc', $billing_id)->get('SalesOrder')->row();

//		if ($this->alreadyApprove($billing_id)) {
//			return self::APR_ALREADY_APPROVE;
////			return "Error: Anda sudah melakukan Approval pada No Billing Document {$billing_id}\n";
//		}

		$cost = $this->getNilaiTotal($billing_id);
		$exchange = $this->getExchange($billing_id);

		if ($cost < 1) {
			return self::APR_TOTAL_NOT_VALID;
//			return "Error: No Billing Document {$billing_id}: Nilai Total tidak Valid. Silahkan cek Sales Order\n";
		}

		$this->db->reset_query();

//		// Fungsi ini di jalankan jika dokumen memerlukan file yang di upload terlebih dahulu
//		if ($this->isFpDocument($sales_order) && $this->ensureFpUploaded($billing_id)) {
//			return self::APR_ENSURE_DOCUMENT_UPLOADED;
//		}

		if ($this->isFinalApproval($cost, $exchange)) {
			/* update approval selesai */
			$action_id = 24;
		} else {
			/* update approval masih berlanjut */
			$action_id = $this->getBillingStatus($billing_id);
			$action_id += 1;
		}

		$this->db->where('NoBillingDoc', $billing_id)
			     ->update('BillingDocument', ['Status' => $action_id]);

		# buat history billdoc
		$this->db->insert('HistoryBillDoc', [
			'NoBillDoc' => $billing_id,
//			'Action' => $action_id,
			'Action' => 21, // get from VL_ACTION
			'UserID' => Auth::id(),
			'Timestamp' => now(),
			'Keterangan' => $action_id == 24 ? 'Final Approve' : 'Approve'
		]);

//		$this->db->trans_complete();
		return self::APR_SUCCESS;
//		return sprintf("No Billing Document %s Berhasil di approve\n", $billing_id);
	}

	public function getBillingStatus($billing_id)
	{
		$this->db->reset_query();
		return $this->db->where('NoBillingDoc', $billing_id)->get('BillingDocument')->row()->Status;
	}

	/**
	 * @param $cost
	 * @param string $exchange
	 * @return bool
	 */
	private function isFinalApproval($cost, $exchange = "IDR")
	{
		$user_id = Auth::id();
		# buat kondisi true jika nilai total sudah sesuai dengan nilai

		// caching query for global setting
		if ($this->global_setting == null) {
			$this->db->reset_query();
			$settings = $this->db->get('globalsettings')->result();

			$global_setting = array();
			foreach ($settings as $setting) {
				$global_setting[$setting->Setting] = $setting->Value;
			}
			$this->global_setting = $global_setting;
		} else {
			$global_setting = $this->global_setting;
		}


		if ($user_id == $global_setting['Approval1FP'] && $cost <= $global_setting['LimitFP1']) {
			if ($exchange != 'IDR') return false;
			return true;
		}

		if ($user_id == $global_setting['Approval2FP'] && $cost > $global_setting['LimitFP1'] && $cost <= $global_setting['LimitFP2']) {
			if ($exchange != 'IDR') return false;
			return true;
		}

		if ($user_id == $global_setting['Approval3FP']) {
			return true;
		}

		return false;
	}

	# TODO : Editor di Disable dalam Approval, mungkin bisa di hapus kode ini
	public function edit($id)
	{
		$row = $this->db->where('ID', $id)->get('BillingDocument', 1)->row();

		if (!isset($row)) {
			die("Whoops!");
		}

		$billDoc = $this->db->where('ID', $id)->get('BillingDocument', 1)->row();
		$histories = $this->db->where('NoBillDoc', $billDoc->NoBillingDoc)->get('HistoryBillDoc')->result();

		$this->blade->view('keuangan.collecting.approval.edit', array(
			'data' => $row,
			'customers' => $this->_getCustomer(),
			'materials' => $this->_getMaterials(),
			'listJenisTransaksi' => $this->_getJenisTransaksi(),
			'listStatus' => $this->_getStatus(),
			'listSalesOrder' => $this->_getSalesOrder(),
			'id' => $id,
			'histories' => $histories
		));
	}

	public function update($id)
	{
		try {
			$this->db->trans_start();
			// $this->load->library('form_validation');
			// $this->form_validation->set_rules('NoBillingDoc','','required|numeric');

			$billDoc = $this->db->where('ID', $id)->get('BillingDocument', 1)->row();

			$this->db->insert('HistoryBillDoc', array(
				'NoBillDoc' => $billDoc->NoBillingDoc,
				'Action' => 1,
				'UserID' => 1, // Auth::user()->ID,
				'Timestamp' => date('d-m-Y H:i:s'),
				'Keterangan' => 'Update Bill Doc'
			));
			$this->db->reset_query();

			$rules = array(
				'NoBillingDoc' => 'required',
				'JenisTransaksi' => 'required',
				'Sektor' => 'required',
				'BillingDate' => 'required',
				'NoSO' => 'required',
				'NoGI' => 'required',
				'NoFakturPajak' => 'required',
				'IDCustomer' => 'required',
				// 'Customer' => 'required',
				'Status' => 'required',
				'IDMaterial' => 'required',
				'Material' => 'required',
				'Kuantum' => 'required',
				'UoM' => 'required',
				'Kurs' => 'required',
				'HargaSatuan' => 'required',
				'Jumlah' => 'required',
				'DPP' => 'required',
				'TaxAmount' => 'required',
				'Total' => 'required',
				'Keterangan' => 'trim',
				'IDPayer' => 'required',
				// 'Payer' => 'required',
				'IDSalesOrg' => 'required',
				// 'SalesOrg' => 'required',
			);

			foreach ($rules as $field => $rule) {
				$data[$field] = $this->input->post($field);
			}

			$data['Customer'] = $this->_getCustomer($data['IDCustomer']);
			$data['SalesOrg'] = $this->_getSalesOrder($data['IDSalesOrg']);
			$data['Payer'] = $this->_getCustomer($data['IDPayer']);
			$data['LastDownload'] = date('Y-m-d H:i:s');

			$this->db->where('ID', $id)
				->update('BillingDocument', $data);

			$this->db->trans_complete();

			header('Content-Type: application/json');
			echo json_encode(array('status' => 'success'));
		} catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	public function list_material()
	{
		$no_billing = $_GET['id'];

		$materials = $this
			->db
			->where('NoBillingDoc', $no_billing)
			->get('billingdocument')
			->result();

		$this->blade->view('keuangan.collecting.approval.materials', compact('materials', 'no_billing'));
	}

	public function getNilaiTotal($billing_id)
	{
		return $this->db
			->select("SUM(NetValueBeforePPH2) as NilaiTotal")
			->where('NoBillingDoc', $billing_id)
			->get('SalesOrder')
			->row()
			->NilaiTotal;
	}

	private function getExchange($billing_id)
	{
		return $this->db->select('Kurs')
			->where('NoBillingDoc', $billing_id)
			->get('BillingDocument')
			->row()->Kurs;
	}

	public function upload($billing_id)
	{
		$this->blade->view('keuangan.collecting.approval.upload', compact('billing_id'));
	}

	public function upload_file($billing_id)
	{
		$config['upload_path'] = './storage/billing_loan';
		$config['allowed_types'] = 'pdf';
		$config['encrypt_name'] = true;

		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('document')) {
			show_error($this->upload->display_errors());
		} else {
			$data = $this->upload->data();

			$this->db->where('IDParent', $billing_id)->where('Kategori',2)->delete('Attachment');

			$this->db->insert('Attachment', [
				'IDParent' => $billing_id,
				'Kategori' => 2,
				'FilePath' => $data['file_name'],
				'Judul' => 'Upload Billing Load ' . $billing_id,
				'UploadedBy' => Auth::user()->ID,
				'UploadedOn' => now()
			]);
		}

		redirect('keuangan/collecting/lists');
	}

	public function isFpDocument($sales)
	{
		return ($sales->PaymentMethod == 'M');
	}

	public function ensureFpUploaded($billing_id)
	{
		return !$this->db
				->where('IDParent', $billing_id)
				->where('Kategori', 2) // 2 untuk modul collecting
				->get('Attachment')
				->num_rows() > 0;
	}

	public function document($billing_id)
	{
		$attachment = $this->db->where('IDParent', $billing_id)->get('Attachment')->row();

		if (!$attachment) show_error('Dokumen Belum di Upload');

		redirect(url('storage/billing_loan/' . $attachment->FilePath));
	}
}
