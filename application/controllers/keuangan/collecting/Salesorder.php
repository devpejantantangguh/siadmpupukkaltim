<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include __DIR__.'/Collecting.php';

class Salesorder extends Collecting_Controller {

    public function __construct()
    {
        parent::__construct();
        Auth::guard();
    }

    public function index()
    {
        $this->blade->view('keuangan.collecting.sales_order.index', array(
            'table' => 'SalesOrder'
        ));
    }

    public function get($status = null)
    {

       $search = isset($_GET['filter']) ? json_decode($_GET['filter']) : '';
        $limit = isset($_GET['take']) ? $_GET['take'] : 10;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;
        
        if(!empty($search)) {
            $this->db->like($search[0], $search[2]);
        }

        if(isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        } else {
            $this->db->order_by('ID', 'DESC');
        }

        $tempdb = clone $this->db; //clone for count all rows
        $count = $tempdb->from('SalesOrder')->count_all_results();
        $items = $this->db->get('SalesOrder', $limit, $offset)->result();

        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $items
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }
}
