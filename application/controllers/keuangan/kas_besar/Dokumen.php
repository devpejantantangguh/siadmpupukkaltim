<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokumen extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        Auth::guard();
    }

    public function index()
    {

        $data['table'] = 'KasBesar';
        $views   = 'keuangan.kas_besar.index';

        $this->blade->view($views,$data);
    }    
    public function input()
    {
        $data['table'] = 'KasBesar';
        $data['listJenisTransaksi'] = $this->getJenisTransaksi();
        $data['workers']    = $this->get_list_karyawan();
        $views   = 'keuangan.kas_besar.inputdoc';


        $this->blade->view($views,$data);
    }


    public function get()
    {
        $table = ucwords('KasBesar');
        $table2 = ucwords('VL_JenisTransaksi');

        $search = isset($_GET['filter']) ? json_decode($_GET['filter']) : '';
        $limit = isset($_GET['take']) ? $_GET['take'] : 10;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;

        if(!empty($search)) {
            if(count($search)==3){
                Filter::dxDataGridFilters($search, $this->db);
            }elseif(count($search)>3){
                // Sub Query
                $this->db->like('NoDokumen', $search[0][2]);
                $this->db->where('LifeCycle', 'FALSE');
                $this->db->select('IDKasBesar')->from('DokumenFinansial');
                $subQuery =  $this->db->get_compiled_select();
                $this->db->where("KasBesar.ID IN ($subQuery)", NULL, FALSE);

                //$this->db->like('NoDokumen', $search[0][2]);
                //$this->db->join('DokumenFinansial','KasBesar.ID = DokumenFinansial.IDKasBesar','LEFT');

            }
            
        }
        if(isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        }else{
            $this->db->order_by('KasBesar.ID','desc');
        }
        //$this->db->select('*');
        $this->db->select('KasBesar.*,VL_JenisTransaksi.JenisTransaksi,Ms_Karyawan.Nama');
        $this->db->where('VL_JenisTransaksi.Kategori','3');
        $this->db->where('VL_JenisTransaksi.Keterangan','Dokumen');
        
        $this->db->join('VL_JenisTransaksi','KasBesar.JenisTransaksi = VL_JenisTransaksi.ID');
        $this->db->join('Ms_Karyawan','KasBesar.NPKRequester = Ms_Karyawan.ID');
        //$this->db->group_by('KasBesar.ID');
        $tempdb = clone $this->db; //clone for count all rows
        $count= $tempdb->from('KasBesar')->count_all_results();
        $query = $this->db->get('KasBesar', $limit, $offset);
        $res = array();
        $items = array();
        
        $i = $offset+1;
        foreach ($query->result_array() as $items){
            $items['No'] = $i++;
            
            array_push($res, $items);
        }
        //$items = $query->result();

        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $res
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function get_doc($id)
    {
        $table = ucwords('DokumenFinansial');

        $search = isset($_GET['filter']) ? json_decode($_GET['filter']) : '';
        $limit = isset($_GET['take']) ? $_GET['take'] : 10;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;

        if(!empty($search)) {
                Filter::dxDataGridFilters($search, $this->db);
        }
        if(isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        }else{
            $this->db->order_by('DokumenFinansial.ID','desc');
        }
        $this->db->where('DokumenFinansial.IDKasBesar',$id);
        
        $tempdb = clone $this->db; //clone for count all rows
        $count= $tempdb->from($table)->count_all_results();
        $query = $this->db->get($table, $limit, $offset);
        $res = array();
        $items = array();
        
        $i = $offset+1;
        foreach ($query->result_array() as $items){
            $items['No'] = $i++;
            
            array_push($res, $items);
        }
        //$items = $query->result();

        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $res
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function getdokumen($ID)
    {
        $table = ucwords('DokumenFinansial');

        $limit = isset($_GET['take']) ? $_GET['take'] : 10;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;
        if(isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        }
        $this->db->where('IDKasBesar',$ID);
        $query = $this->db->get('DokumenFinansial');
        $res = array();
        $items = array();
        $i = 1;
        foreach ($query->result_array() as $items){
            $items['No'] = $i++;
            $items['CreatedBy'] = $this->get_karyawan_by_id($items['CreatedBy']);
            $items['ReturnedBy'] = $this->get_karyawan_by_id($items['ReturnedBy']);
            
            array_push($res, $items);
        }
        //$items = $query->result();
        $count = $this->db->count_all($table);

        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $res
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function approve()
    {
        $now = date("Y-m-d h:i:s");
        $postdata = $this->input->post();
        $ID = (int) $postdata['ID'];
        $status = "1"; // approved
        $data = array(
            'Status'=>$status,
            'ApprovedBy'=>Auth::user()->ID,
            'ApprovedOn'=>$now
        );
        $this->db->set($data);
        $this->db->where('ID', $ID);
        $row = $this->db->update('KasBesar'); // gives UPDATE `mytable` SET `field` = 'field+1' WHERE `id` = 2
        if(!$row) {
            echo json_encode(array('status' => '404', 'data' => array()));
        }else{
            echo json_encode(array('status' => 200, 'data' => $row));    
        }
        
    } 
    public function cancel_dokumen($ID)
    {
        $ID = (int) $ID;
        $status = "2"; // canceled
        $data = array(
            'Status'=>$status,
            'LifeCycle'=>1
        );
        $this->db->set($data);
        $this->db->where('ID', $ID);
        $row = $this->db->update('KasBesar'); 
        if(!$row) {
            echo json_encode(array('status' => '404', 'data' => array()));
            notif('error', 'Gagal mengubah data.');
        }else{

            $data = array(
                'LifeCycle'=>TRUE
            );
            $this->db->set($data);
            $this->db->where('IDKasBesar', $ID);
            $this->db->update('DokumenFinansial');

            echo json_encode(array('status' => 200, 'data' => $row));  
            notif('success', 'Berhasil mengubah data.'); 

        }
        notif('tab', 'dokumen'); 
        redirect('keuangan/kas_besar/kasbesar/?tab=dokumen');
    }    
    public function pengembalian()
    {

        $now = date("Y-m-d h:i:s");
        $postdata = $this->input->post();
        $ID = (int) $postdata['ID'];
            $data = array(
                'LifeCycle'=>TRUE,
                'ReturnedBy'=>Auth::user()->ID,
                'ReturnedOn'=>$now
            );
            $this->db->set($data);
            $this->db->where('ID', $ID);
            $this->db->where('LifeCycle', FALSE);
            $row = $this->db->update('DokumenFinansial');

        if(!$row) {
            echo json_encode(array('status' => '404', 'data' => array()));
        }else{
            echo json_encode(array('status' => 200, 'data' => $row));    
        }
        
    }

    public function select($table,$field) {
        $table = ucwords($table);
        $field = ucwords($field);

        $result = $this->db->get($table)->result_array();

        $items = array();
        $row = array();
        foreach ($result as $items){
            array_push($row, $items);
        }
        $data = json_decode(json_encode($row));

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function submit()
    {

        $this->db->trans_start();
        $now = date("Y-m-d h:i:s");
        $JenisTransaksi = $this->input->post('JenisTransaksi');

        list($d,$m,$y) = explode('-', $this->input->post('DueDate'));
        $duedate = date('Y-m-d', mktime(0,0,0,$m,$d,$y));
        $data = array(
        'JenisTransaksi'=>$JenisTransaksi,
        'NoPeminjaman'=>$this->input->post('NoPeminjaman'),
        'NPKRequester'=>$this->input->post('IDVendor'),
        'CostCenter'=> $this->input->post('CostCenter'),
        'NPKAtasan'=>$this->input->post('NPKAtasan'),
        'Deskripsi'=>$this->input->post('Deskripsi'),
        'DueDate'=>$duedate,
        'Status'=>'0',//Draft
        'CreatedBy'=>Auth::user()->ID,
        'CreatedOn'=>$now,
        'UpdatedBy'=>'',
        'UpdatedOn'=>'',
        'ApprovedOn'=>'',
        'ApprovedBy'=>'',
        'Keterangan'=>$this->input->post('Keterangan'),
        'LifeCycle'=>'1' //OPEN
        );
        $row = $this->db->insert('KasBesar', $data);
        $insert_id = $this->db->insert_id();

        $this->db->trans_complete();
        redirect('keuangan/kas_besar/dokumen/step2/'.$insert_id);

    }
    public function save($ID)
    {
        $this->db->where('IDKasBesar',$ID);
        $check = $this->db->get('DokumenFinansial')->result();
        foreach ($check as $value) {
            $this->db->where('NoDokumen',$value->NoDokumen);
            $this->db->where('TahunDokumen',$value->TahunDokumen);
            $this->db->where('LifeCycle',FALSE);
            $this->db->where_not_in('IDKasBesar',array($ID));
            $exist = $this->db->get('DokumenFinansial')->num_rows();
            if($exist!=0){
                notif('success','GAGAL. Dokumen '.$value->NoDokumen.' ('.$value->TahunDokumen.')  belum dikembalikan.');
                notif('tab','dokumen');
                redirect('keuangan/kas_besar/kasbesar/');
                die();
            }
        }
        $this->db->where('IDKasBesar', $ID);
        $row = $this->db->update('DokumenFinansial', array('LifeCycle' => FALSE ));

        $data = array(
        'Status'=>3
        );
        
        $this->db->where('ID', $ID);
        $row = $this->db->update('KasBesar', $data);
        notif('success','Dokumen berhasil diupdate');
        notif('tab','dokumen');
        redirect('keuangan/kas_besar/kasbesar/');

    }
    public function submitstep2($ID)
    {

        $this->db->where('NoDokumen',$this->input->post('NoDokumen'));
        $this->db->where('TahunDokumen',$this->input->post('Tahun'));
        $this->db->group_start();
        $this->db->where('LifeCycle',FALSE)
                    ->or_group_start()
                        ->where('IDKasBesar',$ID)
                    ->group_end();
        $this->db->group_end();
        $exist = $this->db->get('DokumenFinansial')->num_rows();
        if($exist!=0){
            echo json_encode(array('status' => 404, 'data' => 'GAGAL. Dokumen belum dikembalikan.'));
            die();
        }
        $data = array(
        'IDKasBesar'=>$ID,
        'NoDokumen'=>$this->input->post('NoDokumen'),
        'TahunDokumen'=>$this->input->post('Tahun'),
        'Deskripsi'=>$this->input->post('Deskripsi'),
        'CreatedBy'    => Auth::user()->ID, 
        'CreatedOn'    => date('Y-m-d H:i:s'),
        'LifeCycle'=>FALSE,
        'Keterangan'=> $this->input->post('Keterangan')
        );
        $row = $this->db->insert('DokumenFinansial', $data);

        if($row) {
            echo json_encode(array('status' => 200, 'data' => 'Berhasil menambahkan dokumen.'));
        }else{
            echo json_encode(array('status' => 404, 'data' => 'Gagal menambahkan dokumen.'));    
        }

    }
    public function remove()
    {

        $postdata = $this->input->post();
        $ID = (int) $postdata['ID'];
        $this->db->where('ID', $ID);
        $row = $this->db->delete('DokumenFinansial'); // gives UPDATE `mytable` SET `field` = 'field+1' WHERE `id` = 2
        if(!$row) {
            echo json_encode(array('status' => '404', 'data' => array()));
        }else{
            echo json_encode(array('status' => 200, 'data' => $row));    
        }
        
    }
    public function cancel()
    {

        $postdata = $this->input->post();
        $ID = (int) $postdata['ID'];
        $this->db->where('ID', $ID);
        $row = $this->db->delete('KasBesar'); // gives UPDATE `mytable` SET `field` = 'field+1' WHERE `id` = 2
        $this->db->where('IDKasBesar', $ID);
        $row = $this->db->delete('DokumenFinansial'); // gives UPDATE `mytable` SET `field` = 'field+1' WHERE `id` = 2
        if(!$row) {
            echo json_encode(array('status' => '404', 'data' => array()));
        }else{
            echo json_encode(array('status' => 200, 'data' => $row));    
        }
        
    }
    public function view($ID)
    {
        $data['table'] = 'KasBesar';
        $data['IDKasBesar'] = $ID;
        $data['attachments'] = $this->db->where('IDParent', $ID)->get('Attachment')->result();

        $data['listJenisTransaksi'] = $this->getJenisTransaksi();
        $data['workers']    = $this->get_list_karyawan();
        $this->db->select('*, VL_JenisTransaksi.JenisTransaksi JK');
        $this->db->join('VL_JenisTransaksi','VL_JenisTransaksi.ID = KasBesar.JenisTransaksi');
        $data['data'] = $this->db->where('KasBesar.ID', $ID)->get('KasBesar',1)->row();
        $views   = 'keuangan.kas_besar.inputdocstep2';
        $this->blade->view($views,$data);

    }

    public function step2($ID)
    {
        $data['table'] = 'KasBesar';
        $data['IDKasBesar'] = $ID;
        
        $data['attachments'] = $this->db->where('IDParent', $ID)->get('Attachment')->result();
        $this->db->select('KasBesar.*, VL_JenisTransaksi.JenisTransaksi JK, Ms_Karyawan.Nama,Ms_UnitKerja.UnitKerja');
        $this->db->join('Ms_UnitKerja','Ms_UnitKerja.CostCenter = KasBesar.CostCenter');
        $this->db->join('Ms_Karyawan','Ms_Karyawan.ID = KasBesar.NPKRequester');
        $this->db->join('VL_JenisTransaksi','VL_JenisTransaksi.ID = KasBesar.JenisTransaksi');
        $query = $this->db->where('KasBesar.ID', $ID)->get('KasBesar',1)->row();
        $data['data'] = $query;

        $this->db->select('Ms_Karyawan.Nama');
        $this->db->where('Ms_Karyawan.ID',$query->NPKAtasan);
        $NamaAtasan = $this->db->get('Ms_Karyawan')->row();
        $query->NamaAtasan = $NamaAtasan->Nama;

        $views   = 'keuangan.kas_besar.inputdocstep2';
        $this->blade->view($views,$data);

    }
    private function getJenisTransaksi()
    {
        $result = [];

        $this->db->where('Kategori','3');
        $this->db->where('Keterangan','Dokumen');
        $query = $this->db->get('VL_JenisTransaksi');

        if($query->num_rows() > 0) {
            foreach($query->result_array() as $row) {
                $result[$row['ID']] = $row['JenisTransaksi'];
            }
        }

        return $result;
    }
    private function _getJenisTransaksi()
    {
        $this->db->where('Kategori','3');
        $this->db->where('Keterangan','Dokumen');
        $rows = $this->db->get('VL_JenisTransaksi')->result();

        $listJenisTransaksi = [];
        foreach($rows as $row) {
            $listJenisTransaksi[] = array(
                'id'    => $row->ID,
                'name'  => $row->JenisTransaksi
            );
        }

        return $listJenisTransaksi;
    }
    private function _getUnitKerja()
    {
        $rows = $this->db->get('Ms_UnitKerja')->result();

        $UnitKerja = [];
        foreach($rows as $row) {
            $UnitKerja[] = array(
                'id'    => $row->CostCenter,
                'name'  => $row->UnitKerja
            );
        }

        return $UnitKerja;
    }
    private function get_list_karyawan()
    {
        $query = $this->db->get('Ms_Karyawan');

        if($query->num_rows() > 0) {
            $lists = [];
            foreach($query->result() as $row) {
                $lists[$row->ID] = "$row->ID - $row->Nama";
            }
            return $lists;
        }
        return [];
    }
    public function ajax()
    {
        header('Content-Type: application/json');
        $id = $this->input->post('id');
        $this->db->join('Ms_UnitKerja','Ms_UnitKerja.CostCenter = Ms_Karyawan.CostCenter');
        $query = $this->db->where('ID', $id)->get('Ms_Karyawan')->row_array();

        $this->db->select('Ms_Karyawan.Nama');
        $this->db->where('Ms_UnitKerja.KepalaUnitKerja',$query['KepalaUnitKerja']);
        $this->db->join('Ms_UnitKerja','Ms_UnitKerja.KepalaUnitKerja = Ms_Karyawan.ID');
        $NamaKepalaUnitKerja = $this->db->get('Ms_Karyawan')->row_array();
        $query['NamaKepalaUnitKerja'] = $NamaKepalaUnitKerja['Nama'];
        echo json_encode($query);
    }

    public function upload($id)
    {

        $this->load->model('invoice/m_input');
        $result = $this->m_input->uploadAttachment($id,'./storage/dokumen/');

        if($result == false) {
            redirect('keuangan/kas_besar/dokumen/step2/'. $id.'?status=error_upload');
        } else {
            $data = [
                'IDParent'      => $id,
                'Kategori'      => 9,
                'Judul'         => substr($result['raw_name'],0,50),
                'FilePath'      => $result['file_name'],
                'UploadedBy'    => Auth::user()->ID, 
                'UploadedOn'    => date('Y-m-d H:i:s')
            ];
            $this->db->insert('Attachment', $data);
        }

        redirect('keuangan/kas_besar/dokumen/step2/'. $id);
    }    
    public function remove_attachment($attachment_id)
    {
        $this->db->where('ID', $attachment_id)->delete('Attachment');
        redirect($_SERVER['HTTP_REFERER']);
    }
    private function get_karyawan_by_id($id){
        $this->db->where('Ms_User.ID', $id);
        $this->db->join('Ms_User','Ms_User.IDKaryawan = Ms_Karyawan.ID');
        $rows = $this->db->select('Ms_Karyawan.Nama')->get('Ms_Karyawan')->row();
        if($rows){
            
            return $rows->Nama;
        }
        return '';
        
    }
}
