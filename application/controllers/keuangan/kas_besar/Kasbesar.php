<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kasbesar extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        Auth::guard();
    }

    public function index()
    {

        $data['table'] = 'KasBesar';
        $this->db->where('Setting','SaldoKas');
        $query = $this->db->get('GlobalSettings')->result_array();
        $data['saldo'] = $query[0]['Value'];
        $data['UnitKerja'] = $this->_getUnitKerja();
        $data['listJenisTransaksi'] = $this->getJenisTransaksi();
        $data['listJenisTransaksiDoc'] = $this->_getJenisTransaksiDoc();
        $views   = 'keuangan.kas_besar.index';
        if($this->input->get('tab')){
            notif('tab','dokumen');
        }
        $this->blade->view($views,$data);
    }

    public function input()
    {
        $data['table'] = 'KasBesar';
        $this->db->where('Setting','SaldoKas');
        $query = $this->db->get('GlobalSettings')->result_array();
        $data['saldo'] = $query[0]['Value'];
        $data['listJenisTransaksi'] = $this->_getJenisTransaksi();
        $data['workers']    = $this->get_list_karyawan();
        $array_bulan = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
        $bulan = $array_bulan[date('n')];
        $this->db->select('NoPeminjaman');
        $this->db->where('JenisTransaksi',7);
        $this->db->order_by('ID','DESC');
        $result= $this->db->get('KasBesar',1)->row(); 
        if($result){

            $result = explode('/', $result->NoPeminjaman);
            $result = (int) $result[0];   
            $result ++;
        }else{
            $result = 1;
        }
        
        $data['NoPeminjaman'] = $result.'/KEU/'.$bulan.'/'.date('Y');
        $views   = 'keuangan.kas_besar.input';


        $this->blade->view($views,$data);
    }
    public function view($ID)
    {
        $data['table'] = 'KasBesar';

        $data['IDKasBesar'] = $ID;

        $this->db->select('*, VL_JenisTransaksi.JenisTransaksi JK');
        $this->db->join('VL_JenisTransaksi','VL_JenisTransaksi.ID = KasBesar.JenisTransaksi');
        $data['data'] = $this->db->where('KasBesar.ID', $ID)->get('KasBesar',1)->row();        
        $views   = 'keuangan.kas_besar.view';


        $this->blade->view($views,$data);
    }

    public function get()
    {
        $table = ucwords('KasBesar');
        $table2 = ucwords('VL_JenisTransaksi');

        $search = isset($_GET['filter']) ? json_decode($_GET['filter']) : '';
        $limit = isset($_GET['take']) ? $_GET['take'] : 10;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;
        if(!empty($search)) {
            Filter::dxDataGridFilters($search, $this->db);
        }
        if(isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        }else{
            $this->db->order_by('KasBesar.ID','desc');
        }

        $this->db->select('KasBesar.*,VL_JenisTransaksi.JenisTransaksi,Ms_Karyawan.Nama');
        $this->db->where('VL_JenisTransaksi.Kategori','3');
        $this->db->where('VL_JenisTransaksi.Keterangan','Kasbesar');
        $this->db->join('KasBesar','KasBesar.JenisTransaksi = VL_JenisTransaksi.ID');
        $this->db->join('Ms_Karyawan','KasBesar.NPKRequester = Ms_Karyawan.ID','LEFT');

        $tempdb = clone $this->db; //clone for count all rows
        $count= $tempdb->from('VL_JenisTransaksi')->count_all_results();
        $query = $this->db->get('VL_JenisTransaksi', $limit, $offset);
        $res = array();
        $items = array();
        $i = $offset+1;
        foreach ($query->result_array() as $items){
/*            if($items['Status']=='1'){
                $Status = 'Open';
            }else{
                $Status = 'Closed';
            }
            $items['Status'] = $Status;
            */

            $items['No'] = $i++;
            
            array_push($res, $items);
        }
        //$items = $query->result();

        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $res
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function approve()
    {
        $now = date("Y-m-d h:i:s");
        $postdata = $this->input->post();
        $ID = (int) $postdata['ID'];
        $status = "1"; // approved
        $data = array(
            'Status'=>$status,
            'ApprovedBy'=>Auth::user()->ID,
            'ApprovedOn'=>$now
        );
        $this->db->set($data);
        $this->db->where('ID', $ID);
        $row = $this->db->update('KasBesar'); // gives UPDATE `mytable` SET `field` = 'field+1' WHERE `id` = 2
        if(!$row) {
            echo json_encode(array('status' => '404', 'data' => array()));
        }else{
            echo json_encode(array('status' => 200, 'data' => $row));    
        }
        
    }    
    public function pengembalian()
    {
        $now = date("Y-m-d h:i:s");
        $postdata = $this->input->post();
        $ID = (int) $postdata['ID'];
        $nominal = (int) $postdata['nominal'];
        $dxLifeCycle = (int) $postdata['LifeCycle'];
        list($d,$m,$y) = explode('/', $this->input->post('tanggal_kembali'));
        $tanggal_kembali = date('Y-m-d', mktime(0,0,0,$m,$d,$y));

        if($dxLifeCycle==1){
            $lifecycle = FALSE; // close
            $data = array(
                'LifeCycle'=>$lifecycle,
                'TanggalPengembalian'=>$tanggal_kembali,
                'UpdatedBy'=>Auth::user()->ID,
                'UpdatedOn'=>$now
            );
            $this->db->set($data);
            $this->db->where('ID', $ID);
            $this->db->where('LifeCycle', TRUE);
            $row = $this->db->update('KasBesar');
            $this->db->where('Setting','SaldoKas');
            $query = $this->db->get('GlobalSettings')->row();
            $saldo = (int) $query->Value + $nominal;
            $this->db->set('Value', $saldo, FALSE);
            $this->db->where('Setting', 'SaldoKas');
            $this->db->update('GlobalSettings');
        }
        redirect('keuangan/kas_besar/kasbesar/?status=success');
        
    }

    public function select($table,$field) {
        $table = ucwords($table);
        $field = ucwords($field);

        $result = $this->db->get($table)->result_array();

        $items = array();
        $row = array();
        foreach ($result as $items){
            array_push($row, $items);
        }
        $data = json_decode(json_encode($row));

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function submit()
    {
        if(empty($this->input->post('JenisTransaksi'))){

            notif('error', 'Data gagal diubah.');
            redirect('keuangan/kas_besar/kasbesar/');
            die();
        }
        $this->db->trans_start();
        $now = date("Y-m-d h:i:s");
        $JenisTransaksi = $this->input->post('JenisTransaksi');
        $nominal = (int) str_replace(['.',','],'', $this->input->post('Nominal'));
        if(!empty($this->input->post('DueDate')) OR $this->input->post('DueDate') != '') {
            list($d,$m,$y) = explode('-', $this->input->post('DueDate'));
            $duedate = date('Y-m-d', mktime(0,0,0,$m,$d,$y));
        }else{
            $duedate = '';
        }
        
        //insert -> KasBesar
        if($JenisTransaksi=='7'){// IF Peminjaman
            $data = array(
            'JenisTransaksi'=>$JenisTransaksi,
            'NoPeminjaman'=>$this->input->post('NoPeminjaman'),
            'NPKRequester'=>$this->input->post('IDVendor'),
            'CostCenter'=> $this->input->post('CostCenter'),
            'NPKAtasan'=>$this->input->post('NPKAtasan'),
            'Nominal'=>$nominal,
            'Deskripsi'=>$this->input->post('Deskripsi'),
            'DueDate'=>$duedate,
            'Status'=>'0',//Draft
            'CreatedBy'=>Auth::user()->ID,
            'CreatedOn'=>$now,
            'UpdatedBy'=>'',
            'UpdatedOn'=>'',
            'ApprovedOn'=>'',
            'ApprovedBy'=>'',
            'Keterangan'=>$this->input->post('keterangan'),
            'LifeCycle'=>'1' //OPEN
            );
        }else{
            $data = array(
            'JenisTransaksi'=>$JenisTransaksi,
            'Nominal'=>$nominal,
            'Status'=>'0',//Draft
            'CreatedBy'=>Auth::user()->ID,
            'CreatedOn'=>$now,
            'Keterangan'=>$this->input->post('Keterangan'));
        }

        $insert = $this->db->insert('KasBesar', $data);
        if($insert){
            if($JenisTransaksi=='7'){// IF Peminjaman
                $nominal = $nominal * (-1);
            }
            $this->db->where('Setting','SaldoKas');
            $query = $this->db->get('GlobalSettings')->row();
            $saldo = (int) $query->Value + $nominal;
            $this->db->set('Value', $saldo, FALSE);
            $this->db->where('Setting', 'SaldoKas');
            $this->db->update('GlobalSettings');
            $this->db->trans_complete();
            notif('success', 'Data berhasil diubah.');
            redirect('keuangan/kas_besar/kasbesar/');
        }else{
            notif('error', 'Data gagal diubah.');
            redirect('keuangan/kas_besar/kasbesar/');
        }
            
        
    }
    public function update($id = 0)
    {
        $id = (int) $id;
        if($id > 0){
            $JenisTransaksi = $this->input->post('JenisTransaksi');
            if($JenisTransaksi == 7){
                
                $now = date("Y-m-d h:i:s");
                //insert -> KasBesar
                $postdata = $this->input->post();
                $data = array(
                'CreatedBy'=>'1',
                'CreatedOn'=>$now,
                'UpdatedBy'=>'1',
                'UpdatedOn'=>$now,
                'ApprovedBy'=>'1',
                'ApprovedOn'=>$now,
                'ApprovedBy'=>'1',
                'LifeCycle'=>'0'
                );
                $result = array_merge($data, $postdata);
                $this->db->set($result);
                $this->db->where('ID', $id);
                $this->db->update('KasBesar');
            }
        }

            echo $this->db->last_query();
    }
        public function remove($id = 0)
    {
        $id = (int) $id;
        if($id > 0){
            $this->db->where('ID', $id);
            $this->db->delete('KasBesar');
        }

            echo $this->db->last_query();
    }

    public function cetak($id)
    {

        $this->db->select('KasBesar.*,Ms_Karyawan.Nama,Ms_UnitKerja.UnitKerja');
        $this->db->join('Ms_Karyawan','KasBesar.NPKRequester = Ms_Karyawan.ID');
        $this->db->join('Ms_UnitKerja','KasBesar.CostCenter = Ms_UnitKerja.CostCenter','LEFT');
        $this->db->where('KasBesar.ID', $id);  
        $query = $this->db
                ->get('KasBesar');

        if($query->num_rows() > 0) {
            $data = $query->row();
            $views = $this->blade->render('keuangan/kas_besar/cetak',[
                'data'              => $data
            ]);            echo $views;
        }else{
            echo "Data tidak lengkap";
        }
    }
    private function _getJenisTransaksi()
    {
        $result = [];

        $this->db->where('Kategori','3');
        $this->db->where('Keterangan','KasBesar');
        $query = $this->db->get('VL_JenisTransaksi');

        if($query->num_rows() > 0) {
            foreach($query->result_array() as $row) {
                $result[$row['ID']] = $row['JenisTransaksi'];
            }
        }

        return $result;
    }
    private function getJenisTransaksi()
    {
        $this->db->where('Kategori','3');
        $this->db->where('Keterangan','KasBesar');
        $rows = $this->db->get('VL_JenisTransaksi')->result();

        $listJenisTransaksi = [];
        foreach($rows as $row) {
            $listJenisTransaksi[] = array(
                'id'    => $row->ID,
                'name'  => $row->JenisTransaksi
            );
        }

        return $listJenisTransaksi;
    }
    private function _getJenisTransaksiDoc()
    {
        $this->db->where('Kategori','3');
        $this->db->where('Keterangan','Dokumen');
        $rows = $this->db->get('VL_JenisTransaksi')->result();

        $listJenisTransaksi = [];
        foreach($rows as $row) {
            $listJenisTransaksi[] = array(
                'id'    => $row->ID,
                'name'  => $row->JenisTransaksi
            );
        }

        return $listJenisTransaksi;
    }
    private function _getUnitKerja()
    {
        $rows = $this->db->get('Ms_UnitKerja')->result();

        $UnitKerja = [];
        foreach($rows as $row) {
            $UnitKerja[] = array(
                'id'    => $row->CostCenter,
                'name'  => $row->UnitKerja
            );
        }

        return $UnitKerja;
    }
    private function get_list_karyawan()
    {
        $query = $this->db->get('Ms_Karyawan');

        if($query->num_rows() > 0) {
            $lists = [];
            foreach($query->result() as $row) {
                $lists[$row->ID] = "$row->ID - $row->Nama";
            }
            return $lists;
        }
        return [];
    }
    public function ajax()
    {
        header('Content-Type: application/json');
        $id = $this->input->post('id');
        $this->db->join('Ms_UnitKerja','Ms_UnitKerja.CostCenter = Ms_Karyawan.CostCenter');
        $query = $this->db->where('ID', $id)->get('Ms_Karyawan')->row_array();

        $this->db->select('Ms_Karyawan.Nama');
        $this->db->where('Ms_UnitKerja.KepalaUnitKerja',$query['KepalaUnitKerja']);
        $this->db->join('Ms_UnitKerja','Ms_UnitKerja.KepalaUnitKerja = Ms_Karyawan.ID');
        $NamaKepalaUnitKerja = $this->db->get('Ms_Karyawan')->row_array();
        $query['NamaKepalaUnitKerja'] = $NamaKepalaUnitKerja['Nama'];
        echo json_encode($query);
    }
}
