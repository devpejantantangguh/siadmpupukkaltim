<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Email extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (Auth::guest()) {
            show_error('Anda belum Login!', 405);
        }
    }

    public function index()
    {
        $this->blade->view('email.index');
    }

    public function store()
    {
        try {
            $from_email = 'no-reply@mitraku.pupukkaltim.com';
            $to_email = $_POST['email'];
            $message = 'This is Test Email';

            $this->load->library('email');

            $this->email->from($from_email, 'Mitraku');
            $this->email->to($to_email);
            $this->email->subject('This is a test Email');
            $this->email->message($message);

            if (!$this->email->send()) {
                throw new Exception("Unable to Send Email", 1);
            }
        } catch (\Throwable $th) {
            if (!$this->email->send()) {
                log_message('ERROR', $th->getMessage());
                throw new Exception("Unable to Send Email", 1);
            }
        }
    }
}
