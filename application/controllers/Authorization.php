<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Authorization extends CI_Controller
{

    public function login()
    {
        Auth::logout();

        if (Auth::guest()) {
            $this->blade->view('session.login');
        } else {
            redirect('dashboard');
        }
    }

    public function login_playground()
    {

        $results = $this->db->join('Ms_Role', 'Ms_Role.ID = Ms_User.RoleID', 'left')
            ->join('Ms_Karyawan', 'Ms_Karyawan.ID = Ms_User.IDKaryawan', 'left')
            ->join('Ms_UnitKerja', 'Ms_UnitKerja.CostCenter = Ms_Karyawan.CostCenter', 'left')
            ->get('Ms_User')->result();

        // dd($results);

        $users = null;
        foreach ($results as $result) {
            $users[] = [
                'email' => strtolower($result->Email),
                'nama' => $result->Nama,
                // 'password' => $result->Password,
                'id_karyawan' => $result->IDKaryawan,
                'npk_lama' => $result->NPKLama,
                'role_name' => $result->RoleName,
                'unit_kerja' => $result->UnitKerja . ' - ' . $result->CostCenter,
                'area' => $result->Area,
                'invoice_count' => $this->db->where('CostCenter', $result->CostCenter)->get('Invoice')->num_rows(),
                'is_ldap' => $result->IsLDAP,
                'action' => url('authorization/autologin?email=' . $result->Email . '&password=' . $result->Password, 'Login')
            ];
        }

        $this->blade->view('developer.login', compact('users'));
    }

    public function autologin()
    {
        $username = $_GET['email'];
        $password = $_GET['password'];

        if (Auth::attempt($username, $password, true)) {
            return redirect(url('dashboard'));
        }
        return redirect('/d3v3l0p3r/login?error=1');
    }

    public function area_playground($id = false)
    {

        if (!$id) {
            echo $area = Auth::unitKerja()->Area;
        } elseif ($id == 1) {
            echo $area = 'Dept. PSO 1';
        } elseif ($id == 2) {
            echo $area = 'Dept. PSO 2';
        } else {
            echo $area = 'Pusat';
        }

        echo '<br/>';
        if ($area == 'Dept. PSO 1') {
            $verified = statusID('VERIFIED');
            $paid = statusID('PAID');

            $invs = $this->db->select('Invoice.ID,Invoice.NilaiInvoice,Invoice.NoInvoice,Invoice.Status')->where('us.Area', $area)
                ->join('Ms_User us', 'us.ID = Invoice.CreatedBy')->get('Invoice')->result();
            foreach ($invs as $inv) {
                if (($inv->NilaiInvoice > 25000000) and ($inv->Status == $verified or $inv->Status == $paid)) {
                    $idarea = 1;
                } else {
                    $idarea = 10;
                }
                echo $inv->ID . ' - ' . $inv->NoInvoice . ' - ' . $inv->NilaiInvoice . ' - ' . $inv->Status . ' - ' . $idarea;
                echo '<br/>';
                $this->db->where('ID', $inv->ID)
                    ->update('Invoice', array(
                        'IDArea'    => $idarea
                    ));
            }
        } elseif ($area == 'Dept. PSO 2') {
            $verified = statusID('VERIFIED');
            $paid = statusID('PAID');

            $invs = $this->db->select('Invoice.ID,Invoice.NilaiInvoice,Invoice.NoInvoice,Invoice.Status')->where('us.Area', $area)
                ->join('Ms_User us', 'us.ID = Invoice.CreatedBy')->get('Invoice')->result();
            foreach ($invs as $inv) {
                if (($inv->NilaiInvoice > 25000000) and ($inv->Status == $verified or $inv->Status == $paid)) {
                    $idarea = 2;
                } else {
                    $idarea = 20;
                }
                echo $inv->ID . ' - ' . $inv->NoInvoice . ' - ' . $inv->NilaiInvoice . ' - ' . $inv->Status . ' - ' . $idarea;
                echo '<br/>';
                $this->db->where('ID', $inv->ID)
                    ->update('Invoice', array(
                        'IDArea'    => $idarea
                    ));
            }
        } elseif ($area == 'Pusat') {
            $invs = $this->db->select('Invoice.ID,Invoice.NilaiInvoice,Invoice.NoInvoice,Invoice.Status')->where('us.Area', $area)
                ->join('Ms_User us', 'us.ID = Invoice.CreatedBy')->get('Invoice')->result();
            foreach ($invs as $inv) {

                echo $inv->ID . ' - ' . $inv->NoInvoice;
                echo '<br/>';
                $this->db->where('ID', $inv->ID)
                    ->update('Invoice', array(
                        'IDArea'    => 3
                    ));
            }
        }
    }

    public function store()
    {
        // header('Content-Type: application/json');
        $_POST = json_decode(file_get_contents("php://input"), true);

        $username = $_POST['email'];
        $password = $_POST['password'];

        if (empty($username) || empty($password)) {
            die(json_encode(['status' => 'error', 'message' => 'Username atau Password salah']));
        }

        if (Auth::attempt($username, $password)) {
            if ($this->session->has_userdata('last_page')) {
                $url = $this->session->userdata('last_page');
                if (strpos($url, 'login') === false) {
                    die(json_encode(['redirect' => $url, 'status' => 'success']));
                } else {
                    notif('success', 'Selamat datang ' . Auth::unitKerja()->Nama);
                    die(json_encode(['redirect' => url('dashboard'), 'status' => 'success']));
                }
            } else {
                notif('success', 'Selamat datang.');
                die(json_encode(['redirect' => url('dashboard'), 'status' => 'success']));
            }
        } else {
            die(json_encode(['status' => 'error', 'message' => 'Username atau Password salah']));
        }
    }

    public function logout()
    {
        Auth::logout();
        redirect('login');
    }


    public function pairedvendor_playground()
    {

        $simona = $this->load->database('simona', TRUE);
        $results = $this->db->where('PairedVendorID !=', '')
            ->get('Ms_Customer')->result();

        $data[] = ['IDVendor', 'PairedVendorName'];
        foreach ($results as $result) {
            $simona->where('KODE_VENDOR_SAP', $result->PairedVendorID);

            $rows = $simona->select('KODE_VENDOR_SAP,NAMA')->get('ms_vendor', 1)->result();

            foreach ($rows as $row) {
                $data[] = ['ID' => $row->KODE_VENDOR_SAP, 'Name' => "$row->KODE_VENDOR_SAP - $row->NAMA"];

                $this->db->where('PairedVendorID', $row->KODE_VENDOR_SAP)
                    ->update('Ms_Customer', array(
                        'PairedVendorName'    => $row->NAMA
                    ));
            }
        }
        $this->table->set_template(['table_open' => '<table border="1">']);
        echo $this->table->generate($data);
    }

    public function msuservendor_playground()
    {

        $simona = $this->load->database('simona', TRUE);
        $results = $this->db->where('IDVendor !=', '')
            ->get('Ms_User')->result();

        $data[] = ['IDVendor', 'VendorName'];
        foreach ($results as $result) {
            $simona->where('KODE_VENDOR_SAP', $result->IDVendor);

            $rows = $simona->select('KODE_VENDOR_SAP,NAMA')->get('ms_vendor', 1)->result();

            foreach ($rows as $row) {
                $data[] = ['ID' => $row->KODE_VENDOR_SAP, 'Name' => "$row->KODE_VENDOR_SAP - $row->NAMA"];

                $this->db->where('IDVendor', $row->KODE_VENDOR_SAP)
                    ->update('Ms_User', array(
                        'VendorName'    => $row->NAMA
                    ));
            }
        }
        $this->table->set_template(['table_open' => '<table border="1">']);
        echo $this->table->generate($data);
    }

    public function email_playground()
    {

        $from_email = "no-reply@mitraku.pupukkaltim.com";
        /*Send Email*/
        $to_email = "yogia17@gmail.com";
        //Load email library
        $this->load->library('email');

        $this->email->from($from_email, 'Mitraku');
        $this->email->to($to_email);
        $this->email->subject('Testing Email PKT');
        $this->email->message('Tes email body');

        //Send mail
        if ($this->email->send());
        $msg = 'success';
    }
}
