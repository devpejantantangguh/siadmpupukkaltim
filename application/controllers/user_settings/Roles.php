<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roles extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		Auth::guard();
	}

	public function index()
	{
		$this->blade->view('user_settings.roles.index', array('table' => 'Ms_Role'));
	}

	public function add()
	{
		$this->blade->view('user_settings.roles.add', array('table' => 'Ms_Role'));
	}

	public function store()
	{
		$postdata = $this->input->post();
		$invoice = array('Invoice' => array('1' => 1, '2' => 1, '3' => 1));
		$akuntansi = array('Akuntansi' => array('1' => 1, '2' => 1, '3' => 1, '4' => 1));
		$keuangan = array('Keuangan' => array('1' => 1, '2' => 1, '3' => 1, '4' => 1, '5' => 1, '6' => 1, '7' => 1, '8' => 1, '9' => 1, '10' => 1, '11' => 1));
		$masterdata = array('MasterData' => array('1' => 1));
		$misc = array('Misc' => array('1' => 1, '2' => 1));
		$data = array_merge($invoice, $akuntansi, $keuangan, $masterdata, $misc);
		//print_r($postdata);
		$array = [];
		foreach ($data as $key => $value) {
			if (array_key_exists($key, $postdata)) {
				foreach ($data[$key] as $k => $v) {
					if (array_key_exists($k, $postdata[$key])) {
						if ($v == 1) {
							$array[$key][$k] = 1;
						} else {
							//$data[$key][$k] = 0;
						}

					} else {
						//$data[$key][$k] = 0;
					}

				}

			} else {
				foreach ($data[$key] as $k => $v) {
					//$data[$key][$k] = 0;
				}

			}
		}
		$permission = json_encode($array);
		$rsl = array(
			'ID' => newID('Ms_Role'),
			'RoleName' => $this->input->post('role'),
			'Permission' => $permission
		);
		$this->db->insert('Ms_Role', $rsl);
		redirect("user_settings/roles/?insert=success");
	}

	public function edit($id = 0)
	{
		if ($id == 0) die("Whooops!");
		$data = $this->get_detail_role($id);
		$this->blade->view('user_settings.roles.edit', compact('data', 'id'));
	}

	public function update($id = 0)
	{
		$postdata = $this->input->post();
		$invoice = array('Invoice' => array('1' => 1, '2' => 1, '3' => 1));
		$akuntansi = array('Akuntansi' => array('1' => 1, '2' => 1, '3' => 1, '4' => 1));
		$keuangan = array('Keuangan' => array('1' => 1, '2' => 1, '3' => 1, '4' => 1, '5' => 1, '6' => 1, '7' => 1, '8' => 1, '9' => 1, '10' => 1, '11' => 1));
		$masterdata = array('MasterData' => array('1' => 1));
		$misc = array('Misc' => array('1' => 1, '2' => 1));
		$data = array_merge($invoice, $akuntansi, $keuangan, $masterdata, $misc);
		//print_r($postdata);
		$array = [];
		foreach ($data as $key => $value) {
			if (array_key_exists($key, $postdata)) {

				foreach ($data[$key] as $k => $v) {
					if (array_key_exists($k, $postdata[$key])) {
						if ($v == 1) {
							$array[$key][$k] = 1;
						} else {
							//$data[$key][$k] = 0;
						}

					} else {
						//$data[$key][$k] = 0;
					}

				}

			} else {
				foreach ($data[$key] as $k => $v) {
					//$data[$key][$k] = 0;
				}

			}
		}
		$permission = json_encode($array);
		$this->db->where('ID', $id)
			->update('Ms_Role', array('Permission' => $permission));
		redirect("user_settings/roles/?update=success");
	}

	public function delete($id)
	{
		$id = (int)$id;
		if (in_array($id, array('1', '2', '4', '5', '10', '11', '12', '13', '14', '15', '1010', '1011'))) {
			notif('error', 'Data Primary tidak bisa dihapus => ' . $id);
			redirect("user_settings/roles/?delete=success");
			die();

		}
		if ($id > 0) {
			$this->db->where('ID', $id);
			$this->db->delete('Ms_Role');
		}

		redirect("user_settings/roles/?delete=success");
	}

	private function get_detail_role($id)
	{
		$query = $this->db->where('ID', $id)->get('Ms_Role');
		return $query->num_rows() > 0 ? $query->row() : [];
	}

}
