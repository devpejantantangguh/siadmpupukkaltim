<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        Auth::guard();
    }
    public function index()
    {
        $this->blade->view('user_settings.users.index', array(
            'table' => 'User',
            'roleList'   => $this->get_role()
        ));
    
    }

    private function get_role()
    {
        $result = [];

        $query = $this->db->get('Ms_Role');
        if( $query->num_rows() > 0 ) {
            foreach($query->result_array() as $row) {
                $result[] = [
                    'id'    => trim($row['ID']),
                    'name'  => $row['RoleName']
                ];    
            }
        } 

        return $result;
    }
}
