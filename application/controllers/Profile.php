<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        Auth::guard();
    }

    public function reset_password()
    {
        $this->blade->view('profile.reset_password');
    }

    public function update_password()
    {
        header('Content-Type: application/json');
        $_POST = json_decode(file_get_contents("php://input"),true);

        $user_id = Auth::user()->ID;
        $user = $this->db->where('ID', $user_id)->get('Ms_User')->row();

        if($_POST['old_password'] == $user->Password) {
            if($_POST['new_password'] != $_POST['re_password'] || empty($_POST['new_password']) || empty($_POST['re_password'])) {
                die(json_encode(['status' => 'danger', 'message' => 'Password didn\'t match']));
            } else {
                $this->db->where('ID', $user_id)->update('Ms_User',[
                    'Password' => $_POST['new_password']
                ]);
                die(json_encode(['status' => 'success', 'message' => 'Password Successfully Updated']));
            }
        }
        die(json_encode(['status' => 'danger', 'message' => 'Password didn\'t match']));
    }
}
