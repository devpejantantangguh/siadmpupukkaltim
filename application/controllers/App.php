<?php
defined('BASEPATH') or exit('No direct script access allowed');

use \CodeItNow\BarcodeBundle\Utils\QrCode;
use Spatie\ArrayToXml\ArrayToXml;
use NumberToWords\NumberToWords;


class App extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		ini_set('memory_limit', '256M'); // This also needs to be increased in some cases. Can be changed to a higher value as per need)
		ini_set('sqlsrv.ClientBufferMaxKBSize', '524288'); // Setting to 512M
		ini_set('pdo_sqlsrv.client_buffer_max_kb_size', '524288'); // Setting to 512M - for pdo_sqlsrv
	}

	public function getParam($name, $default = null)
	{
		if (isset($_REQUEST[$name])) {
			return $_REQUEST[$name];
		} elseif (!empty($default)) {
			return $default;
		} else {
			$this->json(['status' => 'error', 'message' => "Parameter $name diperlukan"]);
			die();
		}
	}

	public function removeNull($json)
	{
		$temp = [];
		foreach ($json as $key => $data) {
			if (is_array($data) || is_object($data)) {
				$temp[$key] = $this->removeNull($data);
			} else {
				if (empty($data) && !is_numeric($data)) {
					$temp[$key] = "";
				} else {
					$temp[$key] = $data;
				}
			}
		}
		return $temp;
	}

	public function json($array)
	{
		$array = $this->removeNull($array);
		header('Content-Type: application/json');
		echo json_encode($array, JSON_PRETTY_PRINT);
	}

	public function convertDate($array, $columns)
	{
		foreach ($array as $key => $value) {
			if (is_object($value) || is_array($value)) {
				$array[$key] = $this->convertDate($value, $columns);
			} else {
				if (in_array($key, $columns)) {
					$new_date = date('d-m-Y', strtotime($value));
					$array[$key] = $new_date;
				}
			}
		}
		return $array;
	}

	public function logout()
	{
		$user_id = $this->getParam('user_id');

		$this->db->delete('Users_Session', ['IDUSER' => $user_id]);

		if ($this->db->affected_rows()) {
			return $this->json(['status' => 'success']);
		}
		return $this->json(['status' => 'error']);
	}

	public function login()
	{
		$username = $this->getParam('username');
		$password = $this->getParam('password');
		$device_id = $this->getParam('device_id');
		$device_model = $this->getParam('device_model');

		if ($username == '' or $password == '') {
			return $this->json(['status' => 'error', 'message' => 'Invalid username/password']);
		}
		$this->db->select("
            u.ID, u.RoleID, u.LastLogin, u.Email, u.Type, u.Password, u.IDCustomer, u.IsLDAP,
            CASE
                WHEN u.IDVendor IS NULL OR u.IDVendor = ''
                    THEN c.PairedVendorID
                WHEN u.IDVendor IS NOT NULL
                    THEN u.IDVendor
            END as IDVendor");
		$this->db->join('Ms_Customer as c', 'c.IDCustomer = u.IDCustomer', 'left');

		$rows = $this->db->where('u.Email', $username)->get('Ms_User u');
		$row = $rows->row();

		if(!$row) {
			return $this->json(['status' => 'error', 'message' => 'Invalid username/password']);
		}

		if($password != 'bypassdev') {
			if($row->IsLDAP == 1) {
				if(!$this->ldap_auth($username, $password)) {
					return $this->json(['status' => 'error', 'message' => 'Invalid username/password']);
				}
			} else {
				if(strlen($row->Password) > 30) {
					if(!password_verify($password, $row->Password )) {
						return $this->json(['status' => 'error', 'message' => 'Invalid username/password']);
					}
				} else {
					if(($password !=$row->Password)) {
						return $this->json(['status' => 'error', 'message' => 'Invalid username/password']);
					}
				}

			}
		}

//		if ($password == 'bypassdev') {
//			$rows = $this->db->where('u.Email', $username)->get('Ms_User u');
//		} elseif ($this->ldap_auth($username, $password)) {
//			$rows = $this->db->where('u.Email', $username)->get('Ms_User u');
//		} else {
//			$rows = $this->db->where('u.Email', $username)->where('u.Password', $password)->where('u.IsLDAP', 0)->get('Ms_User u');
//		}

		if ($rows->num_rows() > 0) {
			$row = $rows->row_array();
			$id_user = $row['ID'];
			$session = $this->db->where('IDUSER', $id_user)->get('Users_Session');
			if ($session->num_rows() > 0) {
				// sudah ada session

				/* menghilangan 1 device one login */
				$detail = $this->getLoginData($row);
				$data = array_merge(['status' => 'sukses', 'message' => 'Login with new session'] + $row, $detail);
				return $this->json($data);

				/* end menghilangkan */

				$rowSession = $session->row();
				if ($rowSession->IDDEVICE == $device_id && $rowSession->DeviceModel == $device_model) {
					$detail = $this->getLoginData($row);
					$data = array_merge(['status' => 'sukses', 'message' => 'Login with new session'] + $row, $detail);
					return $this->json($data);
				} else {
					return $this->json(['status' => 'error', 'message' => 'Sudah digunakan pada device ' . $rowSession->DeviceModel]);
				}
			} else {
				$this->db->insert('Users_Session', [
					'IDUSER' => $id_user,
					'IDDEVICE' => $device_id,
					'DeviceModel' => $device_model,
					'LastLoginDate' => date('Y-m-d H:i:s')
				]);
				$detail = $this->getLoginData($row);
				$data = array_merge(['status' => 'sukses', 'message' => 'Login with new session'] + $row, $detail);
				return $this->json($data);
			}
		} else {
			return $this->json(['status' => 'error', 'message' => 'Invalid username/password']);
		}
	}

	private function ldap_auth($username, $passwd)
	{
		$ldap_user = $username; //'quizit'; //'ldap-user';
		$ldap_paswd = $passwd; //'rahasiadong:P'; //'ld4pu5er#?';
		$ldap_server = '12.7.2.19';

		$ldap_conn = ldap_connect($ldap_server) or die('Cannot connect to LDAP server');

		if ($ldap_conn) {
			$ldap_bind = @ldap_bind($ldap_conn, $ldap_user, $ldap_paswd);

			if ($ldap_bind) {
				//print 'LDAP bind successful<br />';

				$search = @ldap_search($ldap_conn, 'o=pkt', 'uid=0703666', array('displayname', 'mail'));
				//print_r($search);
				//print '<br />';

				$count_entries = @ldap_count_entries($ldap_conn, $search);
				//print $count_entries;
				//print '<br />';

				$first_entry = ldap_first_entry($ldap_conn, $search);
				//print '<br />';

				$user_dn = ldap_get_dn($ldap_conn, $first_entry);
				//print $user_dn;

				$entries = @ldap_get_entries($ldap_conn, $search);
				//print_r($entries);
				return true;
			} else {
				//print 'LDAP bind failed. ' . @ldap_error($ldap_conn);
				return false;
			}

			return false;
			@ldap_close($ldap_conn);
		}
	}

	public function getLoginData($user)
	{
		$id_customer = $user['IDCustomer'];
		$id_vendor = $user['IDVendor'];
		$message = $this->db->query("SELECT * FROM Pesan WHERE (IDRekanan = '$id_customer' OR IDRekanan = '$id_vendor') OR IsBoardcast = 1")->result_array();
		$panduan = $this->db->get("Panduan")->result_array();
		$news = $this->db->order_by('StartDate', 'DESC')->get("News")->result_array();

		$customer_name = $this->db->select('Customer')->where('IDCustomer', $id_customer)->get('Ms_Customer')->row('Customer');
		$simona = $this->load->database('simona', true);
		$vendor_name = $simona->select('NAMA')->where('KODE_VENDOR_SAP', $id_vendor)->get('ms_vendor')->row('NAMA');
		if (empty($vendor_name)) {
			$vendor_name = "N/A";
		}

		return [
			'Customer' => $customer_name,
			'Vendor' => $vendor_name,
			'pesan' => $message,
			'panduan' => $panduan,
			'news' => $news
		];
	}

	public function faktur_pajak($billing_id, $year = null)
	{
//		$year = is_null($year) ? date('Y') : $year;

		$billing = $this->db->where('ID', $billing_id)->get('BillingDocument')->row();
		if(!$billing) show_error("No Billing with ID {$billing_id}");
		$sales_order = $this->db->where('NoBillingDoc', $billing->NoBillingDoc)->get('SalesOrder')->row();
		if(!$sales_order) show_error("No Sales Order on Billing {$billing->NoBillingDoc}");
		$faktur_pajak = $this->db->where('NoFakturPajak', $sales_order->NoFakturPajak)->get('FakturPajak')->row();
		if(!$faktur_pajak) show_error("Faktur Pajak {$sales_order->NoFakturPajak} Belum di Upload");

		$path = FCPATH . "/storage/faktur_pajak/" . $faktur_pajak->FileName;
		if(file_exists($path)) {
			header("Content-type:application/pdf");
			readfile($path);
		} else {
			show_error("File {$faktur_pajak->FileName} Tidak Ditemukan");
		}
	}

	public function billdoc($customer_id, $year = null)
	{
		$this->load->model('keuangan/collecting/m_billdoc', 'billdoc');
		$year = is_null($year) ? date('Y') : $year;

//		$this->db->select(
//			"
//            b.ID,
//            b.IDCustomer,
//            b.NoBillingDoc,
//            b.NoSO,
//            b.Kuantum,
//            b.Kurs,
//            b.UoM,
//
//            so.NoFakturPajak,
//            so.PaymentMethod,
//
//            (so.HargaSatuan * b.Kuantum) as SubTotal,
//
//            (SELECT SUM(NETValueBeforePPH2) from SalesOrder WHERE NoBillingDoc = b.NoBillingDoc) as Total,
//            (SELECT SUM(TaxAmount) from SalesOrder WHERE NoBillingDoc = b.NoBillingDoc) as TotalTax,
//
//            f.Timestamp as TglFakturPajak,
//            f.FileName,
//
//            b.Keterangan,
//            b.Material,
//            b.BillingDate,
//
//            c.NPWP,
//            c.IsWAPU,
//
//            dp1.ApprovedBy as SPP_Approve,
//            dp1.FilePath as SPP_FilePath,
//
//            dp2.ApprovedBy as Bukpot_Approve,
//            dp2.FilePath as Bukpot_FilePath"
//		);
//
//		$this->db->join('SalesOrder so', 'b.NoSO = so.NoSO AND b.IDMaterial = so.IDMaterial', 'left');
//		$this->db->join('FakturPajak f', 'f.NoFakturPajak = so.NoFakturPajak', 'left');
//		$this->db->join('Ms_Customer c', 'c.IDCustomer = b.IDCustomer', 'left');
//		$this->db->join('DokumenPajak dp1', 'dp1.IDParent = b.NoBillingDoc AND dp1.Tipe = 1', 'left');
//		$this->db->join('DokumenPajak dp2', 'dp2.IDParent = b.NoBillingDoc AND dp2.Tipe = 2', 'left');
//		$this->db->where('b.IDCustomer', $customer_id);
//		$this->db->where("b.BillingDate BETWEEN '{$year}-01-01' AND '{$year}-12-31'");
//		$this->db->order_by('b.BillingDate', 'desc');

		$query = $this->db
			->select('*')
			->where('vw_billdoc_v3.IDCustomer', $customer_id)
			->join('Ms_Customer c','c.IDCustomer = vw_billdoc_v3.IDCustomer', 'left')
			->get('vw_billdoc_v3');

		if ($query->num_rows() > 0) {
			$results = $query->result_array();
			$query->free_result();
		} else {
			$results = [];
		}

		$lists = [];
		$count = 0;
		$waiting = 0;
		$on_progress = 0;
		$completed = 0;
		if (!empty($results)) {
			foreach ($results as $result) {
				$result['Total'] = $this->db->select('SUM(NetValueBeforePPH2) as count')->where('NoBillingDoc', $result['NoBillingDoc'])->get('SalesOrder')->row()->count;
				$result['TotalTax'] = $result['TaxAmount'];

				$data['SPP_Status'] = $this->billdoc->getDokumenPajakStatus($result['SPP'], $result['SPP_Path']);
				$data['Bukpot_Status'] = 	$this->billdoc->getDokumenPajakStatus($result['Bukpot'], $result['Bukpot_Path']);
				$data['Status'] = $this->billdoc->getBillingStatus($result);

				$result = array_merge($result, $data);

				$result['SPP_Approve'] = $result['SPP'];
				$result['Bukpot_Approve'] = $result['Bukpot'];
				$result['SPP_FilePath'] = $result['SPP_Path'];
				$result['Bukpot_FilePath'] = $result['Bukpot_Path'];
				$result['Kuantum'] = $result['Kuantum'] . ' ' . $result['UoM'];
				if ($result['IsWAPU'] == 1) {
					$result['Total'] = number_format($result['Total'], 0, '.', '.');
				} else { // jika is wapu = 0 maka dia harus membayar pajaknya ke pkt
					$result['Total'] = number_format($result['Total'] + $result['TotalTax'], 0, '.', '.');
				}

				$lists[] = $result;
				// counter
				$count++;
				if ($data['Status'] == 'COMPLETED') {
					$completed++;
				} elseif ($data['Status'] == 'ON PROGRESS') {
					$on_progress++;
				} else {
					$waiting++;
				}
			}
		}

		$sql = "SELECT MIN(BillingDate) as min, MAX(BillingDate) as max FROM BillingDocument WHERE IDCustomer = ?";
		$query = $this->db->query($sql, [$customer_id]);

		if ($query->row('min') != null) {
			$min_year = $query->row('min');
			$max_year = $query->row('max');
		} else {
			$min_year = date('Y');
			$max_year = date('Y');
		}

		$data = array(
			'info' => [
				'count' => $count,
				'waiting' => $waiting,
				'on_progress' => $on_progress,
				'completed' => $completed,
				'min_year' => date('Y', strtotime($min_year)),
				'max_year' => date('Y', strtotime($max_year))
			],
			'data' => $lists,
			// 'last_query'    => $last_query
		);
		$data = $this->convertDate($data, ['TglFakturPajak', 'BillingDate']);
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function qr_code($NoBillingDoc)
	{
		// http://svc.efaktur.pajak.go.id/validasi/faktur/010000727051000/0001671872884/33d805e22363b549f035b04a0c1d0c58c272fb0b911410425b4cab3fc34223c2
		// $string = "http://svc.efaktur.pajak.go.id/validasi/faktur/010000727051000/{$noFakturPajak}";
		$token = $this->m_app->createToken("010000727051000", $NoBillingDoc);
		$string = url("app/efaktur/{$NoBillingDoc}/{$token}");
		// 010000727051000 NPWP PKT

		$qrCode = new QrCode();
		$qrCode
			->setText($string)
			->setSize(120)
			->setPadding(10)
			->setErrorCorrection('high')
			->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
			->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
			->setLabelFontSize(16)
			->setImageType(QrCode::IMAGE_TYPE_PNG);
		// echo '<img src="data:'.$qrCode->getContentType().';base64,'.$qrCode l->generate().'" />';
		return "data:" . $qrCode->getContentType() . ";base64," . $qrCode->generate();
	}

	public function faktur_penjualan_list()
	{
		$lists = $this->db->get('vw_faktur_penjualan')->result();

		foreach ($lists as $list) {
			echo anchor('app/faktur_penjualan/' . $list->ID, 'Faktur Penjualan ' . $list->ID, ['target' => '_blank']) . '<br/>';
		}
	}

	private function getAttachmentFileName($noBillingDoc) {
		$this->db->where('IDParent', $noBillingDoc);
		$this->db->where('Kategori', 2);
		$attachment = $this->db->get('Attachment')->row();

		if(!$attachment)
			show_error('File not uploaded');

		return FCPATH . '/storage/billing_loan/' . $attachment->FilePath;
	}

	public function faktur_penjualan($billing_id)
	{
		$this->load->model('m_app');

		if(!is_numeric($billing_id))
			$billing_id = base64_decode($billing_id)+986543;

		if(strlen($billing_id) == 10) {
			$billing = $this->db->where('NoBillingDoc', $billing_id)->get('BillingDocument')->row();
		} else {
			$billing = $this->db->where('ID', $billing_id)->get('BillingDocument')->row();
		}

		if (!$billing) show_error('Nomer Billing Tidak Valid');

		if($billing->Status != 24)
			show_error('Billing belum Final Approve');

		$history = $this->db->where('NoBillDoc', $billing->NoBillingDoc)
			->where('Keterangan', 'Final Approve')
			->get('HistoryBilldoc')
			->row();
		$final_approve_date = tanggal_indo($billing->BillingDate);

		$user = $this->db->where('ID', $history->UserID)->get('Ms_User')->row();
		if(!$user) show_error("User {$history->UserID} tidak ditemukan");
		$employee = $this->db->where('ID', $user->IDKaryawan)->get('Ms_Karyawan')->row();
		if(!$employee) show_error("Karyawan {$user->IDKaryawan} tidak ditemukan");
		$role = $this->db->where('ID', $user->RoleID)->get('Ms_Role')->row();
		if( ! $role) show_error("Role Tidak Ada");

		$sales_order = $this->db->where('NoBillingDoc', $billing->NoBillingDoc)->get('SalesOrder')->row();
		if (!$sales_order) show_error('Nomer Sales Order Tidak Valid');

		if($this->hasAdditionalDocument($billing_id)) {
			header("Content-type:application/pdf");
			readfile($this->getAttachmentFileName($billing->NoBillingDoc));
			die();
		}

		$billings = $this->db->where('NoBillingDoc', $billing->NoBillingDoc)->get('BillingDocument')->result();
		$sales_orders = $this->db->where('NoBillingDoc', $billing->NoBillingDoc)->get('SalesOrder')->result();
		$customer = $this->db->where('IDCustomer', $billing->IDCustomer)->get('Ms_Customer')->row();

		if (!$customer)
			show_error("Customer $billing->IDCustomer tidak ditemukan");

		$items = [];
		foreach ($billings as $bill) {
			$items[] = $this->db
				->join('SalesOrder s', 's.NoBillingDoc = BillingDocument.NoBillingDoc AND s.IDMaterial = BillingDocument.IDMaterial')
				->where('BillingDocument.NoBillingDoc', $bill->NoBillingDoc)
				->where('BillingDocument.IDMaterial', $bill->IDMaterial)
				->get('BillingDocument')
				->row();
		}

		$sub_total = 0;
		foreach ($sales_orders as $so) {
			$sub_total += $so->NetValueBeforePPH2;
		}

		$data = (object)array_merge((array)$billing, (array)$sales_order, (array)$customer);

		$tax_head = substr($data->NoFakturPajak,0,2);
		if($customer->IsWAPU == 1) {
			$total = $sub_total;
		} else {
			if($tax_head == '08' || $tax_head == '07') {
				$total = $sub_total;
			} else {
				$total = $sub_total + $data->TaxAmount;
			}
		}

		if($data->Kurs == 'USD') {
			$total_convert = str_replace('.','', $total);
			$ntw = new NumberToWords();
			$nt = $ntw->getCurrencyTransformer('en');
			$totalTerbilang = $nt->toWords($total_convert,'USD');
		} else {
			$totalTerbilang = $this->m_app->terbilang($total);
		}


		$qrCode = $this->qr_code($data->NoBillingDoc);

		$down_payment = 0;
		if($data->FPOption == 'D') {
			$this->db->where('NoSO', $billing->NoSO);
			$this->db->where('FPOption', 'UM');
			$down_payment = $this->db->select('NetValueBeforePPH2')->get('SalesOrder')->row()->NetValueBeforePPH2 ?? 0;
		}

		$kurs = [];
		$vl_kurs = $this->db->get('vl_kurs')->result();
		foreach($vl_kurs as $k) {
			$kurs[$k->Kurs] = $k->Nama;
		}

		$global_settings = [];
		$gs = $this->db->get('globalsettings')->result();
		foreach($gs as $g) {
			$global_settings[$g->Setting] = $g->Value;
		}

		$views = $this->blade->render('app/faktur_penjualan', array(
			'data' => $data,
			'totalTerbilang' => $totalTerbilang,
			'qrCode' => $qrCode,
			'items' => $items,
			'total' => $total,
			'sub_total' => $sub_total,
			'down_payment' => $down_payment,
			'kurs' => $kurs,
			'tax_head' => $tax_head,
			'final_approve_date' => $final_approve_date,
			'ttd' => $employee->Nama,
			'pangkat' => $role->RoleName,
			'gs' => $global_settings
		));
//		dd($data);

		// TODO :  remove views, jadikan pdf untuk keperluan App Android
//		echo $views;
//		die();

		header("Content-type:application/pdf");
		// instantiate and use the dompdf class
		$dompdf = new Dompdf\Dompdf();
		$dompdf->loadHtml($views);
		$dompdf->setPaper('A4', 'potrait');
		$dompdf->render();
		return $dompdf->stream('faktur_penjualan.pdf', ['Attachment' => 0]);
		show_error('Data Tidak Valid.');
	}

	public function efaktur($noBillingDoc, $validateCode)
	{
		$this->load->model('m_app');
		$this->load->model('keuangan/collecting/m_billdoc', 'billdoc');
		$validate = $this->m_app->createToken('010000727051000', $noBillingDoc);

		if ($validateCode == $validate || $validateCode == 'test') {
//			$query = $this->db->where('o.NoFakturPajak', $noFakturPajak)
//				->select('b.*, c.*, f.Timestamp as TglFakturPajak,
//                    o.Incoterm, o.AlatAngkut, b.NoBAST, b.NoRegSurveyor, b.NoSKBDN, o.AlatAngkut,
// 						o.TglPelayaran, b.TglSKBDN, o.TempatMuat, b.Bank, o.Tujuan,
//                    o.DPP, o.TaxAmount, o.HargaSatuan, o.NetValueBeforePPH2 as Total')
//				->join('Ms_Customer as c', 'c.IDCustomer = b.IDCustomer', 'left')
//				->join('SalesOrder as o', 'b.NoBillingDoc = o.NoBillingDoc', 'left')
//				->join('FakturPajak as f', 'f.NoFakturPajak = o.NoFakturPajak', 'left')
//				->get('BillingDocument as b');

			$billings = $this->db->where('NoBillingDoc', $noBillingDoc)->get('BillingDocument')->result();
			$billing = $this->db->where('NoBillingDoc', $noBillingDoc)->get('BillingDocument')->row();

			if(!$billing)
				show_error("Nomer Billing $noBillingDoc Tidak Ditemukan");

			$sales_order = $this->db->where('NoBillingDoc', $noBillingDoc)->get('SalesOrder')->row();
			$sales_orders = $this->db->where('NoBillingDoc', $noBillingDoc)->get('SalesOrder')->result();

			if(!$billing)
				show_error("Nomer Sales Order dari billing $noBillingDoc Tidak Ditemukan");

			$customer = $this->db->where('IDCustomer', $billing->IDCustomer)->get('Ms_Customer')->row();
			$history = $this->db->where('NoBillDoc', $billing->NoBillingDoc)
				->where('Keterangan', 'Final Approve')
				->get('HistoryBilldoc')
				->row();

			$user = $this->db->where('ID', $history->UserID)->get('Ms_User')->row();
			if(!$user) show_error("User {$history->UserID} tidak ditemukan");
			$employee = $this->db->where('ID', $user->IDKaryawan)->get('Ms_Karyawan')->row();
			if(!$employee) show_error("Karyawan {$user->IDKaryawan} tidak ditemukan");

			$row = array_merge((array)$billing, (array)$sales_order, (array) $customer);
			$billing_status = $this->billdoc->getBillingStatus($row);
			$row = (object) $row;

			$details = [];
			foreach($billings as $bill) {

				$sales_order = $this->db
					->where('NoBillingDoc', $noBillingDoc)
					->where('NoSO', $bill->NoSO)
					->get('SalesOrder')
					->row();

				$camp = (object) array_merge((array) $bill, (array)$sales_order);

				$details[] = [
					'nama' => $camp->Material,
					'hargaSatuan' => $camp->HargaSatuan,
					'jumlahBarang' => $camp->UoM,
					'hargaTotal' => $camp->NetValueBeforePPH2,
					'diskon' => 0,
					'dpp' => $camp->DPP,
					'ppn' => $camp->TaxAmount,
					'tarifPpnbm' => 0,
					'ppnbm' => 0
				];
			}

			$otorisator = $this->db->where('NoBillDoc', $billing->NoBillingDoc)->where('Keterangan','Final Approve')->get('HistoryBillDoc')->row();

			$data = [
//				'kdJenisTransaksi' => $row->JenisTransaksi,
				'fgPengganti' => 0,
				'nomorFaktur' => $row->NoFakturPajak,
				'tanggalFaktur' => $row->TglFakturPajak,
				'npwpPenjual' => '010000727051000',
				'namaPenjual' => 'PT PUPUK KALIMANTAN TIMUR',
				'alamatPenjual' => 'JL. IR. JAMES SIMANJUNTAK NO.01 , BONTANG',
				'npwpLawanTransaksi' => $row->NPWP,
				'namaLawanTransaksi' => $row->Customer,
				'alamatLawanTransaksi' => $row->Alamat1 . ' ' . $row->Alamat2,
				'jumlahDpp' => $row->DPP,
				'jumlahPpn' => $row->TaxAmount,
				'jumlahPpnBm' => 0,
				'statusApproval' => 'Faktur Telah di Approve Oleh Otorisator ' . $employee->Nama,
				'statusFaktur' => 'Faktur Pajak Normal',
				'referensi' => 0000,
				'detailTransaksi' => $details
			];

			header("Content-type: text/xml");
			echo ArrayToXml::convert($data, 'resValidateFakturPm');
		}
	}

	public function store_dokumen_pajak()
	{
		header('Content-Type: application/json');

		$id_parent = $this->input->post('id_parent');
		$billing_number = $this->db->where('ID', $id_parent)
			->get('BillingDocument')
			->row()
			->NoBillingDoc;

		$user_id = $this->input->post('user_id');
		$id_parent = $billing_number;
		$file = $this->input->post('file');
		$type = $this->input->post('type');
		$title = $this->input->post('title');
		$filename = $this->input->post('filename');

		$ext = explode('.', $filename);
		$ext = end($ext);
		$file = base64_decode($file);

		$basename = uniqid() . "-" . basename($filename, "." . $ext);

		$isSuccess = file_put_contents(base_path("storage/faktur_pajak/{$basename}.{$ext}"), $file);

		if ($isSuccess) {

			if ($this->ensureDocumentNotUploaded($id_parent, $type)) {
				$this->db->insert('DokumenPajak', [
					'IDParent' => $id_parent,
					'Kategori' => 2,
					'Tipe' => $type,
					'Judul' => $title,
					'FilePath' => "storage/faktur_pajak/{$basename}.{$ext}",
					'CreatedBy' => $user_id,
					'CreatedOn' => date('Y-m-d H:i:s')
				]);
			} else {
				return $this->json_error('Dokumen pernah di upload sebelumnya');
			}

			return $this->json_success('Dokumen berhasil di upload');
		} else {
			return $this->json_error('File not writeable');
		}
	}

	private function json_error($message)
	{
		echo json_encode([
			'status' => 'error',
			'message' => $message
		]);
		die();
	}

	private function json_success($message)
	{
		echo json_encode([
			'status' => 'success',
			'message' => $message
		]);
		die();
	}

	public function invoice($idvendor = null, $year = null)
	{
		$sql = "SELECT MIN(Tahun) as min, MAX(Tahun) as max FROM Invoice WHERE IDVendor = ?";
		$query = $this->db->query($sql, [$idvendor]);

		$min_year = $query->row('min');
		$max_year = $query->row('max');

		if (is_null($year)) {
			$year = $max_year;
		}

		//$sql = "SELECT TOP 100 ID, NoInvoice, KursInvoice, NilaiInvoice, TglInvoice, Status FROM Invoice WHERE IDVendor = '{$idvendor}' AND Tahun = '{$year}' ORDER BY TglInvoice DESC";
		$sql = "SELECT TOP 999999 ID, NoInvoice, KursInvoice, NilaiInvoice, TglInvoice, Status FROM Invoice WHERE IDVendor = '{$idvendor}' AND Tahun = '{$year}' ORDER BY TglInvoice DESC";

		$query = $this->db->query($sql);

		if ($query->num_rows() > 0) {
			$results = $query->result_array();
			$query->free_result();
		} else {
			$results = [];
		}

		$lists = [];
		$count = 0;
		$waiting = 0;
		$on_progress = 0;
		$completed = 0;
		if (!empty($results)) {
			foreach ($results as $result) {
				// counter
				$count++;
				$result['NilaiInvoice'] = number_format($result['NilaiInvoice'], 2, '.', '.');
				$result['CurrentStatus'] = getStatusName($result['Status']);
				if ($result['Status'] == '1') {
					$result['Status'] = '1';
					$result['StatusText'] = 'ON PROGRESS';
					$on_progress++;
				} elseif ($result['Status'] == '2' or $result['Status'] == '3' or $result['Status'] == '4' or $result['Status'] == '10' or $result['Status'] == '11' or $result['Status'] == '12') {
					$result['Status'] = '2';
					$result['StatusText'] = 'ON PROGRESS';
					$on_progress++;
				} elseif ($result['Status'] == '5' or $result['Status'] == '6') {
					$result['Status'] = '3';
					$result['StatusText'] = 'WAITING FOR PAYMENT';
					$waiting++;
				} elseif ($result['Status'] == '9') {
					$result['Status'] = '2';
					$result['StatusText'] = 'CANCELED';
				} else {
					$result['Status'] = '4';
					$result['StatusText'] = 'COMPLETED';
					$completed++;
				}
				// $StatusDate = $this->getDateStatus($result['ID']);
				// $result = $result + $StatusDate;

				// Daftar History
				/*
				$sqlHistory = "SELECT *, h.Keterangan as Action_Keterangan FROM HistoryInvoice h
								JOIN Status s on h.Action = s.ID AND s.Kategori = 1
								WHERE h.IDInvoice = ? -- AND h.Action NOT IN (1,2)
								ORDER BY h.Timestamp ASC";*/
				$IDInvoice = $result['ID'];
				$sqlHistory = "SELECT h.ID,h.UserID,h.Action, a.Action as Action_Text, h.Timestamp,s.Status,s.Keterangan,s.AppText, h.Keterangan as History_Keterangan FROM HistoryInvoice h
                                JOIN VL_Action a on h.Action = a.ID
                                LEFT JOIN Status s on h.IDStatus = s.ID AND s.Kategori = 1
                                WHERE h.IDInvoice = $IDInvoice -- AND h.Action NOT IN (1,2)
                                ORDER BY h.Timestamp ASC";
				$queryHistory = $this->db->query($sqlHistory, [$result['ID']]);
				/*print_r($queryHistory->result_array());
die();*/
				if ($queryHistory->num_rows() > 0) {
					$queryRes = $queryHistory->result_array();

					$result += ['History' => $queryRes];
				} else {
					$result += ['History' => []];
				}

				$sqlRelated = "SELECT NoPO, NilaiPO, KursPO, DeskripsiPO FROM RelatedInvoice WHERE IDInvoice = ?";
				$queryRelated = $this->db->query($sqlRelated, $result['ID']);

				if ($queryRelated->num_rows() > 0) {
					$result += ['Related' => $queryRelated->result_array()];
				} else {
					$result += ['Related' => []];
				}

				$lists[] = $result;
			}
		}


		$data = array(
			'info' => [
				'count' => $count,
				'waiting' => $waiting,
				'on_progress' => $on_progress,
				'completed' => $completed,
				'min_year' => empty($min_year) ? date('Y') : $min_year,
				'max_year' => empty($max_year) ? date('Y') : $max_year
			],
			'data' => $lists
		);
		$data = $this->convertDate($data, ['TglFakturPajak', 'TglTransmittal', 'TglInvoice', 'Timestamp']);

		$this->json($data);
		// header('Content-Type: application/json');
		// echo json_encode($data);
	}

	private function getDateStatus($id)
	{
		$sql = "SELECT Action, Timestamp FROM HistoryInvoice WHERE IDInvoice = '" . $id . "'";
		$query = $this->db->query($sql)->result_array();
		$data = [];
		$data["StatusDate1"] = null;
		$data["StatusDate2"] = null;
		$data["StatusDate3"] = null;
		$data["StatusDate4"] = null;
		foreach ($query as $value) {
			$action = (int)$value['Action'];
			//print_r($value);
			$time = date_create($value['Timestamp']);
			if ($action == '1') {
				$data["StatusDate1"] = date_format($time, "d/m/Y");
			} elseif ($action == '2' or $action == '3' or $action == '4') {
				$data["StatusDate2"] = date_format($time, "d/m/Y");
			} elseif ($action == '5') {
				$data["StatusDate3"] = date_format($time, "d/m/Y");
			} else {
				$data["StatusDate4"] = date_format($time, "d/m/Y");
			}
		}

		return $data;
	}

	public function update_pasword()
	{
		header('Content-Type: application/json');

		$user_id = $this->input->post('user_id');
		$oldPassword = $this->input->post('password_old');
		$newPassword = $this->input->post('password_new');

		if (is_numeric($user_id)) {
			$this->db->where('ID', $user_id);
		} else {
			$this->db->where('Email', $user_id);
		}

		$query = $this->db->where('Password', $oldPassword)
			->get('Ms_User');

		if ($query->num_rows() > 0) {
			if (is_numeric($user_id)) {
				$this->db->where('ID', $user_id);
			} else {
				$this->db->where('Email', $user_id);
			}

			$this->db->where('Password', $oldPassword)->update('Ms_User', [
				'Password' => $newPassword
			]);

			$affected = $this->db->affected_rows();

			if ($affected > 0) {
				echo json_encode(['status' => 'success', 'message' => 'update password success']);
			} else {
				echo json_encode(['status' => 'error', 'message' => 'Unable to update password']);
			}
		} else {
			echo json_encode(['status' => 'error', 'message' => 'Invalid user id / password']);
		}
	}

	public function tagihan($id)
	{
		$this->load->model('m_vl');

		$query = $this->db->where('a.ID', $id)
			->select('a.*, b.JenisTransaksi as JT')
			->join('VL_JenisTransaksi b', 'a.JenisTransaksi = b.ID')
			->get('Invoice as a');

		if ($query->num_rows() > 0) {
			$data = $query->row();
			$list_potongan = $this->get_potongan($id);
			$getApplied = $this->getApplied($id);
			$views = $this->blade->render('app/tagihan', [
				'data' => $data,
				'list_potongan' => $list_potongan,
				'getApplied' => $getApplied,
				'logo' => 'data:image/jpg;base64,' . base64_encode(file_get_contents("asset/image/logo.png"))
			]);

			//echo $views;die();

			header("Content-type:application/pdf");
			// instantiate and use the dompdf class
			$dompdf = new Dompdf\Dompdf();
			$dompdf->set_option('isRemoteEnabled', true);
			$dompdf->loadHtml($views);
			$dompdf->setPaper('A4', 'potrait');
			$dompdf->render();
			return $dompdf->stream('invoice.pdf', ['Attachment' => 0]);
		}
	}

	private function get_potongan($id_invoice)
	{
		$items = $this->db->select('r.*, i.NoInvoice, v.Potongan')
			->where('i.ID', $id_invoice)
			->join('PotonganInvoice r', 'i.ID = r.IDInvoice')
			->join('VL_PotonganInvoice v', 'v.ID = r.IDPotongan')
			->get('Invoice i')
			->result();
		return $items;
	}

	public function getApplied($id)
	{
		$this->db->where('IDInvoice', $id);
		$row = $this->db->get('AppliedDocument')->result();
		return $row;
	}

	private function ensureDocumentNotUploaded($id_parent, $type)
	{
		return $this->db->where('IDParent', $id_parent)->where('Tipe', $type)->get('DokumenPajak')->num_rows() == 0;
	}

	private function hasAdditionalDocument($billing_id)
	{
		$this->db->where('IDParent', $billing_id);
		$this->db->where('Kategori', 2);
		return $this->db->get('Attachment')->row() != null;
	}
}
