<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;

class Message extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        Auth::guard();
        $this->load->model('m_message');
    }

    public function delete_ajax()
	{
    	$ids = $_REQUEST['ids'];

    	$this->db->where_in('ID', $ids)->delete('Pesan');
	}

    public function index()
    {
        $this->blade->view('admin.message.index',array(
            'table'     => 'Pesan'
        ));
    }

    public function get()
    {
        $limit = isset($_GET['take']) ? $_GET['take'] : 10;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;

        if(isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        } else {
            $this->db->order_by('ID', 'DESC');
        }

        $items = $this->db->get('Pesan', $limit, $offset)->result();
        $count = $this->db->count_all('Pesan');

        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $items
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function create()
    {
        $this->blade->view('admin.message.create');
    }

    public function search()
    {
        header('Content-Type: application/json');

        $params = $this->input->get('q');

        if($_GET['type'] == 1)  {
            $lists = $this->m_message->searchVendor($params);
        } elseif($_GET['type'] == 2) {
            $lists = $this->m_message->searchCustomer($params);
        }

        echo json_encode($lists);
    }

    public function import()
    {
        echo $this->blade->view('admin.message.import');
    }

    public function upload()
    {
        $config['upload_path']          = './storage/message/';
        $config['allowed_types']        = 'xls|xlsx|csv'; // xlsm|xltx|xltm
        $config['max_size']             = 10 * 1024;
        $config['overwrite']            = true;
        $config['max_filename']         = 255;
        // $config['encrypt_name']         = true;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file')) {
            show_error($this->upload->display_errors());
        } else {
            $file = $this->upload->data('full_path');
            
            $spreadsheet = IOFactory::load($file);
            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

            foreach($sheetData as $key => $row) {
                if($key < 2) continue;

                $data = [];
                $data['IDRekanan'] = $row['A'];
                $data['IsCustomer'] = $row['B'];
                $data['IsVendor'] = $row['C'];
                $data['Judul'] = $row['D'];
                $data['Pesan'] = $row['E'];
                $data['StartDate'] = $row['F'];
                $data['EndDate'] = $row['G'];
                $data['Keterangan'] = $row['H'];
                $data['IsBoardcast'] = $row['I'];
                $data['CreatedBy'] = Auth::user()->ID;
                $data['CreatedOn'] = date('Y-m-d H:i:s');

                $this->db->insert('Pesan', $data);
            }
            
            redirect('admin/message');
        }
    }

    public function store()
    {
        $data = $_POST;
        $data['IsBoardcast'] = isset($_POST['IsBoardcast']) ? true : false;

        if($_POST['who'] == 1) {
            $data['IsVendor'] = true;
            $data['IsCustomer'] = false;
        } else {
            $data['IsVendor'] = true;
            $data['IsCustomer'] = false;
        }
        unset($data['who']);
        $data['CreatedOn'] = date('Y-m-d H:i:s');
        $data['CreatedBy'] = Auth::user()->ID;

        $this->db->insert('Pesan', $data);

        redirect('admin/message');
    }

    public function edit($id)
    {
        $row = $this->db->where('ID', $id)->get('Pesan')->row();
        $who = $row->IsVendor == 1 ? 1 : 2;

        if($row->IsVendor) {
            $optionValue = $this->m_message->vendorName($row->IDRekanan);
        }elseif($row->IsCustomer) {
            $optionValue = $this->m_message->customerName($row->IDRekanan);
        }

        echo $this->blade->view('admin.message.edit', compact('row','who','optionValue'));
    }

    public function update($id)
    {
        $data = $_POST;
        $data['IsBoardcast'] = isset($_POST['IsBoardcast']) ? true : false;

        if($_POST['who'] == 1) {
            $data['IsVendor'] = true;
            $data['IsCustomer'] = false;
        } else {
            $data['IsVendor'] = true;
            $data['IsCustomer'] = false;
        }
        unset($data['who']);

        $this->db->where('ID', $id)->update('Pesan', $data);

        redirect('admin/message');
    }

    public function destroy($id)
    {
        $this->db->where('ID',$id)->delete('Pesan');
        redirect('admin/message');
    }
}
