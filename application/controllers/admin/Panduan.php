<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Panduan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/m_panduan','m');

        Auth::guard();
    }

    public function index()
    {
        $this->blade->view('admin.panduan.index', array('table' => 'Panduan'));
    }

    public function create()
    {
        $this->blade->view('admin.panduan.create');
    }

    public function store()
    {
        $params = $this->input->post();
        $params['CreatedBy'] = Auth::user()->ID;
        $params['CreatedOn'] = date('Y-m-d H:i:s');
        $params['Aktif'] = $params['Aktif'] == 'true' ? 1 : 0;
        $this->db->insert('Panduan', $params);

        redirect('admin/panduan');
    }

    public function edit($id)
    {
        $row = $this->db->where('ID',$id)->get('Panduan',1)->row();
        $this->blade->view('admin.panduan.edit', compact('id','row'));
    }

    public function update($id)
    {
        $params = $this->input->post();
        $params['UpdatedBy'] = Auth::user()->ID; 
        $params['UpdatedOn'] = date('Y-m-d H:i:s');
        $this->db->where('ID',$id)->update('Panduan', $params);

        redirect('admin/panduan');
    }

    public function destroy($id)
    {
        $this->db->delete('Panduan', ['ID' => $id]);
    }

    public function get()
    {
        $this->m->ajax_list();
    }

    public function dummy()
    {
        $this->m->dummy();
    }
}
