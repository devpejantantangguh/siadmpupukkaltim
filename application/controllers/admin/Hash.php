<?php


class Hash extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		Auth::guard();
	}

	public function index()
	{
		$users = $this->db->get('Ms_User')->result();
		$lists = [];
		foreach ($users as $user) {
			$lists[$user->ID] = $user->Email;
		}

		$this->blade->view('hash.reset', [
			'users' => $lists
		]);
	}

	public function update()
	{
		if(empty($_POST['password']))
			show_error("Password tidak boleh kosong");

		if($_POST['password'] != $_POST['password_confirmation']) {
			show_error('Password Tidak Sama');
		}

		$this->db->where('ID', $_POST['user_id'])
			->update('Ms_User',[
				'Password' => password_hash($_POST['password'], PASSWORD_DEFAULT)
			]);

		notif('success', 'Password Berhasil di reset');
		redirect('admin/hash');
	}
}
