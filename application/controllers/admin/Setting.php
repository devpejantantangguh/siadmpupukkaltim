<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data = [];
        $rows = $this->db->get('globalsettings')->result();
        foreach ($rows as $row) {
            $data[$row->Setting] = $row->Value;
        }

        $user_roles = $this->db->get('ms_role')->result();
        $roles = [];
        foreach ($user_roles as $role) {
            $roles[$role->ID] = $role->RoleName;
        }

        $user_rows = $this
            ->db
            ->select('ms_user.ID, ms_user.RoleID, ms_user.Email, ms_karyawan.Nama, ms_user.IDKaryawan')
            ->join('ms_karyawan', 'ms_karyawan.ID = ms_user.IDKaryawan')
            ->order_by('ms_karyawan.Nama', 'ASC')
            ->get('ms_user')
            ->result();
        $users = [];
        foreach ($user_rows as $user) {
            $users[$user->ID] = sprintf(
                "%s - %s (%s)",
                ucwords($user->Nama),
                $user->IDKaryawan,
                !empty($user->RoleID) ? $roles[$user->RoleID] : '-'
            );
        }

        $this->blade->view('admin.setting.index', [
            'table' => 'GlobalSetting',
            'data' => $data,
            'users' => $users
        ]);
    }

    public function update()
    {
        foreach ($_POST as $key => $data) {
            $this->db->reset_query();
            $this->db->update('globalsettings', [
                'Value' => $data
            ], [
                'Setting' => $key,
            ]);
        }

        redirect('admin/setting');
    }
}
