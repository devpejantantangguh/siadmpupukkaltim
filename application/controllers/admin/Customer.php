<?php

use Box\Spout\Common\Type;
use Box\Spout\Reader\ReaderFactory;

defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		Auth::guard();

		$this->load->model('admin/M_master', 'm');
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data['customers'] = $this->m->getListCustomer();
		$data['countries'] = $this->m->getCountry();
		$data['provincies'] = $this->m->getPropinsi();
		$data['cities'] = $this->m->getKota();
		$data['vendors'] = $this->m->getListVendor();
		$data['table'] = 'Customer';

		$this->blade->view('admin.master.customer.index', $data);
	}

	public function create()
	{
		$this->blade->view('admin.master.customer.create');
	}

	public function store()
	{
		$rules = [
			'IDCustomer' => 'required',
			'PairedVendorID' => 'required',
			'NPWP' => 'required',
			'Customer' => 'required',
			'IsWAPU' => 'trim',
			'Alamat1' => 'required',
			'Alamat2' => 'trim',
//            'Kota'              => 'required',
//            'Propinsi'          => 'required',
			'Country' => 'required',
//            'Email'             => 'required|valid_email',
//            'Phone'             => 'required',
//            'KodePos'           => 'required',
			'Keterangan' => 'trim',
		];

		if (!isset($_POST['IsWAPU'])) {
			$_POST['IsWAPU'] = 0;
		}

		foreach ($rules as $key => $rule) {
			$this->form_validation->set_rules($key, $key, $rule);
		}

		if ($this->form_validation->run() == false) {
			dd($this->form_validation->error_array());
		} else {
			$simona = $this->load->database('simona', TRUE);
			$simona->where('KODE_VENDOR_SAP', $_POST['PairedVendorID']);

			$rows = $simona->select('KODE_VENDOR_SAP,NAMA')->get('ms_vendor', 1)->result();

			foreach ($rows as $row) {
				$_POST['PairedVendorName'] = $row->NAMA;
			}
			$this->db->insert('Ms_Customer', $_POST);
		}

		redirect('admin/master/customer');
	}

	public function edit($id)
	{
		// $data['vendors'] = $this->m->getListVendor();

		$data['data'] = $this->db->where('IDCustomer', $id)->get('Ms_Customer')->row();

		// collect data country
		$data['country'] = $this->db->where('ID', $data['data']->Country)->get('Ms_Country')->row();
		$data['propinsi'] = $this->db->where('ID', $data['data']->Propinsi)->get('Ms_Propinsi')->row();
		$data['kota'] = $this->db->where('ID', $data['data']->Kota)->get('Ms_Kota')->row();

		if(!empty($data['data']->PairedVendorID)) {
			$data['vendor'] = $this->load->database('simona', TRUE)->like('KODE_VENDOR_SAP', $data['data']->PairedVendorID)->select('KODE_VENDOR_SAP,NAMA')->get('ms_vendor', 1)->row();
		} else {
			$data['vendor'] = new StdClass;
			$data['vendor']->KODE_VENDOR_SAP = NULL;
			$data['vendor']->NAMA = '';
		}

		$data['id'] = $id;

		$this->blade->view('admin.master.customer.edit', $data);
	}

	public function update($id)
	{
		$rules = [
			'IDCustomer' => 'required',
			'PairedVendorID' => 'trim',
			'NPWP' => 'required',
			'Customer' => 'required',
			'IsWAPU' => 'trim',
			'Alamat1' => 'required',
			'Alamat2' => 'trim',
//            'Kota'              => 'required',
//            'Propinsi'          => 'required',
			'Country' => 'required',
//            'Email'             => 'required|valid_email',
//            'Phone'             => 'required',
//            'KodePos'           => 'required',
			'Keterangan' => 'trim',
		];

		if (!isset($_POST['IsWAPU'])) {
			$_POST['IsWAPU'] = 0;
		}

		foreach ($rules as $key => $rule) {
			$this->form_validation->set_rules($key, $key, $rule);
		}

		if ($this->form_validation->run() == false) {
			dd($this->form_validation->error_array());
		} else {
			$simona = $this->load->database('simona', TRUE);
			$simona->where('KODE_VENDOR_SAP', $_POST['PairedVendorID']);

			$rows = $simona->select('KODE_VENDOR_SAP,NAMA')->get('ms_vendor', 1)->result();

			foreach ($rows as $row) {
				$_POST['PairedVendorName'] = $row->NAMA;
			}
			$this->db->where('IDCustomer', $id)->update('Ms_Customer', $_POST);
		}

		redirect('admin/master/customer');
	}

	public function destroy()
	{
		$ids = explode(',', $_GET['ids']);
		$this->db->where_in('IDCustomer', $ids)->delete('Ms_Customer');

		redirect('admin/master/customer');
	}

	// function kota

	public function kota()
	{
		$parent = isset($_GET['p']) ? $_GET['p'] : '';

		$results = $this->db->where('IDPropinsi', $parent)->get('Ms_Kota')->result();

		$lists = [];
		foreach ($results as $result) {
			$lists[] = array('id' => $result->ID, 'text' => $result->Kota);
		}

		header('Content-Type: application/json');
		echo json_encode($lists);
	}

	public function propinsi()
	{
		$results = $this->db->get('Ms_Propinsi')->result();

		$lists = [];
		foreach ($results as $result) {
			$lists[] = array('id' => $result->ID, 'text' => $result->Propinsi);
		}

		header('Content-Type: application/json');
		echo json_encode($lists);
	}

	public function country()
	{
		$results = $this->db->get('Ms_Country')->result();

		$lists = [];
		foreach ($results as $result) {
			$lists[] = array('id' => $result->ID, 'text' => $result->Country);
		}

		header('Content-Type: application/json');
		echo json_encode($lists);
	}

	public function vendor()
	{
		$search = isset($_GET['q']) ? $_GET['q'] : '';

		$simona = $this->load->database('simona', TRUE);
		if (!empty($search)) {
			$simona->like('KODE_VENDOR_SAP', $search)->or_like('NAMA', $search);
		}

		$rows = $simona->select('KODE_VENDOR_SAP,NAMA')->get('ms_vendor', 50)->result();

		$lists = [];
		foreach ($rows as $row) {
			$lists[] = ['ID' => $row->KODE_VENDOR_SAP, 'Name' => "$row->KODE_VENDOR_SAP - $row->NAMA"];
		}

		echo json_encode($lists);
	}

	public function customer()
	{
		$search = isset($_GET['q']) ? $_GET['q'] : '';

		if (!empty($search)) {
			$this->db->like('IDCustomer', $search)->or_like('LOWER(Customer)', strtolower($search));
		}

		$rows = $this->db->select('IDCustomer,Customer')->get('Ms_Customer')->result();

		$lists = [];
		foreach ($rows as $row) {
			$lists[] = ['ID' => $row->IDCustomer, 'Name' => "$row->IDCustomer - $row->Customer"];
		}
		echo json_encode($lists);
	}

	public function upload()
	{
		$this->blade->view('admin.master.customer.upload');
	}

	public function upload_data()
	{
		$config['upload_path'] = './storage/uploads/';
		$config['allowed_types'] = 'xls|xlsx|csv';
		$config['max_size'] = 10 * 1024;
		$config['overwrite'] = true;
		$config['max_filename'] = 255;
		$config['encrypt_name'] = true;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('file')) {
			show_error($this->upload->display_errors());
		} else {
			$path = $this->upload->data('full_path');

			$this->readExcel($path);

			redirect('admin/master/customer');
		}
	}

	private function readExcel($path)
	{
		try{
			set_time_limit(60);

			$reader = ReaderFactory::create(Type::XLSX);
            $reader->open($path);

            foreach ($reader->getSheetIterator() as $key => $sheet) {
            	if($key != 1) break;

            	foreach($sheet->getRowIterator() as $i => $row) {
            		if($i == 1) continue;

            		$customer_id = $row[0];
            		if( ! $this->isExist($customer_id)) {
            			$this->createNewCustomer($row);
					}
				}
			}
		} catch (Exception $e) {
			show_error($e->getMessage());
		}
	}

	private function isExist($customer_id)
	{
		return $this->db->where('IDCustomer', (string) $customer_id)->get('Ms_Customer')->num_rows() > 0;
	}

	private function createNewCustomer($row)
	{
		$user_id = Auth::user()->ID;
		$this->db->insert('Ms_Customer',[
			'IDCustomer' => $row[0],
			'PairedVendorID' => $row[1],
			'NPWP' => $row[2],
			'Title' => $row[3],
			'Customer' => $row[4],
			'IsWAPU' => $row[5] == 'Y' ? 1 : 0,
			'Alamat1' => $row[6],
			'Alamat2' => $row[7],
			'Kota' => $row[8],
			'Propinsi' => $row[9],
			'Country' => $row[10],
			'Email' => $row[11],
			'Phone' => $row[12],
			'Kodepos' => $row[13],
			'Keterangan' => $row[14],
			'CreatedBy' => $user_id,
			'CreatedOn' => now(),
		]);
	}
}
