<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;

class News extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        Auth::guard();
    }

    public function index()
    {
        $this->blade->view('admin.news.index',array(
            'table'     => 'News'
        ));
    }

    public function get()
    {
        $limit = isset($_GET['take']) ? $_GET['take'] : 10;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;

        if(isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        } else {
            $this->db->order_by('ID', 'DESC');
        }

        $items = $this->db->get('News', $limit, $offset)->result();
        $count = $this->db->count_all('Pesan');

        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $items
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function create()
    {
        echo $this->blade->view('admin.news.create');
    }

    public function import()
    {
        echo $this->blade->view('admin.news.import');
    }

    public function upload()
    {
        $config['upload_path']          = './storage/news/';
        $config['allowed_types']        = 'xls|xlsx|csv'; // xlsm|xltx|xltm
        $config['max_size']             = 10 * 1024;
        $config['overwrite']            = true;
        $config['max_filename']         = 255;
        // $config['encrypt_name']         = true;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file')) {
            show_error($this->upload->display_errors());
        } else {
            $file = $this->upload->data('full_path');
            
            $spreadsheet = IOFactory::load($file);
            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

            foreach($sheetData as $key => $row) {
                if($key < 2) continue;

                $data = [];
                $data['IDRekanan'] = $row['A'];
                $data['IsCustomer'] = $row['B'];
                $data['IsVendor'] = $row['C'];
                $data['Judul'] = $row['D'];
                $data['Pesan'] = $row['E'];
                $data['StartDate'] = $row['F'];
                $data['EndDate'] = $row['G'];
                $data['Keterangan'] = $row['H'];
                $data['IsBoardcast'] = $row['I'];
                $data['CreatedBy'] = Auth::user()->ID;
                $data['CreatedOn'] = date('Y-m-d H:i:s');

                $this->db->insert('Pesan', $data);
            }
            
            redirect('admin/message');
        }
    }

    public function getUser()
    {
        $type = $this->input->post('type');

        if($type == 'Vendor') {
            echo $this->_getVendors();
        } else {
            echo $this->_getCustomers();
        }
    }

    public function store()
    {

        $params = [];
        $params['Judul'] = $this->input->post('Judul');
        $params['Berita'] = $this->input->post('News');
        $params['EndDate'] = $this->input->post('EndDate');
        $params['StartDate'] = date('Y-m-d');
        $params['CreatedOn'] = date('Y-m-d H:i:s');
        $params['CreatedBy'] = '1';//Auth::user()->id;

        $config['upload_path']          = './storage/news/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 10000;
        $config['overwrite']            = true;
        $config['max_filename']         = 255;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file'))
        {
            die($this->upload->display_errors());
        }
        $file_name = $this->upload->data('file_name');
        $params['ImageURL'] = base_url('storage/news/').$this->upload->data('file_name');

        $this->db->insert('News', $params);

        redirect('admin/news');
    }

    public function edit($id)
    {
        $row = $this->db->where('ID', $id)->get('News')->row();
        echo $this->blade->view('admin.news.edit', compact('row'));
    }

    public function update($id)
    {
        //$data = $this->input->post();
        $params = [];
        $params['Judul'] = $this->input->post('Judul');
        $params['Berita'] = $this->input->post('News');
        $params['EndDate'] = $this->input->post('EndDate');
        $params['CreatedBy'] = '1';//Auth::user()->id;
        $config['upload_path']          = './storage/news/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 10000;
        $config['overwrite']            = true;
        $config['max_filename']         = 255;

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('file'))
        {
            die($this->upload->display_errors());
        }
        $params['ImageURL'] = base_url('storage/news/').$this->upload->data('file_name');

        $this->db->where('ID', $id)->update('News', $params);
        redirect('admin/news');
    }

    public function destroy($id)
    {
        $this->db->where('ID',$id)->delete('News');
        redirect('admin/news');
    }

    public function dummy()
    {
        $faker = Faker\Factory::create();

        foreach(range(1,100) as $index) {
            $params = [];
            $params['ID'] = $index;
            $params['IDRekanan'] = $faker->randomElement([1,2,3,4,5,6,7,8,9]);
            $params['Judul'] = $faker->words(4);
            $params['Pesan'] = $faker->sentence(6);
            $params['Keterangan'] = $faker->words(2);
            $params['Boardcast'] = 0;
            $params['CreatedOn'] = date('Y-m-d H:i:s');
            $params['CreatedBy'] = 1; // Auth::user()->id

            $this->db->insert('Pesan', $params);
        }

        echo "Dummy Pesan Done";
    }

    private function _getCustomers()
    {
        $users = $this->db->get('Ms_Customer')->result();

        $lists = [];
        foreach($users as $user) {
            $lists[] = [
                'id'    => $user->IDCustomer,
                'name'  => $user->Customer
            ];
        }

        return json_encode($lists);
    }

    private function _getVendors()
    {
        $users = $this->db->get('Ms_Vendor')->result();

        $lists = [];
        foreach($users as $user) {
            $lists[] = [
                'id'    => $user->IDVendor,
                'name'  => $user->Vendor
            ];
        }

        return json_encode($lists);
    }
}
