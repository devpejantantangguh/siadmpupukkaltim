<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 *
 */
class Users extends CI_Controller
{

    private $rules = [
        'Email'     => 'required|is_unique[Ms_User.Email]',
    ];

    function __construct()
    {
        parent::__construct();
        Auth::guard();

        $this->load->model('admin/M_master','m');
        $this->load->library('form_validation');
    }

    public function index()
    {

        $data['user_roles'] = $this->m->getUserRole();
        $data['user_types'] = $this->m->getUserType();
        $data['table'] = 'Users';
        $this->blade->view('admin.master.users.index',$data);
    }

    public function create($type = "website")
    {
        if($type == "website"){

            $data['roles'] = $this->m->getUserRole();

            $this->blade->view('admin.master.users.createwebsite', $data);
        }else{
            $data['roles'] = $this->m->getUserRole();
            $data['types'] = $this->m->getUserType();

            $this->blade->view('admin.master.users.create', $data);
        }
    }

    public function store($type = "website")
    {
        if($type == "website"){
            foreach($this->rules as $key => $rule) {
                $this->form_validation->set_rules($key, $key, $rule);
            }

            if($this->form_validation->run() == false) {
                $errors = [];
                foreach($this->form_validation->error_array() as $error) {
                    $errors[] = $error;
                }
                redirect('admin/master/users/create?message='.implode("<br>",$errors));
            } else {

                $simona = $this->load->database('simona', TRUE); 
                $simona->where('KODE_VENDOR_SAP', $_POST['IDVendor']);

                $rows = $simona->select('KODE_VENDOR_SAP,NAMA')->get('ms_vendor',1)->result();    

                foreach($rows as $row) {
                    $_POST['VendorName'] = $row->NAMA; 
                }
                $_POST['Password'] = password_hash($_POST['Password'], PASSWORD_DEFAULT);
                $this->db->insert('Ms_User', $_POST);
                redirect('admin/master/users');
            }
        }else{
            foreach($this->rules as $key => $rule) {
                $this->form_validation->set_rules($key, $key, $rule);
            }

            if($this->form_validation->run() == false) {
                $errors = [];
                foreach($this->form_validation->error_array() as $error) {
                    $errors[] = $error;
                }
                redirect('admin/master/users/create/apps?message='.implode("<br>",$errors));
            } else {
            	$_POST['Password'] = password_hash($_POST['Password'], PASSWORD_DEFAULT);
                $this->db->insert('Ms_User', $_POST);
                redirect('admin/master/users');
            }
        }
    }

    public function edit($id, $type = "website")
    {
        if($type == 'website'){
            $this->db->select("Ms_User.*,Ms_Karyawan.Nama,Ms_UnitKerja.UnitKerja");

            $this->db->join("Ms_Karyawan","Ms_Karyawan.ID = Ms_User.IDKaryawan","LEFT");

            $this->db->join("Ms_UnitKerja","Ms_UnitKerja.CostCenter = Ms_Karyawan.CostCenter","LEFT");
            $user = $this->db->where('Ms_User.ID',$id)->get('Ms_User')->row();
            $data['user'] = $user;
            $data['roles'] = $this->m->getUserRole();
            $data['types'] = $this->m->getUserType();
            $data['table'] = 'Users';
            $role = array_search($user->RoleID, array_column($data['roles'], 'Ms_User.ID'));
            $data['role'] = $data['roles'][$role];

            $type = array_search($user->Type, array_column($data['types'], 'Ms_User.ID'));
            $data['type'] = $data['types'][$type];


            $this->blade->view('admin.master.users.editwebsite', $data);
        }else{

            $this->db->select("Ms_User.*,Ms_Customer.IDCustomer,Ms_Customer.Customer");
            $this->db->join("Ms_Customer","Ms_Customer.IDCustomer = Ms_User.IDCustomer","LEFT");
            $user = $this->db->where('Ms_User.ID',$id)->get('Ms_User')->row();
            $data['user'] = $user;
            $data['table'] = 'Users';

            $this->blade->view('admin.master.users.edit', $data);
        }
    }

    public function update($id, $type = "website")
    {
        if($type == "website"){
            $rules = [
                'Email'     => 'required|valid_email'
            ];

            foreach($rules as $key => $rule) {
                $this->form_validation->set_rules($key, $key, $rule);
            }

            if($this->form_validation->run() == false) {
                $errors = [];
                foreach($this->form_validation->error_array() as $error) {
                    $errors[] = $error;
                }
                redirect('admin/master/users/edit/'.$id.'?message='.implode("<br>",$errors));
            } else {
                if(empty($_POST['Password'])){
                    unset($_POST['Password']);
                } else {
                	$_POST['Password'] = password_hash($_POST['Password'], PASSWORD_DEFAULT);
				}

                $_POST['IsLDAP'] = isset($_POST['IsLDAP']) ? 1 : 0;
                $this->db->where('ID', $id)->update('Ms_User', $_POST);
                redirect('admin/master/users');
            }

        }else{
            $rules = [
                'Email'     => 'required'
            ];

            foreach($rules as $key => $rule) {
                $this->form_validation->set_rules($key, $key, $rule);
            }

            if($this->form_validation->run() == false) {
                $errors = [];
                foreach($this->form_validation->error_array() as $error) {
                    $errors[] = $error;
                }
                redirect('admin/master/users/edit/'.$id.'/apps?message='.implode("<br>",$errors));
            } else {
                if(empty($_POST['Password'])){
                    unset($_POST['Password']);
                } else {
                	$_POST['Password'] = password_hash($_POST['Password'], PASSWORD_DEFAULT);
				}


                $simona = $this->load->database('simona', TRUE); 
                $simona->where('KODE_VENDOR_SAP', $_POST['IDVendor']);

                $rows = $simona->select('KODE_VENDOR_SAP,NAMA')->get('ms_vendor',1)->result();    

                foreach($rows as $row) {
                    $_POST['VendorName'] = $row->NAMA; 
                }
                $this->db->where('ID', $id)->update('Ms_User', $_POST);
                redirect('admin/master/users');
            }

        }
    }

    public function destroy($id)
    {
        $this->db->where('ID',$id)->delete('Ms_User');
        redirect('admin/master/users');
    }

    public function get_karyawan()
    {
        $search = isset($_GET['q']) ? $_GET['q'] : '';

        if(!empty($search)) {
            $this->db->like('ID', $search)->or_like('LOWER(Nama)', strtolower($search));
        }
        $this->db->join('Ms_UnitKerja','Ms_UnitKerja.CostCenter = Ms_Karyawan.CostCenter','LEFT');
        $rows = $this->db->select('Ms_Karyawan.ID,Ms_Karyawan.Nama,Ms_UnitKerja.UnitKerja')->get('Ms_Karyawan')->result();

        $lists = [];
        foreach($rows as $row) {
            $lists[] = ['ID' => $row->ID,'Name' => "$row->ID - $row->Nama - $row->UnitKerja"];
        }
        header('Content-Type: application/json');
        echo json_encode($lists);
    }
}
