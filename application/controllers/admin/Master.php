<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        Auth::guard();

        $this->load->model('admin/M_master','m');
    }

    public function index($slug)
    {
        $views['country']   = 'admin.master.country';
        $views['propinsi']  = 'admin.master.propinsi';
        $views['kota']      = 'admin.master.kota';
        $views['material']  = 'admin.master.material';
        $views['customer']  = 'admin.master.customer';
        $views['vendor']    = 'admin.master.vendor';
        $views['karyawan']  = 'admin.master.karyawan';
        $views['unitkerja'] = 'admin.master.unitkerja';
        $views['users'] = 'admin.master.users';
        
        if(!isset($views[$slug])){
            show_404();
            die();
        }
        $tableName = explode('.', $views[$slug]);
        $tableName = ucwords(end($tableName));

        $data = [];
        $data['table'] = $tableName;

        if(strtolower($slug) == 'users') {
            $data['user_roles'] = $this->m->getUserRole();
            $data['user_types'] = $this->m->getUserType();
            $data['customers'] = $this->m->getListCustomer();
            // $data['vendors'] = $this->m->getListVendor();
        } else if (strtolower($slug) == 'vendor') {
            $data['countries'] = $this->m->getCountry();
            $data['provincies'] = $this->m->getPropinsi();
            $data['cities'] = $this->m->getKota();
        } else if (strtolower($slug) == 'customer') {
            $data['countries'] = $this->m->getCountry();
            $data['provincies'] = $this->m->getPropinsi();
            $data['cities'] = $this->m->getKota();
            $data['vendors'] = $this->m->getListVendor();
        } elseif(strtolower($slug) == 'karyawan') {
            $data['cost_center'] = $this->m->getCostCenter();
            $data['cost_center2'] = $this->m->getCostCenter2();
        } elseif(strtolower($slug) == 'unitkerja') {
            $data['list_karyawan'] = $this->m->getKaryawan();
        } 

        $this->blade->view($views[$slug],$data);
    }

    public function list_vendor()
    {
        $search = isset($_GET['filter'][2]) ? $_GET['filter'][2] : '';

        $simona = $this->load->database('simona', TRUE);  
        if(!empty($search)) {
            $simona->like('KODE_VENDOR_SAP', $search)->or_like('NAMA', $search);
        }

        $rows = $simona->select('KODE_VENDOR_SAP,NAMA')->get('ms_vendor')->result();    

        $lists = [];
        foreach($rows as $row) {
            $lists[] = ['ID' => $row->KODE_VENDOR_SAP,'Name' => "$row->KODE_VENDOR_SAP - $row->NAMA"];
        }

        echo json_encode($lists);
    }
    public function get($type = 'website')
    {
        $search = isset($_GET['filter']) ? json_decode($_GET['filter']) : '';
        $limit = isset($_GET['take']) ? $_GET['take'] : 10;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;
        if(!empty($search)) {
            Filter::dxDataGridFilters($search, $this->db);
        }

        if(isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        } else {
            $this->db->order_by('i.ID', 'DESC');
        }

        if($type == 'website'){

            $tempdb = clone $this->db; //clone for count all rows
            $count= $tempdb->from('Vw_UserWeb i')->count_all_results();
            $items = $this->db
                        ->get('Vw_UserWeb i', $limit, $offset)->result();
        }else{

            $tempdb = clone $this->db; //clone for count all rows
            $count= $tempdb->from('Vw_UserApps i')->count_all_results();
            $items = $this->db
                        ->get('Vw_UserApps i', $limit, $offset)->result();
        }

        foreach ($items as $item) {
        }
        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $items
        )));


        header('Content-Type: application/json');
        echo json_encode($data);
    }
}
