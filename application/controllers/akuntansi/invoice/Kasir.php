<?php

class Kasir extends CI_Controller
{

    public $updater_role = 1010; //Kasir
    function __construct()
    {
        parent::__construct();
        Auth::guard();

        $this->load->model('m_kasir');
        $this->load->model('m_po');
        $this->load->model('m_vl');
        $this->load->model('m_invoice');
    }

    public function index()
    {
        if($this->input->get('tab')){
            notif('tab','wum');
        }
        echo $this->blade->view('akuntansi/invoice_tracking/kasir/index',[
            'list_status'   => $this->m_vl->get_status(1)
        ]);
    }

    public function get_kasir($status = null, $optional = null)
    {
        
        if($optional == 'po'){
            $lists = $this->db->where('Kategori',1)->where('Keterangan',NULL)->get('VL_JenisTransaksi')->result();
            $jt = [];
            foreach($lists as $list) {
                $jt[] = $list->ID;
            }
            $this->db->where_in('JenisTransaksi', $jt);
        }
        $this->db->select('i.*');
        $area = Auth::unitKerja()->Area;
        $this->db->where('us.Area',$area)
                ->join('Ms_User us','us.ID = i.CreatedBy');

        if(strpos($status,'-') !== false) {
            $xstatus = explode('-', $status);
            $this->db->where_in('Status', $xstatus);
        }elseif(empty($status)) {

        }else {
            $this->db->where('Status', $status);
        }
        /* StatusTransmittal != TRUE */
        $this->db->group_start();
        $this->db->where('NoTransmittal', '');
        $this->db->or_where('NoTransmittal', null);
        $this->db->group_end();
        $search = isset($_GET['filter']) ? json_decode($_GET['filter']) : '';
        $limit = isset($_GET['take']) ? $_GET['take'] : 10;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;
        if(!empty($search)) {
            Filter::dxDataGridFilters($search, $this->db);
        }
        
        Filter::areaFilter('i', $this->db);

        if(isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        } else {
            $this->db->order_by('i.ID', 'DESC');
        }

        $this->db->select('i.*');

        $tempdb = clone $this->db; //clone for count all rows
        $count= $tempdb->from('Vw_AllInvoice i')->count_all_results();
        $items = $this->db->get('Vw_AllInvoice i', $limit, $offset)->result();
        foreach ($items as $item) {
            $item->ID = encode_url($item->ID);
            //$key->ID = $this->encrypt->encode($key->ID);
        }
        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $items
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }
     public function get($id_vendor = null)
    {
        if($id_vendor == null){
            header('Content-Type: application/json');
            echo json_encode(['totalCount' => 0, 'data' => []]);
        } else {
            $this->m_po->get_invoice_po($id_vendor);
        }

    }

    public function edit($id)
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            notif('error', 'Anda tidak berhak mengubah data.');
            redirect('akuntansi/invoice/kasir/');
            die();
        }
        $encode_id = $id;
        $id = decode_url($id);
        $data = $this->m_po->get_detail_invoice($id);
        if(!$data){
            show_404();
            die();
        }
        $this->blade->view('akuntansi.invoice_tracking.kasir.edit', [
            'id'              => $id,
            'data'              => $data,
            'list_transaksi'    => $this->m_vl->get_jenis_transaksi('po'),
            'documents'         => $this->m_po->get_kelengkapan_dokumen($id),
            'current_note'      => $this->m_po->get_current_note($id),
            'table'             => 'Invoice',
            'list_kurs'         => $this->m_vl->get_kurs(),
            'attachments'       => $this->db->where('IDParent', $id)->get('Attachment')->result(),
            'histories'         => $this->m_invoice->getHistory($id),
            'term' => $this->m_vl->get_PaymentTerm()
        ]);
    }

    public function update($id)
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            notif('error', 'Anda tidak berhak mengubah data.');
            redirect('akuntansi/kasir/');
            die();
        }
        $this->db->trans_start();

        $status = $this->db->where('ID',$id)->get('Invoice')->row()->Status;
        if($status == 3 || $status == 4 || $status == 11)
        {
            // history
            $Keterangan = $this->input->post('note');
            //$this->m_invoice->createHistory($id,4,$Keterangan);
            $this->m_invoice->newHistory($id,4,statusID('CHECKED'),$Keterangan);

            // matriks
            if(isset($_POST['matriks'])) {
                $this->m_po->updateMatriks($id, $_POST['matriks']);
            }
            $this->db->where('ID',$id)->update('Invoice',array('Status' => 4));
            $this->m_po->update_priority($id);

            /* update: payment term */
            $this->db->where('ID', $id)->update('Invoice',[
                'PaymentTerm'   => $this->input->post('PaymentTerm')
            ]);

            $submit = $this->input->post('submit');

            $this->db->trans_complete();

            notif('success', 'Invoice selesai diperiksa.');
        }else{
            $submit = 'fail';

            notif('error', 'Data gagal diubah.');
        }
        redirect("akuntansi/invoice/kasir?id=".encode_url($id)."&update={$submit}");
    }

    public function destroy($id)
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            notif('error', 'Anda tidak berhak mengubah data.');
            redirect('akuntansi/kasir/');
            die();
        }
        $this->db->trans_start();

        $this->db->where('ID',$id)->where_in('Status',array(3,11))->update('Invoice', ['Status' => 1]);
        $isSuccess = $this->db->affected_rows();

        if($isSuccess > 0) {
            $Keterangan = $this->input->post('Keterangan');
            //$this->m_invoice->createHistory($id, 9, $Keterangan);
            $this->m_invoice->newHistory($id, 9,statusID('DRAFT'), $Keterangan);
            $this->db->trans_complete();
            


            $from_email = "no-reply@mitraku.pupukkaltim.com"; 
            /*Send Email when reject*/
            $invoice = $this->m_invoice->getDetail($id);
             //$to_email = "sony.candra@pupukkaltim.com"; 
            $to_email = $this->m_invoice->getEmailCreator($id);
             $data = array(
                'NamaVendor' => $invoice->NamaVendor,
                'NoInvoice' => $invoice->NoInvoice,
                'NilaiInvoice' => 'Rp. '.number_format($invoice->NilaiInvoice,2,',','.'),
                'Keterangan' => $Keterangan,             );
            $mesg = $this->load->view('templates/email',$data, true);
             //Load email library 
             $this->load->library('email'); 
       
             $this->email->from($from_email, 'Mitraku'); 
             $this->email->to($to_email);
             $this->email->subject('Revisi pengajuan pembayaran Invoice No. '.$invoice->NoInvoice); 
             $this->email->message($mesg); 
       
             //Send mail 
             if($this->email->send());
            notif('success', 'Data berhasil direject.');
            redirect('akuntansi/invoice/kasir?status=success');
        } else {
            $this->db->trans_complete();
            notif('error', 'Data gagal direject.');
            redirect('akuntansi/invoice/kasir?status=error&m=has_checked');
        }
    }

    public function transmittal()
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            echo json_encode(array('status' => 404, 'msg' => 'Anda tidak berhak mengubah data.'));
            die();
        }
        $ids = $this->input->post('ids');
        $ids = array_unique($ids);
        $new_ids = [];
        //filter only checked
        foreach ($ids as $key => $id) {
            if(!(int)$id){
                $ids[$key] = decode_url($id);
                $encode_id = $id;
                $id = decode_url($id);

            }
            $status = $this->db->where('ID',$id)->get('Invoice')->row()->Status;
            if($status == 4) //selain checked gagal
            {
                array_push($new_ids, $id);
            }
        }
        if(!empty($new_ids))
        {
            $number= $this->m_po->transmittal($new_ids);
            $id = encode_url($number);
            echo json_encode(array('status' => 200, 'number' => $id,'msg'=>'Transmittal Created ['.$number.']'));
        }else{
            echo json_encode(array('status' => 404, 'msg' => 'Gagal: Dokumen harus checked.'));
        }
    }

    public function pdf_transmittal($id)
    {
        $id = decode_url($id);
        $query = $this->db->like('a.NoTransmittal', $id,'after')
                ->get('Invoice as a');
        $num = $query->num_rows();
        if($num > 0) {
            $row = $query->row_array();
            $data = $query->result();
            $row['NoTransmittal'] = $id;
            $row['Num'] = $num;
            $row['Nama'] = $this->get_karyawan_by_id(Auth::user()->ID);
            $views = $this->blade->render('akuntansi/invoice_tracking/pdf/kasir',[
                'data'  => $data,
                'row'   => $row
            ]
            );

            //echo $views;die();

            header("Content-type:application/pdf");
            // instantiate and use the dompdf class
            $dompdf = new Dompdf\Dompdf();
            //print_r($dompdf);
            $dompdf->set_option('isRemoteEnabled', TRUE);
            $dompdf->loadHtml($views);
            $dompdf->setPaper('A4', 'potrait');
            $dompdf->render();
            return $dompdf->stream('invoice.pdf',['Attachment' => 0]);
        }

    }
    public function pdf_transmittal_wum($id)
    {
        $id = decode_url($id);
        $query = $this->db->like('a.NoTransmittal', $id,'after')
                ->get('Invoice as a');
        $num = $query->num_rows();
        if($num > 0) {
            $row = $query->row_array();
            $data = $query->result();
            $row['NoTransmittal'] = $id;
            $row['Num'] = $num;
            $row['Nama'] = $this->get_karyawan_by_id(Auth::user()->ID);
            $views = $this->blade->render('akuntansi/invoice_tracking/pdf/wum',[
                'data'  => $data,
                'row'   => $row
            ]
            );

            //echo $views;die();

            header("Content-type:application/pdf");
            // instantiate and use the dompdf class
            $dompdf = new Dompdf\Dompdf();
             $dompdf->set_option('isRemoteEnabled', TRUE);
            $dompdf->loadHtml($views);
            $dompdf->setPaper('A4', 'potrait');
            $dompdf->render();
            return $dompdf->stream('invoice.pdf',['Attachment' => 0]);
        }

    }

    private function get_karyawan_by_id($id){
        $this->db->where('Ms_User.ID', $id);
        $this->db->join('Ms_User','Ms_User.IDKaryawan = Ms_Karyawan.ID');
        $rows = $this->db->select('Ms_Karyawan.Nama')->get('Ms_Karyawan')->row();
        if($rows){

            return $rows->Nama;
        }
        return '';

    }
}
