<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Kurs extends CI_Controller 
{
    
    function __construct()
    {
        parent::__construct();
        Auth::guard();
    }

    public function index()
    {

        
        $data['Currency'] = $this->_getCurrency();
        echo $this->blade->view('akuntansi/kurs/index',$data);
    }
    public function update()
    {
        if(isset($_POST['tanggal'])){
            //print_r($_POST['kurs']);
            list($d,$m,$y) = explode('-', $this->input->post('tanggal'));
            $tanggal = date('Y-m-d', mktime(0,0,0,$m,$d,$y));
            $kurs = $_POST['kurs'];

            foreach ($kurs as $key => $value) {
                $this->db->where('Currency',$key);
                $count = $this->db->where(['Tanggal'=>$tanggal])->get("KursValas")->num_rows();

            if($value>0){
                list($d,$m,$y) = explode('-', $this->input->post('tanggal'));
                $tanggal = date('Y-m-d', mktime(0,0,0,$m,$d,$y));
                $data = array(
                    'Tanggal'=>$tanggal,
                    'Currency'=>$key,
                    'Kurs'=>$value,
                    'CreatedBy'=>Auth::user()->ID,
                    'CreatedOn'=>date("Y-m-d h:i:s")
                );
                $this->db->where('Tanggal',$tanggal);
                $this->db->where('Currency',$key);
                $num = $this->db->get('KursValas')->num_rows();
                if($num==1){
                    $this->db->where('Tanggal',$tanggal);
                    $this->db->where('Currency',$key);
                    $this->db->update('KursValas', $data);
                }elseif($num==0){
                    $this->db->insert('KursValas', $data);
                }

                 
            }

            }
             
        }
    	redirect('akuntansi/invoice/kurs');
    }
    public function get()
    {
    	/*print_r(substr($_GET['$filter'],0,3));
    	die();*/
    	$id = substr($_GET['$filter'],0,3);
    	$this->db->where('Ms_Kurs.Currency',$id);
        $this->db->limit('30');
    	$this->db->order_by('KursValas.Tanggal','ASC');
    	$this->db->join('Ms_Kurs','Ms_Kurs.ID = KursValas.Currency');
    	$query = $this->db->get('KursValas');
        if( $query->num_rows() > 0 ) {
            foreach($query->result_array() as $row) {
                $tanggal = date('M d', strtotime($row['Tanggal']));
                $result[] = [
                    'arg'    => $tanggal,
                    'val'  => $row['Kurs']
                ];    
            }
        } 
        echo '{"odata.metadata":"","value":[{"DayItems":';
        echo json_encode($result);
   		echo '}]}';
    }
    private function _getCurrency()
    {
        $today = date('Y-m-d');
    	$yesterday = date('Y-m-d',strtotime("-1 days"));
    	//$this->db->limit('1');
    	//$this->db->join('KursValas','KursValas.Currency = Ms_Kurs.ID');
        $rows = $this->db->get('Ms_Kurs')->result();

        $Currency = [];
        foreach($rows as $row) {
        	$this->db->order_by('Tanggal','DESC');
        	$this->db->limit('2');
            $this->db->where_in('Tanggal', array($today,$yesterday));
        	$this->db->where('Currency', $row->ID);
        	$kurs = $this->db->get('KursValas')->result();
            $KursToday = 0;
        	$TanggalToday = 0;
            $CreatedOnToday = 0;
            $KursYesterday = 0;
            $CreatedOnYesterday = 0;
        	$TanggalYesterday = 0;
        	$tanggal = '';
        	$i = 1;
        	foreach ($kurs as $value) {

        		if($value->Tanggal == $today){
        			$KursToday = $value->Kurs;
                    $TanggalToday = $value->Tanggal;
        			$CreatedOnToday = $value->CreatedOn;
        		}
        		if($value->Tanggal == $yesterday){
        			$KursYesterday = $value->Kurs;
                    $CreatedOnYesterday = $value->CreatedOn;
                    $TanggalYesterday = $value->Tanggal;
        		}
        		$i++;
        	}
            $Currency[] = array(
                'ID'    => $row->ID,
                'Currency'    => $row->Currency,
                'Image'  => $row->Image,
                'KursToday' => $KursToday,
                'TanggalToday' => $TanggalToday,
                'CreatedOnToday' => $CreatedOnToday,
                'KursYesterday' => $KursYesterday,
                'TanggalYesterday' => $TanggalYesterday,
                'CreatedOnYesterday' => $CreatedOnYesterday
            );
        }
        return $Currency;
    }
    public function ajax()
    {
        header('Content-Type: application/json');
        list($d,$m,$y) = explode('-', $this->input->post('tanggal'));
        $tanggal = date('Y-m-d', mktime(0,0,0,$m,$d,$y));
        $query = $this->db->where('Tanggal', $tanggal)->get('KursValas')->result_array();
        echo json_encode($query);
    }
}
