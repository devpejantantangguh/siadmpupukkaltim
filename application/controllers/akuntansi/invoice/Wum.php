<?php

class Wum extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        Auth::guard();
        $this->load->model('m_wum');
        $this->load->model('m_vl');
        $this->load->model('m_invoice');
    }

    public function get()
    {
        $transactions = $this->db
                        ->where('Keterangan','WUM')
                        ->where('Kategori',1)
                        ->get('VL_JenisTransaksi')
                        ->result();

        $lists = [];
        foreach($transactions as $trans) {
            $lists[] = $trans->ID;
        }
        $this->db->select('Invoice.*');
        $area = Auth::unitKerja()->Area;
        $this->db->where('us.Area',$area)
                ->join('Ms_User us','us.ID = Invoice.CreatedBy');
        $search = isset($_GET['filter']) ? json_decode($_GET['filter']) : '';
        $limit = isset($_GET['take']) ? $_GET['take'] : 10;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;
        if(!empty($search)) {
            Filter::dxDataGridFilters($search, $this->db);
        }

        if(isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        } else {
            $this->db->order_by('Invoice.ID', 'DESC');
        }

        $this->db->where('NoTransmittal',null);
        $tempdb = clone $this->db; //clone for count all rows
        $count= $tempdb->where_in('JenisTransaksi', $lists)->from('Invoice')->count_all_results();
        $query = $this->db->where_in('JenisTransaksi', $lists)->get('Invoice', $limit, $offset);
        $items = $query->result();

        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $items
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function create()
    {
        $this->blade->view('akuntansi.invoice_tracking.wum.create',[
            'workers'       => $this->m_wum->get_list_karyawan(),
            'payment_terms' => $this->m_wum->get_payment_term(),
            'ls_transaksi'  => $this->m_vl->get_jenis_transaksi('wum'),
            'ls_kurs'       => $this->m_vl->get_kurs(),

        ]);
    }

    public function store()
    {
        $this->db->trans_start();

        $documents = isset($_POST['documents']) ? $_POST['documents'] : [];
        $sdoc = serialize($documents);

        $pay_term = $this->input->post('PaymentTerm');
        $addDay = $this->db->where('PaymentTerm', $pay_term)->get('VL_PaymentTerm')->row('JumlahHari');
        $date = date('d M Y', strtotime("+{$addDay} day"));

        $data = $_POST;
        $data['NilaiInvoice'] = str_replace(',','.',str_replace('.','', $data['NilaiInvoice']));
        $data['CreatedOn'] = date('Y-m-d H:i:s');
        $data['CreatedBy'] = Auth::user()->ID;
        unset($data['documents']);
        unset($data['keterangan']);
        unset($data['note']);

        $data = $data + [
            'TglEstimasiBayar' => $date,
            'Matriks' => $sdoc,
            'Status' => 4
        ];

        $this->db->insert('Invoice', $data);

        // make HistoryInvoice
        $invoice_id = $this->db->insert_id();
        $data2 = [
            'IDInvoice' => $invoice_id,
            'Action'    => 4,
            'UserID'    => Auth::user()->ID,
            'Timestamp' => date('Y-m-d H:i:s'),
            'Keterangan'    => $_POST['note']
        ];
        $this->db->insert('HistoryInvoice', $data2);

        $this->db->trans_complete();
        notif('tab','wum');
        redirect('akuntansi/invoice/kasir');
    }

    public function ajax()
    {
        header('Content-Type: application/json');
        $id = $this->input->post('id');

        $query = $this->db->where('ID', $id)->get('Ms_Karyawan')->row_array();
        echo json_encode($query);
    }

    public function get_payment_term_detail()
    {
        $id = $this->input->post('id');
        echo $this->m_wum->get_payment_term_time($id);
    }

    public function edit($id)
    {
        $data = $this->m_wum->get_detail($id);

        if( ! $data) {
            die('Whoops, something wrong');
        }
        $action = 'edit';
        if($data->Status == 4){
            $action = 'view';
        }
        $this->blade->view('akuntansi.invoice_tracking.wum.'.$action, [
            'id'            => $id,
            'data'          => $data,
            'workers'       => $this->m_wum->get_list_karyawan(),
            'payment_terms' => $this->m_wum->get_payment_term(),
            'ls_transaksi'  => $this->m_vl->get_jenis_transaksi('wum'),
            'ls_kurs'       => $this->m_vl->get_kurs(),
            'time'          => $this->m_wum->get_payment_term_time($data->PaymentTerm),
            'cb'            => $this->m_wum->decode_matriks($data->Matriks),
            'histories'     => $this->m_invoice->getHistory($id)
        ]);
    }

    public function update($id)
    {
        $this->db->trans_start();

        $documents = isset($_POST['documents']) ? $_POST['documents'] : [];
        $sdoc = serialize($documents);

        $pay_term = $this->input->post('PaymentTerm');
        $addDay = $this->db->where('PaymentTerm', $pay_term)->get('VL_PaymentTerm')->row('JumlahHari');
        $date = date('d M Y', strtotime("+{$addDay} day"));

        $data = $_POST;
        $data['NilaiInvoice'] = str_replace([',','.'],'', $data['NilaiInvoice']);
        unset($data['documents']);
        unset($data['keterangan']);
        unset($data['note']);

        $data = $data + ['TglEstimasiBayar' => $date, 'Matriks' => $sdoc, 'Status' => 4];

        $this->db->where('ID', $id)->update('Invoice', $data);

        // make HistoryInvoice
        $this->m_invoice->createHistory($id, 4, $_POST['note']);

        $this->db->trans_complete();
         notif('tab','wum');
        redirect('akuntansi/invoice/kasir');
    }
}
