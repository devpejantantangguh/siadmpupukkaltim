<?php 

class Invoice_Controller extends CI_Controller
{
    public function __construct() {
        parent::__construct();
    }    

    protected function _getStatus($kategori)
    {
        if( ! $kategori == 0) {
            $this->db->where('Kategori',$kategori);
        }

        $rows = $this->db->get('Status')->result();

        $results = [];
        foreach($rows as $row) {

            $id = trim($row->ID);
            if(is_numeric($id)) {
                $id = intval($id);
            }

            $results[] = array(
                'id' => $id,
                'name' => $row->Status
            );
        }

        return $results;
    }

    protected function _getJenisTransaksi()
    {
        $rows = $this->db->get('VL_JenisTransaksi')->result();

        $listJenisTransaksi = [];
        foreach($rows as $row) {
            $listJenisTransaksi[] = array(
                'id'  => $row->ID,
                'name'  => $row->JenisTransaksi
            );
        }

        return $listJenisTransaksi;
    }
    protected function _PaymentTerm()
    {
        $rows = $this->db->get('VL_PaymentTerm')->result();

        $listJenisTransaksi = [];
        foreach($rows as $row) {
            $listJenisTransaksi[] = array(
                'id'  => $row->ID,
                'name'  => $row->PaymentTerm
            );
        }

        return $listJenisTransaksi;
    }

}
