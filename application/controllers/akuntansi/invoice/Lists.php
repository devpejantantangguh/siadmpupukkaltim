<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Lists extends CI_Controller 
{
    public $updater_role = 10; //Verifikator Akuntansi

    public $status_current = 4;  //checked 
    public $status_updated = 10; //ar checked
    public $status_updated2 = 5; //verified
    public $status_reject = 11; //rejected
    public $action_verify = 5; //vl action verifikasi
    public $action = 9; //vl action reject
    function __construct()
    {
        parent::__construct();
        Auth::guard();

        $this->load->model('m_po');
        $this->load->model('m_vl');
        $this->load->model('m_invoice');
    }

    public function index()
    {
        
        if($this->input->get('tab')){
            notif('tab','dokumen');
        }
        echo $this->blade->view('akuntansi/invoice_tracking/index', array(
            'table'         => 'Invoice',
            'list_status'   => $this->m_vl->get_status(1),
            'Status' => $this->status_current.'-'.$this->status_updated.'-'.$this->status_updated2
        ));
        //define NilaiVerifikasi =  NilaiInvoice
        $this->db->set("NilaiVerifikasi", "NilaiInvoice", FALSE); 
        $this->db->where("Status", $this->status_current);
        $this->db->update("Invoice");
        
    }
    public function get($status = null, $optional = null)
    {
        if($optional == 'po'){
            $lists = $this->db->where('Kategori',1)->where('Keterangan',NULL)->get('VL_JenisTransaksi')->result();
            $jt = [];
            foreach($lists as $list) {
                $jt[] = $list->ID;
            }
            $this->db->where_in('JenisTransaksi', $jt);
        }

        $this->db->select('i.*');
        $area = Auth::unitKerja()->Area;
        $this->db->where('us.Area',$area)
                ->join('Ms_User us','us.ID = i.CreatedBy');

        if(strpos($status,'-') !== false) {
            $xstatus = explode('-', $status);
            $this->db->where_in('Status', $xstatus);
        }elseif(empty($status)) {
            
        }else {
            $this->db->where('Status', $status);
        }
        /* StatusTransmittal != TRUE */
        $this->db->where('StatusTransmittal !=', true);

        $search = isset($_GET['filter']) ? json_decode($_GET['filter']) : '';
        $limit = isset($_GET['take']) ? $_GET['take'] : 10;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;
        if(!empty($search)) {
            Filter::dxDataGridFilters($search, $this->db);
        }
        
        Filter::areaFilter('i', $this->db,'custom');

        if(isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        } else {
            $this->db->order_by('i.ID', 'DESC');
        }

        $tempdb = clone $this->db; //clone for count all rows
        $count= $tempdb->from('Vw_AllInvoice i')->count_all_results();
        $items = $this->db
                    
                    ->get('Vw_AllInvoice i', $limit, $offset)->result();
        foreach ($items as $item) {
            $item->ID = encode_url($item->ID);
            //$key->ID = $this->encrypt->encode($key->ID);
        }
        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $items
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function get_wum()
    {
        $transactions = $this->db
                        ->where('Keterangan','WUM')
                        ->where('Kategori',1)
                        ->get('VL_JenisTransaksi')
                        ->result();

        $lists = [];
        foreach($transactions as $trans) {
            $lists[] = $trans->ID;
        }

        $this->db->select('Invoice.*');
        $area = Auth::unitKerja()->Area;
        $this->db->where('us.Area',$area)
                ->join('Ms_User us','us.ID = Invoice.CreatedBy');
        $search = isset($_GET['filter']) ? json_decode($_GET['filter']) : '';
        $limit = isset($_GET['take']) ? $_GET['take'] : 10;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;
        if(!empty($search)) {
            Filter::dxDataGridFilters($search, $this->db);
        }

        if(isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        } else {
            $this->db->order_by('Invoice.ID', 'DESC');
        }

        //$this->db->where('NoTransmittal !=',null);
        $this->db->not_like('NoTransmittal','AK');
        $tempdb = clone $this->db; //clone for count all rows
        $count= $tempdb->where_in('JenisTransaksi', $lists)->from('Invoice')->count_all_results();
        $query = $this->db->where_in('JenisTransaksi', $lists)->get('Invoice', $limit, $offset);
        $items = $query->result();

        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $items
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function getPotongan($id_invoice = 0)
    {
        if($id_invoice == 0) {
            $items = [];
            $count = 0;
        } else {
            $limit = isset($_GET['take']) ? $_GET['take'] : 10;
            $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;

            if(isset($_GET['orderby'])) {
                $this->db->order_by($_GET['orderby']);
            } else {
                $this->db->order_by('r.ID', 'DESC');
            }

            $items = $this->db->select('r.*, i.NoInvoice, v.Potongan, k.Nama')
                                ->where('i.ID', $id_invoice)
                                ->join('PotonganInvoice r', 'i.ID = r.IDInvoice')
                                ->join('VL_PotonganInvoice v', 'v.ID = r.IDPotongan')
                                ->join('Ms_User u', 'u.ID = r.CreatedBy')
                                ->join('Ms_Karyawan k', 'k.ID = u.IDKaryawan')
                                ->get('Invoice i', $limit, $offset)
                                ->result();
            $count = $this->db->where('IDInvoice', $id_invoice)->count_all_results('PotonganInvoice');
        }

        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $items
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function sumPotongan($id_invoice = 0)
    {
        if($id_invoice == 0) {
            $items = [];
            $count = 0;
        } else {
            $this->db->select_sum('Nominal');
            $sum = $this->db->where('IDInvoice', $id_invoice)->get('PotonganInvoice')->row()->Nominal;
        }
        return $sum;
    }

    public function open($id)
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            notif('error', 'Anda tidak berhak mengubah data.');
            redirect('akuntansi/invoice/lists/');
            die();
        }
        $encode_id = $id;
        $id = decode_url($id);
        $data = $this->m_po->get_detail_invoice($id);
        if(!$data){

            show_404();
            die();
        }
        if($data->Status != $this->status_current){
            redirect('invoice/invoice/view/'.$encode_id);
        }
        foreach ($data as $value) {
            $data->NilaiVerifikasi = $data->NilaiInvoice - $this->sumPotongan($id);
        }
        $list_transaksi = $this->m_vl->get_jenis_transaksi('po');
        $documents = $this->m_po->get_kelengkapan_dokumen($id);
        $current_note = $this->m_po->get_current_note($id);
        $last_note = $this->m_po->get_last_note($id);
        $table = 'Invoice';
        $list_kurs = $this->m_vl->get_kurs();
        $attachments = $this->db->where('IDParent', $id)->get('Attachment')->result();
        $Keterangan = $this->m_invoice->getLastKeterangan($id);
        $histories = $this->m_invoice->getHistory($id);
        $list_potongan = $this->m_vl->get_potongan();
        $term   = $this->m_vl->get_PaymentTerm();
        $Checked = $this->getTimestampChecked($id);
        
        $TimestampChecked = ($Checked) ? $Checked->Timestamp : date("Y-m-d");

        $this->blade->view('akuntansi.invoice_tracking.open', compact('data','table','documents','id','list_transaksi','current_note','last_note','list_kurs','term','attachments','Keterangan','histories','list_potongan','TimestampChecked'));
    }
    public function transmittalwum()
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            echo json_encode(array('status' => 404, 'msg' => 'Anda tidak berhak mengubah data.'));
            die();
        }
        $ids = $this->input->post('ids');
        $ids = array_unique($ids);
        $new_ids = [];
        //filter only checked
        foreach ($ids as $key => $id) {
            if(!(int)$id){
                $ids[$key] = decode_url($id);
                $encode_id = $id;
                $id = decode_url($id);

            }
            $status = $this->db->where('ID',$id)->get('Invoice')->row()->Status;
            if($status == $this->status_current)
            {
                array_push($new_ids, $id);
            }
        }
        if(!empty($new_ids))
        {
            
            $this->db->trans_start();

            $transmittalAKT = $this->get_transmittalAKT($new_ids);
            if($transmittalAKT != 0 ){
                $number = $this->get_last_trans_number();
                $next   = $number+1;
                $tahun  = substr($transmittalAKT, 0, 2);
                $number = $transmittalAKT.'AK'.$tahun.str_pad($number, 4, "0", STR_PAD_LEFT);
            }else{

                echo json_encode(array('status' => 404, 'msg' => 'Gagal: Transmittal AKT kosong (Kasir)'));
        
                die();
                $number = $this->get_last_trans_number();
                $next   = $number+1;
                $number = date('y').'0000AK'.str_pad($number, 4, "0", STR_PAD_LEFT);
            }
            $this->db->where_in('ID', $new_ids)->update('Invoice',[
                'NoTransmittal' => $number,
                'TglTransmittal'    => date('Y-m-d'),
                'StatusTransmittal'    => TRUE
            ]);

            $this->db->where('Setting','TransmittalKEU')
                     ->update('GlobalSettings', ['Value' => $next]);

            $this->db->trans_complete();
            $id = encode_url($number);
            echo json_encode(array('status' => 200, 'number' => $id,'msg'=>'Transmittal Created ['.$number.']')); 
        
        }else{
            echo json_encode(array('status' => 404, 'msg' => 'Gagal: Transmittal tidak bisa dicetak.'));
        }

    }
    public function transmittal($wum = false)
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            echo json_encode(array('status' => 404, 'msg' => 'Anda tidak berhak mengubah data.'));
            die();
        }
        $ids = $this->input->post('ids');
        $ids = array_unique($ids);
        $new_ids = [];
        //filter only checked
        foreach ($ids as $key => $id) {
            if(!(int)$id){
                $ids[$key] = decode_url($id);
                $encode_id = $id;
                $id = decode_url($id);

            }
            $status = $this->db->where('ID',$id)->get('Invoice')->row()->Status;
            if($status == $this->status_updated || $status == $this->status_updated2 || $status == $this->status_reject)
            {
                array_push($new_ids, $id);
            }
        }
        if(!empty($new_ids))
        {
            
            $this->db->trans_start();

            $transmittalAKT = $this->get_transmittalAKT($new_ids);
            if($transmittalAKT != 0 ){
                $number = $this->get_last_trans_number();
                $next   = $number+1;
                $tahun  = substr($transmittalAKT, 0, 2);
                $number = $transmittalAKT.'AK'.$tahun.str_pad($number, 4, "0", STR_PAD_LEFT);
            }else{

                echo json_encode(array('status' => 404, 'msg' => 'Gagal: Transmittal AKT kosong (Kasir)'));
        
                die();
                $number = $this->get_last_trans_number();
                $next   = $number+1;
                $number = date('y').'0000AK'.str_pad($number, 4, "0", STR_PAD_LEFT);
            }
            $this->db->where_in('ID', $new_ids)->update('Invoice',[
                'NoTransmittal' => $number,
                'TglTransmittal'    => date('Y-m-d'),
                'StatusTransmittal'    => TRUE
            ]);

            $this->db->where('Setting','TransmittalKEU')
                     ->update('GlobalSettings', ['Value' => $next]);

            $this->db->trans_complete();
            $id = encode_url($number);
            echo json_encode(array('status' => 200, 'number' => $id,'msg'=>'Transmittal Created ['.$number.']')); 
        
        }else{
            echo json_encode(array('status' => 404, 'msg' => 'Gagal: Dokumen harus terverifikasi.'));
        }

    }
    private function get_last_trans_number()
    {
        $query = $this->db->where('Setting','TransmittalKEU')->get('GlobalSettings');

        if($query->num_rows() > 0) {
            return $query->row('Value');
        } else {
            return 0;
        }
    }    
    private function get_transmittalAKT($ids)
    {
        $this->db->where('NoTransmittal !=','');
        $query = $this->db->where_in('ID',$ids)->get('Invoice');
        if($query->num_rows() > 0) {
            return $query->row('NoTransmittal');
        } else {
            return 0;
        }
    }
    public function store($id)
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            notif('error', 'Anda tidak berhak mengubah data.');
            redirect('akuntansi/invoice/lists/');
            die();
        }
        $this->db->where('NoDokumenSAP',$this->input->post('NoDokumenSAP'));
        $this->db->where('Tahun',$this->input->post('Tahun'));
        $count = $this->db->get('Invoice')->num_rows();
        if($count!=0){
            notif('error', 'No Dokumen SAP dan Tahun sudah digunakan');
            redirect('akuntansi/invoice/lists/open/'.encode_url($id));
            die();
        }
        $this->load->library('form_validation');
        $this->form_validation->set_rules('NoDokumenSAP','Nomor Dokumen SAP','exact_length[10]|numeric');
        $this->form_validation->set_message('exact_length', '%s harus 10 digit angka');
        if($this->form_validation->run() == FALSE) {
            notif('error', 'Nomor Dokumen SAP harus 10 digit angka.');
            redirect('akuntansi/invoice/lists/open/'.encode_url($id));
                die();
        } 
        $this->update_nilaiverifikasi($id);
        $this->db->trans_start();

        $status = $this->db->where('ID',$id)->get('Invoice')->row()->Status;
        if($status == $this->status_current) //checked
        {

            
            if(isset($_POST['matriks'])) {
                $this->m_po->updateMatriks($id, $_POST['matriks']);
            }
            
            $this->m_po->update_priority($id);
            $submit = $this->input->post('submit');

            $NoDokumenSAP = $this->input->post('NoDokumenSAP');
            $PaymentTerm = $this->input->post('PaymentTerm');
            $NoFakturPajak = $this->input->post('NoFakturPajak');
            
            list($d,$m,$y) = explode('-', $this->input->post('TglFakturPajak'));
            $TglFakturPajak = date('Y-m-d', mktime(0,0,0,$m,$d,$y));
            $Tahun = $this->input->post('Tahun');
            $IDVendor = $this->input->post('IDVendor');

            $this->db->where('ID',$id)->update('Invoice',[
                'NoDokumenSAP' => $NoDokumenSAP,
                'PaymentTerm'=>$PaymentTerm,
                'NoFakturPajak'=>$NoFakturPajak,
                'TglFakturPajak'=>$TglFakturPajak,
                'Tahun'=>$Tahun
            ]);

            $countPiutang = $this->countPiutang($IDVendor);
            $Keterangan = $this->input->post('note');
            if($countPiutang>0){
                $update_id = $this->status_updated; //AR Checked
                //$this->createHistory($id, $this->action_verify);
                $this->m_invoice->newHistory($id, $this->action_verify,$update_id, $Keterangan);
            }else{
                $update_id = $this->status_updated2; //Verified
                //$this->createHistory($id, $this->action_verify);
                $this->m_invoice->newHistory($id, $this->action_verify,$update_id, $Keterangan);
            }

            $this->update_status($id, $update_id);
            $this->update_nilaiverifikasi($id);
            

            $this->m_invoice->updateIDArea($id,'update');
            $this->db->trans_complete();

        notif('success', 'Data berhasil diubah.');
        redirect('akuntansi/invoice/lists/?update='.$submit);
        }else{
            $submit = 'fail';

        notif('error', 'Data gagal diubah.');
        redirect('akuntansi/invoice/lists/?update='.$submit);
        }
    }

    private function createHistory($id, $status)
    {
        $data = [
            'IDInvoice' => $id,
            'Action'    => $status,
            'UserID'    => Auth::user()->ID,
            'Timestamp' => date('Y-m-d H:i:s'), 
            'Keterangan'    => $this->input->post('note'),
        ];
        $this->db->insert('HistoryInvoice', $data);
    }    
    private function update_status($id, $update_id)
    {
        $this->db->where('ID',$id)
            ->update('Invoice',array('Status' => $update_id ));
    }    
    private function update_nilaiverifikasi($id_invoice)
    {
            $items = $this->db->select('(Nominal)')
                                ->where('IDInvoice', $id_invoice)
                                ->get('PotonganInvoice')
                                ->result();
            $sum = 0;
            foreach ($items as $value) {
                $sum = (float) $sum + ((float) $value->Nominal);
            }
            $items = $this->db->select('*')
                    ->where('ID', $id_invoice)
                    ->get('Invoice')
                    ->result();
            $this->db->set("NilaiVerifikasi", "NilaiInvoice - $sum", FALSE); 
            $this->db->where("ID", $id_invoice);
            $this->db->update("Invoice");
    }


    private function countPiutang($IDVendor)
    {
        $this->db->where('Ms_Customer.PairedVendorID', $IDVendor);
        $this->db->join('Ms_Customer', 'ListPiutang.IDCustomer = Ms_Customer.IDCustomer');
        $row = $this->db->get('ListPiutang')->num_rows();
        return $row;
    }

    public function reject_update($id)
    {
        $roleID = (int) Auth::roleID();
        if($this->updater_role != $roleID){
            notif('error', 'Anda tidak berhak mengubah data.');
            redirect('akuntansi/invoice/lists/');
            die();
        }
        $this->db->trans_start();

        $this->db->where('ID',$id)
            ->where('Status',$this->status_current) // update status checked
            ->update('Invoice', array(
                    'Status' => $this->status_reject,
                    'NoTransmittal' => NULL,
                    'StatusTransmittal' => false)); // (rejected)
        $isSuccess = $this->db->affected_rows();

        $Keterangan = $this->input->post('Keterangan');
        $this->db->trans_complete();
        
        if($isSuccess) {
            //$this->m_invoice->createHistory($id, $this->action, $Keterangan);
            $this->m_invoice->newHistory($id, $this->action,statusID('REJECTED'), $Keterangan);
            


            $from_email = "no-reply@mitraku.pupukkaltim.com"; 
            /*Send Email when reject*/
            $invoice = $this->m_invoice->getDetail($id);
             //$to_email = "sony.candra@pupukkaltim.com"; 
             $to_email = $this->m_invoice->getEmailCreator($id);
             $data = array(
                'NamaVendor' => $invoice->NamaVendor,
                'NoInvoice' => $invoice->NoInvoice,
                'NilaiInvoice' => 'Rp. '.number_format($invoice->NilaiInvoice,2,',','.'),
                'Keterangan' => $Keterangan,
             );
            $mesg = $this->load->view('templates/email',$data, true);
             //Load email library 
             $this->load->library('email'); 
       
             $this->email->from($from_email, 'Mitraku'); 
             $this->email->to($to_email);
             $this->email->subject('Revisi pengajuan pembayaran Invoice No. '.$invoice->NoInvoice); 
             $this->email->message($mesg); 
       
             //Send mail 
             if($this->email->send());
            notif('success', 'Invoice berhasil di-reject');
            redirect('akuntansi/invoice/lists?status=reject_ok');
        }else{
            notif('error', 'Invoice gagal di-reject');
            redirect('akuntansi/invoice/lists?status=reject_failed');
        }
    }

    public function add_potongan($id)
    {
        $res = $this->input->post();

        $data['Nominal'] = (float) str_replace(',', '.', str_replace('.', '', $res['Nominal']));
        $data['Keterangan'] = $res['Ket'];
        $data['IDPotongan'] = $res['Potongan'];
        $data['IDInvoice'] = $id;
        $data['CreatedBy'] = Auth::user()->ID;
        $data['CreatedOn'] = date('Y-m-d H:i:s');
        $this->db->insert('PotonganInvoice', $data);

        $this->db->select_sum('Nominal');
        $this->db->from('PotonganInvoice');
        $this->db->where('IDInvoice', $id);
        $query = $this->db->get();
        echo (float) $query->row()->Nominal;
    }
    public function delete_potongan($id)
    {
        $ids = $this->input->post('ids');
        if(!$ids){
            die('Woops...');
        }
        $this->db->where_in('ID', $ids)->delete('PotonganInvoice');

        $this->db->select_sum('Nominal');
        $this->db->from('PotonganInvoice');
        $this->db->where('IDInvoice', $id);
        $query = $this->db->get();
        echo (int) $query->row()->Nominal;
    }
    private function getTimestampChecked($id_invoice)
    {
        return $this->db->select('HistoryInvoice.Timestamp')
                        ->join('VL_Action','VL_Action.ID = HistoryInvoice.Action','left')
                        ->join('Ms_User','Ms_User.ID = HistoryInvoice.UserID','left')
                        ->where('IDInvoice', $id_invoice)
                        ->where('HistoryInvoice.Action', 4)
                        ->order_by('Timestamp', 'asc')
                        ->get('HistoryInvoice')->row();
    }

    public function pdf_transmittal($id)
    {
        $id = decode_url($id);
        $query = $this->db->like('a.NoTransmittal', $id,'before')
                ->get('Invoice as a');
        $num = $query->num_rows();
        if($num > 0) {
            $row = $query->row_array();
            $data = $query->result();
            $id = explode('AK',$id);
            $row['NoTransmittal'] = 'AK'.$id[1];
            $row['Num'] = $num;
            $row['Nama'] = $this->get_karyawan_by_id(Auth::user()->ID);
            $views = $this->blade->render('akuntansi/invoice_tracking/pdf/kasir',[
                'data'  => $data,
                'row'   => $row
            ]
            );

            //echo $views;die();

            header("Content-type:application/pdf");
            // instantiate and use the dompdf class
            $dompdf = new Dompdf\Dompdf();
             $dompdf->set_option('isRemoteEnabled', TRUE);
            $dompdf->loadHtml($views);
            $dompdf->setPaper('A4', 'potrait');
            $dompdf->render();
            return $dompdf->stream('invoice.pdf',['Attachment' => 0]);
        }

    }
    public function pdf_transmittal_wum($id)
    {
        $id = decode_url($id);
        $query = $this->db->like('a.NoTransmittal', $id,'after')
                ->get('Invoice as a');
        $num = $query->num_rows();
        if($num > 0) {
            $row = $query->row_array();
            $data = $query->result();
            $row['NoTransmittal'] = $id;
            $row['Num'] = $num;
            $row['Nama'] = $this->get_karyawan_by_id(Auth::user()->ID);
            $views = $this->blade->render('akuntansi/invoice_tracking/pdf/wum',[
                'data'  => $data,
                'row'   => $row
            ]
            );

            //echo $views;die();

            header("Content-type:application/pdf");
            // instantiate and use the dompdf class
            $dompdf = new Dompdf\Dompdf();
             $dompdf->set_option('isRemoteEnabled', TRUE);
            $dompdf->loadHtml($views);
            $dompdf->setPaper('A4', 'potrait');
            $dompdf->render();
            return $dompdf->stream('invoice.pdf',['Attachment' => 0]);
        }

    }

    private function get_karyawan_by_id($id){
        $this->db->where('Ms_User.ID', $id);
        $this->db->join('Ms_User','Ms_User.IDKaryawan = Ms_Karyawan.ID');
        $rows = $this->db->select('Ms_Karyawan.Nama')->get('Ms_Karyawan')->row();
        if($rows){
            
            return $rows->Nama;
        }
        return '';
        
    }
}
