<?php

function areaIDByCreatorInvoice($id_invoice = false)
{
	if (!$id) {
		return false;
	}
	$CI = &get_instance();
	$table = 'Invoice';
	$query = $CI->db->where('ID', $id)->get($table)->row();
	if ($query) {
		return ($query->$field);
	}
	return false;

	if ($id == 3) {
		return '';
	}
	if ($id == 1) {
		return '';
	}
	if ($id == 2) {
		return '';
	}
	if ($id == 10) {
		return '';
	}
	if ($id == 20) {
		return '';
	}
	return false;
}
function infoInvoice($id = false, $field = false)
{
	if (!$id) {
		return false;
	}
	$CI = &get_instance();
	$table = 'Invoice';
	$query = $CI->db->where('ID', $id)->get($table)->row();
	if ($query) {
		return ($query->$field);
	}
	return false;
}
function newID($table = false)
{
	if (!$table) {
		return false;
	}
	$CI = &get_instance();
	$CI->db->select_max('ID');
	$query = $CI->db->get($table)->row();
	if ($query) {
		return (int)($query->ID) + 1;
	}
	return 1;
}
function roleID($name = false)
{
	if (!$name) {
		return false;
	}
	$CI = &get_instance();
	$table = 'Ms_Role';
	$db = $CI->db->get_where($table, array('RoleName' => $name));
	if ($db) {
		foreach ($db->result() as $row) {
			return $row->ID;
		}
	}
	return false;
}
function getStatusName($statusID = false)
{
	if (!$statusID) {
		return '';
	}
	$CI = &get_instance();
	$table = 'Status';
	$db = $CI->db->get_where($table, array('ID' => $statusID));
	if ($db) {

		foreach ($db->result() as $row) {
			return $row->Status;
		}
	}
	return '';
}

function statusID($status = false)
{
	if (!$status) {
		return false;
	}
	$CI = &get_instance();
	$table = 'Status';
	$db = $CI->db->get_where($table, array('Status' => $status));
	if ($db) {
		foreach ($db->result() as $row) {
			return (int)$row->ID;
		}
	}
	return false;
}

function actionID($action = false)
{
	if (!$action) {
		return false;
	}
	$CI = &get_instance();
	$table = 'VL_Action';
	$db = $CI->db->get_where($table, array('Action' => $action));
	if ($db) {
		foreach ($db->result() as $row) {
			return $row->ID;
		}
	}
	return false;
}
function getNames($uid = false)
{
	if (!$uid) {
		return '';
	}
	$CI = &get_instance();
	$table = 'Ms_User';
	$CI->db->join('Ms_Karyawan', 'Ms_Karyawan.ID = Ms_User.IDKaryawan');
	$db = $CI->db->get_where($table, array('Ms_User.ID' => $uid));
	if ($db) {
		foreach ($db->result() as $row) {
			return $row->Nama;
		}
	}
	return '';
}

if (!function_exists('url')) {
	function url($path)
	{
		return base_url($path);
	}
}

if (!function_exists('dd')) {
	function dd($vars = [])
	{
		var_dump($vars);
		die();
	}
}

if (!function_exists('_')) {
	function _($vars = [])
	{
		echo '<pre>';
		print_r($vars);
		echo '</pre>';
		die();
	}
}

if (!function_exists('optional')) {
	function optional(&$vars, $default = null)
	{
		return isset($vars) ? $vars : $default;
	}
}

if (!function_exists('events')) {
	function events($name, $data = null)
	{
		$path = base_path('application/logs/events.log');
		file_put_contents($path, serialize($data), FILE_APPEND);
	}
}

if (!function_exists('path')) {
	function path($dir)
	{
		return str_replace(array('/', '\\'), DIRECTORY_SEPARATOR, $dir);
	}
}

if (!function_exists('base_path')) {
	function base_path($dir)
	{
		return FCPATH . path($dir);
	}
}

if (!function_exists('max_str')) {
	function max_str($str, $max = 10)
	{
		return substr($str, 0, $max);
	}
}
if (!function_exists('notif')) {
	function notif($act = 'success', $msg = 'Data berhasil ditambahkan.')
	{
		$ci = &get_instance();
		$ci->session->set_flashdata($act, ($msg));
	}
}
if (!function_exists('flashdata')) {
	function flashdata($act = 'success')
	{
		$ci = &get_instance();
		return $ci->session->flashdata($act);
	}
}

function tanggal_indo($tanggal)
{
	$tanggal = date('Y-m-d', strtotime($tanggal));

	$bulan = array(
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$split = explode('-', $tanggal);
	return $split[2] . ' ' . $bulan[(int)$split[1]] . ' ' . $split[0];
}

function tanggalSekarang()
{
	return tanggal_indo(date('Y-m-d'));
}

function hari_indo($hari)
{
	$hari = date('D', strtotime($hari));

	switch ($hari) {
		case 'Sun':
			$hari_ini = "Minggu";
			break;

		case 'Mon':
			$hari_ini = "Senin";
			break;

		case 'Tue':
			$hari_ini = "Selasa";
			break;

		case 'Wed':
			$hari_ini = "Rabu";
			break;

		case 'Thu':
			$hari_ini = "Kamis";
			break;

		case 'Fri':
			$hari_ini = "Jumat";
			break;

		case 'Sat':
			$hari_ini = "Sabtu";
			break;

		default:
			$hari_ini = "Tidak di ketahui";
			break;
	}

	return $hari_ini;
}

function penyebut($nilai)
{
	$nilai = abs($nilai);
	$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
	$temp = "";
	if ($nilai < 12) {
		$temp = " " . $huruf[$nilai];
	} else if ($nilai < 20) {
		$temp = penyebut($nilai - 10) . " belas";
	} else if ($nilai < 100) {
		$temp = penyebut($nilai / 10) . " puluh" . penyebut($nilai % 10);
	} else if ($nilai < 200) {
		$temp = " seratus" . penyebut($nilai - 100);
	} else if ($nilai < 1000) {
		$temp = penyebut($nilai / 100) . " ratus" . penyebut($nilai % 100);
	} else if ($nilai < 2000) {
		$temp = " seribu" . penyebut($nilai - 1000);
	} else if ($nilai < 1000000) {
		$temp = penyebut($nilai / 1000) . " ribu" . penyebut($nilai % 1000);
	} else if ($nilai < 1000000000) {
		$temp = penyebut($nilai / 1000000) . " juta" . penyebut($nilai % 1000000);
	} else if ($nilai < 1000000000000) {
		$temp = penyebut($nilai / 1000000000) . " milyar" . penyebut(fmod($nilai, 1000000000));
	} else if ($nilai < 1000000000000000) {
		$temp = penyebut($nilai / 1000000000000) . " trilyun" . penyebut(fmod($nilai, 1000000000000));
	}
	return $temp;
}

function terbilang($nilai)
{
	if ($nilai < 0) {
		$hasil = "minus " . trim(penyebut($nilai));
	} else {
		$hasil = trim(penyebut($nilai));
	}
	return $hasil;
}

if (!function_exists('now')) {
	function now()
	{
		return date('Y-m-d H:i:s');
	}
}
