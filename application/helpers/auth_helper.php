<?php

class Auth
{

    private static $table = 'Ms_User';

    public static function init()
    {
        $CI = &get_instance();
        return $CI;
    }

    public static function loginWithId($id)
    {
        $table = self::$table;
        $db = self::init()->db->get_where($table, array('ID' => $id))->row();
        if ($db) {
            self::init()->db->where(array('ID' => $id))->update($table, array('LastLogin' => date('Y-m-d H:i:s')));
            self::init()->session->set_userdata(array('users' => $db));
            return $db;
        }
        return false;
    }

    public static function loginWithEmail($email)
    {
        $table = self::$table;
        $db = self::init()->db->get_where($table, array('Email' => $email))->row();
        if ($db) {
            self::init()->db->where(array('Email' => $email))->update($table, array('LastLogin' => date('Y-m-d H:i:s')));
            self::init()->session->set_userdata(array('users' => $db));
            return $db;
        }
        return false;
    }

    public static function attempt($username, $password, $autologin = false)
    {
        $table = self::$table;
        self::init()->db->select("Ms_User.*, Ms_Role.RoleName, Ms_Role.Permission, Ms_Karyawan.NPKLama")
            ->join('Ms_Role', 'Ms_Role.ID = Ms_User.RoleID')
            ->join("Ms_Karyawan", "Ms_Karyawan.ID = Ms_User.IDKaryawan", "left");

        if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
            $db = self::init()->db->where('Email', $username)->get($table)->row();
        } else {
            $db = self::init()->db->where('NPKLama', $username)->get($table)->row();
        }

        if (isset($db->ID) && $autologin == false) {
            if ($db->IsLDAP == '1') {
                $db = Auth::ldap_auth($username, $password) ? $db : false;
            } else {
            	if(strlen($db->Password) > 30) {
            		$db = password_verify($password, $db->Password) ? $db : false;
				} else {
            		$db = $db->Password == $password ? $db : false;
				}
            }
        }

        if (isset($db->ID)) {
            self::init()->session->set_userdata(array('users' => $db));

            self::init()->db->reset_query();
            $unitKerja = self::init()->db->select("Ms_UnitKerja.*,Ms_Karyawan.Nama")
                ->where('Ms_User.ID', Auth::user()->ID)
                ->join('Ms_Karyawan', 'Ms_Karyawan.ID = Ms_User.IDKaryawan', 'left')
                ->join('Ms_UnitKerja', 'Ms_UnitKerja.CostCenter = Ms_Karyawan.CostCenter', 'left')
                ->get('Ms_User')
                ->row();

            self::init()->session->set_userdata(array('unitKerja' => $unitKerja));
            return $db;
        }
        return false;
    }

    public static function guest()
    {
        if (isset($_SERVER['HTTP_REFERER'])) {
            self::init()->session->set_userdata(array('last_page' => $_SERVER['HTTP_REFERER']));
        }
        return !self::init()->session->has_userdata('users');
    }

    public static function user()
    {
        if (!Auth::guest()) {
            self::init()->db->reset_query();
            $user_id = self::init()->session->userdata('users')->ID;
            return self::init()->db->where('ID', $user_id)->get('Ms_User')->row(); // get updated data, not session
        }
        return null;
    }

    public static function id()
    {
        return Auth::user()->ID;
    }

    public static function logout()
    {
        self::init()->session->unset_userdata('users');
    }

    public static function role($roleID, $redirect = 'login')
    {
        if (Auth::guest()) {
            redirect($redirect);
        } else {
            if (Auth::user()->RoleID != $roleID) {
                redirect($redirect);
            }
        }

        return true;
    }

    public static function guard()
    {
        if (Auth::guest()) {
            redirect('login');
        }
    }

    public static function permission($roleName, $arr = false)
    {

        $id = Auth::user()->RoleID;
        //pertama, cek apakah role ada di db
        $db = self::init()->db->get_where('Ms_Role', array('ID' => $id))->row();
        if ($db) {
            $data = json_decode($db->Permission, true);
            foreach ($data as $key => $value) {
                if (array_key_exists($roleName, $data)) {
                    if ($arr) {
                        foreach ($data[$roleName] as $k => $v) {
                            if (array_key_exists($arr, $data[$roleName])) {
                                if ($v == 1) {
                                    return true;
                                } else {
                                    return false;
                                }
                            } else {
                                return false;
                            }
                        }
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    public static function get_permission($roleName, $arr = null, $id = false)
    {
        //pertama, cek apakah role ada di db
        $db = self::init()->db->get_where('Ms_Role', array('ID' => $id))->row();
        if ($db) {
            $data = json_decode($db->Permission, true);
            foreach ($data as $key => $value) {
                if (array_key_exists($roleName, $data)) {
                    foreach ($data[$roleName] as $k => $v) {
                        if (array_key_exists($arr, $data[$roleName])) {
                            if ($v == 1) {
                                return true;
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    }
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }
    public static function unitKerja()
    {
        //pertama, cek IDKaryawan
        //kedua, join dengan Ms_Karyawan, join dengan Ms Unit Kerja
        if (!Auth::guest()) {
            return self::init()->session->userdata('unitKerja');
        }
        return null;
    }
    public static function getUnitKerja($CostCenter)
    {

        $query = self::init()->db->where('CostCenter', $CostCenter)->get('Ms_UnitKerja');
        if ($query->num_rows() > 0) {
            return $query->row()->UnitKerja;
        }
        return null;
    }
    public static function matriksUnitKerja($custom = false)
    {
        $result = [];
        if (!Auth::guest()) {
            self::init()->db->reset_query();
            $npklama = Auth::unitKerja()->NPKLama;

            self::init()->db->join('Ms_UnitKerja', 'Ms_UnitKerja.CostCenter = vw_Matriks_User-CC.CostCenter', 'left');
            $query = self::init()->db->where('NPKLama', $npklama)->get('vw_Matriks_User-CC');
            if ($query->num_rows() > 0) {
                foreach ($query->result_array() as $row) {
                    if ($custom) {
                        array_push($result, $row['CostCenter']);
                    } else {
                        $result[$row['CostCenter']] = $row['CostCenter'] . ' - ' . $row['UnitKerja'];
                    }
                }
            }
        }
        return $result;
    }

    public static function roleID()
    {
        /*
            Pemjelasan Role
            - Level 1 : Kasie, kasiepenagihan@pkt.com, 14
            - Level 2 : Kabag, kabagpenagihan@pkt.com, 15
            - Level 3 : Manager, managerkeuangan@pkt.com, 2
        */
        $user_id = Auth::user()->ID;

        $result = self::init()->db->where('ID', $user_id)->get('Ms_User')->row();
        if (empty($result)) {
            return null;
        } else {
            return $result->RoleID;
        }
    }

    /*
        1. Administrator - mengelola administrasi aplikasi
        2. IT Support - Maintaince sistem aplikasi
        3. Dept. Keuangan - Input data penagihan ke Customer, pengelolaan piutang, dan pengelola kas besar
        4. Dept. Akuntansi - Memverifikasi dokumen invoice dari supplier
        5. Dept. PPBJ - Input data invoice dari supplier yang tergolong PO Barang
        6. Unit Kerja - Input data invoice dari supplier yang tergolong PO Jasa
    */

    public static function ldap_auth($username, $passwd)
    {
        $ldap_user = $username; //'quizit'; //'ldap-user';
        $ldap_paswd = $passwd; //'rahasiadong:P'; //'ld4pu5er#?';
        $ldap_server = '12.7.2.19';

        $ldap_conn = ldap_connect($ldap_server) or die('Cannot connect to LDAP server');

        if ($ldap_conn) {
            $ldap_bind = @ldap_bind($ldap_conn, $ldap_user, $ldap_paswd);

            if ($ldap_bind) {
                //print 'LDAP bind successful<br />';

                $search = @ldap_search($ldap_conn, 'o=pkt', 'uid=0703666', array('displayname', 'mail'));
                //print_r($search);
                //print '<br />';

                $count_entries = @ldap_count_entries($ldap_conn, $search);
                //print $count_entries;
                //print '<br />';

                $first_entry = ldap_first_entry($ldap_conn, $search);
                //print '<br />';

                $user_dn = ldap_get_dn($ldap_conn, $first_entry);
                //print $user_dn;

                $entries = @ldap_get_entries($ldap_conn, $search);
                //print_r($entries);
                return true;
            } else {
                //print 'LDAP bind failed. ' . @ldap_error($ldap_conn);
                return false;
            }

            return false;
            @ldap_close($ldap_conn);
        }
    }
}
