<?php
class Filter {

    public static function dxDataGridFilters($filters, $query)
    {

        if(in_array('and',$filters)) {

            $filters = ($filters);
                	
            if( !empty($filters) ) {
                foreach($filters as $filter) {
                    if(is_array($filter)) {

                        self::doFilter($filter, $query);
                    }
                }    
            }    
        } else {
            $filter = ($filters);
            if(is_array($filter)) {
                self::doFilter($filter, $query);
            }
        }
    }

    private static function doFilter($filter, $query)
    {
        if($filter[1] == 'contains') {
            $query->like($filter[0], $filter[2]);
        } elseif($filter[1] == 'notcontains') {
            $query->not_like($filter[0], $filter[2]);
        } elseif($filter[1] == 'startswith') {
            $query->like($filter[0], $filter[2],'after');
        } elseif($filter[1] == 'endswith') {
            $query->like($filter[0], $filter[2],'before');
        } elseif($filter[1] == '=') {
            $query->where($filter[0], $filter[2]);
        } elseif($filter[1] == '<>') {
            $query->where($filter[0].' !=', $filter[2]);
        } elseif($filter[1] == '>') {
            $query->where($filter[0].' >', $filter[2]);
        } elseif($filter[1] == '<') {
            $query->where($filter[0].' <', $filter[2]);
        }  elseif($filter[1] == '>=') {
            $query->where($filter[0].' >', $filter[2]);
        } elseif($filter[1] == '<=') {
            $query->where($filter[0].' <', $filter[2]);
        } else {
            $query->where($filter[0], $filter[2]);        
        }                
    }
    public static function areaFilter($primary_table,$query, $option = 'default')
    {
        $area = Auth::unitKerja()->Area;
        if($area=='Pusat'){
            $query->group_start(); // Open group
            $query->where("$primary_table.IDArea",1);
            $query->or_where("$primary_table.IDArea",2);
            $query->or_where("$primary_table.IDArea",3);
            $query->group_end(); // end group
        }elseif($area=='Dept. PSO 1'){
            $query->group_start(); // Open group
            $query->where("$primary_table.IDArea",10);
            if($option == 'default'){
                //$query->or_where("$primary_table.IDArea",1);
            }else{
                $query->or_where("$primary_table.IDArea",1);
            }
            $query->group_end(); // end group
        }else{
            $query->group_start(); // Open group
            $query->where("$primary_table.IDArea",20);
            
            if($option == 'default'){
                //$query->or_where("$primary_table.IDArea",2);
            }else{
                $query->or_where("$primary_table.IDArea",2);
            }
            $query->group_end(); // end group
        }              
    }
}