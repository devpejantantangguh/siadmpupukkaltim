<?php 

/**
* 
*/
class Log
{

    private static $filename = 'pkt.log';

    public static function writeLog($type, $message)
    {
        $message = is_array($message) || is_object($message) ? serialize($message) : $message;
        $message = $type . " [" . date('Y-m-d H:i:s') . "] " . $message;
        file_put_contents(base_path('application/logs/').static::$filename, $message.PHP_EOL, FILE_APPEND);
    }

    public static function info($message)
    {
        self::writeLog('info', $message);
    }

    public static function error($message)
    {
        self::writeLog('error', $message);
    }

    public static function debug($message)
    {
        self::writeLog('debug', $message);
    }
}
