@extends('templates.admin')

@section('heading')
{{-- code/link styleshet here --}}
@endsection

@section('contentheader')
<section class="content-header">
    <h1>Roles</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>User Setting</li>
        <li class="active">Roles</li>
    </ol>
</section>
@endsection

@section('title', 'Add Role')

@section('content')
<div class="box-header with-border">
    <h3 class="box-title">Add Roles</h3>
</div>
<form action="{{ url('user_settings/roles/store') }}" method="POST" class="form-horizontal">
  <div class="box-body">
    @if(flashdata('success'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error!</strong> {{ flashdata('error') }}
        </div>

        @elseif(flashdata('warning'))

        <div class="alert alert-warning">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Warning!</strong> {{ flashdata('warning') }}
        </div>

        @elseif(flashdata('info'))

        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Info!</strong> {{ flashdata('info') }}
        </div>
        @endif
        <div class="form-group">
            <div class="col-md-2"><label>Role Actor</label></div>
            <div class="col-md-6">
                <input type="text" name="role" class="form-control" value="" required>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-6">
                <div class="widget-box">
                    <div class="widget-title">
                        <span class="icon">
                            <i class="icon-eye-open"></i>
                        </span>
                        <h5>Pilih untuk memberikan hak akses</h5>
                    </div>

                        <div class="widget-content nopadding">
                            <table class="table table-bordered">
                                <tbody>
                                    <tr>
                                        <td><strong>Invoice</strong></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><span class="fa fa-table"></span> Input Invoice</td>
                                        <td><input type="checkbox" name="Invoice[1]" value="1" ></td>
                                    </tr>
                                    <tr>
                                        <td><span class="fa fa-table"></span> Submit</td>
                                        <td><input type="checkbox" name="Invoice[3]" value="1" ></td>
                                    </tr>

                                    <tr>
                                        <td><span class="fa fa-table"></span> Approval</td>
                                        <td><input type="checkbox" name="Invoice[2]" value="1" ></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Akuntansi</strong></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><span class="fa fa-align-justify" style="text-indent: 25px;"></span> Invoice Tracking</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td><span class="fa fa-align-justify" style="text-indent: 25px;"></span> List & Verifikasi Invoice</td>
                                        <td><input type="checkbox" name="Akuntansi[1]" value="1" ></td>
                                    </tr>
                                    <tr>
                                        <td><span class="fa fa-align-justify" style="text-indent: 25px;"></span> Kasir</td>
                                        <td><input type="checkbox" name="Akuntansi[2]" value="1" ></td>
                                    </tr>
                                    <tr>
                                        <td><span class="fa fa-align-justify" style="text-indent: 25px;"></span> All Invoice</td>
                                        <td><input type="checkbox" name="Akuntansi[3]" value="1" ></td>
                                    </tr>
                                    <tr>
                                        <td><span class="fa fa-table"></span> Kurs</td>
                                        <td><input type="checkbox" name="Akuntansi[4]" value="1" ></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Keuangan</strong></td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td><span class="fa fa-table"></span> Collecting</td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td><span class="fa fa-align-justify" style="text-indent: 25px;"></span> Upload Billing Document</td>
                                        <td><input type="checkbox" name="Keuangan[1]" value="1" ></td>
                                    </tr>
                                    <tr>
                                        <td><span class="fa fa-align-justify" style="text-indent: 25px;"></span> List Billing Document</td>
                                        <td><input type="checkbox" name="Keuangan[2]" value="1" ></td>
                                    </tr>
                                    <tr>
                                        <td><span class="fa fa-align-justify" style="text-indent: 25px;"></span> Approval Billing Document</td>
                                        <td><input type="checkbox" name="Keuangan[3]" value="1" ></td>
                                    </tr>
                                    <tr>
                                        <td><span class="fa fa-align-justify" style="text-indent: 25px;"></span> Upload Sales Order</td>
                                        <td><input type="checkbox" name="Keuangan[4]" value="1" ></td>
                                    </tr>
                                    <tr>
                                        <td><span class="fa fa-align-justify" style="text-indent: 25px;"></span> List Sales Order</td>
                                        <td><input type="checkbox" name="Keuangan[5]" value="1" ></td>
                                    </tr>
                                    <tr>
                                        <td><span class="fa fa-table"></span> Invoice Tracking</td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td><span class="fa fa-align-justify" style="text-indent: 25px;"></span> Upload List Piutang</td>
                                        <td><input type="checkbox" name="Keuangan[6]" value="1" ></td>
                                    </tr>
                                    <tr>
                                        <td><span class="fa fa-align-justify" style="text-indent: 25px;"></span> List Piutang</td>
                                        <td><input type="checkbox" name="Keuangan[7]" value="1" ></td>
                                    </tr>
                                    <tr>
                                        <td><span class="fa fa-align-justify" style="text-indent: 25px;"></span> Verifikasi (Apply Invoice)</td>
                                        <td><input type="checkbox" name="Keuangan[8]" value="1" ></td>
                                    </tr>
                                    <tr>
                                        <td><span class="fa fa-align-justify" style="text-indent: 25px;"></span> Pembayaran</td>
                                        <td><input type="checkbox" name="Keuangan[9]" value="1" ></td>
                                    </tr>
                                    <tr>
                                        <td><span class="fa fa-align-justify" style="text-indent: 25px;"></span> List All Invoice</td>
                                        <td><input type="checkbox" name="Keuangan[10]" value="1" ></td>
                                    </tr>
                                    <tr>
                                        <td><span class="fa fa-table"></span> Kas Besar</td>
                                        <td><input type="checkbox" name="Keuangan[11]" value="1" ></td>
                                    </tr>
                                    <tr>
                                        <td><strong>Master Data</strong></td>
                                        <td><input type="checkbox" name="MasterData[1]" value="1" ></td>
                                    </tr>
									<tr>
                                        <td><strong>Misc. </strong></td>
                                        <td></td>
                                    </tr>
									<tr>
                                        <td><span class="fa fa-align-justify" style="text-indent: 25px;"></span> Pesan</td>
                                        <td><input type="checkbox" name="Misc[1]" value="1" ></td>
                                    </tr>
									<tr>
                                        <td><span class="fa fa-align-justify" style="text-indent: 25px;"></span> Panduan</td>
                                        <td><input type="checkbox" name="Misc[2]" value="1" ></td>
                                    </tr>
                                </tbody>
							</table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
        @endsection

        @section('script')
        {{-- code/script here --}}
        @endsection
