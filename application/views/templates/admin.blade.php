<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{ optional($title) }} @yield('title') - MitraKu</title>
        <link id="favicon" rel="shortcut icon" href="{{ url('asset/image/favicon.ico') }}" type="image/png" />
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <script src="{{url('/asset/')}}js/jquery.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.2/jszip.min.js"></script>
        <script type="text/javascript" src="{{url('/asset/')}}js/vue.js"></script>
        <script type="text/javascript" src="{{url('/asset/')}}js/axios.min.js"></script>
        @yield('heading')
        <link rel="stylesheet" href="{{url('/asset/')}}css/select2.min.css" />
        <link rel="stylesheet" href="{{url('/asset/')}}css/bootstrap.min.css">
        <link rel="stylesheet" href="{{url('/asset/')}}css/bootstrap-datepicker.min.css" />
        <link rel="stylesheet" href="{{url('/asset/')}}font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="{{url('/asset/')}}ionicons/css/ionicons.min.css">
        <link rel="stylesheet" type="text/css" href="{{ url('asset/css/menu.css') }}">
        <link rel="stylesheet" href="{{url('/asset/')}}css/AdminLTE.css">
        <link rel="stylesheet" href="{{url('/asset/')}}css/_all-skins.min.css">
        <style type="text/css">
            .dx-dialog-message {text-align: center;}
            .dx-toolbar.dx-widget.dx-visibility-change-handler.dx-collection.dx-popup-title .dx-toolbar-before {
                margin: 0 auto;
                height: 100%;
                text-align: center;
                position: relative;
            }
            .skin-blue .main-header .navbar {background-color: #2067a8;}
            .skin-blue .main-header li.user-header {background-color: #2067a8;}
			.swal2-popup {font-size: 1.6rem !important;}
        </style>
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic"> -->
    </head>
    <!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
    <body class="hold-transition skin-blue layout-top-nav dx-viewport">
        <div class="wrapper" id="app">
            <header class="main-header">
                <nav class="navbar navbar-fixed-top">
                    <div class="container">
                        <div class="navbar-header">
                            <a href="/" class="navbar-brand">
                                <img src="{{ url('asset/image/logo_header.png') }}" style="width: 120px">
                            </a>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                            <i class="fa fa-bars"></i>
                            </button>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                            <ul class="nav navbar-nav">
                                <li><a href="{{ url('dashboard') }}"><i class="fa fa-home"></i> Home</a></li>
                                <!--  -->
                                @if(Auth::permission('MasterData'))
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i> User Settings <span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ url('admin/master/users') }}">Users</a></li>
                                        <li><a href="{{ url('user_settings/roles') }}">Roles</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user-circle"></i> Admin <span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ url('admin/setting') }}">Global Settings</a></li>
                                        <li><a href="{{ url('admin/hash') }}">Reset Password</a></li>
                                        <li><a href="{{ url('admin/message') }}"> Pesan</a></li>
                                        <li><a href="{{ url('admin/panduan') }}"> Panduan</a></li>
                                        <li><a href="{{ url('admin/news') }}"> News</a></li>
                                        <li class="dropdown-submenu">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Master Data</a>
                                            <ul class="dropdown-menu">
                                                <li><a href="{{ url('admin/master/country') }}">Country</a></li>
                                                <li><a href="{{ url('admin/master/propinsi') }}">Propinsi</a></li>
                                                <li><a href="{{ url('admin/master/kota') }}">Kota</a></li>
                                                <li><a href="{{ url('admin/master/material') }}">Material</a></li>
                                                <li><a href="{{ url('admin/master/customer') }}">Customer</a></li>
                                                <li><a href="{{ url('admin/master/vendor') }}">Vendor</a></li>
                                                <li><a href="{{ url('admin/master/karyawan') }}">Karyawan</a></li>
                                                <li><a href="{{ url('admin/master/unitkerja') }}">Unit Kerja</a></li>
                                                <li><a href="{{ url('admin/master/users') }}">Users</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                @endif
								@if(Auth::permission('Misc'))
								<li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-wrench"></i> Misc. <span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
										@if(Auth::permission('Misc',1))
                                        <li><a href="{{ url('admin/message') }}"> Pesan</a></li>
										@endif
										@if(Auth::permission('Misc',2))
                                        <li><a href="{{ url('admin/panduan') }}"> Panduan</a></li>
										@endif
                                    </ul>
                                </li>
								@endif
                                @if(Auth::permission('Keuangan'))
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-money"></i> Keuangan <span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                            @if(Auth::permission('Keuangan','1') || Auth::permission('Keuangan','2') || Auth::permission('Keuangan','3') || Auth::permission('Keuangan','4') || Auth::permission('Keuangan','5'))
                                                <li class="dropdown-submenu">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Collecting</a>
                                                    <ul class="dropdown-menu">
                                                        @if(Auth::permission('Keuangan','1'))
                                                            <li><a href="{{ url('keuangan/collecting/upload') }}"><i class="fa fa-upload"></i> Upload Billing Document</a></li>
                                                        @endif
                                                        @if(Auth::permission('Keuangan','4'))
                                                            <li><a href="{{ url('keuangan/collecting/salesdoc') }}"><i class="fa fa-upload"></i> Upload Sales Document</a></li>
                                                        @endif
                                                        @if(Auth::permission('Keuangan','1'))
                                                            <li><a href="{{ url('keuangan/collecting/cancel') }}"><i class="fa fa-close"></i> Cancel Billing Document</a></li>
                                                        @endif
                                                        {{-- @if(Auth::permission('Keuangan','2'))
                                                            <li><a href="{{ url('keuangan/collecting/cancelso') }}"><i class="fa fa-close"></i> Cancel Sales Order</a></li>
                                                        @endif --}}
                                                        @if(Auth::permission('Keuangan','2'))
                                                        <li><a href="{{ url('keuangan/collecting/lists') }}"><i class="fa fa-list"></i> List Billing Document</a></li>
                                                        @endif
                                                        @if(Auth::permission('Keuangan','5'))
                                                        <li><a href="{{ url('keuangan/collecting/salesorder') }}"><i class="fa fa-list"></i> List Sales Order</a></li>
                                                        @endif
                                                        @if(Auth::permission('Keuangan','3'))
                                                        <li><a href="{{ url('keuangan/collecting/approval') }}"><i class="fa fa-check-square"></i>Approval Billing Document</a></li>
                                                        @endif
                                                    </ul>
                                                </li>
                                            @endif
                                            @if(Auth::permission('Keuangan','6') || Auth::permission('Keuangan','7') || Auth::permission('Keuangan','8') || Auth::permission('Keuangan','9') || Auth::permission('Keuangan','10'))
                                                <li class="dropdown-submenu">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Invoice Tracking</a>
                                                    <ul class="dropdown-menu">
                                                        @if(Auth::permission('Keuangan','6'))
                                                        <li><a href="{{ url('keuangan/invoice/upload') }}">Upload List Piutang</a></li>
                                                        @endif
                                                        @if(Auth::permission('Keuangan','7'))
                                                        <li><a href="{{ url('keuangan/invoice/lists') }}">List Piutang</a></li>
                                                        @endif
                                                        @if(Auth::permission('Keuangan','8'))
                                                        <li><a href="{{ url('keuangan/invoice/verifikasi') }}">Verifikasi (Invoice Apply)</a></li>
                                                        @endif
                                                        @if(Auth::permission('Keuangan','9'))
                                                        <li><a href="{{ url('keuangan/invoice/pembayaran') }}">Pembayaran</a></li>
                                                        @endif
                                                        @if(Auth::permission('Keuangan','10'))
                                                        <li><a href="{{ url('invoice/invoice/all') }}">All Invoice</a></li>
                                                        @endif
                                                    </ul>
                                                </li>
                                            @endif
                                                @if(Auth::permission('Keuangan','11'))
                                                <li class="dropdown-submenu">
                                                    <li><a href="{{ url('keuangan/kas_besar/kasbesar/index') }}">Kas Besar</a></li>
                                                </li>
                                                @endif
                                        </ul>
                                    </li>
                                @endif
                                @if(Auth::permission('Akuntansi'))
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-area-chart"></i> Akuntansi <span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                            @if(Auth::permission('Akuntansi','1') || Auth::permission('Akuntansi','2') || Auth::permission('Akuntansi','3'))
                                            <li class="dropdown-submenu">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Invoice Tracking</a>
                                                <ul class="dropdown-menu">
                                                    @if(Auth::permission('Akuntansi','1'))
                                                    <li><a href="{{ url('akuntansi/invoice/lists/index') }}">List &amp; Verifikasi Invoice</a></li>
                                                    @endif
                                                    @if(Auth::permission('Akuntansi','2'))
                                                    <li><a href="{{ url('akuntansi/invoice/kasir') }}">Kasir</a></li>
                                                    @endif
                                                    @if(Auth::permission('Akuntansi','3'))
                                                    <li><a href="{{ url('invoice/invoice/all') }}">All Invoice</a></li>
                                                    @endif
                                                </ul>
                                            </li>
                                            @endif
                                            @if(Auth::permission('Akuntansi','4'))
                                            <li class="dropdown-submenu">
                                                <li><a href="{{ url('akuntansi/invoice/kurs') }}">Kurs</a></li>
                                            </li>
                                            @endif
                                        </ul>
                                    </li>
                                @endif
                                @if(Auth::permission('Invoice'))
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-file-text-o"></i> Invoice <span class="caret"></span></a>
                                        <ul class="dropdown-menu" role="menu">
                                            @if(Auth::permission('Invoice','2'))
                                            <li><a href="{{ url('invoice/approval') }}">Approval Invoice</a></li>
                                            @endif
                                            @if(Auth::permission('Invoice','3'))
                                            <li><a href="{{ url('invoice/submit') }}">Submit Invoice</a></li>
                                            @endif
                                            @if(Auth::permission('Invoice','1'))
                                            <li><a href="{{ url('invoice/invoice') }}">Input Invoice</a></li>
                                            @endif
                                        </ul>
                                    </li>
                                @endif
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                        <!-- Navbar Right Menu -->
                        <div class="navbar-custom-menu">
                            <ul class="nav navbar-nav">
                                <!-- User Account Menu -->
                                <li class="dropdown user user-menu">
                                    <!-- Menu Toggle Button -->
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <!-- The user image in the navbar-->
                                        <img src="{{url('/asset/')}}image/user.png" class="user-image" alt="">
                                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                                        <span class="hidden-xs">{{ @Auth::unitKerja()->Nama }}</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <!-- The user image in the menu -->
                                        <li class="user-header">
                                            <img src="{{url('/asset/')}}image/user.png" class="img-circle" alt="User Image">
                                            <p>
                                                {{ @Auth::user()->Email }}
                                                <small>{{ @Auth::UnitKerja()->UnitKerja }} ({{ @Auth::UnitKerja()->CostCenter }})<br/>
                                                    {{ @Auth::user()->RoleName }}
                                            </small>
                                                <!-- <small>Member since Nov. 2012</small> -->
                                            </p>
                                        </li>
                                        <!-- Menu Footer-->
                                        <li class="user-footer">
                                            <div class="pull-left">
                                                <a href="{{ url('profile/reset_password') }}" class="btn btn-default btn-flat">Reset Password</a>
                                            </div>
                                            <div class="pull-right">
                                                <a href="{{ url('logout') }}" class="btn btn-default btn-flat">Sign out</a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>

            <div class="content-wrapper">
                <div class="container">
                    <div class="content-wrapper">
                        <div class="container">
                            @yield('contentheader')
                            <section class="content">
                                <!-- <div class="callout callout-info">
                                    <h4>Tip!</h4>
                                    <p>Add the layout-top-nav class to the body tag to get this layout. This feature can also be used with a sidebar! So use this class if you want to remove the custom dropdown menus from the navbar and use regular links instead.</p>
                                </div>
                                <div class="callout callout-danger">
                                    <h4>Warning!</h4>
                                    <p>The construction of this layout differs from the normal one. In other words, the HTML markup of the navbar and the content will slightly differ than that of the normal layout.</p>
                                </div> -->
                                <div class="box box-default">
                                    @yield('content')
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="main-footer">
                <div class="container">
                    <div class="col-md-4">
                        Copyright &copy; {{ date('Y') }} PT Pupuk Kaltim
                    </div>
                    <div class="col-md-4">
                        <center>
                            <img src="{{ url('asset/image/logo_small.png') }}">
                        </center>
                    </div>
                    <div class="col-md-4">
                        <div class="pull-right hidden-xs">
                            v1.0.0
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <script src="{{url('/asset/')}}js/bootstrap.min.js"></script>
        <script src="{{url('/asset/')}}js/jquery.slimscroll.min.js"></script>
        <script src="{{url('/asset/')}}js/fastclick.min.js"></script>
        <script src="{{url('/asset/')}}js/adminlte.min.js"></script>
        <script src="{{url('/asset/')}}js/select2.full.min.js"></script>
        <script src="{{url('/asset/')}}js/jquery.mask.min.js"></script>
        <script src="{{url('/asset/')}}js/jquery.inputmask.bundle.js"></script>
        <script src="{{url('/asset/')}}js/bootstrap-datepicker.min.js"></script>
        <script src="{{url('/asset/')}}js/swal2.js"></script>
        <script type="text/javascript">
            $('form').submit(function() {
                $(this).find("button[type='submit']").prop('disabled',true);
            });
        </script>
        @yield('script')
    </body>
</html>
