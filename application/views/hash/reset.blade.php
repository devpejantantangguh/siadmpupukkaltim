@extends('templates.admin')

@section('title','Reset Password')

@section('contentheader')
	<section class="content-header">
	    <h1>Home</h1>
	    <ol class="breadcrumb">
	        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    </ol>
	</section>
@endsection

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">Reset Password</h3>
        <span class="pull-right">Area: {{Auth::unitKerja()->Area}}</span>
    </div>
    <div class="box-body">
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {{ flashdata('error') }}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> {{ flashdata('warning') }}
            </div>

        @elseif(flashdata('info'))
            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Info!</strong> {{ flashdata('info') }}
            </div>
        @endif

		<form action="{{ url('admin/hash/update') }}" method="POST">
			<div class="form-group">
				<label for="">User</label>
				{!! form_dropdown('user_id', $users,null, ['class' => 'form-control select2']) !!}
			</div>
			<div class="form-group">
				<label for="">Password</label>
				<input type="password" value="" name="password" class="form-control">
			</div>
			<div class="form-group">
				<label for="">Password Confirmation</label>
				<input type="password" value="" name="password_confirmation" class="form-control">
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary">Reset</button>
			</div>
		</form>

    </div>
@endsection

@section('script')
	<script>
		$(function () {
			$('.select2').select2();
		})
	</script>
@endsection
