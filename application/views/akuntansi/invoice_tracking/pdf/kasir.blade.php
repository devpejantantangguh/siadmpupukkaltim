<!DOCTYPE html>
<html>
<head>
    <title>Print Transmittal</title>
    <style type="text/css">
    body {font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;}
        #fp {
            
            border-collapse: collapse;
            width: 100%;
        }

        #fp td, #fp th {
            border: 1px solid #777;
            padding: 8px;
        }
    </style>
</head>
<body style="font-size: 12px">
    <div class="container" style="width:700px;margin:auto;padding:10px">
        <table style="font-size:13px;width: 100%">
            <tr>
                <td style="width:200px">TRANSMITTAL DOKUMEN</td>
                <td style="width:250px">: <h1 style="display: inline;">{{ $row['NoTransmittal'] }}</h1></td>
            </tr>
            <tr>
                <td>JUMLAH DOKUMEN</td>
                <td>: <h2 style="display: inline;font-weight: normal;">{{ $row['Num'] }} DOKUMEN</h2></td>
            </tr>
            <tr>
                <td>TANGGAL TRANSMITTAL</td>
                <td>:  <h2 style="display: inline;font-weight: normal;">{{ date('d-m-Y', strtotime($row['TglTransmittal'])) }}</h2></td>
                <td></td>
                <td align=right>Printed By : {{ $row['Nama'] }}</td>
            </tr>
        </table>

        <br>

        <table border="1" style="width: 100%" id="fp">
            <tr>
                <td style="text-align: center;width: 10px">No</td>
                <td style="text-align: center;">No Invoice</td>
                <td style="text-align: center;width: 65px">Tgl Invoice</td>
                <td style="text-align: center;">Kode Vendor</td>
                <td style="text-align: center;">Nama Vendor</td>
                <td style="text-align: center;">Nilai Invoice</td>
                <td style="text-align: center;">Kurs</td>
                <td style="text-align: center;">No Dok SAP</td>
            </tr>
            <?php 
$i = 0;
foreach ($data as $value) { 
    $i++;
    ?>
            <tr style="vertical-align: top">
                <td>{{$i}}</td>
                <td style="text-align: left">{{ $value->NoInvoice }}</td>
                <td style="text-align: left">{{ date('d-m-Y', strtotime($value->TglInvoice)) }}</td>
                <td style="text-align: left">{{ $value->IDVendor }}</td>
                <td style="text-align: left">{{ $value->NamaVendor }}</td>
                <td>
                    
                    <div style="float:right;text-align:right">
                        {{ number_format($value->NilaiInvoice,0,null,'.') }}
                    </div>
                </td>
                <td>
                    <div style="float:left;width:50%">
                        {{ $value->KursInvoice }} 
                    </div>
                </td>
                
                <td style="text-align: left">{{ $value->NoDokumenSAP }}</td>
            </tr>
<?php } ?>
        </table>


    </div>
</body>
</html>
