@extends('templates.admin')

@section('title','View WUM')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
    <script src="{{ url('asset/js/phpjs.js') }}"></script>
    <style type="text/css">
        h3.panel-title{font-size: 14px !important}
        .dx-fileuploader-show-file-list .dx-fileuploader-files-container{padding-top:0px}
        .dx-fileuploader-wrapper{padding:0px;}
        .form-horizontal .control-label{text-align: left !important;padding-left:20px;}
    </style>
@endsection

@section('contentheader')
    <section class="content-header">
        <h1>Kasir</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Invoice Tracking</li>
            <li class="active">Kasir</li>
        </ol>
    </section>
@endsection

@section('title','Kasir (Web Uang Muka)')

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">Kasir (Web Uang Muka)</h3>
    </div>

    <div class="box-body">

        <div role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a>
                </li>
                <li role="presentation">
                    <a href="#history" aria-controls="history" role="tab" data-toggle="tab">History</a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content" style="padding-top: 10px">
                <div role="tabpanel" class="tab-pane active" id="details">
                     <form action="{{ url('akuntansi/invoice/wum/update/'.$id) }}" method="POST">
                        @if(isset($_GET['update']) && $_GET['update'] == 'checked')
                            <div class="alert alert-success">
                                <p>Dokumen Berhasil di Checked</p>
                            </div>
                        @elseif(isset($_GET['update']) && $_GET['update'] == 'reject')
                            <div class="alert alert-warning">
                                <strong>Reject</strong> Dokumen Berhasil di Reject
                            </div>
                        @elseif(isset($_GET['update']) && $_GET['update'] == 'fail')
                            <div class="alert alert-danger">
                                <strong>Error</strong> You did't allow to update this document
                            </div>
                        @endif

                        <div class="panel panel-default form-horizontal">
                            <div class="panel-heading">
                                <h3 class="panel-title">Detail Dokumen</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Jenis Transaksi</label>
                                    <div class="col-sm-3">
                                        {!! form_dropdown('JenisTransaksi', $ls_transaksi,$data->JenisTransaksi, ['class' => 'form-control s2-jenis-transaksi', 'disable']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">NPK Requester</label>
                                    <div class="col-sm-3">
                                        {!! form_dropdown('IDVendor', $workers,$data->IDVendor, ['class' => 'form-control s2-npk-requester','id' => 'IDVendor', 'disable']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nama Karyawan</label>
                                    <div class="col-sm-3">
                                        <input type="text" name="NamaVendor" class="form-control" id="name" value="{{ $data->NamaVendor }}" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Cost Center</label>
                                    <div class="col-sm-3">
                                        <input type="text" name="NoDokumen" class="form-control" id="cost-center" value="{{ $data->NoDokumen }}" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">No. Dokumen</label>
                                    <div class="col-sm-3">
                                        <input type="text" name="NoInvoice" class="form-control" value="{{ $data->NoInvoice }}" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nilai. Dokumen</label>
                                    <div class="col-sm-3">
                                        <input type="text" name="NilaiInvoice" class="form-control" value="{{ number_format($data->NilaiInvoice,2,',','.') }}" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Kurs</label>
                                    <div class="col-sm-2">
                                        {!! form_dropdown('KursInvoice', $ls_kurs, $data->KursInvoice, ['class' => 'form-control s2-kurs', 'disable']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Payment Term</label>
                                    <div class="col-sm-2">
                                        {!! form_dropdown('PaymentTerm', $payment_terms, $data->PaymentTerm, ['class' => 'form-control s2-term','id' => 'p-term', 'disable']) !!}
                                    </div>
                                    <div class="col-sm-8">
                                        <span id="est">{{ $time }}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Keterangan</label>
                                    <div class="col-sm-3">
                                        <textarea name="keterangan" class="form-control" rows="5" readonly=""></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default form-horizontal">
                            <div class="panel-heading">
                                <h3 class="panel-title">Kelengkapan Dokumen</h3>
                            </div>
                            <div class="panel-body">
                                <div class="checkbox">
                                    <label>
                                        {!! form_checkbox('documents[1]', 'on', isset($cb[1]) ? true : false) !!} Form Ijin Prinsip
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        {!! form_checkbox('documents[2]', 'on', isset($cb[2]) ? true : false) !!} Form Internal
                                    </label>
                                </div>
                                <br>
                            </div>
                        </div>

                        <center>
                            <a onclick="window.open(document.referrer,'_self')"  class="btn btn-primary btn-sm" style="color:#fff">
                                Back
                            </a>
                        </center>
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane" id="history">
                    <table class="table table-condensed table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Action</th>
                                <th>User</th>
                                <th>Keterangan</th>
                                <th>Timestamp</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($histories as $key => $history)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $history->Action }}</td>
                                <td>{{ $history->Nama }}</td>
                                <td>{{ $history->Keterangan }}</td>
                                <td>{{ date('d-m-Y H:i:s', strtotime($history->Timestamp)) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){

            $('form').submit(function(e){

                e.preventDefault();
                var form = this;
                var result = DevExpress.ui.dialog.confirm("Apakah anda yakin?", "Confirm");
                    result.done(function (dialogResult) {
                        console.log(dialogResult);
                        if(dialogResult) {
                            $(form).unbind('submit');
                            $(form).find("button[type='submit']").prop('disabled',false);
                            $('.submit').click();
                        }else{
                            $(form).find("button[type='submit']").prop('disabled',false);
                        }
                });

            });

            $('.s2-jenis-transaksi').select2({placeholder:'Pilih Jenis Transaksi',minimumResultsForSearch:-1}).prop("disabled", true);
            $('.s2-npk-requester').select2({placeholder:'Pilih NPK Requester',minimumResultsForSearch:-1}).prop("disabled", true);
            $('.s2-term').select2({placeholder:'Pilih Term',minimumResultsForSearch:-1}).prop("disabled", true);
            $('.s2-kurs').select2({placeholder:'Pilih Kurs',minimumResultsForSearch:-1}).prop("disabled", true);

            $('#IDVendor').change(function(){
                var id = $('#IDVendor').val();
                $.post("{{ url('akuntansi/invoice/wum/ajax') }}",{id:id})
                    .done(function(data){
                        $('#name').val(data.Nama);
                        $('#cost-center').val(data.CostCenter);
                    }).fail(function(){
                        alert('Something Wrong');
                    })
            })

            $('#p-term').change(function(){
                var id = $('#p-term').val();
                $.post("{{ url('akuntansi/invoice/wum/get_payment_term_detail') }}",{id:id})
                    .done(function(data){
                        $('#est').html(data);
                    }).fail(function(){
                        alert('Something Wrong');
                    })
            })
        })
    </script>
@endsection
