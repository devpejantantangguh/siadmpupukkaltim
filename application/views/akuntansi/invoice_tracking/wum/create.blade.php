@extends('templates.admin')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
    <script src="{{ url('asset/js/phpjs.js') }}"></script>
    <style type="text/css">
        h3.panel-title{font-size: 14px !important}
        .dx-fileuploader-show-file-list .dx-fileuploader-files-container{padding-top:0px}
        .dx-fileuploader-wrapper{padding:0px;}
        .form-horizontal .control-label{text-align: left !important;padding-left:20px;}
    </style>
@endsection

@section('contentheader')
    <section class="content-header">
        <h1>Kasir</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Invoice Tracking</li>
            <li class="active">Kasir</li>
        </ol>
    </section>
@endsection

@section('title','Kasir (Web Uang Muka)')

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">Kasir (Web Uang Muka)</h3>
    </div>

    <div class="box-body">
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {{ flashdata('error') }}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> {{ flashdata('warning') }}
            </div>

        @elseif(flashdata('info'))

            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Info!</strong> {{ flashdata('info') }}
            </div>
        @endif
        <form action="{{ url('akuntansi/invoice/wum/store') }}" method="POST">

            @if(isset($_GET['update']) && $_GET['update'] == 'checked')
                <div class="alert alert-success">
                    <p>Dokumen Berhasil di Checked</p>
                </div>
            @elseif(isset($_GET['update']) && $_GET['update'] == 'reject')
                <div class="alert alert-warning">
                    <strong>Reject</strong> Dokumen Berhasil di Reject
                </div>
            @elseif(isset($_GET['update']) && $_GET['update'] == 'fail')
                <div class="alert alert-danger">
                    <strong>Error</strong> You did't allow to update this document
                </div>
            @endif

            <div class="panel panel-info form-horizontal">
                <div class="panel-heading">
                    <h3 class="panel-title">Detail Dokumen</h3>
                </div>
                <div class="panel-body">

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Jenis Transaksi</label>
                        <div class="col-sm-3">
                            {!! form_dropdown('JenisTransaksi', ['' => ''] + $ls_transaksi,null, ['class' => 'form-control s2-jenis-transaksi','required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">NPK Requester</label>
                        <div class="col-sm-3">
                            {!! form_dropdown('IDVendor', ['' => ''] + $workers,null, ['class' => 'form-control s2-npk-requester','id' => 'IDVendor', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Karyawan</label>
                        <div class="col-sm-3">
                            <input type="text" name="NamaVendor" class="form-control" id="name" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Cost Center</label>
                        <div class="col-sm-3">
                            <input type="text" name="NoDokumen" class="form-control" id="cost-center" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">No. Dokumen</label>
                        <div class="col-sm-3">
                            <input type="text" name="NoInvoice" class="form-control" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nilai. Dokumen</label>
                        <div class="col-sm-3">
                            <input type="text" name="NilaiInvoice" class="form-control" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Kurs</label>
                        <div class="col-sm-2">
                            {!! form_dropdown('KursInvoice', $ls_kurs, null, ['class' => 'form-control s2-kurs','required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Payment Term</label>
                        <div class="col-sm-2">
                            {!! form_dropdown('PaymentTerm', ['' => ''] + $payment_terms, null, ['class' => 'form-control s2-term','id' => 'p-term','required' => 'required']) !!}
                        </div>
                        <div class="col-sm-8">
                            <span id="est"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-3">
                            <textarea name="keterangan" class="form-control" rows="5"></textarea>
                        </div>
                    </div>

                </div>
            </div>

            <div class="panel panel-info form-horizontal">
                <div class="panel-heading">
                    <h3 class="panel-title">Kelengkapan Dokumen</h3>
                </div>
                <div class="panel-body">
                    <div class="checkbox">
                        <label>
                            {!! form_checkbox('documents[1]', 'on', false) !!} Form Ijin Prinsip
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            {!! form_checkbox('documents[2]', 'on', false) !!} Form Internal
                        </label>
                    </div>
                    <br>
                    <div class="form-group">
                        <label class="col-sm-5 control-label">Catatan Pemeriksaan : </label>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-5">
                            <textarea name="note" class="form-control" rows="5"></textarea>
                        </div>
                    </div>
                </div>
            </div>

            <center>
                <button type="submit" class="btn btn-success btn-sm submit">
                    <i class="fa fa-check"></i> Submit
                </button>
                <a href="{{ url('akuntansi/invoice/kasir?tab=wum') }}" class="btn btn-default btn-sm" style="color:#000">
                    <i class="fa fa-close"></i> Close
                </a>
            </center>
        </form>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){
            $('form').submit(function(e){

                e.preventDefault();
                var form = this;
                var result = DevExpress.ui.dialog.confirm("Apakah anda yakin?", "Confirm");
                    result.done(function (dialogResult) {
                        console.log(dialogResult);
                        if(dialogResult) {
                            $(form).unbind('submit');
                            $(form).find("button[type='submit']").prop('disabled',false);
                            $('.submit').click();
                        }else{
                            $(form).find("button[type='submit']").prop('disabled',false);
                        }
                });

            });

            $('.s2-jenis-transaksi').select2({placeholder:'Pilih Jenis Transaksi'});
            $('.s2-npk-requester').select2({placeholder:'Pilih NPK Requester'});
            $('.s2-term').select2({placeholder:'Pilih Term'});
            $('.s2-kurs').select2({placeholder:'Pilih Kurs'});

            $('input[name=NilaiInvoice]').inputmask('numeric', {
                groupSeparator: '.',
                radixPoint : ',',
                autoGroup: true,
                digits: 2,
                digitsOptional: false,
                placeholder: '0,00',
            });

            $('#IDVendor').change(function(){
                var id = $('#IDVendor').val();
                $.post("{{ url('akuntansi/invoice/wum/ajax') }}",{id:id})
                    .done(function(data){
                        $('#name').val(data.Nama);
                        $('#cost-center').val(data.CostCenter);
                    }).fail(function(){
                        alert('Something Wrong');
                    })
            })

            $('#p-term').change(function(){
                var id = $('#p-term').val();
                $.post("{{ url('akuntansi/invoice/wum/get_payment_term_detail') }}",{id:id})
                    .done(function(data){
                        $('#est').html(data);
                    }).fail(function(){
                        alert('Something Wrong');
                    })
            })
        })
    </script>
@endsection
