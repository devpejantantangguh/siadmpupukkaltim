@extends('templates.admin')

@section('heading')
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
<script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>

<script src="{{ url('asset/js/phpjs.js') }}"></script>
@endsection

@section('title','Verifikasi Invoice')

@section('contentheader')
<section class="content-header">
    <h1>Akuntansi</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Akuntansi</li>
        <li>Invoice Tracking</li>
        <li class="active">Verifikasi</li>
    </ol>
</section>
@endsection

@section('content')
<div class="box-header with-border">
    <h3 class="box-title">Verifikasi Invoice</h3>
    <span class="pull-right">Area: {{Auth::unitKerja()->Area}}</span>
</div>
<div class="box-body">
    <?php if(flashdata('tab')){ $tab = true; }else{ $tab = false;} ?>
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {{ flashdata('error') }}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> {{ flashdata('warning') }}
            </div>

        @elseif(flashdata('info'))

            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Info!</strong> {{ flashdata('info') }}
            </div>
        @endif
    <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#invoice_po" aria-controls="home" role="tab" data-toggle="tab">Invoice PO</a>
            </li>
            <li role="presentation">
                <a href="#web_uang_muka" aria-controls="tab" role="tab" data-toggle="tab">Web Uang Muka</a>
            </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="invoice_po" style="margin-top:15px">
                <div id="btn-po-open"></div>
                <div id="btn-po-transmittal"></div>
                <div id="invoice-po-grid"></div>
                <div id="buttonContainer"></div>
                <span class="label label-info" style="font-size:14px">Count : <span id="po_select">0</span></span>
            </div>
            <div role="tabpanel" class="tab-pane" id="web_uang_muka" style="margin-top:15px">
                <div id="btn-wum-open"></div>
                <div id="btn-wum-transmittal"></div>
                <div id="invoice-wum-grid"></div>
            </div>
        </div>
    </div>


</div>
@endsection

@section('script')
<script type="text/javascript">
    $(function(){
        var dataSource = new DevExpress.data.DataSource({
            key: "ID",
            load: function (loadOptions) {
                var d = $.Deferred()
                var params = {};
               // Getting filter options
               if (loadOptions.filter)  {
                   params.filter = JSON.stringify(loadOptions.filter);
               }
               //Getting sort options
               if (loadOptions.sort)  {
                   params.sort = JSON.stringify(loadOptions.sort);
               }
               //Getting group options
               if (loadOptions.group)  {
                   params.group = JSON.stringify(loadOptions.group);
               }
              //skip and take are used for paging
              params.skip = loadOptions.skip; //A number of records that should be skipped
              params.take = loadOptions.take; //A number of records that should be taken

               //If the select expression is specified
               if (loadOptions.select)  {
                   params.select= JSON.stringify(loadOptions.select);
               }

               //If a user typed something in dxAutocomplete, dxSelectBox or dxLookup
               if (loadOptions.searchValue)  {
                   params.searchValue= loadOptions.searchValue;
                   params.searchOperation= loadOptions.searchOperation;
                   params.searchExpr= loadOptions.searchExpr;
               }

              $.getJSON('{{ url("akuntansi/invoice/lists/get/".$Status."/po") }}', params).done(function (data) {
                    d.resolve(data.data, { totalCount: data.totalCount });
              });
              return d.promise();
            }
        });

        var btnPoOpen = $("#btn-po-open").dxButton({
            text: "Open",height: 34,width: 100,disabled: true,
            onClick: function () {
                $.each(dataGridPO.getSelectedRowsData(), function() {
                    location.href = "{{ url('akuntansi/invoice/lists/open/') }}" + this.ID;
                });
            }
        }).dxButton("instance");

        var btnPoTransmital = $("#btn-po-transmittal").dxButton({
            text: "Create Transmittal",height: 34,width: 150,disabled: true,
            onClick: function () {
                var result = DevExpress.ui.dialog.confirm("Apakah anda yakin?", "Confirm");
                    result.done(function (dialogResult) {
                        console.log(dialogResult);
                        if(dialogResult) {

                var lists = [];
                $.each(dataGridPO.getSelectedRowsData(), function() {
                    lists.push(this.ID);
                });
                $.post("{{ url('akuntansi/invoice/lists/transmittal') }}", {ids:lists})
                    .done(function(data){

                        dataGridPO.refresh();
                        var obj = JSON.parse(data);
                        console.log(obj);
                        if(obj.status == 200){
                            var result = DevExpress.ui.dialog.alert(obj.msg, "Print Transmittal");

                            result.done(function (dialogResult) {
                                if(dialogResult){
                                    window.open ("{{ url('akuntansi/invoice/lists/pdf_transmittal/') }}"+obj.number,"_blank");
                                }
                            });
                        }else{
                            DevExpress.ui.notify({
                            message: obj.msg,
                            position: {
                                my: "center top",
                                at: "center top"
                            }
                            }, "error", 3000);
                        }
                    });
                        }
                    });
            }
        }).dxButton("instance");

        var list_status = {!! json_encode($list_status) !!}
                var btnContainer = $("#buttonContainer").dxButton({
            text: "Get Count",
            onClick: function (e) {
                var filterExpr = $("#invoice-po-grid").dxDataGrid("instance").getCombinedFilter();
                dataSource.filter(filterExpr);
                dataSource.load().done(function (result) {
                    $('#po_select').html(result.length);
                    // for (i = 0; i < result.length; i++) {
                    //     console.log(result[i]);
                    // }
                });

            }
        }).dxButton("instance");

        function okejoss() {
            var filterExpr = $("#invoice-po-grid").dxDataGrid("instance").getCombinedFilter();
                dataSource.filter(filterExpr);
                dataSource.load().done(function (result) {
                    for (i = 0; i < result.length; i++) {
                        console.log(result[i]);
                    }
                });
        }

        var dataGridPO = $("#invoice-po-grid").dxDataGrid({
            dataSource : dataSource,

            paging: {
                pageSize: 20
            },
            remoteOperations: {sorting: true,paging: true,filtering:true},
            "export" : {enable:true, fileName : "Invoice_PO"},
            editing : {mode:'popup', allowUpdating : false,allowDeleting : false},
            selection:{mode:'multiple',showCheckBoxesMode:'always',selectAllMode:'page'},
            onSelectionChanged: function(data){
                btnPoOpen.option('disabled', !(data.selectedRowsData.length == 1))
                btnPoTransmital.option('disabled', !(data.selectedRowsData.length))
            },
            allowColumnReordering : true,
            allowColumnResizing : true,
            searchPanel : {visible : false},
            headerFilter:{visible:true},
            filterRow:{visible:true,applyFilter:'auto'},
            groupPanel : {visible:true},
            popup : {
                title:'Invoice_PO',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
            },
            pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: true,visible: true},
            columns : [
                // {dataField:'NoDokumen',caption:'No. Dokumen'},
                {dataField:'NoInvoice',caption:'No. Invoice'},
                {dataField:'IDVendor',caption:'Kode Vendor'},
                {dataField:'NamaVendor',caption:'Nama Vendor'},

                {dataField:'NilaiInvoice',caption:'Nilai Invoice',
                dataType: "number",calculateCellValue:function(data){
                    return data.KursInvoice + ' ' + number_format(data.NilaiInvoice,2,',','.')
                }},
                {dataField:'NilaiInvoice',
                dataType: "number",caption:'Nilai Verifikasi',calculateCellValue:function(data){
                    return data.KursInvoice + ' ' + number_format(data.NilaiVerifikasi,2,',','.')
                }},
                {dataField:'Status',caption:'Status',dataType:'integer',lookup:{dataSource: list_status, displayExpr: "name", valueExpr: "id"}},

                    {dataField:'IDArea',caption:'ID Area'},
                    {dataField:'Keterangan',caption:'Keterangan'},
                {dataField:'NoFakturPajak'},
                {dataField:'TglFakturPajak'},
                {dataField:'NoTransmittal',caption:'No. Transmittal'},
                {dataField:'TglTransmittal',caption:'Tgl. Transmittal'}
            ],

            onRowPrepared: function(e) {
                 console.log(e.rowType);
                if (e.rowType === 'data') {
                    console.log(e.data);
                    if(e.data.IsPriority == 1){
                        e.rowElement.css({ "background-color": "#fcd691" });
                    }
                }

            },
            columnAutoWidth: true,
            columnChooser: {enabled: true},
            columnFixing: {enabled: true}
        }).dxDataGrid("instance");

    });
</script>
<script type="text/javascript">

    $(function(){

        // Web Uang Muka

        var list_status = {!! json_encode($list_status) !!}
        var dataSource2 = new DevExpress.data.CustomStore({
            key: "ID",
            load: function (loadOptions) {
                var deferred = $.Deferred(),
                args = {};

                if (loadOptions.sort) {
                    args.orderby = loadOptions.sort[0].selector;
                    if (loadOptions.sort[0].desc)
                        args.orderby += " desc";
                }
                if (loadOptions.filter != 'undefined') {
                    args.filter = JSON.stringify(loadOptions.filter);
                }

                args.skip = loadOptions.skip || 0;
                args.take = loadOptions.take || 12;

                $.ajax({
                    url: "{{ url("akuntansi/invoice/lists/get_wum") }}",
                    data: args,
                    success: function(response) {
                        console.log(response);
                        deferred.resolve(response.data, { totalCount: response.totalCount });
                    },
                    error: function() {
                        deferred.reject("Data Loading Error");
                    },
                    timeout: 5000
                });

                return deferred.promise();
            }
        });

        var btnWumOpen = $("#btn-wum-open").dxButton({
            text: "Open",height: 34,width: 100,disabled: true,
            onClick: function () {
                $.each(dataGridWUM.getSelectedRowsData(), function() {
                    location.href = "{{ url('akuntansi/invoice/wum/edit/') }}" + this.ID;
                });
            }
        }).dxButton("instance");

        var btnWumTransmital = $("#btn-wum-transmittal").dxButton({
            text: "Create Transmittal",height: 34,width: 190,disabled: true,
            onClick: function () {

                var result = DevExpress.ui.dialog.confirm("Apakah anda yakin?", "Confirm");
                    result.done(function (dialogResult) {
                        console.log(dialogResult);
                        if(dialogResult) {

                            var lists = [];
                            $.each(dataGridWUM.getSelectedRowsData(), function() {
                                lists.push(this.ID);
                            });
                            $.post("{{ url('akuntansi/invoice/lists/transmittalwum') }}", {ids:lists})
                                .done(function(data){
                                    dataGridWUM.refresh();
                                    var obj = JSON.parse(data);
                                    console.log(obj);
                                    if(obj.status == 200){
                                        var result = DevExpress.ui.dialog.alert(obj.msg, "Print Transmittal");

                                        result.done(function (dialogResult) {
                                            if(dialogResult){
                                                window.open ("{{ url('akuntansi/invoice/lists/pdf_transmittal_wum/') }}"+obj.number,"_blank");
                                            }
                                        });
                                    }else{
                                        DevExpress.ui.notify({
                                        message: obj.msg,
                                        position: {
                                            my: "center top",
                                            at: "center top"
                                        }
                                        }, "error", 3000);
                                    }
                                });

                        }
                });
                }
        }).dxButton("instance");

        var dataGridWUM = $("#invoice-wum-grid").dxDataGrid({
            dataSource : dataSource2,
            remoteOperations: {sorting: true,paging: true,filtering:true},
            "export" : {enable:true, fileName : "WebUangMuka"},
            editing : {mode:'popup', allowUpdating : false,allowDeleting : false},
            selection:{mode:'multiple',showCheckBoxesMode:'always',selectAllMode:'page'},
            onSelectionChanged: function(data){
                btnWumOpen.option('disabled', !(data.selectedRowsData.length == 1))
                btnWumTransmital.option('disabled', !(data.selectedRowsData.length))
            },
            allowColumnReordering : true,
            allowColumnResizing : true,
            searchPanel : {visible : false},
            headerFilter:{visible:true},
            filterRow:{visible:true,applyFilter:'auto'},
            groupPanel : {visible:true},
            popup : {
                title:'WebUangMuka',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
            },
            pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: true,visible: true},
            columns : [
                {dataField:'NoInvoice',caption:'No. Invoice'},
                {dataField:'IDVendor',caption:'Kode Vendor'},
                {dataField:'NamaVendor',caption:'Nama Vendor'},
                {caption:'Nilai Invoice',calculateCellValue:function(data){
                    return data.KursInvoice + ' ' + number_format(data.NilaiInvoice,2,',','.')
                }},
                {dataField:'Status',caption:'Status',dataType:'integer',lookup:{dataSource: list_status, displayExpr: "name", valueExpr: "id"}},
                {dataField:'Keterangan',caption:'Keterangan'},
                {dataField:'NoFakturPajak'},
                {dataField:'TglFakturPajak'},
                {dataField:'NoTransmittal',caption:'No. Transmittal'},
                {dataField:'TglTransmittal',caption:'Tgl. Transmittal'}
            ],
            columnAutoWidth: true,
            columnChooser: {enabled: true},
            columnFixing: {enabled: true}
        }).dxDataGrid("instance");
        @if($tab)
            $('a[href="#web_uang_muka"]').tab('show');
        @endif

    });
</script>
@endsection
