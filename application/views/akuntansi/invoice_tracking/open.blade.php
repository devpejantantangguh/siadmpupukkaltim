@extends('templates.admin')

@section('heading')
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
<script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
<script src="{{ url('asset/js/phpjs.js') }}"></script>
<style type="text/css">
h3.panel-title{font-size: 14px !important}
.dx-fileuploader-show-file-list .dx-fileuploader-files-container{padding-top:0px}
.dx-fileuploader-wrapper{padding:0px;}
.form-horizontal .control-label{text-align: left !important;padding-left:20px;}
</style>
@endsection

@section('title','Verifikasi Invoice')

@section('contentheader')
<section class="content-header">
    <h1>Akuntansi</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Invoice Tracking</li>
        <li class="active">Akuntansi</li>
    </ol>
</section>
@endsection

@section('content')
<div class="box-header with-border">
    <h3 class="box-title">Verifikasi Invoice</h3>
    <?php $area = Auth::unitKerja()->Area; ?>
    <span class="pull-right">Area: {{$area}}</span>
</div>

<div class="box-body">
    @if(flashdata('success'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error!</strong> {{ flashdata('error') }}
        </div>

        @elseif(flashdata('warning'))

        <div class="alert alert-warning">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Warning!</strong> {{ flashdata('warning') }}
        </div>

        @elseif(flashdata('info'))

        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Info!</strong> {{ flashdata('info') }}
        </div>
        @endif
        <div role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a>
                </li>
                <li role="presentation">
                    <a href="#history" aria-controls="history" role="tab" data-toggle="tab">History</a>
                </li>
            </ul>
        </div>
        <div class="tab-content" style="padding-top:10px">
            <div role="tabpanel" class="tab-pane active" id="details">

                <form id="invoice_form" action="{{ url('akuntansi/invoice/lists/store/'.$id) }}" method="POST" id="akuntansi">
                    <input type="hidden" name="IDVendor" value="{{ $data->IDVendor }}">
                    <div class="panel panel-default form-horizontal">
                        <div class="panel-heading">
                            <h3 class="panel-title">Detail Invoice</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Vendor</label>
                                <div class="col-sm-4">
                                    <input type="text" value="{{ $data->IDVendor }} - {{ $data->NamaVendor }}" class="form-control" readonly>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Jenis Invoice</label>
                                <div class="col-sm-4">
                                    <input type="text" value="{{ $list_transaksi[$data->JenisTransaksi] }}" class="form-control" readonly>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">No. Invoice</label>
                                <div class="col-sm-4">
                                    <input type="text" value="{{ $data->NoInvoice }}" class="form-control" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Tgl. Invoice</label>
                                <div class="col-sm-2">
                                    <div class="input-group date">
                                        <input type="text" class="form-control pull-right datepicker" value="{{ date('d-m-Y', strtotime($data->TglInvoice)) }}" readonly>
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Nilai Invoice</label>
                                <div class="col-sm-4">
                                    <input type="text" value="{{ number_format($data->NilaiInvoice,2,',','.') }}" class="form-control" id="money" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Kurs</label>
                                <div class="col-sm-1">
                                    <input type="text" value="{{ $list_kurs[$data->KursInvoice] }}" class="form-control" readonly>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">No. Faktur Pajak</label>
                                <div class="col-sm-4">
                                    <input <?php echo ($area=='Pusat')? 'readonly' : ''; ?> type="text" name="NoFakturPajak" value="{{ $data->NoFakturPajak }}" id="NoFakturPajak" class="form-control" minlength="16" maxlength="16" required>
                                        <input type="hidden" name="NoFakturPajak_OLD" value="{{ $data->NoFakturPajak }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Tanggal Faktur Pajak</label>
                                <div class="col-sm-2">
                                    <div class="input-group date">
                                        <input <?php echo ($area=='Pusat')? 'readonly' : ''; ?> type="text"  name="TglFakturPajak" class="form-control pull-right datepicker" value="{{ date('d-m-Y',strtotime($data->TglFakturPajak)) }}" required>
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Unit Kerja</label>
                                    <div class="col-sm-4">
                                        <input type="text"  class="form-control" value="{{ $data->CostCenter }} - {{ Auth::getUnitKerja($data->CostCenter) }}" readonly>
                                    </div>
                                </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Keterangan</label>
                                <div class="col-sm-4">
                                    <textarea name="Keterangan" class="form-control" readonly>{{ $data->Keterangan }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="panel panel-default form-horizontal" id="potongan">
                        <div class="panel-heading">
                            <h3 class="panel-title">Potongan</h3>
                        </div>
                        <div class="panel-body">

                            <div class="col-md-6">

                                <div id="first">

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Potongan</label>
                                        <div class="col-sm-8">
                                            {!! form_dropdown('Potongan', $list_potongan, '',['class' => 'form-control potongan select2']) !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Nominal</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="Nominal" value="" class="form-control nominal"  id="money">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Keterangan</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="Ket" value="" class="form-control keterangan">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label"></label>
                                    <div class="col-sm-8">
                                        <div id="btn-add"></div>
                                        <div id="btn-delete"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Jumlah Tagihan Setelah Potongan</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="SumPotongan" value="{{ number_format($data->NilaiVerifikasi,2,',','.') }}" class="form-control SumPotongan" id="money" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div id="gridPotongan"></div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="panel panel-default form-horizontal" id="dokumensap">
                        <div class="panel-heading">
                            <h3 class="panel-title">Dokumen SAP</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group {{ form_error('NoInvoice') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label">No Dokumen SAP</label>
                                <div class="col-sm-2">
                                    <input type="text"maxlength="10" name="NoDokumenSAP" value="{{ $data->NoDokumenSAP }}" class="form-control" required>
                                </div>

                                <label class="col-sm-1 control-label">Tahun</label>
                                <div class="col-sm-1">
                                    <input type="text" name="Tahun" value="{{ $data->Tahun }}" id="tahun" class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">Payment Term</label>
                                <div class="col-sm-2">
                                    <select name="PaymentTerm" class="form-control select2" id="PaymentTerm" required></select>
                                </div>


                                <div class="col-sm-4 control-label">
                                    <div class="help-text" id="help-PaymentTerm">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>







                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Attachments</h3>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered table-striped table-condensed">
                                <tr>
                                    <th>File</th>
                                    <th>Action</th>
                                </tr>
                                @foreach ($attachments as $attachment)
                                <tr>
                                    <td>{{ $attachment->FilePath }}</td>
                                    <td>
                                        <a href="{{ url('storage/invoice/'.$attachment->FilePath) }}" class="btn btn-xs btn-success" style="color:#fff">View</a>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>

                    <div class="panel panel-default form-horizontal">
                        <div class="panel-heading">
                            <h3 class="panel-title">Detail Purchase Order</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <div id="gridContainer"></div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Jaminan</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div id="gridContainerJaminan"></div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    <div class="panel panel-info form-horizontal">
                        <div class="panel-heading">
                            <span class="clickable panel-collapsed">
                                <h3 class="panel-title">Kelengkapan Dokumen</h3>
                                <span class="pull-right"><i class="glyphicon glyphicon-chevron-down"></i></span>
                            </span>
                        </div>

                        <div class="panel-body" style="display: none">
                            <div class="col-sm-6">
                                @foreach($documents as $document)
                                <div class="form-group">
                                    <div class=" col-sm-10">
                                        <div class="checkbox">
                                            <label>
                                                <?php
                                                $name = "matriks[".$document['ID']."]";
                                                $extra = $document['Required'] ? ['required' => 'required'] : [];
                                                ?>
                                                {!! form_checkbox($name, $document['Dokumen'], $document['Value'], $extra) !!}
                                                {{ $document['Dokumen'] }}
                                                @if(!empty($extra))
                                                *
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                @endforeach


                            </div>
                            <div class="col-sm-6">
                                <div class="checkbox pull-right">
                                    <label>
                                        {!! form_checkbox('IsPriority', 'IsPriority', $data->IsPriority) !!} Priority
                                    </label>
                                </div>
                            </div>

                        </div>
                    </div>
                    <style>
                    .clickable{
                        cursor: pointer;
                    }

                    .panel-heading span {
                        margin-top: -20px;
                        font-size: 15px;
                    }

                </style>
                <script>

                    $(document).on('click', '.panel-heading span.clickable', function(e){
                        var $this = $(this);
                        if(!$this.hasClass('panel-collapsed')) {
                            $this.parents('.panel').find('.panel-body').slideUp();
                            $this.addClass('panel-collapsed');
                            $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
                        } else {
                            $this.parents('.panel').find('.panel-body').slideDown();
                            $this.removeClass('panel-collapsed');
                            $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
                        }
                    })
                </script>
                <div class="panel panel-info form-horizontal">
                    <div class="panel-heading">
                        <h3 class="panel-title">Catatan Pemeriksaan</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="col-sm-10 control-label">
                                <textarea name="note" type="text" class="form-control" rows="4" cols="30"></textarea>
                            </label>
                        </div>
                    </div>
                </div>


                <center>
                    <button class="btn btn-success btn-sm submit" name="submit" value="verifikasi" >
                        <i class="fa fa-check"></i> Verify</button>


                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#m-revisi">
                            <i class="fa fa-close"></i> Reject
                        </button>
                    </center>
                </form>
            </div>

            <div role="tabpanel" class="tab-pane" id="history">
                <table class="table table-condensed table-hover table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Action</th>
                            <th>User</th>
                            <th>Keterangan</th>
                            <th>Timestamp</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($histories as $key => $history)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $history->Action }}</td>
                            <td>{{ $history->Nama }}</td>
                            <td>{{ $history->Keterangan }}</td>
                            <td>{{ date('d-m-Y H:i:s', strtotime($history->Timestamp)) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div id="m-revisi" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <form id="revisi_form" action="{{ url('akuntansi/invoice/lists/reject_update/'.$data->ID) }}" method="POST">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Alasan Reject</h4>
                    </div>
                    <div class="modal-body">
                        <textarea name="Keterangan" class="form-control" placeholder="Dokumen di revisi karena...."" required="required"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger btn-sm reject">Reject</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @endsection

    @section('script')

    <script type="text/javascript">
        $(function() {
            $('input[id=NoFakturPajak]').mask('0000000000000000', {reverse:true})
            var dataSourceJaminan = new DevExpress.data.CustomStore({
                key: "ID",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};

                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }

                    if (loadOptions.filter != 'undefined') {
                        args.filter = JSON.stringify(loadOptions.filter);
                    }

                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 12;

                    $.ajax({
                        url: "{{ url("invoice/input/getJaminan/".$id) }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });

                    return deferred.promise();
                }
            });

            var dataGridJaminan = $("#gridContainerJaminan").dxDataGrid({
                dataSource : dataSourceJaminan,
                onSelectionChanged: function(data){
                    $('#btn_delete').prop('disabled', !(data.selectedRowsData.length));/*
                    $('#btn_edit').prop('disabled', !(data.selectedRowsData.length == 1));*/
                },
                selection:{mode:'multiple',showCheckBoxesMode:'always',selectAllMode:'page'},
                remoteOperations: {sorting: true,paging: true,filtering:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: false,visible: true},
                columns : [
                    {caption:'No',cellTemplate:function(cellElement, cellInfo){
                    cellElement.text(cellInfo.row.rowIndex+1)
                    }},
                    {dataField:'NO_PO',caption:'No. PO'},
                    {dataField:'ID_JENIS_JAMINAN',caption:'Jenis Jaminan'},
                    {dataField:'NOMOR_JAMINAN',caption:'No. Jaminan'},
                    {dataField:'NILAI_JAMINAN',caption:'Nilai Jaminan'},
                    {dataField:'MASA_BERLAKU',caption:'Masa Berlaku'},
                    {dataField:'ID_BANK_PENERBIT',caption:'Bank Penerbit'},
                    {dataField:'KETERANGAN_JAMINAN',caption:'Keterangan'},
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
                rowAlternationEnabled: true,
                rowAlternationEnabled: true,
            }).dxDataGrid("instance");
        })

    </script>
    <script type="text/javascript">

$('.datepicker').datepicker({autoclose: true, format:'dd-mm-yyyy'});

        $('input[name=Nominal]').inputmask('numeric', {
            groupSeparator: '.',
            radixPoint : ',',
            autoGroup: true,
            digits: 2,
            digitsOptional: false,
            placeholder: '0,00'
        });
        $('input[name=SumPotongan]').inputmask('numeric', {
            groupSeparator: '.',
            radixPoint : ',',
            autoGroup: true,
            digits: 2,
            digitsOptional: false,
            placeholder: '0,00'
        });

        $('input[type=checkbox]').attr('disabled','true');

        $('#tahun').datepicker({autoclose: true, format: "yyyy",
            viewMode: "years",
            minViewMode: "years"});
        $(function() {
            $('form#invoice_form').submit(function(e){

                e.preventDefault();
                var form = this;
                var result = DevExpress.ui.dialog.confirm("Apakah anda yakin?", "Confirm");
                    result.done(function (dialogResult) {
                        console.log(dialogResult);
                        if(dialogResult) {
                            $(form).unbind('submit');
                            $(form).find("button[type='submit']").prop('disabled',false);
                            $('.submit').click();
                        }else{
                            $(form).find("button[type='submit']").prop('disabled',false);
                        }
                });

            });

            $('form#revisi_form').submit(function(e){

                e.preventDefault();
                var form = this;
                var result = DevExpress.ui.dialog.confirm("Apakah anda yakin?", "Confirm");
                    result.done(function (dialogResult) {
                        console.log(dialogResult);
                        if(dialogResult) {
                            $(form).unbind('submit');

                            $(form).find("button[type='submit']").prop('disabled',false);
                            $('.reject').click();
                        }else{
                            $(form).find("button[type='submit']").prop('disabled',false);
                        }
                });

            });
            var TimestampChecked = "{{ date('Y,m,d', strtotime($TimestampChecked)) }}";
            var checked = new Date(TimestampChecked);
            var est = new Date(checked);

            var monthNames = [
            "Januari", "Februari", "Maret",
            "April", "Mei", "Juni", "Juli",
            "Agustus", "September", "October",
            "November", "Desember"
            ];
            var data = {!! json_encode($term) !!}
            var i = 0;
            $.each(data, function(index, item) { // Iterates through a collection
                i++;
                $("#PaymentTerm").append( // Append an object to the inside of the select box
                $("<option></option>") // Yes you can do this.
                .text(item.PaymentTerm)
                .val(item.ID)
                );

                if(i==1){
                    $('#PaymentTerm').val(item.ID);
                    var res = est.setTime(est.getTime() + (parseInt(item.JumlahHari) * 24 * 60 * 60 * 1000));
                    var day = est.getDate();
                    var monthIndex = est.getMonth();
                    var year = est.getFullYear();
                    $('#help-PaymentTerm').html('Estimasi Pembayaran: '+day+ ' ' +monthNames[monthIndex]+' '+year);
                }
                if(item.ID=='{{$data->PaymentTerm}}'){
                    $('#PaymentTerm').val(item.ID);
                    var res = est.setTime(est.getTime() + (parseInt(item.JumlahHari) * 24 * 60 * 60 * 1000));
                    var day = est.getDate();
                    var monthIndex = est.getMonth();
                    var year = est.getFullYear();
                    $('#help-PaymentTerm').html('Estimasi Pembayaran: '+day+ ' ' +monthNames[monthIndex]+' '+year);
                }
            });

            $('#PaymentTerm').change(function(){
                var est = new Date(checked);
                var id = $('#PaymentTerm').val();
                $.each(data, function(index, item) {
                    if(item.ID==id){
                        var res = est.setTime(est.getTime() + (parseInt(item.JumlahHari) * 24 * 60 * 60 * 1000));
                        var day = est.getDate();
                        var monthIndex = est.getMonth();
                        var year = est.getFullYear();
                        $('#help-PaymentTerm').html('Estimasi Pembayaran: '+day+ ' ' +monthNames[monthIndex]+' '+year);

                    }
                });
                    //$('#help-ARDocNo').html(data.Deskripsi);

                })

            //var list_term = json_encode($term)!!};

            $('#PaymentTerm').change(function(){

                //$('#help-PaymentTerm').html(data.Deskripsi);

            })
            var dataSource = new DevExpress.data.CustomStore({
                key: "ID",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                    args = {};

                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }

                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 12;

                    $.ajax({
                        url: "{{ url("invoice/input/get/".$data->ID) }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });

                    return deferred.promise();
                }
            });
            var dataSourcePotongan = new DevExpress.data.CustomStore({
                key: "ID",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                    args = {};

                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }

                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 999;

                    $.ajax({
                        url: "{{ url("akuntansi/invoice/lists/getPotongan/".$data->ID) }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });

                    return deferred.promise();
                }
            });

            var dataGrid = $("#gridContainer").dxDataGrid({
                dataSource : dataSource,
                // columnHidingEnabled: false,
                remoteOperations: {sorting: true,paging: true,filtering:true},
                "export" : {enable:true, fileName : "{{ $table }}"},
                editing : {mode:'form', allowAdding : false,allowUpdating : false,allowDeleting : false},
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : true},
                filterRow : true,
                groupPanel : {visible:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: false,visible: true},
                columns : [
                    // {caption:'No',cellTemplate:function(cellElement, cellInfo){
                    //     cellElement.text(cellInfo.row.rowIndex+1)
                    // }},
                    {dataField:'NoPO',caption:'No. PO'},
                    {dataField:'NilaiPO',caption:'Nilai PO',calculateCellValue:function(data){
                        return number_format(data.NilaiPO,2,',','.')
                    }},
                    {dataField:'NomorKontrak',caption:'No Kontrak'},
                    {dataField:'NilaiKontrak',caption:'Nilai Kontrak'},
                    {dataField:'NoSAGR',caption:'No. SA/GRS'},
                    {dataField:'TglSAGR',caption:'Tgl SA/GRS',dataType: 'date',format: 'dd/MM/yyyy'},
                    {dataField:'Nama',caption:'Created By'},
                    /*
                    {dataField:'DeskripsiPO',caption:'Deskripsi Pekerjaan'},*/
                    {dataField:'NomorBA',caption:'No. BAPP'},
                    {dataField:'TanggalBA',caption:'Tgl BAPP',dataType: 'date',format: 'dd/MM/yyyy'},
                    {dataField:'Nama',caption:'Created By'},
                    {dataField:'Keterangan',caption:'Keterangan'},
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
                rowAlternationEnabled: true,
            }).dxDataGrid("instance");



            var dataGridPotongan = $("#gridPotongan").dxDataGrid({
                dataSource : dataSourcePotongan,
                // columnHidingEnabled: false,
                selection:{mode:'multiple',showCheckBoxesMode:'always',selectAllMode:'page'},                onSelectionChanged: function(data){
                    $('#btn_delete').prop('disabled', !(data.selectedRowsData.length));
                },
                remoteOperations: {sorting: true,paging: false},
                "export" : {enable:true, fileName : "{{ $table }}"},
                editing : {mode:'form', allowAdding : false,allowUpdating : false,allowDeleting : false},
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : false},
                headerFilter:{visible:false},
                filterRow:{visible:false,applyFilter:'auto'},
                groupPanel : {visible:false},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                columns : [
                {caption:'No',cellTemplate:function(cellElement, cellInfo){
                    cellElement.text(cellInfo.row.rowIndex+1)
                }},
                {dataField:'Potongan',caption:'Potongan'},
                {caption:'Nominal',calculateCellValue:function(data){
                    return number_format(data.Nominal,2,',','.')
                }},
                {caption:'Kurs',cellTemplate:function(cellElement, cellInfo){
                    cellElement.text('{{ $list_kurs[$data->KursInvoice] }}')
                }},
                {dataField:'Keterangan',caption:'Keterangan'},
                {dataField:'Nama',caption:'Created By'},
                {dataField:'CreatedOn',caption:'Created On',dataType: 'date',format: 'dd/MM/yyyy'},
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: false},
                columnFixing: {enabled: false},
            }).dxDataGrid("instance");
            var addButton = $("#btn-add").dxButton({
                text: "Add",
                height: 34,
                width: 100,
                type: 'default',
                onClick: function () {

                    var url = "{{ url('akuntansi/invoice/lists/add_potongan/'.$id) }}";
                    var forms = $('#first input').serialize();
                    var forms = forms +'&'+ $('#first select').serialize();
                    addButton.option('text', 'Loading...');
                    $.ajax({
                     type: "POST",
                     url: url,
                     data: forms ,

                     success: function(results)
                     {
                        if(results != '') {
                            $( '.potongan' ).val('');
                            $( '.nominal' ).val('');
                            $( '.keterangan' ).val('');
                            var NilaiInvoice = "{{ $data->NilaiInvoice }}";
                            $('.SumPotongan').val(number_format(parseFloat(NilaiInvoice)-parseFloat(results),2,',','.'));

                            dataGridPotongan.refresh();
                        }
                    }
                });

                    addButton.option('text', 'Add');

                }
            }).dxButton("instance");
            var deleteButton = $("#btn-delete").dxButton({
                text: "Delete",
                height: 34,
                width: 100,
                type: 'danger',
                onClick: function () {
                    $.each(dataGridPotongan.getSelectedRowsData(), function() {
                        var ID = this.ID;
                        var url = "{{ url('akuntansi/invoice/lists/delete_potongan/').$id }}";
                        var forms = {ids:ID};
                        deleteButton.option('text', 'Loading...');
                        $.ajax({
                         type: "POST",
                         url: url,
                         data: forms ,

                         success: function(results)
                         {
                            if(results != '') {
                                var NilaiInvoice = "{{ $data->NilaiInvoice }}";
                                $('.SumPotongan').val(number_format(parseFloat(NilaiInvoice)-parseFloat(results),2,',','.'));
                                dataGridPotongan.refresh();
                            }
                        }
                    });
                    });

                    deleteButton.option('text', 'Delete');

                }
            }).dxButton("instance");

        });
</script>
@endsection
