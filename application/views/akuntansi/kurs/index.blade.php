@extends('templates.admin')

@section('heading')
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
<script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>

<script src="{{ url('asset/js/phpjs.js') }}"></script>
@endsection  

@section('contentheader')
<section class="content-header">
    <h1>Akuntansi</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Akuntansi</li>
        <li class="active">Kurs</li>
    </ol>
</section>
@endsection

@section('title','Kurs Valuta Asing')

@section('content')
<div class="box-header with-border">
    <h3 class="box-title">Kurs Valuta Asing</h3>
</div>

    <div class="box-body">
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {{ flashdata('error') }}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> {{ flashdata('warning') }}
            </div>

        @elseif(flashdata('info'))

            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Info!</strong> {{ flashdata('info') }}
            </div>
        @endif
        <button class="btn btn-success btn-sm" id="btn_update" data-toggle="modal" data-target="#m-update" style="color:#fff;margin-bottom: 15px;"><i class="fa fa-pencil"></i> Update</button>

            <div id="m-update" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <form action="{{ url('akuntansi/invoice/kurs/update') }}" method="POST" id="form_revisi">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Update Kurs</h4>
          </div>
          <div class="modal-body">
            <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Tanggal Kurs</label>

                  <div class="col-sm-5">
                    <input type="text" name="tanggal" value="<?php echo date('d-m-Y'); ?>" id="tanggal" class="form-control datepicker" placeholder="" >
                  </div>
                </div>
          </div>
          <table class="table table-striped" id="kurs">
                <tbody><tr>
                  <th style="width: 10px">No</th>
                  <th>Mata Uang</th>
                  <th>Kurs <?php echo date('d/m/Y',strtotime("-1 days")); ?></th>
                  <th>Kurs Saat Ini</th>
                </tr>
                <?php 
                $i = 1;
                foreach ($Currency as $value) {
                    $today = '';
                    $disabled = '';
                    if(date('Y-m-d',strtotime($value['TanggalToday'])) == date('Y-m-d')){
                      $today = $value['KursToday'];
                      $disabled = 'disabled';
                    }
                 ?>
                <tr>
                  <td><?php echo $i++; ?></td>
                  <td><?php echo $value['Currency']; ?></td>
                  <td>
                    <?php
                    $tanggal = date('Y-m-d',strtotime($value['TanggalToday']));
                    $tanggaly = date('Y-m-d',strtotime($value['TanggalYesterday']));
                    if($tanggaly == date('Y-m-d',strtotime("-1 days"))){
                        echo "".$value['KursYesterday']."";
                    }elseif($tanggal == date('Y-m-d',strtotime("-1 days"))){
                        echo "".$value['KursToday']."";
                    }
                  ?>
                  </td>
                  <td>
                        <input type="text" class="form-control number" id="<?php echo $value['ID']; ?>" value="<?php echo $today; ?>" name="kurs[<?php echo $value['ID']; ?>]" <?php //echo $disabled; ?>>
                  </td>
                  
                </tr>
                <?php } ?>
              </tbody></table>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-danger btn-sm submit">Update</button>
          </div>
        </div>
        </form>
      </div>
    </div>
        <h6 class="" href="#" style="color:#000;margin-bottom: 15px;float: right">per <span class="tanggal"></span></h6>
    <div role="tabpanel">
        <!-- Nav tabs -->
        <style type="text/css">
            .nav-tabs li {
                width: 180px;
            }
            .border-left {
                width: 50px;float: left;
            }
        </style>
        <ul class="nav nav-tabs" role="tablist">
            <?php foreach ($Currency as $value) {
             ?>
            <li role="presentation" <?php if($value['Currency']=="USD"){echo 'class="active"';} ?> style="">
                <a href="<?php echo $value['Currency']; ?>" aria-controls="home" role="tab" data-toggle="tab" date="<?php 
                $time = strtotime($value['CreatedOnToday']);
                $tanggal = date('d M Y h:i',$time).' WITA';
                echo $tanggal;

                 ?>">
                <div class="description-block border-left" style="">
                     <img src="{{ url('asset/image/').$value['Image'] }}" width="50" />
                </div>
                <div class="description-block border-right">

                    <h4 class="description-header  text-blue"><?php echo $value['Currency']; ?>-IDR</h4>
                    <h6 class="description-header text-black"><?php echo number_format($value['KursToday'],0,',','.'); ?></h6>
                    <?php 
                    if($value['KursToday']>0){
                        $sum = $value['KursToday'] - $value['KursYesterday'];
                        $percent = number_format(($sum/$value['KursToday']) * 100,2,',','.');
                            if(($sum) >= 0){
                                echo '<span class="description-percentage text-green">
                                <i class="fa fa-caret-up"></i> '.$percent.'%</span>';
                            }elseif(($sum) < 0){
                                echo '<span class="description-percentage text-red">
                                <i class="fa fa-caret-up"></i> '.$percent.'%</span>';
                            }
                    }
                    ?>
                  </div>
                  </a>
            </li>
            <?php } ?>
        </ul>

    </div><!-- 
        <div class="action">
                <div id="selectbox"></div>
                <div class="label">Choose a month:</div>
            </div> -->
        <div id="chart"></div>
    </div>
@endsection

@section('script')
<script type="text/javascript">
    $(function(){
        $('form').submit(function(e){
                
                e.preventDefault();
                var form = this;
                var result = DevExpress.ui.dialog.confirm("Apakah anda yakin?", "Confirm");
                    result.done(function (dialogResult) {
                        console.log(dialogResult);
                        if(dialogResult) {   
                            $(form).unbind('submit');  
                            $(form).find("button[type='submit']").prop('disabled',false); 
                            $('.submit').click(); 
                        }else{
                            $(form).find("button[type='submit']").prop('disabled',false); 
                        }
                });
                
            });

$('.number').keypress(function (e) {
    var character = String.fromCharCode(e.keyCode)
    var newValue = this.value + character;
    if (isNaN(newValue) || hasDecimalPlace(newValue, 3)) {
        e.preventDefault();
        return false;
    }
});

function hasDecimalPlace(value, x) {
    var pointIndex = value.indexOf('.');
    return  pointIndex >= 0 && pointIndex < value.length - x;
}
var chartDataSource = new DevExpress.data.DataSource({
        store: {
            type: "odata",
            url: "{{ url('akuntansi/invoice/kurs/get') }}"
        },
        postProcess: function(results) {
            return results[0].DayItems;
        },
        expand: "DayItems",
        filter: "USD"
    });

    var chartOptions = {
        dataSource: chartDataSource,
        size: {
            height: 420
        },
        series: {
            argumentField: "arg",
            valueField: "val",
            type: "spline"
        },
        legend: {
            visible: false
        },
        commonPaneSettings: {
            border: {
                visible: true,
                width: 2,
                top: false,
                right: false
            }
        },
        "export": {
            enabled: true
        },
        tooltip: {
            enabled: true,
            customizeTooltip: function (arg) {
                return {
                    text: arg.argumentText + "<br/>" + arg.valueText
                };
            }
        },
        valueAxis: {
            valueType: "numeric",
            grid: {
                opacity: 0.2
            }
        },
        argumentAxis: {
            argumentType: "string",
            type: "discrete",
            grid: {
                visible: true,
                opacity: 0.5
            }
        }
    };

    
    $("#chart").dxChart(chartOptions);
   $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
  var target = $(e.target).attr("href") // activated tab
  var tanggal = $(e.target).attr("date") // activated tab
  $( "span.tanggal" ).html(tanggal);
  chartDataSource.filter(target);
  chartDataSource.load();
  //alert(target);

});    
  var tanggal = $('a[href="USD"]').attr("date") // activated tab
  $( "span.tanggal" ).html(tanggal);
  $('.datepicker').datepicker({autoclose: true, format:'dd-mm-yyyy'});

});
        $('#tanggal').change(function(){
            var tanggal = $('#tanggal').val();
            $.post("{{ url('akuntansi/invoice/kurs/ajax') }}",{tanggal:tanggal})
            .done(function(data){
              console.log(data);

                $('#kurs input').val('');
                $("#kurs input").prop('disabled', false);
                $.each(data, function(i, v) {
                    $('#'+v.Currency).val(v.Kurs);
                    if(v.Kurs>0){
                      $('#'+v.Currency).prop('disabled', false);
                    }
                });

            }).fail(function(){
                alert('Something Wrong');
            })
        });
</script>
@endsection
