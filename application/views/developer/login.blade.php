<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login Developer</title>
    <meta name="robots" content="noindex">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <style>*{font-size:12px}</style>
</head>
<body>
    <div class="col" style="padding:20px">
        <table id="myTable" class="table table-bordered table-sm">
            <thead>
                <tr>
                    <td>Nama &amp; Email</td>
                    {{-- <td>Password</td> --}}
                    <td>NIK / NPK Lama</td>
                    <td>Role</td>
                    <td>Unit Kerja</td>
                    <td>Area</td>
                    <td>Invoice</td>
                    <td>LDAP</td>
                    <td>Action</td>
                </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                <tr>
                    <td>{{ $user['nama'] }} ({{ $user['email'] }})</td>
                    {{-- <td>{{ $user['password'] }}</td> --}}
                    <td>{{ $user['id_karyawan'] }}-{{ $user['npk_lama'] }}</td>
                    <td>{{ $user['role_name'] }}</td>
                    <td>{{ $user['unit_kerja'] }}</td>
                    <td>{{ $user['area'] }}</td>
                    <td>{{ $user['invoice_count'] }}</td>
                    <td>{{ $user['is_ldap'] }}</td>
                    <td>
                        <a href="{{ $user['action'] }}" class="btn btn-primary btn-sm">
                            Login
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <script>
    $(document).ready( function () {
        $('#myTable').DataTable({paging:false});
    });
    </script>
</body>
</html>