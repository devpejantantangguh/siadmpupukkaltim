@extends('templates.admin')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
@endsection

@section('title','Global Setting')

@section('contentheader')
    <section class="content-header">
        <h1>Global Setting</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Master Data</li>
            <li class="active">Global Setting</li>
        </ol>
    </section>
@endsection

@section('script')
<script>
    $(function(){
        $('.select2').select2();
    });
</script>
@endsection

@section('content')
    <form action="{{ url('admin/setting/update') }}" method="POST">
        <div class="box-header with-border">
            <h3 class="box-title">Global Setting</h3>
        </div>
        <div class="box-body">
            @if(flashdata('success'))
                <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

            @elseif(flashdata('error'))

                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Error!</strong> {{ flashdata('error') }}
                </div>

            @elseif(flashdata('warning'))

                <div class="alert alert-warning">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Warning!</strong> {{ flashdata('warning') }}
                </div>

            @elseif(flashdata('info'))

                <div class="alert alert-info">
                    <a href="#" class="close" data-dismiss="alert">&times;</a>
                    <strong>Info!</strong> {{ flashdata('info') }}
                </div>
            @endif

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Transmittal KEU</label>
                        {!! form_input('TransmittalKEU', $data['TransmittalKEU'], ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        <label for="">Transmittal AKT</label>
                        {!! form_input('TransmittalAKT', $data['TransmittalAKT'], ['class' => 'form-control']) !!}
                    </div>

                    <div class="form-group">
                        <label for="">Saldo KAS</label>
                        {!! form_input('SaldoKas', $data['SaldoKas'], ['class' => 'form-control']) !!}
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Limit FP 1</label>
                                {!! form_input('LimitFP1', $data['LimitFP1'], ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Limit FP 2</label>
                                {!! form_input('LimitFP2', $data['LimitFP2'], ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Limit FP 3</label>
                                {!! form_input('LimitFP3', $data['LimitFP3'], ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Approval 1 FP</label>
                                {!! form_dropdown('Approval1FP', $users, $data['Approval1FP'], ['class' => 'form-control select2']) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Approval 2 FP</label>
                                {!! form_dropdown('Approval2FP', $users, $data['Approval2FP'], ['class' => 'form-control select2']) !!}
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="">Approval 3 FP</label>
                                {!! form_dropdown('Approval3FP', $users, $data['Approval3FP'], ['class' => 'form-control select2']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="">Last Number</label>
                        {!! form_input('LastNumber', $data['LastNumber'], ['class' => 'form-control']) !!}
                    </div>

					<div class="form-group">
                        <label for="">Rekening 1</label>
                        {!! form_input('Rekening1', $data['Rekening1'] ?? '', ['class' => 'form-control']) !!}
                    </div>

					<div class="form-group">
                        <label for="">Rekening 2</label>
                        {!! form_input('Rekening2', $data['Rekening2'] ?? '', ['class' => 'form-control']) !!}
                    </div>

                </div>
            </div>

            {{-- <div id="gridContainer"></div> --}}
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary btn-sm">Save</button>
        </div>
    </form>
@endsection
