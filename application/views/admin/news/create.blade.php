@extends('templates.admin')

@section('title','Buat Berita')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
@endsection 

@section('contentheader')
    <section class="content-header">
        <h1>Create News</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Master Data</li>
            <li class="active">Create News</li>
        </ol>
    </section>
@endsection 

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">Create News</h3>
    </div>
    <div class="box-body">
        <form action="{{ url('admin/news/store') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">

            <div class="form-group">
                <div class="col-md-2"><label>Judul : </label></div>
                <div class="col-md-4">
                    <div id="dxTextSubject"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-2"><label>Berita : </label></div>
                <div class="col-md-4">
                    <div id="dxTextMessage"></div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-2"><label>End Date : </label></div>
                <div class="col-md-4">
                    <div id="dxEndDate"></div>
                </div>
            </div>

                    <div class="form-group">
                        <label class="col-sm-2">Gambar</label>
                        <div class="col-sm-10">
                            <input name="file" type="file">
                            <p>Image only</p>
                    </div>
                </div>

            <div class="form-group">
                <div class="col-md-4 col-md-offset-2">
                    <div id="dxBtnSend"></div>
                </div>
            </div>


        </form>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){


            var dxTextSubject = $('#dxTextSubject').dxTextBox({
                name:'Judul',value:"",placeholder:"Tulis Judul..."
            }).dxValidator({validationRules: [{type: "required",message: "Subject is required"}]}).dxTextBox("instance");

            var dxTextMessage = $('#dxTextMessage').dxTextArea({
                name:'News',value:"",placeholder:"Tulis Berita...", height:200
            }).dxValidator({validationRules: [{type: "required",message: "Berita is required"}]}).dxTextArea("instance");

            var dxEndDate = $('#dxEndDate').dxDateBox({
                name:'EndDate',value:""
            }).dxValidator({validationRules: [{type: "required",message: "End Date is required"}]}).dxDateBox("instance");

            var dxBtnSend = $("#dxBtnSend").dxButton({
                "text": "Send Message", useSubmitBehavior:true, type:'success'
            }).dxButton("instance");

            var dxBtnSend = $("#dxBtnSend").dxButton({
                "text": "Create News", useSubmitBehavior:true, type:'success'
            }).dxButton("instance");

        });
    </script>
@endsection
