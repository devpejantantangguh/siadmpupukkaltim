@extends('templates.admin')

@section('title','Berita')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
    <style type="text/css">
        .bulet {height: 15px;width: 15px;display: block;border-radius: 30px;margin: 0 auto;}
        .b-green {background: green;}
        .b-red {background: red;}
        .dx-link{padding-right:5px;}
    </style>
@endsection 

@section('contentheader')
    <section class="content-header">
        <h1>Message</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Master Data</li>
            <li class="active">Message</li>
        </ol>
    </section>
@endsection 

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">News</h3>
    </div>
    <div class="box-body">
        <a class="btn btn-success" href="{{ url('admin/news/create') }}" style="color:white"><i class="fa fa-newspaper-o"></i> Create News</a>
        <br>
        <div id="dataGrid"></div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){
            var dataSource = new DevExpress.data.CustomStore({
                // key: "IDBillDocHeader",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};
            
                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }
            
                    if (loadOptions.filter != 'undefined') {
                        args.filter = JSON.stringify(loadOptions.filter);
                    }
            
                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 12;
            
                    $.ajax({
                        url: "{{ url("admin/news/get") }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });
            
                    return deferred.promise();
                }
            });

            var dataGrid = $("#dataGrid").dxDataGrid({
                dataSource : dataSource,
                // columnHidingEnabled: true,
                remoteOperations: {sorting: true,paging: true,filtering:false},
                "export" : {enabled:true, fileName:'{{ $table }}',allowExportSelectedData:true},
                editing : {mode:'form', allowAdding : false,allowUpdating : false,allowDeleting : false},
                // selection:{mode:'multiple'},
                onSelectionChanged:function(data){
                    editButton.option('disabled', !(data.selectedRowsData.length == 1))
                },
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : true},
                groupPanel : {visible:true},
                headerFilter:{visible:true},
                filterRow:{visible:true,applyFilter:'auto'},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: true,visible: true},
                columns : [
                    {dataField:'Judul', caption: 'Judul'},
                    {dataField:'StartDate', caption: 'StartDate'},
                    {dataField:'EndDate', caption: 'End Date', width:100},
                    {caption: '', width:100, cellTemplate:function(container,options){
                        $('<a/>').addClass('dx-link')
                        .text('Edit')
                        .on('dxclick', function () {
                            location.href = "{{ url('admin/news/edit/') }}" + options.data.ID;
                        })
                        .appendTo(container);
                        $('<a/>').addClass('dx-link')
                        .text('Delete')
                        .on('dxclick', function () {
                            var result = DevExpress.ui.dialog.confirm("Are you sure?", "Confirm Delete");
                            result.done(function (dialogResult) {
                                location.href = "{{ url('admin/news/destroy/') }}" + options.data.ID;    
                            });
                        })
                        .appendTo(container);
                    }},
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
                rowAlternationEnabled: true,
            }).dxDataGrid("instance");
        });
    </script>
@endsection
