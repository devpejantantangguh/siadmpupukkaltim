@extends('templates.admin')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
@endsection 

@section('contentheader')
    <section class="content-header">
        <h1>Master Data</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Master Data</li>
            <li class="active">New User</li>
        </ol>
    </section>
@endsection 

@section('content')
<form action="{{ url('admin/master/users/store') }}" method="POST" class="form-horizontal">
    <input type="hidden" name="Type" value="2">
    <div class="box-header with-border">
        <h3 class="box-title">Master Data User Apps</h3>
    </div>
    <div class="box-body">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3">Email</label>
                <div class="col-md-9">
                    <input type="text" name="Email" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Password</label>
                <div class="col-md-9">
                    <input type="password" name="Password" id="password" class="form-control">
                    <div class="ldap"></div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <div class="checkbox">
                      <label><input type="checkbox" name="IsLDAP" value="1" id="ldap">LDAP User?</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">ID Customer</label>
                <div class="col-md-9">
                    <select name="IDCustomer" class="form-control select-customer"></select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">ID Vendor</label>
                <div class="col-md-9">
                    <select name="IDVendor" class="form-control select-vendor"></select>
                </div>
            </div>
            
        </div>
    </div>
    <div class="box-footer">
        <a href="{{ url('admin/master/users') }}" class="btn btn-warning btn-sm" style="color:#fff"><i class="fa fa-arrow-left"></i> Kembali</a>
        <button type="submit" class="btn btn-primary btn-sm">Create</button>
    </div>
</form>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){

            var checkbox = document.getElementById('ldap');
                checkbox.addEventListener('click', function () {
                    if (document.getElementById('mn')) {
                        document.getElementById('mn').remove();
                        $('#password').prop("disabled", false);
                    } else {
                        var input = document.createElement("input");
                        input.id = 'mn';
                        input.className = 'form-control';
                        input.type = 'hidden';
                        input.name = 'Password';
                        input.value = '';
                        document.body.appendChild(input);
                        $( ".ldap" ).append( input );
                        $('#password').prop("disabled", true);

                    }
                });
            $('.select2').select2({
                placeholder:'Pilih...',
            });

            $('.select-customer').select2({
                ajax: {
                    type:'GET',
                    dataType: 'json',
                    url: '{{ url('admin/customer/customer') }}',
                    data: function (params) {
                        return {q: params.term};
                    },
                    processResults: function (data) {
                        return {results: $.map(data, function(obj){return {id:obj.ID, text:obj.Name}})}
                    },
                    cache:true
                },
                minimumInputLength:3,
                placeholder:'Pilih Customer...',
            });
            $('.select-vendor').select2({
                ajax: {
                    type:'GET',
                    dataType: 'json',
                    url: '{{ url('admin/customer/vendor') }}',
                    data: function (params) {
                        return {q: params.term};
                    },
                    processResults: function (data) {
                        return {results: $.map(data, function(obj){return {id:obj.ID, text:obj.Name}})}
                    },
                    cache:true
                },
                minimumInputLength:3,
                placeholder:'Pilih Vendor...',
            });
        });
    </script>
@endsection
