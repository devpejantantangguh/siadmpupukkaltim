@extends('templates.admin')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
    <style type="text/css">
        .dx-link{padding-right:5px;}
    </style>
@endsection 

@section('contentheader')
    <section class="content-header">
        <h1>Master Data</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Master Data</li>
            <li class="active">Users</li>
        </ol>
    </section>
@endsection

@section('title', 'Master Data Users')

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">Master Data Users</h3>
    </div>
    <div class="box-body">

    <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#website" aria-controls="home" role="tab" data-toggle="tab">Website</a>
            </li>
            <li role="presentation">
                <a href="#apps" aria-controls="tab" role="tab" data-toggle="tab">Apps</a>
            </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="website" style="margin-top:15px">
                <a href="{{ url('admin/master/users/create/') }}" class="btn btn-success btn-sm" style="color:white">New User</a>
                <div id="gridContainer"></div>
            </div>
            <div role="tabpanel" class="tab-pane" id="apps" style="margin-top:15px">
                <a href="{{ url('admin/master/users/create/apps') }}" class="btn btn-success btn-sm" style="color:white">New User</a>
                <div id="gridContainerApps"></div>
            </div>
        </div>
    </div>
        
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){
            var dataSource = new DevExpress.data.CustomStore({
                key: "ID",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};
            
                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }
            
                    if (loadOptions.filter != 'undefined') {
                        args.filter = JSON.stringify(loadOptions.filter);
                    }
            
                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 12;
            
                    $.ajax({
                        url: "{{ url("admin/master/get/website") }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });
            
                    return deferred.promise();
                }
            });

            var listRole = {!! json_encode($user_roles); !!};
/*            var karyawan = new DevExpress.data.CustomStore({
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};
            
                    $.ajax({
                        url: "{{ url("admin/master/get_karyawan/all") }}",
                        dataType: "json",
                        data: args,
                        success: function(result) {
                            deferred.resolve(result.items, { totalCount: result.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });
            
                    return deferred.promise();
                }
            });*/

            $("#gridContainer").dxDataGrid({
                dataSource : dataSource,
                // columnHidingEnabled: false,
                remoteOperations:  {sorting: true,paging: true,filtering:true},
                "export" : {enabled:true, fileName:'{{ $table }}',allowExportSelectedData:true},
                editing : {mode:'inline', allowAdding : false,allowUpdating : false,allowDeleting : false},
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : true},
                headerFilter:{visible:true},
                filterRow:{visible:true,applyFilter:'auto'},
                groupPanel : {visible:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: true,visible: true},
                columns : [
                    {dataField:'No',caption:'No',width:75,validationRules:[{type:"required"}],allowEditing:false},
                    {dataField:'NPKLama',caption:'NPK Lama'},
                    // {dataField:'Email',caption:'Email',validationRules:[{type:"required"}]},
                    {dataField:'RoleID',caption:'Roles',dataType:'number', lookup:{dataSource:listRole, displayExpr:'Name', valueExpr:'ID'}, validationRules:[{type:"required"}]},
                    {dataField:'Nama',caption:'Karyawan'},
                    {dataField:'UnitKerja',caption:'Unit Kerja'},
                    {dataField:'Area',caption:'Area'},
                    
                    // {dataField:'LastLogin',caption:'Last Login', dataType:'date', format:'dd MMM yyyy H:mm:ss',allowEditing:false},
                    {caption: 'Action', width:100, cellTemplate:function(container,options){
                        $('<a/>').addClass('dx-link')
                        .text('Edit')
                        .on('dxclick', function () {
                            location.href = "{{ url('admin/master/users/edit/') }}" + options.data.ID;
                        })
                        .appendTo(container);
                        $('<a/>').addClass('dx-link')
                        .text('Delete')
                        .on('dxclick', function () {
                            var result = DevExpress.ui.dialog.confirm("Are you sure?", "Confirm Delete");
                            result.done(function (dialogResult) {
                                if(dialogResult) {
                                    location.href = "{{ url('admin/master/users/delete/') }}" + options.data.ID;        
                                }
                            });
                        })
                        .appendTo(container);
                    }},
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
                rowAlternationEnabled: true,
            }).dxDataGrid("instance");
        });
    </script>
    <script type="text/javascript">
        $(function(){
            var dataSource = new DevExpress.data.CustomStore({
                key: "ID",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};
            
                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }
            
                    if (loadOptions.filter != 'undefined') {
                        args.filter = JSON.stringify(loadOptions.filter);
                    }
            
                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 12;
            
                    $.ajax({
                        url: "{{ url("admin/master/get/apps") }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });
            
                    return deferred.promise();
                }
            });

            $("#gridContainerApps").dxDataGrid({
                dataSource : dataSource,
                // columnHidingEnabled: false,
                remoteOperations: {sorting: true,paging: true,filtering:true},
                "export" : {enable:true, fileName : "{{ $table }}"},
                editing : {mode:'inline', allowAdding : false,allowUpdating : false,allowDeleting : false},
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : true},
                headerFilter:{visible:true},
                filterRow:{visible:true,applyFilter:'auto'},
                groupPanel : {visible:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: true,visible: true},
                columns : [
                    {dataField:'No',caption:'No',width:75,validationRules:[{type:"required"}],allowEditing:false},
                    {dataField:'Email',caption:'Email',validationRules:[{type:"required"}]},
                    {dataField:'Name',caption:'Customer'},
                    {dataField:'Vendor',caption:'Vendor'},
                    
                    // {dataField:'LastLogin',caption:'Last Login', dataType:'date', format:'dd MMM yyyy H:mm:ss',allowEditing:false},
                    {caption: 'Action', width:100, cellTemplate:function(container,options){
                        $('<a/>').addClass('dx-link')
                        .text('Edit')
                        .on('dxclick', function () {
                            location.href = "{{ url('admin/master/users/edit/') }}" + options.data.ID + "/apps";
                        })
                        .appendTo(container);
                        $('<a/>').addClass('dx-link')
                        .text('Delete')
                        .on('dxclick', function () {
                            var result = DevExpress.ui.dialog.confirm("Are you sure?", "Confirm Delete");
                            result.done(function (dialogResult) {
                                if(dialogResult) {
                                    location.href = "{{ url('admin/master/users/delete/') }}" + options.data.ID;        
                                }
                            });
                        })
                        .appendTo(container);
                    }},
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
                rowAlternationEnabled: true,
            }).dxDataGrid("instance");
        });
    </script>
@endsection
