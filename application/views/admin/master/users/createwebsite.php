@extends('templates.admin')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
@endsection 

@section('contentheader')
    <section class="content-header">
        <h1>Master Data</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Master Data</li>
            <li class="active">New User</li>
        </ol>
    </section>
@endsection 

@section('content')
<form action="{{ url('admin/master/users/store') }}" method="POST" class="form-horizontal">
    <input type="hidden" name="Type" value="1">
    <div class="box-header with-border">
        <h3 class="box-title">Master Data User Website</h3>
    </div>
    <div class="box-body">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3">Email</label>
                <div class="col-md-9">
                    <input type="text" name="Email" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Password</label>
                <div class="col-md-9">
                    <input type="password" name="Password" id="password" class="form-control">
                    <div class="ldap"></div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <div class="checkbox">
                      <label><input type="checkbox" name="IsLDAP" value="1" id="ldap">LDAP User?</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">ID Karyawan</label>
                <div class="col-md-9">
                    <select name="IDKaryawan" class="form-control select-karyawan"></select>
                </div>
            </div>
            
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3">Roles</label>
                <div class="col-md-9">
                    <select name="RoleID" class="form-control select2">
                        <option></option>
                        @foreach($roles as $role)
                            <option value="{{ $role['ID'] }}">{{ $role['Name']}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3">Area</label>
                <div class="col-md-9">
                    <select name="Area" class="form-control select2">
                        
                            <option value="Pusat">Pusat</option>
                            <option value="Dept. PSO 1">Dept. PSO 1</option>
                            <option value="Dept. PSO 2">Dept. PSO 2</option>
                        
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <a href="{{ url('admin/master/users') }}" class="btn btn-warning btn-sm" style="color:#fff"><i class="fa fa-arrow-left"></i> Kembali</a>
        <button type="submit" class="btn btn-primary btn-sm">Create</button>
    </div>
</form>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){

            var checkbox = document.getElementById('ldap');
                checkbox.addEventListener('click', function () {
                    if (document.getElementById('mn')) {
                        document.getElementById('mn').remove();
                        $('#password').prop("disabled", false);
                    } else {
                        var input = document.createElement("input");
                        input.id = 'mn';
                        input.className = 'form-control';
                        input.type = 'hidden';
                        input.name = 'Password';
                        input.value = '';
                        document.body.appendChild(input);
                        $( ".ldap" ).append( input );
                        $('#password').prop("disabled", true);

                    }
                });
            $('.select2').select2({
                placeholder:'Pilih...',
            });

            $('.select-karyawan').select2({
                ajax: {
                    type:'GET',
                    dataType: 'json',
                    url: '{{ url('admin/users/get_karyawan') }}',
                    data: function (params) {
                        return {q: params.term};
                    },
                    processResults: function (data) {
                        return {results: $.map(data, function(obj){return {id:obj.ID, text:obj.Name}})}
                    },
                    cache:true
                },
                minimumInputLength:3,
                placeholder:'Pilih Karyawan...',
            });
        });
    </script>
@endsection
