@extends('templates.admin')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
@endsection

@section('title','Customer')

@section('contentheader')
    <section class="content-header">
        <h1>Master Data</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Master Data</li>
            <li class="active">Customer</li>
        </ol>
    </section>
@endsection 

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">Master Data Customer</h3>
    </div>
    <div class="box-body">
        <div id="gridContainer"></div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){
            var dataSource = new DevExpress.data.CustomStore({
                key: "IDCustomer",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};
            
                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }
            
                    if (loadOptions.filter != 'undefined') {
                        args.filter = JSON.stringify(loadOptions.filter);
                    }
            
                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 12;
            
                    $.ajax({
                        url: "{{ url("api/get/Ms_Customer") }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });
            
                    return deferred.promise();
                },
                insert :  function(values){
                    return $.ajax({
                        url : "{{ url("api/store/Ms_Customer") }}",
                        method : 'POST',
                        data : values
                    });
                },
                remove : function(key) {
                    return $.ajax({
                        url: "{{ url("api/destroy/Ms_Customer/") }}" + encodeURIComponent(key),
                        method: "DELETE",
                    })
                },
                update : function(key, values) {
                    return $.ajax({
                        url: "{{ url("api/update/Ms_Customer/") }}" + encodeURIComponent(key),
                        method: "POST",
                        data: values
                    })
                }
            });

             var provincies = {!! json_encode($provincies) !!}
             var cities = {!! json_encode($cities) !!}
             var countries = {!! json_encode($countries) !!}
             var yes_no = [{'ID':1,'Name':'Yes'},{'ID':0,'Name':'No'}];

             var vendors =  new DevExpress.data.CustomStore({
                load: function (loadOptions) {
                    var d = new $.Deferred();
                    var filter; 
                    if (loadOptions.searchValue) {
                        filter = [loadOptions.searchExpr, loadOptions.searchOperation, loadOptions.searchValue]; 
                    }
                    $.getJSON('{{ url('admin/master/list_vendor') }}', { filter: filter, skip: loadOptions.skip, take: loadOptions.take }).done(function (data) {
                        d.resolve(data); 
                    });                
                    return d.promise();
                },
                byKey: function(key, extra){
                    
                },
                paginate:true
            });

            $("#gridContainer").dxDataGrid({
                dataSource : dataSource,
                // columnHidingEnabled: false,
                remoteOperations: {sorting: true,paging: true,filtering:true},
                "export" : {enabled:true, fileName:'{{ $table }}',allowExportSelectedData:true},
                editing : {mode:'popup', allowAdding : true,allowUpdating : true,allowDeleting : true},
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : true},
                headerFilter:{visible:true},
                filterRow:{visible:true,applyFilter:'auto'},
                groupPanel : {visible:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: true,visible: true},
                columns : [
                    {dataField:'IDCustomer',caption:'IDCustomer',validationRules:[{type:"required"}]},
                    {dataField:'PairedVendorID',caption:'PairedVendorID',lookup:{dataSource:vendors, displayExpr:'Name', valueExpr:'ID', paginate:true,pageSize:10},validationRules:[{type:"required"}]},
                    {dataField:'NPWP',caption:'NPWP'},
                    {dataField:'Customer',caption:'Customer'},
                    {dataField:'IsWAPU',caption:'IsWAPU',dataType:'number',lookup:{dataSource:yes_no, displayExpr:'Name', valueExpr:'ID'}, validationRules:[{type:"required"}]},
                    {dataField:'Alamat1',caption:'Alamat1'},
                    {dataField:'Alamat2',caption:'Alamat2'},
                    {dataField:'Kota',caption:'Kota',dataType:'number',lookup:{dataSource:cities, displayExpr:'Name', valueExpr:'ID'},validationRules:[{type:"required"}]},
                    {dataField:'Propinsi',caption:'Propinsi',dataType:'number',lookup:{dataSource:provincies, displayExpr:'Name', valueExpr:'ID'},validationRules:[{type:"required"}]},
                    {dataField:'Country',caption:'Country',dataType:'number',lookup:{dataSource:countries, displayExpr:'Name', valueExpr:'ID'},validationRules:[{type:"required"}]},
                    {dataField:'Email',caption:'Email',validationRules:[{type:"required"}]},
                    {dataField:'Phone',caption:'Phone',validationRules:[{type:"required"}]},
                    {dataField:'KodePos',caption:'KodePos',validationRules:[{type:"required"}]},
                    {dataField:'Keterangan',caption:'Keterangan'},
                    // {dataField:'CreatedBy',caption:'CreatedBy',allowEditing:false},
                    // {dataField:'CreatedOn',caption:'CreatedOn', dataType:'date', format:'dd MMM yyyy H:mm:ss',allowEditing:false},
                    // {dataField:'UpdatedBy',caption:'UpdatedBy',allowEditing:false},
                    // {dataField:'UpdatedOn',caption:'UpdatedOn', dataType:'date', format:'dd MMM yyyy H:mm:ss',allowEditing:false}
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
                rowAlternationEnabled: true,
            }).dxDataGrid("instance");
        });
    </script>
@endsection
