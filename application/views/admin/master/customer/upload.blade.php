@extends('templates.admin')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
@endsection

@section('contentheader')
	<section class="content-header">
		<h1>Master Data</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li>Master Data</li>
			<li class="active">Customer</li>
		</ol>
	</section>
@endsection

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">Upload Customer</h3>
    </div>
    <div class="box-body">
		<form action="{{ url('admin/customer/upload_data') }}" method="POST" enctype="multipart/form-data">
			<div class="form-group">
				<label for="">Files</label>
				<input type="file" name="file" />
			</div>
			<div class="form-group">
				<button class="btn btn-primary">Upload</button>
			</div>
		</form>
    </div>
@endsection
