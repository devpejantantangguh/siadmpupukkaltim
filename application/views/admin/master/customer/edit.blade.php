@extends('templates.admin')

@section('title','Edit Customer')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
@endsection 

@section('contentheader')
    <section class="content-header">
        <h1>Master Data</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Master Data</li>
            <li class="active">Customer</li>
        </ol>
    </section>
@endsection 

@section('content')
<form action="{{ url('admin/master/customer/update/'.$id) }}" method="POST" class="form-horizontal">
    <div class="box-header with-border">
        <h3 class="box-title">Master Data Customer</h3>
    </div>
    <div class="box-body">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3">ID Customer</label>
                <div class="col-md-9">
                    <input type="text" name="IDCustomer" class="form-control" value="{{ $data->IDCustomer }}" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Paired Vendor</label>
                <div class="col-md-9">
                    <select name="PairedVendorID" class="select-vendor form-control">
                        @if(!empty($vendor))
                            <option value="{{ $vendor->KODE_VENDOR_SAP }}" selected>{{ $vendor->NAMA }} - {{ $vendor->KODE_VENDOR_SAP }}</option>
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">NPWP</label>
                <div class="col-md-9">
                    <input type="text" name="NPWP" class="form-control" value="{{ $data->NPWP }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Customer Name</label>
                <div class="col-md-9">
                    <input type="text" name="Customer" class="form-control" value="{{ $data->Customer }}" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">IsWAPU</label>
                <div class="col-md-9">
                    <label>
                        <input type="checkbox" name="IsWAPU" value="1" {{ $data->IsWAPU == 1 ? 'selected' : '' }}> Customer Wajib Pungut
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Alamat 1</label>
                <div class="col-md-9">
                    <textarea name="Alamat1" class="form-control" required>{{ $data->Alamat1 }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Alamat 2</label>
                <div class="col-md-9">
                    <textarea name="Alamat2" class="form-control">{{ $data->Alamat2 }}</textarea>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3">Country</label>
                <div class="col-md-9">
                    <select name="Country" class="select-country form-control" required>
                        @if(!empty($country))
                            <option value="{{ $country->ID }}" selected>{{ $country->Country }}</option>
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Propinsi</label>
                <div class="col-md-9">
                    <select name="Propinsi" class="select-propinsi form-control">
                        @if(!empty($propinsi))
                            <option value="{{ $propinsi->ID }}" selected>{{ $propinsi->Propinsi }}</option>
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Kota</label>
                <div class="col-md-9">
                    <select name="Kota" class="select-kota form-control">
                        @if(!empty($kota))
                            <option value="{{ $kota->ID }}" selected>{{ $kota->Kota }}</option>
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Email</label>
                <div class="col-md-9">
                    <input type="text" name="Email" class="form-control" value="{{ $data->Email }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Phone</label>
                <div class="col-md-9">
                    <input type="text" name="Phone" class="form-control" value="{{ $data->Phone }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Kodepos</label>
                <div class="col-md-9">
                    <input type="text" name="KodePos" class="form-control" value="{{ $data->KodePos }}">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Keterangan</label>
                <div class="col-md-9">
                    <textarea name="Keterangan" class="form-control">{{ $data->Keterangan }}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <a href="{{ url('admin/master/customer') }}" class="btn btn-warning btn-sm" style="color:#fff"><i class="fa fa-arrow-left"></i> Kembali</a>
        <button type="submit" class="btn btn-primary btn-sm">Update</button>
    </div>
</form>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){
            $('.select-country').select2({
                ajax: {
                    type:'GET',
                    dataType: 'json',
                    url: '{{ url('admin/customer/country') }}',
                    data: function (params) {
                        return {q: params.term};
                    },
                    processResults: function (data) {
                        return {results: $.map(data, function(obj){return {id:obj.id, text:obj.text}})}
                    },
                    cache:true
                },
                minimumResultsForSearch:-1,
                placeholder:'Pilih Country...',
            });

            $('.select-propinsi').select2({
                ajax: {
                    type:'GET',
                    dataType: 'json',
                    url: '{{ url('admin/customer/propinsi') }}',
                    data: function (params) {
                        return {q: params.term};
                    },
                    processResults: function (data) {
                        return {results: $.map(data, function(obj){return {id:obj.id, text:obj.text}})}
                    },
                    cache:true
                },
                placeholder:'Pilih Propinsi...',
            });

            $('.select-kota').select2({
                ajax: {
                    type:'GET',
                    dataType: 'json',
                    url: function(){
                        var id_propinsi = $('.select-propinsi').val();
                        console.log(id_propinsi);
                        return '{{ url('admin/customer/kota') }}' + '?p=' + id_propinsi
                    },
                    data: function (params) {
                        return {q: params.term};
                    },
                    processResults: function (data) {
                        return {results: $.map(data, function(obj){return {id:obj.id, text:obj.text}})}
                    },
                    cache:true
                },
                minimumResultsForSearch:-1,
                placeholder:'Pilih Kota...',
            });

            $('.select-vendor').select2({
                ajax: {
                    type:'GET',
                    dataType: 'json',
                    url: '{{ url('admin/customer/vendor') }}',
                    data: function (params) {
                        return {q: params.term};
                    },
                    processResults: function (data) {
                        return {results: $.map(data, function(obj){return {id:obj.ID, text:obj.Name}})}
                    },
                    cache:true
                },
                minimumInputLength:3,
                placeholder:'Pilih Vendor...',
            });
        });
    </script>
@endsection
