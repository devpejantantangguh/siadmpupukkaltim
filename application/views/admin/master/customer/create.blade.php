@extends('templates.admin')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
@endsection 

@section('contentheader')
    <section class="content-header">
        <h1>Master Data</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Master Data</li>
            <li class="active">Customer</li>
        </ol>
    </section>
@endsection 

@section('content')
<form action="{{ url('admin/master/customer/store') }}" method="POST" class="form-horizontal">
    <div class="box-header with-border">
        <h3 class="box-title">Master Data Customer</h3>
    </div>
    <div class="box-body">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3">ID Customer</label>
                <div class="col-md-9">
                    <input type="text" name="IDCustomer" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Paired Vendor</label>
                <div class="col-md-9">
                    <select name="PairedVendorID" class="select-vendor form-control"></select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">NPWP</label>
                <div class="col-md-9">
                    <input type="text" name="NPWP" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Customer Name</label>
                <div class="col-md-9">
                    <input type="text" name="Customer" class="form-control" required>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">IsWAPU</label>
                <div class="col-md-9">
                    <label>
                        <input type="checkbox" name="IsWAPU" value="1"> Customer Wajib Pungut
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Alamat 1</label>
                <div class="col-md-9">
                    <textarea name="Alamat1" class="form-control" required></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Alamat 2</label>
                <div class="col-md-9">
                    <textarea name="Alamat2" class="form-control"></textarea>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3">Country</label>
                <div class="col-md-9">
                    <select name="Country" class="select-country form-control" required></select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Propinsi</label>
                <div class="col-md-9">
                    <select name="Propinsi" class="select-propinsi form-control"></select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Kota</label>
                <div class="col-md-9">
                    <select name="Kota" class="select-kota form-control"></select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Email</label>
                <div class="col-md-9">
                    <input type="text" name="Email" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Phone</label>
                <div class="col-md-9">
                    <input type="text" name="Phone" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Kodepos</label>
                <div class="col-md-9">
                    <input type="text" name="KodePos" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Keterangan</label>
                <div class="col-md-9">
                    <textarea name="Keterangan" class="form-control"></textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <a href="{{ url('admin/master/customer') }}" class="btn btn-warning btn-sm" style="color:#fff"><i class="fa fa-arrow-left"></i> Kembali</a>
        <button type="submit" class="btn btn-primary btn-sm">Create</button>
    </div>
</form>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){
            $('.select-country').select2({
                ajax: {
                    type:'GET',
                    dataType: 'json',
                    url: '{{ url('admin/customer/country') }}',
                    data: function (params) {
                        return {q: params.term};
                    },
                    processResults: function (data) {
                        return {results: $.map(data, function(obj){return {id:obj.id, text:obj.text}})}
                    },
                    cache:true
                },
                minimumResultsForSearch:-1,
                placeholder:'Pilih Country...',
            });

            $('.select-propinsi').select2({
                ajax: {
                    type:'GET',
                    dataType: 'json',
                    url: '{{ url('admin/customer/propinsi') }}',
                    data: function (params) {
                        return {q: params.term};
                    },
                    processResults: function (data) {
                        return {results: $.map(data, function(obj){return {id:obj.id, text:obj.text}})}
                    },
                    cache:true
                },
                placeholder:'Pilih Propinsi...',
            });

            $('.select-kota').select2({
                ajax: {
                    type:'GET',
                    dataType: 'json',
                    url: function(){
                        var id_propinsi = $('.select-propinsi').val();
                        console.log(id_propinsi);
                        return '{{ url('admin/customer/kota') }}' + '?p=' + id_propinsi
                    },
                    data: function (params) {
                        return {q: params.term};
                    },
                    processResults: function (data) {
                        return {results: $.map(data, function(obj){return {id:obj.id, text:obj.text}})}
                    },
                    cache:true
                },
                minimumResultsForSearch:-1,
                placeholder:'Pilih Kota...',
            });

            $('.select-vendor').select2({
                ajax: {
                    type:'GET',
                    dataType: 'json',
                    url: '{{ url('admin/customer/vendor') }}',
                    data: function (params) {
                        return {q: params.term};
                    },
                    processResults: function (data) {
                        return {results: $.map(data, function(obj){return {id:obj.ID, text:obj.Name}})}
                    },
                    cache:true
                },
                minimumInputLength:3,
                placeholder:'Pilih Vendor...',
            });
        });
    </script>
@endsection
