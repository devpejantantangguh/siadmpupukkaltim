@extends('templates.admin')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
@endsection

@section('title','Material')

@section('contentheader')
    <section class="content-header">
        <h1>Master Data</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Master Data</li>
            <li class="active">Material</li>
        </ol>
    </section>
@endsection 

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">Master Data Material</h3>
    </div>
    <div class="box-body">
        <div id="gridContainer"></div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){
            var dataSource = new DevExpress.data.CustomStore({
                key: "IDMaterial",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};
            
                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }
            
                    if (loadOptions.filter != 'undefined') {
                        args.filter = JSON.stringify(loadOptions.filter);
                    }
            
                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 12;
            
                    $.ajax({
                        url: "{{ url("api/get/Ms_Material") }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });
            
                    return deferred.promise();
                },
                insert :  function(values){
                    return $.ajax({
                        url : "{{ url("api/store/Ms_Material") }}",
                        method : 'POST',
                        data : values
                    });
                },
                remove : function(key) {
                    return $.ajax({
                        url: "{{ url("api/destroy/Ms_Material/") }}" + encodeURIComponent(key),
                        method: "DELETE",
                    })
                },
                update : function(key, values) {
                    console.log(key);
                    console.log(values);
                    return $.ajax({
                        url: "{{ url("api/update/Ms_Material/") }}" + encodeURIComponent(key),
                        method: "POST",
                        data: values
                    })
                }
            });

            $("#gridContainer").dxDataGrid({
                dataSource : dataSource,
                // columnHidingEnabled: false,
                remoteOperations: {sorting: true,paging: true,filtering:true},
                "export" : {enabled:true, fileName:'{{ $table }}',allowExportSelectedData:true},
                editing : {mode:'form', allowAdding : true,allowUpdating : true,allowDeleting : true},
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : true},
                headerFilter:{visible:true},
                filterRow:{visible:true,applyFilter:'auto'},
                groupPanel : {visible:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: true,visible: true},
                columns : [
                    {dataField:'IDMaterial',caption:'ID',validationRules:[{type:"required"}]/*,allowEditing:false*/},
                    {dataField:'Material',caption:'Material',validationRules:[{type:"required"}]},
                    {dataField:'Keterangan',caption:'Keterangan'},
                    // {dataField:'CreatedBy',caption:'Created By',allowEditing:false,formItem:{visible:false}},
                    // {dataField:'CreatedOn',caption:'Created On', dataType:'date', format:'dd MMM yyyy H:mm:ss',allowEditing:false,formItem:{visible:false}},
                    // {dataField:'UpdatedBy',caption:'Updated By',allowEditing:false,formItem:{visible:false}},
                    // {dataField:'UpdatedOn',caption:'Updated On', dataType:'date', format:'dd MMM yyyy H:mm:ss',allowEditing:false,formItem:{visible:false}},
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
                rowAlternationEnabled: true,
            }).dxDataGrid("instance");
        });
    </script>
@endsection
