@extends('templates.admin')

@section('title','Panduan')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
    <style type="text/css">
        .bulet {height: 15px;width: 15px;display: block;border-radius: 30px;margin: 0 auto;}
        .b-green {background: green;}
        .b-red {background: red;}
        .dx-link{padding-right:5px;}
    </style>
@endsection 

@section('contentheader')
    <section class="content-header">
        <h1>Panduan</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Master Data</li>
            <li class="active">Panduan</li>
        </ol>
    </section>
@endsection 

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">Panduan</h3>
    </div>
    <div class="box-body">
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {{ flashdata('error') }}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> {{ flashdata('warning') }}
            </div>

        @elseif(flashdata('info'))

            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Info!</strong> {{ flashdata('info') }}
            </div>
        @endif
        <div id="btn-create"></div>
        <div id="btn-edit"></div>
        <div id="gridContainer"></div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){
            var dataSource = new DevExpress.data.CustomStore({
                key: "ID",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};
            
                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }
            
                    if (loadOptions.filter != 'undefined') {
                        args.filter = JSON.stringify(loadOptions.filter);
                    }
            
                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 12;
            
                    $.ajax({
                        url: "{{ url("admin/panduan/get") }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });
            
                    return deferred.promise();
                },
                remove : function(key) {
                    return $.ajax({
                        url: "{{ url("admin/panduan/destroy/") }}" + encodeURIComponent(key),
                        method: "DELETE",
                    })
                },
            });

            var dxBtnCreate = $("#btn-create").dxButton({
                text: "Create",
                height: 34,
                width: 100,
                disabled: false,
                type:'success',
                onClick: function () {
                    location.href = "{{ url('admin/panduan/create') }}";
                }
            }).dxButton("instance");

            var dataGrid = $("#gridContainer").dxDataGrid({
                dataSource : dataSource,
                remoteOperations: {sorting: true,paging: true,filtering:false},
                "export" : {enabled:true, fileName:'{{ $table }}',allowExportSelectedData:true},
                editing : {mode:'form', allowAdding : false,allowUpdating : false,allowDeleting : false},
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : true},
                headerFilter:{visible:true},
                filterRow:{visible:true,applyFilter:'auto'},
                groupPanel : {visible:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                selection:{mode:'single'},
                pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: true,visible: true},
                columns : [
                    {dataField:'ID',caption:'ID', width:100},
                    {dataField:'Judul',caption:'Judul'},
                    {dataField:'Deskripsi',caption:'Deskripsi'},
                    {dataField:'FilePath',caption:'FilePath'},
                    {dataField:'Keterangan',caption:'Keterangan'},
                    {dataField:'Aktif',caption:'Aktif', allowFiltering:false, allowSorting:true, width:50, cellTemplate:function(container,options){
                        var color = (options.value == 1) ? "bulet b-green" : "bulet b-red";
                        $("<div>").append($("<div>",{"class" : color})).appendTo(container);
                    }},
                    {caption: '', width:100, cellTemplate:function(container,options){
                        $('<a/>').addClass('dx-link')
                        .text('Edit')
                        .on('dxclick', function () {
                            location.href = "{{ url('admin/panduan/edit/') }}" + options.data.ID;
                        })
                        .appendTo(container);
                        $('<a/>').addClass('dx-link')
                        .text('Delete')
                        .on('dxclick', function () {
                            var result = DevExpress.ui.dialog.confirm("Are you sure?", "Confirm Delete");
                            result.done(function (dialogResult) {
                                return $.ajax({
                                    url: "{{ url("admin/panduan/destroy/") }}" + options.data.ID,
                                    method: "DELETE",
                                }).done(function(){
                                    dataGrid.refresh();    
                                });
                                
                            });
                        })
                        .appendTo(container);
                    }},
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
                rowAlternationEnabled: true,
            }).dxDataGrid("instance");
        });
    </script>
@endsection
