@extends('templates.admin')

@section('title','Edit Panduan')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
@endsection 

@section('contentheader')
    <section class="content-header">
        <h1>Edit Panduan</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Admin</li>
            <li class="active">Edit Panduan</li>
        </ol>
    </section>
@endsection 

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">Edit Panduan</h3>
    </div>
    <div class="box-body">
        <form action="{{ url('admin/panduan/update/'.$id) }}" method="POST" class="form-horizontal">

            <div class="form-group">
                <div class="col-md-2"><label>Judul : </label></div>
                <div class="col-md-3">
                    <div id="dxTextJudul"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-2"><label>Deskripsi : </label></div>
                <div class="col-md-4">
                    <div id="dxAreaDeskripsi"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-2"><label>FilePath : </label></div>
                <div class="col-md-4">
                    <div id="dxTextFilePath"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-2"><label>Keterangan : </label></div>
                <div class="col-md-4">
                    <div id="dxAreaKeterangan"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-2"><label>Aktif : </label></div>
                <div class="col-md-4">
                    <div id="dxCbAktif"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-4 col-md-offset-2">
                    <div id="dxBtnCreate"></div>
                </div>
            </div>

        </form>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){

            var dxTextJudul = $('#dxTextJudul').dxTextBox({
                name:'Judul', placeholder:'Tuliskan Judul...', value: "{{ $row->Judul }}"
            }).dxValidator({validationRules: [{type: "required",message: "Judul is required"}]}).dxTextBox("instance");

            var dxAreaDeskripsi = $('#dxAreaDeskripsi').dxTextArea({
                name:'Deskripsi',value:"",placeholder:"Tuliskan Deskripsi...", value:"{{ $row->Deskripsi }}"
            }).dxValidator({validationRules: [{type: "required",message: "Deskripsi is required"}]}).dxTextArea("instance");

             var dxTextFilePath = $('#dxTextFilePath').dxTextBox({
                name:'FilePath', placeholder:'Tuliskan Path...', value:"{{ $row->FilePath }}"
            }).dxValidator({validationRules: [{type: "required",message: "Path is required"}]}).dxTextBox("instance");

            var dxAreaKeterangan = $('#dxAreaKeterangan').dxTextArea({
                name:'Keterangan',value:"",placeholder:"Tuliskan Keterangan...", value : "{{ $row->Keterangan }}"
            }).dxValidator({validationRules: [{type: "required",message: "Keterangan is required"}]}).dxTextArea("instance");

            var dxCbAktif = $("#dxCbAktif").dxCheckBox({
                name:'Aktif', "text": "Aktif", value:{{$row->Aktif}}
            }).dxCheckBox("instance");

            var dxBtnSend = $("#dxBtnCreate").dxButton({
                "text": "Update Panduan", useSubmitBehavior:true, type:'success'
            }).dxButton("instance");

        });
    </script>
@endsection
