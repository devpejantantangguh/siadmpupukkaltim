@extends('templates.admin')

@section('title','Pesan')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
    <style type="text/css">
        .bulet {height: 15px;width: 15px;display: block;border-radius: 30px;margin: 0 auto;}
        .b-green {background: green;}
        .b-red {background: red;}
        .dx-link{padding-right:5px;}
    </style>
@endsection 

@section('contentheader')
    <section class="content-header">
        <h1>Message</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Master Data</li>
            <li class="active">Message</li>
        </ol>
    </section>
@endsection 

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">Message</h3>
    </div>
    <div class="box-body">
        <a class="btn btn-success btn-sm" href="{{ url('admin/message/create') }}" style="color:white"><i class="fa fa-envelope"></i> New Message</a>
        <a class="btn btn-success btn-sm" href="{{ url('admin/message/import') }}" style="color:white"><i class="fa fa-arrow-up"></i> Import Message</a>
		<a class="btn btn-danger btn-sm" style="color:white" id="delete"><i class="fa fa-trash"></i> Delete</a>
        <br>
        <div id="dataGrid"></div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){

            $('#delete').click(function(e){
				e.preventDefault();

				var lists = []
				$.each(dataGrid.getSelectedRowsData(), function(){
				    lists.push(this.ID);
				});

				$.post("{{ url("admin/message/delete_ajax") }}?ids=" + lists.join(','))
					.always(function(){
					    dataGrid.refresh();
					});

			});

            var dataSource = new DevExpress.data.CustomStore({
                // key: "IDBillDocHeader",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};
            
                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }
            
                    if (loadOptions.filter != 'undefined') {
                        args.filter = JSON.stringify(loadOptions.filter);
                    }
            
                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 12;
            
                    $.ajax({
                        url: "{{ url("admin/message/get") }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });
            
                    return deferred.promise();
                }
            });

            var dataGrid = $("#dataGrid").dxDataGrid({
                dataSource : dataSource,
                // columnHidingEnabled: true,
                remoteOperations: {sorting: true,paging: true,filtering:false},
                "export" : {enabled:true, fileName:'{{ $table }}',allowExportSelectedData:true},
                editing : {mode:'form', allowAdding : false,allowUpdating : false,allowDeleting : false},
                selection:{mode:'multiple',showCheckBoxesMode:'always',selectAllMode:'page'},
                onSelectionChanged:function(data){
                    // editButton.option('disabled', !(data.selectedRowsData.length == 1))
                },
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : true},
                groupPanel : {visible:true},
                headerFilter:{visible:true},
                filterRow:{visible:true,applyFilter:'auto'},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: true,visible: true},
                columns : [
                    {dataField:'IDRekanan', caption: 'Tujuan',width:100},
                    {caption: 'Type', width:100, calculateCellValue:function(data){
                        if(data.IsBoardcast == 1) {
                            return 'Boardcast';
                        }else if(data.IsCustomer == 1) {
                            return 'Customer';
                        }else if(data.IsVendor == 1) {
                            return 'Vendor';
                        }
                    }},
                    {dataField:'IsBoardcast', caption: 'Boardcast', width:100, cellTemplate:function(container,options){
                        var color = (options.value == 1) ? "bulet b-green" : "bulet b-red";
                        $("<div>").append($("<div>",{"class" : color})).appendTo(container);
                    }},
                    {dataField:'Judul', caption: 'Judul'},
                    {dataField:'Pesan', caption: 'Pesan'},
                    {dataField:'EndDate', caption: 'End Date', width:100},
                    {caption: 'Action', width:100, cellTemplate:function(container,options){
                        $('<a/>').addClass('dx-link')
                        .text('Edit')
                        .on('dxclick', function () {
                            location.href = "{{ url('admin/message/edit/') }}" + options.data.ID;
                        })
                        .appendTo(container);
                        $('<a/>').addClass('dx-link')
                        .text('Delete')
                        .on('dxclick', function () {
                            var result = DevExpress.ui.dialog.confirm("Are you sure?", "Confirm Delete");
                            result.done(function (dialogResult) {
                                if(dialogResult) {
                                    location.href = "{{ url('admin/message/destroy/') }}" + options.data.ID;    
                                }
                            });
                        })
                        .appendTo(container);
                    }},
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
                rowAlternationEnabled: true,
            }).dxDataGrid("instance");
        });
    </script>
@endsection
