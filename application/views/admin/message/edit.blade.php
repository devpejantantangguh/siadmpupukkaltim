@extends('templates.admin')

@section('title','Edit Pesan')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
@endsection 

@section('contentheader')
    <section class="content-header">
        <h1>Create Message</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Master Data</li>
            <li class="active">Create Message</li>
        </ol>
    </section>
@endsection 

@section('content')
<form action="{{ url('admin/message/update/' . $row->ID) }}" method="POST" class="form-horizontal">
    <div class="box-header with-border">
        <h3 class="box-title">Create Message</h3>
    </div>
    <div class="box-body">
        <div class="form-group">
            <div class="col-md-2"><label>To : </label></div>
            <div class="col-md-2">
                {!! form_dropdown('who',[1 => 'Vendor',2 => 'Customer'], $who, ['class' => 'form-control select2','id' => 'who']) !!}
            </div>
            <div class="col-md-4">
                <select name="IDRekanan" class="js-select2 form-control">
                    <option value="{{ $row->IDRekanan }}" selected="selected">{{ $optionValue }}</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2"><label>Subject : </label></div>
            <div class="col-md-4">
                <input type="text" name="Judul" value="{{ $row->Judul }}" class="form-control">
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2"><label>Message : </label></div>
            <div class="col-md-4">
                <textarea name="Pesan" class="form-control" rows="10">{{ $row->Pesan }}</textarea>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2"><label>End Date : </label></div>
            <div class="col-md-4">
                <input type="text" name="EndDate" value="{{ $row->EndDate }}" class="form-control datepicker">
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-2"><label>Boardcast : </label></div>
            <div class="col-md-4">
                <label>
                    {!! form_checkbox('IsBoardcast',null, $row->IsBoardcast) !!} Boardcast
                </label>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <a href="{{ url('admin/message') }}" class="btn btn-warning btn-sm" style="color:#fff"><i class="fa fa-arrow-left"></i> Kembali</a>
        <button type="submit" class="btn btn-success btn-sm">Update</button>
    </div>
</form>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){
            $('.datepicker').datepicker({autoclose:true});

            $('.select2').select2({minimumResultsForSearch:-1});
            $('.select2').on('select2:selecting', function(e){
                $('.js-select2').val('').trigger('change');
            });
            $('.js-select2').select2({
                ajax: {
                    type:'GET',
                    dataType: 'json',
                    url: '{{ url('admin/message/search') }}',
                    data: function (params) {
                        var whois = $('#who').val();

                        var query = {
                            q: params.term,
                            type: whois
                        }
                        return query;
                    },
                    minimumInputLength:3,
                    placeholder:'select',
                    processResults: function (data) {
                        return {
                            results: $.map(data, function(obj){
                                return {id:obj.id, text:obj.text}
                            })
                        }
                    },
                    cache:true
                }
            });
        });
    </script>
@endsection
