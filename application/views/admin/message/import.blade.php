@extends('templates.admin')

@section('heading')
@endsection

@section('title','Import Pesan')

@section('contentheader')
    <section class="content-header">
        <h1>Import Message</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Master Data</li>
            <li class="active">Import Message</li>
        </ol>
    </section>

    <section class="content" style="max-height: 100px;min-height: 0px">
        <div class="callout callout-info">
            <h4>Important</h4>
            <p>Download Template <a href="{{ url('asset/message.xlsx') }}">Download</a></p>
        </div>
    </section>
@endsection 

@section('content')
    
    <div class="box-header with-border">
        <h3 class="box-title">Import Message</h3>
    </div>
    <div class="box-body">
        <form action="{{ url('admin/message/upload') }}" method="POST" enctype="multipart/form-data">
            <div class="form-group">
                <label>File Excel</label>
                <input type="file" name="file" value="file">
            </div>
            <div class="form-group">
                <a href="{{ url('admin/message') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left"></i> Kembali</a>
                <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-arrow-up"></i> Upload</button>
            </div>
        </form>
    </div>
@endsection

@section('script')
@endsection
