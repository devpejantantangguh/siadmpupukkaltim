@extends('templates.admin')

@section('title','List Invoice')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/jszip.min.js"></script>
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
    <script src="{{ url('asset/js/phpjs.js') }}"></script>
@endsection

@section('contentheader')
    <section class="content-header">
        <h1>List Invoice</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Invoice (Unit Kerja)</li>
            <li class="active">List Invoice</li>
        </ol>
    </section>
@endsection

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">List Invoice</h3>
        <span class="pull-right">Area: {{Auth::unitKerja()->Area}}</span>
    </div>
    <div class="box-body">
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {{ flashdata('error') }}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> {{ flashdata('warning') }}
            </div>

        @elseif(flashdata('info'))

            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Info!</strong> {{ flashdata('info') }}
            </div>
        @endif
        @if($updater_role == $role)
            <a class="btn btn-success btn-sm" href="{{ url('invoice/invoice/create') }}" style="color:#fff"><i class="fa fa-plus"></i> Add Invoice</a>
            <button class="btn btn-primary btn-sm" id="btn_edit" disabled><i class="fa fa-pencil"></i> Edit</button>
        @endif

        <button class="btn btn-info btn-sm" id="btn_view" disabled><i class="fa fa-eye"></i> View</button>

        @if($updater_role == $role)
            <button class="btn btn-danger btn-sm" id="btn_cancel" disabled><i class="fa fa-close"></i> Cancel</button>
        @endif
        <br><br>
        <div id="gridContainer"></div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){

            $('#btn_edit').click(function(e){
                e.preventDefault();

                var opt = dataGrid.getSelectedRowsData();
                if(opt[0].Status == {{statusID('CONFIRMED')}}) {
                    DevExpress.ui.dialog.confirm("Apakah anda akan membuat menjadi draft?", "Confirm")
                        .done(function(dialogResult){
                            if(dialogResult) {
                                location.href = "{{ url('invoice/invoice/edit/') }}" + opt[0].ID + '?setAsDraft=true';
                            }
                        });
                } else {
                    location.href = "{{ url('invoice/invoice/edit/') }}" + opt[0].ID;
                }
            });

            $('#btn_view').click(function(e){
                e.preventDefault();
                $.each(dataGrid.getSelectedRowsData(), function() {
                    location.href = "{{ url('invoice/invoice/view/') }}" + this.ID
                });
            });
            $('#btn_cancel').click(function(e){
                var result = DevExpress.ui.dialog.confirm("Apakah anda yakin akan melakukan cancel invoice?", "Confirm process");
                result.done(function (dialogResult) {
                    if(dialogResult){
                        e.preventDefault();
                        $.each(dataGrid.getSelectedRowsData(), function() {
                            var ID = this.ID
                            $.post("{{ url('invoice/invoice/cancel/') }}", {id:this.ID})
                            .done(function(response){
                                location.href = "{{ url('invoice/invoice/') }}"
                            }).fail(function(){

                            })
                        });
                    }
                });
            });

            var dataSource = new DevExpress.data.CustomStore({
                key: "ID",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};

                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }

                    if (loadOptions.filter != 'undefined') {
                        args.filter = JSON.stringify(loadOptions.filter);
                    }
                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 20;

                    $.ajax({
                        url: "{{ url("invoice/invoice/get/") }}", // status draft/waiting approval
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });

                    return deferred.promise();
                }
            });

            var list_status = {!! json_encode($statuses)!!};

            var dataGrid = $("#gridContainer").dxDataGrid({
                dataSource : dataSource,
                // columnHidingEnabled: true,
                paging: {
                    pageSize: 10
                },
                remoteOperations: {sorting: true,paging: true,filtering:true},
                export:{enabled:true,allowExportSelectedData: true},
                editing : {mode:'form', allowAdding : false,allowUpdating : false,allowDeleting : false},
                selection:{mode:'multiple',showCheckBoxesMode:'always',selectAllMode:'page'},
                onSelectionChanged: function(data){
                    $('#btn_cancel').prop('disabled', true);
                    $('#btn_edit').prop('disabled', true);
                    $.each(data.selectedRowsData, function() {
                        console.log(data.selectedRowsData[0].Status);
                        if((data.selectedRowsData.length != 1)){
                            $('#btn_cancel').prop('disabled', true)
                            $('#btn_edit').prop('disabled', true)
                        }else{
                            console.log(data.selectedRowsData[0].Status);
                            if(data.selectedRowsData[0].Status == "{{statusID('DRAFT')}}"){
                                $('#btn_cancel').prop('disabled', false)
                                $('#btn_edit').prop('disabled', false)
                            }else if(data.selectedRowsData[0].Status == "{{statusID('CONFIRMED')}}"){
                                $('#btn_cancel').prop('disabled', true)
                                $('#btn_edit').prop('disabled', false)
                            }else{
                                $('#btn_cancel').prop('disabled', true)
                                $('#btn_edit').prop('disabled', true)
                            }
                        }
                    });
                    $('#btn_view').prop('disabled', !(data.selectedRowsData.length == 1));
                    //$('#btn_cancel').prop('disabled', !(data.selectedRowsData.length == 1));

                },
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : false},
                headerFilter:{visible:true},
                filterRow:{visible:true,applyFilter:'auto'},
                groupPanel : {visible:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: false,visible: true},
                columns : [
                    {dataField:'NoInvoice',caption:'No Invoice'},
                    {dataField:'IDVendor',caption:'Kode Vendor'},
                    {dataField:'NamaVendor',caption:'Nama Vendor'},
                    {dataField:'NilaiInvoice',caption:'Nilai Invoice',
                dataType: "number",calculateCellValue:function(data){
                        return data.KursInvoice + ' ' + number_format(data.NilaiInvoice,2,',','.')
                    }},

                    {dataField:'IDArea',caption:'ID Area'},
                    {dataField:'NoFakturPajak',caption:'No Faktur Pajak'},
                    {dataField:'Status',caption:'Status',dataType:'integer',lookup:{dataSource: list_status, displayExpr: "name", valueExpr: "id"}, groupIndex: 0},

                    {dataField:'UnitKerja',caption:'Unit Kerja'},
                    {dataField:'Keterangan',caption:'Keterangan'},
                    {dataField:'Catatan',caption:'Catatan'},
                    {dataField:'CreatedOn',caption:'Created On',dataType: 'date',format: 'dd/MM/yyyy'},
                    {dataField:'NamaCreatedBy',caption:'Created By'},
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
                rowAlternationEnabled: false,

            onRowPrepared: function(e) {
                 console.log(e.rowType);
                if (e.rowType === 'data') {
                    console.log(e.data);
                    if(e.data.Status == "{{statusID('DRAFT')}}"){
                        e.rowElement.css({ "background-color": "#fcd691" });
                    }
                }

            },
            }).dxDataGrid("instance");
        });

    </script>
@endsection
