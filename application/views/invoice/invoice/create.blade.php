@extends('templates.admin')

@section('title', 'Create Invoice')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
    <script src="{{ url('asset/js/phpjs.js') }}"></script>
    <style type="text/css">
        h3.panel-title{font-size: 14px !important}
        .dx-fileuploader-show-file-list .dx-fileuploader-files-container{padding-top:0px}
        .dx-fileuploader-wrapper{padding:0px;}
    </style>
@endsection

@section('contentheader')
    <section class="content-header">
        <h1>Input Invoice</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Invoice (Unit Kerja)</li>
            <li class="active">Input Invoice</li>
        </ol>
    </section>
@endsection

@section('content')
    <form action="{{ url('invoice/invoice/store') }}" method="POST" class="form-horizontal">
        <div class="box-header with-border">
            <h3 class="box-title">Input Invoice</h3>
        </div>
        <div class="box-body">
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {{ flashdata('error') }}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> {{ flashdata('warning') }}
            </div>

        @elseif(flashdata('info'))

            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Info!</strong> {{ flashdata('info') }}
            </div>
        @endif
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Detail Invoice</h3>
                </div>
                <div class="panel-body">

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Vendor</label>
                        <div class="col-sm-4">
                            <select name="IDVendor" class="js-vendor form-control" required>
                            </select>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Jenis Invoice*</label>
                        <div class="col-sm-4">
                            {!! form_dropdown('JenisTransaksi', $list_transaksi, set_value('JenisTransaksi'), ['class' => 'form-control select2', 'required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group {{ form_error('NoInvoice') ? 'has-error' : '' }}">
                        <label class="col-sm-2 control-label">No. Invoice</label>
                        <div class="col-sm-4">
                            <input type="text" name="NoInvoice" value="{{ set_value('NoInvoice') }}" class="form-control" required>
                        </div>
                        <div class="col-sm-4">
                            <span class="help-block" style="color:red">{!! flashdata('error_invoice') !!}</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tgl. Invoice</label>
                        <div class="col-sm-2">
                            <div class="input-group date">
                                <input type="text" name="TglInvoice" class="form-control pull-right datepicker" value="{{ date('d-m-Y') }}" required>
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nilai Invoice</label>
                        <div class="col-sm-4">
                            <input type="text" name="NilaiInvoice" value="{{ set_value('NilaiInvoice') }}" class="form-control" required>
                            <span class="help-block">* Nilai sebelum pajak</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Kurs</label>
                        <div class="col-sm-1">
                            {!! form_dropdown('KursInvoice', $list_kurs, set_value('KursInvoice'), ['class' => 'form-control select2','required' => 'required']) !!}
                        </div>
                    </div>

                    <div class="form-group {{ form_error('NoFakturPajak') ? 'has-error' : '' }}">
                        <label class="col-sm-2 control-label">No. Faktur Pajak</label>
                        <div class="col-sm-4">
                            <input type="text" name="NoFakturPajak" value="{{ set_value('NoFakturPajak') }}" class="form-control" required maxlength="16" minlength="16">
                        </div>
                        <div class="col-sm-4">
                            <span class="help-block" style="color:red">{!! flashdata('error_faktur_pajak') !!}</span>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal Faktur Pajak</label>
                        <div class="col-sm-2">
                            <div class="input-group date">
                                <input type="text" name="TglFakturPajak" class="form-control pull-right datepicker" value="{{ date('d-m-Y') }}" required>
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Unit Kerja</label>
                        <div class="col-sm-4">
                            {!! form_dropdown('CostCenter', Auth::matriksUnitKerja(), set_value('CostCenter'), ['class' => 'form-control select2', 'required' => 'required']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-4">
                            <textarea name="Keterangan" class="form-control">{{ set_value('Keterangan') }}</textarea>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="box-footer">
            <a href="{{ url('invoice/invoice') }}" class="btn btn-warning btn-sm" style="color:#fff">
                <i class="fa fa-arrow-left"></i> Back
            </a>
            <button type="submit" class="btn btn-success btn-sm pull-right submit">
                Next <i class="fa fa-arrow-right"></i>
            </button>
        </div>
    </form>
@endsection

@section('script')
    <script type="text/javascript">
        $(function() {

            $('form').submit(function(e){
                e.preventDefault();
                var lengthPajak = $('input[name=NoFakturPajak]').val().length;
                if(lengthPajak != 16) {
                    DevExpress.ui.dialog.alert('No Faktur Pajak Harus 16 Karakter','Error');
                    $('form').find("button[type='submit']").prop('disabled',false);
                    return;
                }
                var result = DevExpress.ui.dialog.confirm("Apakah anda yakin?", "Confirm");
                    result.done(function (dialogResult) {
                        if(dialogResult) {
                            $('form').unbind('submit').submit();
                        }else{
                            $('form').find("button[type='submit']").prop('disabled',false);
                        }
                });

            });
            $('input[name=NoFakturPajak]').mask('0000000000000000', {reverse:true})
            $('input[name=NilaiInvoice]').inputmask('numeric', {
                groupSeparator: '.',
                radixPoint : ',',
                autoGroup: true,
                digits: 2,
                digitsOptional: false,
                placeholder: '0,00'
            });
            $('.datepicker').datepicker({autoclose: true, format:'dd-mm-yyyy'});

            $('.select2').select2();
            $('.js-vendor').select2({
                ajax: {
                    type:'GET',
                    dataType: 'json',
                    url: '{{ url('invoice/input/search') }}',
                    data: function (params) {
                        var query = {
                            q: params.term
                        }
                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function(obj){
                                return {id:obj.id, text:obj.text}
                            })
                        }
                    },
                    cache:true
                },
                minimumInputLength:3,
                placeholder:'Pilih Vendor...',
            });


        });
    </script>
@endsection
