@extends('templates.admin')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
    <script src="{{ url('asset/js/phpjs.js') }}"></script>
    <style type="text/css">
        h3.panel-title{font-size: 14px !important}
        /*.dx-fileuploader-show-file-list .dx-fileuploader-files-container{padding-top:0px}*/
        .dx-fileuploader-wrapper{padding:0px;}
        .help-text {color: #aaa;margin-top: 10px;}
    </style>
@endsection

@section('contentheader')
    <section class="content-header">
        <h1>Invoice</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Invoice</li>
        </ol>
    </section>
@endsection

@section('content')

    <div class="box-header with-border">
        <h3 class="box-title">Invoice</h3>
    </div>
    <div class="box-body">
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {{ flashdata('error') }}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> {{ flashdata('warning') }}
            </div>

        @elseif(flashdata('info'))

            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Info!</strong> {{ flashdata('info') }}
            </div>
        @endif

        <div role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#detail" aria-controls="detail" role="tab" data-toggle="tab">Details</a>
                </li>
                <li role="presentation">
                    <a href="#history" aria-controls="history" role="tab" data-toggle="tab">History</a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content" style="padding-top:10px">
                <div role="tabpanel" class="tab-pane active" id="detail">
                    <form  class="form-horizontal">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Detail Invoice</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Vendor</label>
                                    <div class="col-sm-4">
                                        <input type="text"  class="form-control" value="{{ $data->IDVendor }} - {{ $data->NamaVendor }}" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Jenis Invoice*</label>
                                    <div class="col-sm-4">

                                        {!! form_dropdown('JenisTransaksi', $list_transaksi, $data->JenisTransaksi, ['class' => 'form-control select2','disable' => '']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">No. Invoice</label>
                                    <div class="col-sm-4">
                                        <input type="text" value="{{ $data->NoInvoice }}" class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tgl. Invoice</label>
                                    <div class="col-sm-2">
                                        <div class="input-group date">
                                            <input type="text" name="TglInvoice" class="form-control pull-right datepicker" value="{{ date('d-m-Y', strtotime($data->TglInvoice)) }}" readonly>
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nilai Invoice</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="NilaiInvoice" value="{{ $data->NilaiInvoice }}" class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Kurs</label>
                                    <div class="col-sm-1">
                                        <input type="text"  value="{{ $data->KursInvoice }}" class="form-control" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">No. Faktur Pajak</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="NoFakturPajak" value="{{ $data->NoFakturPajak }}" class="form-control" readonly>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tanggal Faktur Pajak</label>
                                    <div class="col-sm-2">
                                        <div class="input-group date">
                                            <input type="text" name="TglFakturPajak" class="form-control pull-right datepicker" value="{{ date('d-m-Y',strtotime($data->TglFakturPajak)) }}" readonly>
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Unit Kerja</label>
                                    <div class="col-sm-4">
                                        <input type="text"  class="form-control" value="{{ $data->CostCenter }} - {{ Auth::getUnitKerja($data->CostCenter) }}" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Keterangan</label>
                                    <div class="col-sm-4">
                                        <textarea name="Keterangan" class="form-control" readonly>{{ $data->Keterangan }}</textarea>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>

                        <div class="panel panel-default form-horizontal" id="potongan">
                            <div class="panel-heading">
                                <h3 class="panel-title">Dokumen SAP</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group {{ form_error('NoInvoice') ? 'has-error' : '' }}">
                                    <label class="col-sm-2 control-label">No Dokumen SAP</label>
                                    <div class="col-sm-2">
                                        <input type="text"maxlength="10" name="NoDokumenSAP" value="{{ $data->NoDokumenSAP }}" class="form-control" readonly>
                                    </div>

                                    <label class="col-sm-1 control-label">Tahun</label>
                                    <div class="col-sm-1">
                                        <input type="text" name="Tahun" value="{{ $data->Tahun }}" id="tahun" class="form-control" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Payment Term</label>
                                    <div class="col-sm-2">
                                        <select name="PaymentTerm" class="form-control select2" id="PaymentTerm" readonly></select>
                                    </div>


                                    <div class="col-sm-4 control-label">
                                        <div class="help-text" id="help-PaymentTerm">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <form >
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Invoice Attachment</h3>
                            </div>
                            <div class="panel-body">

                                <div class="form-group">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">
                                        <table class="table table-bordered table-striped table-condensed">
                                            <tr>
                                                <th>Filename</th>
                                                <th>Action</th>
                                            </tr>
                                            @foreach($attachments as $attachment)
                                            <tr>
                                                <td>{{ $attachment->FilePath }}</td>
                                                <td >
                                                    <a href="{{ url('storage/invoice/'.$attachment->FilePath) }}" class="btn btn-primary btn-xs" style="color:#fff !important">View</a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Detail Purchase Order</h3>
                            </div>
                            <div class="panel-body">

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div id="gridContainer"></div>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Jaminan</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div id="gridContainerJaminan"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
            <div class="panel panel-default form-horizontal" id="potongan">
                            <div class="panel-heading">
                                <h3 class="panel-title">Potongan</h3>
                            </div>
                            <div class="panel-body">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div id="gridPotongan"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>

            <div class="panel panel-info form-horizontal">
                <div class="panel-heading">
                    <h3 class="panel-title">Apply Piutang</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div id="gridContainer2"></div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Sisa Tagihan yang Akan Dibayarkan</label>
                        <div class="col-sm-4">
                            <input type="text" value="{{ $data->NilaiVerifikasi }}" class="form-control SisaTagihan" id="money" readonly>
                        </div>
                    </div>

                </div>
            </div>
            <div class="panel panel-info form-horizontal">
                <div class="panel-heading">
                    <span class="clickable panel-collapsed">
                    <h3 class="panel-title">Kelengkapan Dokumen</h3>
                    <span class="pull-right"><i class="glyphicon glyphicon-chevron-down"></i></span>
                    </span>
                </div>

                        <div class="panel-body" style="display: none">
                            <div class="col-sm-6">
                                @foreach($documents as $document)
                                <div class="form-group">
                                    <div class=" col-sm-10">
                                        <div class="checkbox">
                                            <label>
                                                <?php
                                                $name = "matriks[".$document['ID']."]";
                                                $extra = $document['Required'] ? ['required' => 'required'] : [];
                                                ?>
                                                {!! form_checkbox($name, $document['Dokumen'], $document['Value'], $extra) !!}
                                                {{ $document['Dokumen'] }}
                                                @if(!empty($extra))
                                                *
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                @endforeach

                            </div>
                            <div class="col-sm-6">
                                <div class="checkbox pull-right">
                                    <label>
                                        {!! form_checkbox('IsPriority', 'IsPriority', $data->IsPriority) !!} Priority
                                    </label>
                                </div>
                            </div>

                        </div>
            </div>
        <style>
            .clickable{
                cursor: pointer;
            }

            .panel-heading span {
                margin-top: -20px;
                font-size: 15px;
            }

        </style>
        <script>

                $(document).on('click', '.panel-heading span.clickable', function(e){
                var $this = $(this);
                if(!$this.hasClass('panel-collapsed')) {
                    $this.parents('.panel').find('.panel-body').slideUp();
                    $this.addClass('panel-collapsed');
                    $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
                } else {
                    $this.parents('.panel').find('.panel-body').slideDown();
                    $this.removeClass('panel-collapsed');
                    $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
                }
            })
        </script>
                    <div class="box-footer">

                        <center>
                            <a onclick="window.open(document.referrer,'_self')"  class="btn btn-primary btn-sm" style="color:#fff">
                                Back
                            </a>
                        </center>
                    </div>

                </div>
                <div role="tabpanel" class="tab-pane" id="history">
                    <table class="table table-condensed table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Action</th>
                                <th>User</th>
                                <th>Keterangan</th>
                                <th>Timestamp</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($histories as $key => $history)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $history->Action }}</td>
                                <td>{{ $history->Nama }}</td>
                                <td>{{ $history->Keterangan }}</td>
                                <td>{{ date('d-m-Y H:i:s', strtotime($history->Timestamp)) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        $(function() {
            $('input[id=money]').inputmask('numeric', {
            groupSeparator: '.',
            radixPoint : ',',
            autoGroup: true,
            digits: 2,
            digitsOptional: false,
            placeholder: '0,00'
        });

            var dataSourceJaminan = new DevExpress.data.CustomStore({
                key: "ID",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};

                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }

                    if (loadOptions.filter != 'undefined') {
                        args.filter = JSON.stringify(loadOptions.filter);
                    }

                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 12;

                    $.ajax({
                        url: "{{ url("invoice/input/getJaminan/".$id) }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });

                    return deferred.promise();
                }
            });

            var dataGridJaminan = $("#gridContainerJaminan").dxDataGrid({
                dataSource : dataSourceJaminan,
                onSelectionChanged: function(data){
                    $('#btn_delete').prop('disabled', !(data.selectedRowsData.length));/*
                    $('#btn_edit').prop('disabled', !(data.selectedRowsData.length == 1));*/
                },
                selection:{mode:'multiple',showCheckBoxesMode:'always',selectAllMode:'page'},
                remoteOperations: {sorting: true,paging: true,filtering:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: false,visible: true},
                columns : [
                    {caption:'No',cellTemplate:function(cellElement, cellInfo){
                    cellElement.text(cellInfo.row.rowIndex+1)
                    }},
                    {dataField:'NO_PO',caption:'No. PO'},
                    {dataField:'ID_JENIS_JAMINAN',caption:'Jenis Jaminan'},
                    {dataField:'NOMOR_JAMINAN',caption:'No. Jaminan'},
                    {dataField:'NILAI_JAMINAN',caption:'Nilai Jaminan'},
                    {dataField:'MASA_BERLAKU',caption:'Masa Berlaku'},
                    {dataField:'ID_BANK_PENERBIT',caption:'Bank Penerbit'},
                    {dataField:'KETERANGAN_JAMINAN',caption:'Keterangan'},
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
                rowAlternationEnabled: true,
                rowAlternationEnabled: true,
            }).dxDataGrid("instance");
        })

    </script>
    <script type="text/javascript">
        $('input[type=checkbox]').attr('disabled','true');
        $(function() {
            $('.draft').click(function(e){

                var count = dataGrid.totalCount();
                if(count == 0) {
                    alert("Detail Purchase Order Tidak Kosong");
                    e.preventDefault();
                }else{
                    $(".savetext").css('display', 'none');
                    $('<img>').attr('src', '{{ url("asset/image/loading.gif") }}').insertAfter($(".savetext"));
                    var url = "{{ url('invoice/invoice/update/'.$id) }}";
                    var forms = $('#invoice_form').serialize();
                    var forms = forms +'&submit=draft';
                    $.ajax({
                           type: "POST",
                           url: url,
                           data: forms ,

                           success: function(results)
                           {
                                window.location = results;

                           }
                         });
                }


                e.preventDefault();
            });
            $('.submit').click(function(e){


                var count = dataGrid.totalCount();
                if(count == 0) {
                    alert("Detail Purchase Order Tidak Kosong");
                    e.preventDefault();
                }else{
                     $(".submittext").css('display', 'none');
                    $('<img>').attr('src', '{{ url("asset/image/loading.gif") }}').insertAfter($(".submittext"));

                    var url = "{{ url('invoice/invoice/update/'.$id) }}";
                    var forms = $('#invoice_form').serialize();
                    var forms = forms +'&submit=submit';
                    $.ajax({
                           type: "POST",
                           url: url,
                           data: forms ,

                           success: function(results)
                           {
                              window.location = results;

                           }
                         });
                }


                e.preventDefault();
            });

            $('#btn_edit').click(function(e){
                e.preventDefault();
                $.each(dataGrid.getSelectedRowsData(), function() {
                    console.log(this.ID);
                });

                dataGrid.refresh();
            });

            $('#btn_delete').click(function(e){
                e.preventDefault();
                var lists = [];
                $.each(dataGrid.getSelectedRowsData(), function() {
                    lists.push(this.ID);
                });

                $.post("{{ url('invoice/input/delete_related_invoice') }}", {ids:lists})
                    .done(function(results){
                        dataGrid.refresh();
                    });
            });

            $('.select2').select2();
            $('.select2').prop("disabled", true);
            $('.js-vendor').select2({
                ajax: {
                    type:'GET',
                    dataType: 'json',
                    url: '{{ url('invoice/input/search') }}',
                    data: function (params) {
                        var query = {q: params.term}
                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function(obj){
                                return {id:obj.id, text:obj.text}
                            })
                        }
                    },
                    cache:true
                },
                minimumInputLength:3,
                placeholder:'Pilih Vendor...',
                language: { errorLoading:function(){ return "Searching..." }}
            });

            function clearData() {
                $( '#related_invoice' ).each(function(){
                    this.reset();
                });
                $('#no_po').val('').trigger('change');
                $('#no_sagr').val('').trigger('change');
                $('#nilai_po').val('');
                $('#po_kurs').html('');
                $('#po_desc').html('');
                //dataGrid.refresh();
            }

            $('#btn_add').click(function(e){
                e.preventDefault();
                var data = $('#related_invoice').serialize();
                $.post('{{ url('invoice/input/add_related_invoice/'.$id) }}', data)
                    .done(function(results){
                        if(results == 'ok') {
                            clearData();
                        } else {
                            alert(results);
                        }
                    }).fail(function(){

                    });
            });

            $('#no_sagr').select2({
                ajax: {
                    type:'GET',
                    dataType: 'json',
                    url: function(){
                        var no_po = $('#no_po').val();
                        return '{{ url("invoice/input/get_sagr/{$id}/") }}' + no_po;
                    },
                    data: function (params) {
                        var query = {q: params.term}
                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function(obj){
                                return {id:obj.id, text:obj.text}
                            })
                        }
                    },
                    cache:true
                },
                placeholder:'Pilih Nomor SA/GRS...',
            });

            $('#no_po').on('change', function(e){
                var nomor_po = $(this).val();
                $.post("{{ url("invoice/input/get_detail_po/{$id}/") }}" + nomor_po)
                    .done(function(data){
                        $('input[name=NilaiPO]').val(data.NilaiPO);
                        if(typeof data.NilaiKontrak !== 'undefined') {
                            $('input[name=NilaiKontrak]').val(data.NilaiKontrak);
                        }
                        $('#po_kurs').html(data.KursPO);
                        $('#po_desc').html(data.DeskripsiPO);

                        $('input[name=KursPO]').val(data.KursPO);
                        $('input[name=DeskripsiPO]').val(data.DeskripsiPO);
                    }).fail(function(){
                        alert('Terjadi Kesalahan pada get_detail_po');
                    });
            });

            $('#no_sagr').on('change', function(e){
                var no_sagr = $(this).val();
                $.post("{{ url("invoice/input/get_detail_sagr/{$id}/") }}" + no_sagr)
                    .done(function(data){
                        $('input[name=TglSAGR]').datepicker('update', data.TanggalSAGR);
                        if(typeof data.NomorBA !== 'undefined') {
                            $('input[name=NomorBA]').val(data.NomorBA);
                        }
                    }).fail(function(){
                        alert('Something Wrong');
                    });
            });

            $('#no_po').on("change", function(e){
                $( '#related_invoice' ).each(function(){
                    this.reset();
                });
                $('#no_sagr').val('').trigger('change');
                $('#nilai_po').val('');
                $('#po_kurs').html('');
                $('#po_desc').html('');
                //dataGrid.refresh();
            });

            $('#no_po').select2({
                ajax: {
                    type:'GET',
                    dataType: 'json',
                    url: '{{ url("invoice/input/get_po/{$id}") }}',
                    data: function (params) {
                        var query = {q: params.term}
                        return query;
                    },
                    processResults: function (data) {
                        return {results: data}
                    },
                    cache:true
                },
                placeholder:'Pilih Nomor PO...',
            });

            $('.datepicker').datepicker({
                autoclose: true, format:'dd-mm-yyyy',
            });

            $('input[name=NilaiInvoice]').inputmask('numeric', {
                groupSeparator: '.',
                radixPoint : ',',
                autoGroup: true,
                digits: 2,
                digitsOptional: false,
                placeholder: '0,00',
            });
            $('input[name=NilaiPO]').mask('000.000.000.000.000', {reverse:true})

            var dataSource = new DevExpress.data.CustomStore({
                key: "ID",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};

                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }

                    if (loadOptions.filter != 'undefined') {
                        args.filter = JSON.stringify(loadOptions.filter);
                    }

                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 12;

                    $.ajax({
                        url: "{{ url("invoice/input/get/".$id) }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });

                    return deferred.promise();
                }
            });

            var dataGrid = $("#gridContainer").dxDataGrid({
                dataSource : dataSource,
                onSelectionChanged: function(data){
                    $('#btn_delete').prop('disabled', !(data.selectedRowsData.length));
                    $('#btn_edit').prop('disabled', !(data.selectedRowsData.length == 1));
                },
                selection:{mode:'multiple',showCheckBoxesMode:'always',selectAllMode:'page'},
                remoteOperations: {sorting: true,paging: true,filtering:true},
                "export" : {enable:true, fileName : "{{ $table }}"},
                editing : {mode:'form', allowAdding : false,allowUpdating : false,allowDeleting : false},
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : false},
                headerFilter:{visible:true},
                filterRow:{visible:true,applyFilter:'auto'},
                groupPanel : {visible:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: false,visible: true},
                columns : [
                    // {caption:'No',cellTemplate:function(cellElement, cellInfo){
                    //     cellElement.text(cellInfo.row.rowIndex+1)
                    // }},
                    {dataField:'NoPO',caption:'No. PO'},
                    {dataField:'NilaiPO',caption:'Nilai PO',calculateCellValue:function(data){
                        return number_format(data.NilaiPO,2,',','.')
                    }},
                    {dataField:'NomorKontrak',caption:'No Kontrak'},
                    {dataField:'NilaiKontrak',caption:'Nilai Kontrak'},
                    {dataField:'NoSAGR',caption:'No. SA/GRS'},
                    {dataField:'TglSAGR',caption:'Tgl SA/GRS',dataType: 'date',format: 'dd/MM/yyyy'},
                    // {dataField:'Nama',caption:'Created By'},
                    /*
                    {dataField:'DeskripsiPO',caption:'Deskripsi Pekerjaan'},*/
                    {dataField:'NomorBA',caption:'No. BAPP'},
                    {dataField:'TanggalBA',caption:'Tgl BAPP',dataType: 'date',format: 'dd/MM/yyyy'},
                    {dataField:'Nama',caption:'Created By'},
                    {dataField:'Keterangan',caption:'Keterangan'},
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
                rowAlternationEnabled: true,
                rowAlternationEnabled: true,
            }).dxDataGrid("instance");


 var dataSourcePotongan = new DevExpress.data.CustomStore({
                key: "ID",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                    args = {};

                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }

                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 999;

                    $.ajax({
                        url: "{{ url("akuntansi/invoice/lists/getPotongan/".$id) }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });

                    return deferred.promise();
                }
            });

             var dataGridPotongan = $("#gridPotongan").dxDataGrid({
                dataSource : dataSourcePotongan,
                // columnHidingEnabled: false,
                remoteOperations: {sorting: true,paging: false},
                "export" : {enable:true, fileName : "{{ $table }}"},
                editing : {mode:'form', allowAdding : false,allowUpdating : false,allowDeleting : false},
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : false},
                headerFilter:{visible:false},
                filterRow:{visible:false,applyFilter:'auto'},
                groupPanel : {visible:false},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                columns : [
                {caption:'No',cellTemplate:function(cellElement, cellInfo){
                    cellElement.text(cellInfo.row.rowIndex+1)
                }},
                {dataField:'Potongan',caption:'Potongan'},
                {caption:'Nominal',calculateCellValue:function(data){
                    return number_format(data.Nominal,2,',','.')
                }},
                {caption:'Kurs',cellTemplate:function(cellElement, cellInfo){
                    cellElement.text('{{ $list_kurs[$data->KursInvoice] }}')
                }},
                {dataField:'Keterangan',caption:'Keterangan'},
                {dataField:'Nama',caption:'Created By'},
                {dataField:'CreatedOn',caption:'Created On',dataType: 'date',format: 'dd/MM/yyyy'},
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: false},
                columnFixing: {enabled: false},
            }).dxDataGrid("instance");


            var dataSource2 = new DevExpress.data.CustomStore({
                key: "ARDocNo",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                    args = {};

                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }

                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 999;

                    $.ajax({
                        url: "{{ url("keuangan/invoice/verifikasi/getAppliedDocument/".$id) }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });

                    return deferred.promise();
                }
            });

            var dataGrid2 = $("#gridContainer2").dxDataGrid({
                dataSource : dataSource2,
                onSelectionChanged: function(data){
                    $('#btn_delete').prop('disabled', !(data.selectedRowsData.length));
                    $('#btn_edit').prop('disabled', !(data.selectedRowsData.length == 1));
                },
                selection:{mode:'multiple',showCheckBoxesMode:'always',selectAllMode:'page'},
                remoteOperations: {sorting: true,paging: true,filtering:true},
                "export" : {enable:true, fileName : "{{ $table }}"},
                editing : {mode:'form', allowAdding : false,allowUpdating : false,allowDeleting : false},
                allowColumnReordering : false,
                allowColumnResizing : true,
                searchPanel : {visible : false},
                headerFilter:{visible:false},
                filterRow:{visible:false,applyFilter:'auto'},
                groupPanel : {visible:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                columns : [
                {caption:'No',cellTemplate:function(cellElement, cellInfo){
                    cellElement.text(cellInfo.row.rowIndex+1)
                }},
                {dataField:'ARDocNo',caption:'No. Dokumen Piutang'},
                {caption:'Nilai Piutang',calculateCellValue:function(data){
                return number_format(data.ARAmount,2,',','.')
                }},
                {caption:'Nilai Apply',calculateCellValue:function(data){
                return number_format(data.AppliedAmount,2,',','.')
                }},
                {caption:'Kurs',cellTemplate:function(cellElement, cellInfo){
                    cellElement.text('{{ $list_kurs[$data->KursInvoice] }}')
                }},
                {dataField:'AppliedBy',caption:'Created By'},
                {dataField:'AppliedOn',caption:'Created On'},
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: false},
                columnFixing: {enabled: false},
            }).dxDataGrid("instance");

        });
    </script>
@endsection
