@extends('templates.admin')

@section('title', 'Edit Invoice')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
    <script src="{{ url('asset/js/phpjs.js') }}"></script>
    <style type="text/css">
        h3.panel-title{font-size: 14px !important}
        /*.dx-fileuploader-show-file-list .dx-fileuploader-files-container{padding-top:0px}*/
        .dx-fileuploader-wrapper{padding:0px;}
        .help-text {color: #aaa;margin-top: 10px;}
    </style>
@endsection

@section('contentheader')
    <section class="content-header">
        <h1>Input Invoice</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Invoice (Unit Kerja)</li>
            <li class="active">Input Invoice</li>
        </ol>
    </section>
@endsection

@section('content')

    <div class="box-header with-border">
        <h3 class="box-title">Input Invoice</h3>
    </div>
    <div class="box-body">
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {!! flashdata('error') !!}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> {{ flashdata('warning') }}
            </div>

        @elseif(flashdata('info'))

            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Info!</strong> {!! flashdata('info') !!}
            </div>
        @endif

        <div role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#detail" aria-controls="detail" role="tab" data-toggle="tab">Details</a>
                </li>
                <li role="presentation">
                    <a href="#history" aria-controls="history" role="tab" data-toggle="tab">History</a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content" style="padding-top:10px">
                <div role="tabpanel" class="tab-pane active" id="detail">
                    <form class="form-horizontal" id="detail_invoice">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Detail Invoice</h3>
                            </div>
                            <div class="panel-body">
                                <input type="hidden" name="id" value="{{ $id }}">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Vendor</label>
                                    <div class="col-sm-4">
                                        <input type="text"  class="form-control" value="{{ $data->IDVendor }} - {{ $data->NamaVendor }}" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Jenis Invoice*</label>
                                    <div class="col-sm-4">
                                        {!! form_dropdown('JenisTransaksi', $list_transaksi, $data->JenisTransaksi, ['class' => 'form-control jenis select2','disable' => '']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">No. Invoice</label>
                                    <div class="col-sm-4">
                                        <input name="NoInvoice" type="text" value="{{ $data->NoInvoice }}" class="form-control">
                                        <input name="NoInvoice_OLD" type="hidden" value="{{ $data->NoInvoice }}" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tgl. Invoice</label>
                                    <div class="col-sm-2">
                                        <div class="input-group date">
                                            <input type="text" name="TglInvoice" class="form-control pull-right datepicker" value="{{ date('d-m-Y', strtotime($data->TglInvoice)) }}">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nilai Invoice</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="NilaiInvoice" value="{{ $data->NilaiInvoice }}" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Kurs</label>
                                    <div class="col-sm-2">
                                        {!! form_dropdown('KursInvoice', $list_kurs, $data->KursInvoice, ['class' => 'form-control kurs']) !!}
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">No. Faktur Pajak</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="NoFakturPajak" value="{{ $data->NoFakturPajak }}" id="NoFakturPajak" class="form-control" minlength="16" maxlength="16" required>
                                        <input type="hidden" name="NoFakturPajak_OLD" value="{{ $data->NoFakturPajak }}">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tanggal Faktur Pajak</label>
                                    <div class="col-sm-2">
                                        <div class="input-group date">
                                            <input type="text" name="TglFakturPajak" class="form-control pull-right datepicker" value="{{ date('d-m-Y',strtotime($data->TglFakturPajak)) }}">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Unit Kerja</label>
                                    <div class="col-sm-4">
                                        {!! form_dropdown('CostCenter', Auth::matriksUnitKerja(), $data->CostCenter, ['class' => 'form-control select2', 'required' => 'required']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Keterangan</label>
                                    <div class="col-sm-4">
                                        <textarea name="Keterangan" class="form-control" >{{ $data->Keterangan }}</textarea>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </form>
                    <form action="{{ url('invoice/invoice/upload/'.$id) }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Invoice Attachment</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Attachment</label>
                                    <div class="col-sm-4">
                                        <input type="file" name="file">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-4">
                                        <button type="submit" class="btn btn-success btn-sm">Upload</button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">
                                        <table class="table table-bordered table-striped table-condensed">
                                            <tr>
                                                <th>Filename</th>
                                                <th>Action</th>
                                            </tr>
                                            @foreach($attachments as $attachment)
                                            <tr>
                                                <td>{{ $attachment->FilePath }}</td>
                                                <td >
                                                    <a href="{{ url('storage/invoice/'.$attachment->FilePath) }}" class="btn btn-primary btn-xs" style="color:#fff !important">View</a>
                                                    <a href="{{ url('invoice/input/remove_attachment/'.$id.'/'.$attachment->ID) }}" class="btn btn-danger btn-xs" style="color:#fff !important">Delete</a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form action="#" class="form-horizontal" method="POST" id="related_invoice">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Detail Purchase Order</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nomor PO*</label>
                                    <div class="col-sm-4">
                                        <div id="box_po">
                                            <select class="form-control js-nomorpo" id="no_po" name="NoPO"></select>
                                        </div>

                                        <div class="manual"></div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div id="po_desc" class="help-text"></div>
                                        <input type="hidden" name="DeskripsiPO" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-4">
                                        <div class="checkbox">
                                          <label><input type="checkbox" name="PO_Manual" value="1" id="manual">PO Manual?</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nilai PO</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="NilaiPO" class="form-control" readonly id="po_nilai">
                                    </div>
                                    <div class="col-sm-4">
                                        <div id="po_kurs" class="help-text"></div>
                                        <input type="hidden" name="KursPO" value="">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">No Kontrak</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="NomorKontrak" class="form-control" id="no_kontrak" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Nilai Kontrak</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="NilaiKontrak" class="form-control" id="nilai_kontrak" readonly>
                                    </div>
                                    <div class="col-sm-4">
                                        <div id="kontrak_kurs" class="help-text"></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">No. SA / GRS*</label>
                                    <div class="col-sm-4">
                                        <div id="box_sagr">
                                            <select class="form-control" id="no_sagr" name="NoSAGR"></select>
                                        </div>
                                        <div class="sagrsmanual"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-4">

                                        <!-- <div class="checkbox">
                                          <label><input type="checkbox" id="sagrsmanual">SAGR Manual?</label>
                                        </div> -->

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tgl. SA / GRS*</label>
                                    <div class="col-sm-2">
                                        <div class="input-group date">
                                            <input type="text" name="TglSAGR" class="form-control pull-right datepicker" id="tgl_sagrs" readonly>
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">No. BAPP /  BAPB</label>
                                    <div class="col-sm-4">
                                        <input type="text" name="NomorBA" value="" class="form-control" id="NomorBA">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tgl. BAPP /  BAPB</label>
                                    <div class="col-sm-2">
                                        <div class="input-group date">
                                            <input type="text" name="TanggalBA" class="form-control pull-right datepicker" id="TanggalBA">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Keterangan</label>
                                    <div class="col-sm-4">
                                        <textarea name="Keterangan" class="form-control" rows="5" id="Keterangan"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-4">
                                        <button class="btn btn-sm btn-primary" id="btn_add">Add</button>
                                        <button class="btn btn-sm btn-warning" id="btn_edit" disabled>Edit</button>
                                        <button class="btn btn-sm btn-warning" id="btn_update" style="display:none">Update</button>
                                        <button class="btn btn-sm btn-danger" id="btn_delete" disabled>Delete</button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div id="gridContainer"></div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Jaminan</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div id="gridContainerJaminan"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="box-footer">
                        <input  type="hidden" name="is_manual" id="is_manual" value="0" />
                        <center>
                            <input  type="submit" class="btn btn-success btn-sm draft" name="submit" value="Save Draft"/>
                            @if($status != statusID('CONFIRMED'))
                                <input type="submit" class="btn btn-primary btn-sm submit" name="submit" value="Confirm" />
                            @endif
                            <a href="{{ url('invoice/invoice') }}" class="btn btn-warning btn-sm" style="color:#fff">
                                Close
                            </a>
                        </center>
                    </div>

                </div>
                <div role="tabpanel" class="tab-pane" id="history">
                    <table class="table table-condensed table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Action</th>
                                <th>User</th>
                                <th>Keterangan</th>
                                <th>Timestamp</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($histories as $key => $history)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $history->Action }}</td>
                                <td>{{ $history->Nama }}</td>
                                <td>{{ $history->Keterangan }}</td>
                                <td>{{ date('d-m-Y H:i:s', strtotime($history->Timestamp)) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        $(function() {
            $('input[id=NoFakturPajak]').mask('0000000000000000', {reverse:true})
            $('.draft').click(function(e){
                var count = dataGrid.totalCount();
                if(false) { /* count == 0 */
                    /*
                    var result = DevExpress.ui.dialog.alert("Detail Purchase Order Tidak Boleh Kosong");
                    e.preventDefault();
                    */
                }else{
                    e.preventDefault();
                    var lengthPajak = $('input[name=NoFakturPajak]').val().length;
                    if(lengthPajak != 16) {
                        DevExpress.ui.dialog.alert('No Faktur Pajak Harus 16 Karakter','Error');
                        return;
                    }
                    var result = DevExpress.ui.dialog.confirm("Apakah anda yakin ?", "Save Draft Confirmation");
                        result.done(function (dialogResult) {
                            if(dialogResult) {
                                $.post("{{ url('invoice/invoice/update_detail') }}", $('#detail_invoice').serialize())
                                    .done(function(result){
                                        if(result == 'success') {
                                        var is_manual = $('#is_manual').val();
                                    <?php if($status == statusID('DRAFT')){ ?>
                                        location.href = "{{ url('invoice/invoice/setDraft/').$id }}";
                                    <?php }else{ ?>
                                        location.href = "{{ url('invoice/invoice/') }}";
                                    <?php } ?>
                                        } else {
                                            alert(result);
                                        }
                                    }).fail(function(result){
                                        alert('Whoops, penyimpanan data gagal');
                                    });
                            }else{
                                $('form').find("button[type='submit']").prop('disabled',false);
                            }
                    });
                }
            });

            $('.submit').click(function(e){
                var count = dataGrid.totalCount();
                if(count == 0) {
                    var result = DevExpress.ui.dialog.alert("Detail Purchase Order Tidak Boleh Kosong");
                    e.preventDefault();
                }else{
                    e.preventDefault();
                    var lengthPajak = $('input[name=NoFakturPajak]').val().length;
                    if(lengthPajak != 16) {
                        DevExpress.ui.dialog.alert('No Faktur Pajak Harus 16 Karakter','Error');
                        return;
                    }
                    var result = DevExpress.ui.dialog.confirm("Apakah anda yakin?", "Submit Confirmation");
                        result.done(function (dialogResult) {
                            if(dialogResult) {
                                $.post("{{ url('invoice/invoice/update_detail') }}", $('#detail_invoice').serialize())
                                    .done(function(result){
                                        if(result == 'success') {
                                            var is_manual = $('#is_manual').val();
                                            location.href = "{{ url('invoice/invoice/update/').$id }}"+"/"+is_manual;
                                        } else {
                                            alert(result);
                                        }
                                    }).fail(function(result){
                                        alert('Whoops, penyimpanan data gagal');
                                    });
                            }else{
                                $('form').find("button[type='submit']").prop('disabled',false);
                            }
                    });
                }
            });



            $('#btn_edit').click(function(e){
                e.preventDefault();
                $.each(dataGrid.getSelectedRowsData(), function() {
                    console.log(this.ID);
                    var ID = this.ID;

                    $.post("{{ url('invoice/input/get_related_invoice') }}",{id:ID})
                        .done(function(result){


                                if ($('#manual').is(':checked')) {
                                }else{
                                    $('#manual').click();
                                }

                                $('#mn').val(result.NoPO);
                                $('#mn1').val(result.NoSAGR);
                                $('#po_nilai').val(result.NilaiPO);
                                $('#po_nilai').val(result.NilaiPO);
                                $('#no_kontrak').val(result.NomorKontrak);
                                $('#nilai_kontrak').val(result.NilaiKontrak);
                                $('#tgl_sagrs').val(result.TglSAGR);
                                $('#NomorBA').val(result.NomorBA);
                                $('#TanggalBA').val(result.TanggalBA);
                                $('#Keterangan').val(result.Keterangan);


                                $('#btn_update').show();
                                $('#btn_update').val(result.ID);
                                $('#btn_add').hide();
                                $('#btn_edit').hide();
                                $('#btn_delete').hide();


                        }).fail(function(result){
                            alert('Whoops, pengambilan data po gagal');
                        });

                });

                dataGrid.refresh();
            });

            $('#btn_delete').click(function(e){
                e.preventDefault();
                var result = DevExpress.ui.dialog.confirm("Apakah anda yakin?", "Confirm");
                    result.done(function (dialogResult) {
                        console.log(dialogResult);
                        if(dialogResult) {
                            var lists = [];
                            $.each(dataGrid.getSelectedRowsData(), function() {
                                lists.push(this.ID);
                            });

                            $.post("{{ url('invoice/input/delete_related_invoice') }}", {ids:lists,id_invoice:<?php echo $id ?>})
                                .done(function(results){
                                    dataGrid.refresh();
                                    dataGridJaminan.refresh();
                                });
                        }
                });
            });

            $('.select2').select2();
            $('.kurs').select2();
            $('.jenis').prop("disabled", true);

            $('.js-vendor').select2({
                ajax: {
                    type:'GET',
                    dataType: 'json',
                    url: '{{ url('invoice/input/search') }}',
                    data: function (params) {
                        var query = {q: params.term}
                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function(obj){
                                return {id:obj.id, text:obj.text}
                            })
                        }
                    },
                    cache:true
                },
                minimumInputLength:3,
                placeholder:'Pilih Vendor...',
                language: { errorLoading:function(){ return "Searching..." }}
            });

            function clearData() {
                var is_manual = $('#is_manual').val();
                $( '#related_invoice' ).each(function(){
                    this.reset();
                });
                $('#no_po').val('').trigger('change');
                $('#no_sagr').val('').trigger('change');
                $('#nilai_po').val('');
                $('#po_kurs').html('');
                $('#po_desc').html('');
                if(is_manual == 1){
                    $('#manual').prop("checked", true).trigger("change");
                }
                //dataGrid.refresh();
            }

            $('#btn_add').click(function(e){

                e.preventDefault();
                var po = $('#no_po').val();
                var sagr = $('#no_sagr').val();
                var tgl_sagrs = $('#tgl_sagrs').val();
                var mn = $('#mn').val();
                var mn1 = $('#mn1').val();
                /*
                var po = $('#no_po').val();
                var sagr = $('#no_sagr').val();*/
                /*if(po == null && po.length != 10 && mn == null && mn.length != 10){
                    DevExpress.ui.dialog.alert("No.PO dan No. SAGR harus 10 digit.", "Error");
                }*/
                console.log(tgl_sagrs);
                if((po == null && mn == null) || (sagr == null && mn1 == null) || (tgl_sagrs == '')){

                    DevExpress.ui.dialog.alert("Data tidak lengkap.", "Error");
                }else{
                    if( mn != null ){
                        console.log(mn);
                        if(mn.length != 10){
                            DevExpress.ui.dialog.alert("No.PO harus 10 digit.", "Error");
                            return;
                        }
                    }
                    if( mn1 != null ){
                        if(mn1.length != 10){
                            DevExpress.ui.dialog.alert("No. SAGR harus 10 digit.", "Error");
                            return;
                        }
                    }
                    var result = DevExpress.ui.dialog.confirm("Apakah anda yakin?", "Confirm");
                    result.done(function (dialogResult) {
                        console.log(dialogResult);
                        if(dialogResult) {
                            var data = $('#related_invoice').serialize();
                            $.post('{{ url('invoice/input/add_related_invoice/'.$id) }}', data)
                                .done(function(results){
                                    if(results == 'ok') {
                                        clearData();
                                        dataGrid.refresh();
                                        //dataGridJaminan.option("dataSource", '{{ url("invoice/input/getJaminan/") }}'+ nomor_po + '/jaminan');
                                        dataGridJaminan.refresh();
                                    } else {

                                        DevExpress.ui.dialog.alert(results, "Error");
                                    }
                                }).fail(function(){

                                });

                                // Save detail invoice ketika tombol add detail purchase ditekan
                                var count = dataGrid.totalCount();
                            
                                e.preventDefault();
                                var lengthPajak = $('input[name=NoFakturPajak]').val().length;
                                if(lengthPajak != 16) {
                                    DevExpress.ui.dialog.alert('No Faktur Pajak Harus 16 Karakter','Error');
                                    return;
                                }
                                $.post("{{ url('invoice/invoice/update_detail/').statusID('DRAFT') }}", $('#detail_invoice').serialize())
                                    .done(function(result){
                                
                                    }).fail(function(result){
                                        alert('Whoops, penyimpanan data gagal');
                                    });
                                
                        }
                    });
                }
            });
            $('#btn_update').click(function(e){


                e.preventDefault();
                var po = $('#no_po').val();
                var sagr = $('#no_sagr').val();
                var tgl_sagrs = $('#tgl_sagrs').val();
                var mn = $('#mn').val();
                var mn1 = $('#mn1').val();
                /*
                var po = $('#no_po').val();
                var sagr = $('#no_sagr').val();*/
                /*if(po == null && po.length != 10 && mn == null && mn.length != 10){
                    DevExpress.ui.dialog.alert("No.PO dan No. SAGR harus 10 digit.", "Error");
                }*/
                console.log(tgl_sagrs);
                if((po == null && mn == null) || (sagr == null && mn1 == null) || (tgl_sagrs == '')){

                    DevExpress.ui.dialog.alert("Data tidak lengkap.", "Error");
                }else{
                    if( mn != null ){
                        console.log(mn);
                        if(mn.length != 10){
                            DevExpress.ui.dialog.alert("No.PO harus 10 digit.", "Error");
                            return;
                        }
                    }
                    if( mn1 != null ){
                        if(mn1.length != 10){
                            DevExpress.ui.dialog.alert("No. SAGR harus 10 digit.", "Error");
                            return;
                        }
                    }
                    var result = DevExpress.ui.dialog.confirm("Apakah anda yakin?", "Confirm");
                    result.done(function (dialogResult) {
                        console.log(dialogResult);
                        if(dialogResult) {
                            var ID = $('#btn_update').val();
                            var data = $('#related_invoice').serialize();
                            $.post('{{ url('invoice/input/update_related_invoice/'.$id) }}/'+ID, data)
                                .done(function(results){
                                    if(results == 'ok') {

                                        $('#btn_add').show();
                                        $('#btn_edit').show();
                                        $('#btn_delete').show();
                                        $('#btn_update').hide();
                                        $('#manual').click();
                                        clearData();
                                        dataGrid.refresh();
                                        //dataGridJaminan.option("dataSource", '{{ url("invoice/input/getJaminan/") }}'+ nomor_po + '/jaminan');
                                        dataGridJaminan.refresh();
                                    } else {

                                        DevExpress.ui.dialog.alert(results, "Error");
                                    }
                                }).fail(function(){

                                });
                        }
                    });
                }
            });

            $('#no_sagr').select2({
                ajax: {
                    type:'GET',
                    dataType: 'json',
                    url: function(){
                        var no_po = $('#no_po').val();
                        return '{{ url("invoice/input/get_sagr/{$id}/") }}' + no_po;
                    },
                    data: function (params) {
                        var query = {q: params.term}
                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data, function(obj){
                                return {id:obj.id, text:obj.text}
                            })
                        }
                    },
                    cache:true
                },

                placeholder:'Pilih Nomor SA/GRS...',
            });
            $('#no_po').on('change', function(e){
                var nomor_po = $(this).val();
                $('#no_sagr').val('').trigger('change');
                $('#po_nilai').val('');
                $('#po_kurs').html('');
                $('#po_desc').html('');
                $.post("{{ url("invoice/input/get_detail_po/{$id}/") }}" + nomor_po)
                    .done(function(data){
                        $('input[name=NilaiPO]').val(data.NilaiPO);
                        if(typeof data.NilaiKontrak !== 'undefined') {
                            $('input[name=NilaiKontrak]').val(data.NilaiKontrak);
                        }
                        $('#po_kurs').html(data.KursPO);
                        $('#po_desc').html(data.DeskripsiPO);

                        $('input[name=KursPO]').val(data.KursPO);
                        $('input[name=DeskripsiPO]').val(data.DeskripsiPO);

                    }).fail(function(){
                        DevExpress.ui.dialog.alert('Periksa data Anda sebelum melanjutkan.', "Error");
                    });
            });

            $('#no_sagr').on('change', function(e){
                var no_sagr = $(this).val();
                $.post("{{ url("invoice/input/get_detail_sagr/{$id}/") }}" + no_sagr)
                    .done(function(data){
                        $('input[name=TglSAGR]').datepicker('update', data.TanggalSAGR);
                        if(typeof data.NomorBA !== 'undefined') {
                            $('input[name=NomorBA]').val(data.NomorBA);
                        }
                    }).fail(function(){
                        alert('Something Wrong');
                    });
            });

            $('#no_po').on("change", function(e){
                /*$( '#related_invoice' ).each(function(){
                    this.reset();
                });*/
                $('#no_sagr').val('').trigger('change');
                $('#nilai_po').val('');
                $('#po_kurs').html('');
                $('#po_desc').html('');
                //dataGrid.refresh();
            });

            $('#no_po').select2({
                ajax: {
                    type:'GET',
                    dataType: 'json',
                    url: '{{ url("invoice/input/get_po/{$id}") }}',
                    data: function (params) {
                        var query = {q: params.term}
                        return query;
                    },
                    processResults: function (data) {
                        return {results: data}
                    },
                    cache:true
                },
                placeholder:'Pilih Nomor PO...',
            });

            var checkbox = document.getElementById('manual');
                checkbox.addEventListener('click', function () {

                    if (document.getElementById('mn')) {
                        document.getElementById('mn').remove();
                        $('#po_nilai').prop("readonly", true);
                        $('#no_kontrak').prop("readonly", true);
                        $('#nilai_kontrak').prop("readonly", true);
                        $('#no_po').prop("name", "NoPO");
                        $('#is_manual').val('0');
                    } else {
                        var input = document.createElement("input");
                        input.id = 'mn';
                        input.className = 'form-control';
                        input.type = 'text';
                        input.name = 'NoPO';
                        input.setAttribute("maxlength", "10");
                        input.placeholder = 'Manual PO';
                        document.body.appendChild(input);
                        $('input[name=NoPO]').mask('0000000000', {reverse:true})
                        $( ".manual" ).append( input );
                        $('#no_po').removeAttr("name");
                        $('#po_nilai').removeAttr("readonly");
                        $('#no_kontrak').removeAttr("readonly");
                        $('#nilai_kontrak').removeAttr("readonly");

                        //is_manual
                        $('#is_manual').val('1');

                    }
                    var x = document.getElementById("box_po");
                    if (x.style.display === "none") {
                        x.style.display = "block";
                    } else {
                        x.style.display = "none";
                    }
                    var x = document.getElementById("box_sagr");
                    if (x.style.display === "none") {
                        x.style.display = "block";
                    } else {
                        x.style.display = "none";
                    }
                });
            var checkbox1 = document.getElementById('manual');
                checkbox1.addEventListener('click', function () {
                    if (document.getElementById('mn1')) {
                        document.getElementById('mn1').remove();
                        $('#no_sagr').prop("name", "NoSAGR");
                        $('#tgl_sagrs').prop("readonly", true);
                    } else {
                        var input = document.createElement("input");
                        input.id = 'mn1';
                        input.className = 'form-control';
                        input.type = 'text';
                        input.name = 'NoSAGR';
                        input.setAttribute("maxlength", "10");
                        input.placeholder = 'Manual SA/GRS';
                        document.body.appendChild(input);
                        $('input[id=mn1]').mask('0000000000', {reverse:true})
                        $( ".sagrsmanual" ).append( input );

                        $('#no_sagr').removeAttr("name");
                        $('#tgl_sagrs').removeAttr("readonly");
                    }
                });

            $('.datepicker').datepicker({
                autoclose: true, format:'dd-mm-yyyy',
            });
            $('input[name=NilaiKontrak]').mask('000.000.000.000.000',{reverse:true});
            $('input[name=NilaiInvoice]').inputmask('numeric', {
                groupSeparator: '.',
                radixPoint : ',',
                autoGroup: true,
                digits: 2,
                digitsOptional: false,
                placeholder: '0,00',
            });
            $('input[name=NilaiPO]').mask('000.000.000.000.000', {reverse:true})

            var dataSource = new DevExpress.data.CustomStore({
                key: "ID",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};

                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }

                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 12;

                    $.ajax({
                        url: "{{ url("invoice/input/get/".$id) }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });

                    return deferred.promise();
                }
            });
            var dataSourceJaminan = new DevExpress.data.CustomStore({
                key: "ID",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};

                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }

                    if (loadOptions.filter != 'undefined') {
                        args.filter = JSON.stringify(loadOptions.filter);
                    }

                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 12;

                    $.ajax({
                        url: "{{ url("invoice/input/getJaminan/".$id) }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });

                    return deferred.promise();
                }
            });

            var dataGrid = $("#gridContainer").dxDataGrid({
                dataSource : dataSource,
                onSelectionChanged: function(data){
                    $('#btn_delete').prop('disabled', !(data.selectedRowsData.length));
                    $('#btn_edit').prop('disabled', !(data.selectedRowsData.length == 1));
                },
                selection:{mode:'multiple',showCheckBoxesMode:'always',selectAllMode:'page'},
                remoteOperations: {sorting: true,paging: true,filtering:true},
                "export" : {enable:true, fileName : "{{ $table }}"},
                editing : {mode:'form', allowAdding : false,allowUpdating : false,allowDeleting : false},
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : false},
                headerFilter:{visible:true},
                filterRow:{visible:true,applyFilter:'auto'},
                groupPanel : {visible:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: false,visible: true},
                columns : [
                    // {caption:'No',cellTemplate:function(cellElement, cellInfo){
                    //     cellElement.text(cellInfo.row.rowIndex+1)
                    // }},
                    {dataField:'NoPO',caption:'No. PO'},
                    {dataField:'NilaiPO',caption:'Nilai PO',calculateCellValue:function(data){
                        return number_format(data.NilaiPO,2,',','.')
                    }},
                    {dataField:'NomorKontrak',caption:'No Kontrak'},
                    {dataField:'NilaiKontrak',caption:'Nilai Kontrak'},
                    {dataField:'NoSAGR',caption:'No. SA/GRS'},
                    {dataField:'TglSAGR',caption:'Tgl SA/GRS',dataType: 'date',format: 'dd/MM/yyyy'},
                    // {dataField:'Nama',caption:'Created By'},
                    /*
                    {dataField:'DeskripsiPO',caption:'Deskripsi Pekerjaan'},*/
                    {dataField:'NomorBA',caption:'No. BAPP'},
                    {dataField:'TanggalBA',caption:'Tgl BAPP',dataType: 'date',format: 'dd/MM/yyyy'},
                    {dataField:'Nama',caption:'Created By'},
                    {dataField:'Keterangan',caption:'Keterangan'},/*
                    {dataField:'PO_Manual',caption:'PO Manual'},*/
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
                rowAlternationEnabled: true,
                rowAlternationEnabled: true,
            }).dxDataGrid("instance");

            var dataGridJaminan = $("#gridContainerJaminan").dxDataGrid({
                dataSource : dataSourceJaminan,
                onSelectionChanged: function(data){
                    $('#btn_delete').prop('disabled', !(data.selectedRowsData.length));/*
                    $('#btn_edit').prop('disabled', !(data.selectedRowsData.length == 1));*/
                },
                selection:{mode:'multiple',showCheckBoxesMode:'always',selectAllMode:'page'},
                remoteOperations: {sorting: true,paging: true,filtering:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: false,visible: true},
                columns : [
                    {caption:'No',cellTemplate:function(cellElement, cellInfo){
                    cellElement.text(cellInfo.row.rowIndex+1)
                    }},
                    {dataField:'NO_PO',caption:'No. PO'},
                    {dataField:'ID_JENIS_JAMINAN',caption:'Jenis Jaminan'},
                    {dataField:'NOMOR_JAMINAN',caption:'No. Jaminan'},
                    {dataField:'NILAI_JAMINAN',caption:'Nilai Jaminan'},
                    {dataField:'MASA_BERLAKU',caption:'Masa Berlaku'},
                    {dataField:'ID_BANK_PENERBIT',caption:'Bank Penerbit'},
                    {dataField:'KETERANGAN_JAMINAN',caption:'Keterangan'},
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
                rowAlternationEnabled: true,
                rowAlternationEnabled: true,
            }).dxDataGrid("instance");

        });
    </script>
@endsection
