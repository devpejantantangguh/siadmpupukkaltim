@extends('templates.admin')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
    <script src="{{ url('asset/js/phpjs.js') }}"></script>
@endsection

@section('contentheader')
    <section class="content-header">
        <h1>All Invoice</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Invoice (Unit Kerja)</li>
            <li class="active">All Invoice</li>
        </ol>
    </section>
@endsection

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">All Invoice</h3>
    <span class="pull-right">Area: {{Auth::unitKerja()->Area}}</span>
    </div>
    <div class="box-body">
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {{ flashdata('error') }}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> {{ flashdata('warning') }}
            </div>

        @elseif(flashdata('info'))

            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Info!</strong> {{ flashdata('info') }}
            </div>
        @endif

    <div id="btn_view"></div>
    <br><br>
        <div id="gridContainer"></div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){

            var openButton = $("#btn_view").dxButton({
                    text: "View",
                    height: 34,
                    width: 100,
                    disabled: true,
                    onClick: function () {
                        $.each(dataGrid.getSelectedRowsData(), function() {
                            location.href = "{{ url('invoice/invoice/view/') }}" + this.ID;
                        });
                    }
                }).dxButton("instance");
            var dataSource = new DevExpress.data.CustomStore({
                key: "ID",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};

                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }
                    if (loadOptions.filter != 'undefined') {
                        args.filter = JSON.stringify(loadOptions.filter);
                    }

                    if (loadOptions.filter != 'undefined') {
                        args.filter = JSON.stringify(loadOptions.filter);
                    }
                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 20;

                    $.ajax({
                        url: "{{ url("invoice/invoice/get_all") }}", // status draft/waiting approval
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });

                    return deferred.promise();
                }
            });

            var list_status = {!! json_encode($statuses)!!};

            var dataGrid = $("#gridContainer").dxDataGrid({
                dataSource : dataSource,
                // columnHidingEnabled: true,
                paging: {
                    pageSize: 20
                },
                remoteOperations: {sorting: true,paging: true,filtering:true},

                selection:{mode:'multiple'},
                "export" : {enabled:true, fileName : "export",allowExportSelectedData: true},
                onSelectionChanged: function(data){
                    openButton.option('disabled', !(data.selectedRowsData.length == 1))
                },
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : false},
                headerFilter:{visible:true},
                filterRow:{visible:true,applyFilter:'auto'},
                groupPanel : {visible:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [20, 100, 200, 500],showInfo: true,showNavigationButtons: true,showPageSizeSelector: true,visible: true},
                columns : [
                    {dataField:'NoInvoice',caption:'No Invoice'},
                    {dataField:'IDVendor',caption:'Kode Vendor'},
                    {dataField:'NamaVendor',caption:'Nama Vendor'},
                    {dataField:'NilaiInvoice',caption:'Nilai Invoice', dataType: "number",calculateCellValue:function(data){
                        return data.KursInvoice + ' ' + number_format(data.NilaiInvoice,2,',','.')
                    }},
                    {dataField:'IDArea',caption:'ID Area'},
                    {dataField:'NoFakturPajak',caption:'No Faktur Pajak'},
                    {dataField:'Status',caption:'Status',dataType:'integer',lookup:{dataSource: list_status, displayExpr: "name", valueExpr: "id"}},
                    {dataField:'NoDokumenSAP',caption:'No Dokumen'},
                    {dataField:'Tahun',caption:'Tahun'},
                    {dataField:'UnitKerja',caption:'Unit Kerja'},
                    {dataField:'Keterangan',caption:'Keterangan'},
                    {dataField:'NoTransmittal',caption:'No. Transmittal'},
                    {dataField:'TglTransmittal',caption:'Tgl. Transmittal'}
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
                rowAlternationEnabled: true,
            }).dxDataGrid("instance");
        });

    </script>
@endsection
