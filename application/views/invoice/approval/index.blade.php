@extends('templates.admin')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
    <script src="{{ url('asset/js/phpjs.js') }}"></script>
@endsection

@section('contentheader')
    <section class="content-header">
        <h1>Approval Dokumen</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Invoice (Unit Kerja)</li>
            <li class="active">Approval Dokumen</li>
        </ol>
    </section>
@endsection

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">Approval Dokumen</h3>
    </div>
    <div class="box-body">
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {{ flashdata('error') }}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> {{ flashdata('warning') }}
            </div>

        @elseif(flashdata('info'))

            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Info!</strong> {{ flashdata('info') }}
            </div>
        @endif
        <button class="btn btn-primary btn-sm" id="btn_approve" disabled><i class="fa fa-check"></i> Approve</button><!-- 
        <button class="btn btn-warning btn-sm" id="btn_revisi2"disabled><i class="fa fa-close"></i> Revisi</button> -->
        
        <button class="btn btn-warning btn-sm" id="btn_revisi" data-toggle="modal" data-target="#m-revisi" disabled><i class="fa fa-close"></i> Revisi</button>
        <button class="btn btn-success btn-sm" id="btn_view" disabled><i class="fa fa-eye"></i> View</button>
        <div id="gridContainer"></div>
    </div>

    <div id="m-revisi" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <form action="" method="POST" id="form_revisi">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Alasan Revisi</h4>
          </div>
          <div class="modal-body">
            <textarea name="Keterangan" class="form-control" placeholder="Dokumen di revisi karena...."" required></textarea>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-danger btn-sm">Revisi</button>
          </div>
        </div>
        </form>
      </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){
            var dataSource = new DevExpress.data.CustomStore({
                key: "ID",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};

                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }
                    if (loadOptions.filter != 'undefined') {
                        args.filter = JSON.stringify(loadOptions.filter);
                    }

                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 20;

                    $.ajax({
                        url: "{{ url("invoice/invoice/get/") }}{{statusID('WAITING APPROVAL')}}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });

                    return deferred.promise();
                }
            });

            $('#btn_approve').click(function(e){
                e.preventDefault();
                var result = DevExpress.ui.dialog.confirm("Apakah Anda Yakin Melakukan Approval?", "Approval");
                result.done(function (dialogResult) {
                    if (dialogResult) {
                        var lists = [];
                        $.each(dataGrid.getSelectedRowsData(), function() {
                            lists.push(this.ID);
                        });
                        $.post("{{ url('invoice/approval/approve') }}", {ids:lists})
                            .done(function(response){
                                var result = DevExpress.ui.dialog.alert(response);
                                dataGrid.refresh();
                            });
                    }
                });
            });

            $('#btn_revisi2').click(function(e){
                e.preventDefault();
                var result = DevExpress.ui.dialog.confirm("Apakah Anda Yakin Melakukan Revisi?", "Approval");
                result.done(function (dialogResult) {
                    if (dialogResult) {
                        var lists = [];
                        $.each(dataGrid.getSelectedRowsData(), function() {
                            lists.push(this.ID);
                        });
                        $.post("{{ url('invoice/approval/setDraft') }}", {ids:lists})
                            .done(function(response){
                                
                                dataGrid.refresh();
                                $.post("{{ url('invoice/approval/sentEmail') }}", {ids:lists})
                                .done(function(responses){
                                    
                                        DevExpress.ui.notify({
                                        message: responses,
                                        position: {
                                            my: "center top",
                                            at: "center top"
                                        }
                                        }, "info", 3000);
                                });
                                DevExpress.ui.dialog.alert(response);

                            });
                    }
                });
            });

            $('#btn_revisi').click(function(e){
                e.preventDefault();

                $.each(dataGrid.getSelectedRowsData(), function() {
                    $('#form_revisi').attr('action', "{{ url('invoice/approval/reject_update/') }}" + this.ID)
                });
            });
            $('form').submit(function(e){

                e.preventDefault();
                var result = DevExpress.ui.dialog.confirm("Apakah anda yakin?", "Confirm");
                    result.done(function (dialogResult) {
                        console.log(dialogResult);
                        if(dialogResult) {
                            $('form').unbind('submit').submit();
                        }else{
                            $('form').find("button[type='submit']").prop('disabled',false);
                        }
                });

            });

            $('#btn_view').click(function(){
                $.each(dataGrid.getSelectedRowsData(), function() {
                    location.href = "{{ url('invoice/approval/view/') }}" + this.ID;
                });
            });

            var list_status = {!! json_encode($list_status) !!}

            var dataGrid = $("#gridContainer").dxDataGrid({
                dataSource : dataSource,
                // columnHidingEnabled: true,
                paging: {
                    pageSize: 20
                },
                remoteOperations: {sorting: true,paging: true,filtering:true},
                "export" : {enable:true, fileName : "{{ $table }}"},
                editing : {mode:'form', allowAdding : false,allowUpdating : false,allowDeleting : false},
                selection:{mode:'multiple',showCheckBoxesMode:'always',selectAllMode:'page'},
                onSelectionChanged: function(data){
                    $('#btn_view').prop('disabled', !(data.selectedRowsData.length == 1));
                    $('#btn_approve').prop('disabled', !(data.selectedRowsData.length));
                    $('#btn_revisi').prop('disabled', !(data.selectedRowsData.length == 1));
                    /*$('#btn_revisi2').prop('disabled', !(data.selectedRowsData.length));*/
                },
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : false},
                headerFilter:{visible:true},
                filterRow:{visible:true,applyFilter:'auto'},
                groupPanel : {visible:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: false,visible: true},
                columns : [
                    {dataField:'NoInvoice',caption:'No Invoice'},
                    {dataField:'IDVendor',caption:'Kode Vendor'},
                    {dataField:'NamaVendor',caption:'Nama Vendor'},
                    {dataField:'NilaiInvoice',caption:'Nilai Invoice',
                dataType: "number",calculateCellValue:function(data){
                        return data.KursInvoice + ' ' + number_format(data.NilaiInvoice,2,',','.')
                    }},
                    {dataField:'Status',caption:'Status',dataType:'integer',lookup:{dataSource: list_status, displayExpr: "name", valueExpr: "id"}},
                    {dataField:'IDArea',caption:'ID Area'},
                    {dataField:'Keterangan',caption:'Keterangan'},
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
                rowAlternationEnabled: true,/*
                onSelectionChanged: function(e) {
                    var data = e.selectedRowsData[0];

  var selectedRows = e.selectedRowKeys;
  console.log(data);
  $.each(selectedRows, function(i, v) {

      e.component.deselectRows([v]); return;
    
  })
}*/
            }).dxDataGrid("instance");
        });

    </script>
@endsection
