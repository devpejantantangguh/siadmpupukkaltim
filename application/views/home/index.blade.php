@extends('templates.admin')

@section('title','Dashboard')

@section('contentheader')
	<section class="content-header">
	    <h1>Home</h1>
	    <ol class="breadcrumb">
	        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	    </ol>
	</section>
@endsection

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">Selamat Datang</h3>
        <span class="pull-right">Area: {{Auth::unitKerja()->Area}}</span>
    </div>
    <div class="box-body">
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {{ flashdata('error') }}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> {{ flashdata('warning') }}
            </div>

        @elseif(flashdata('info'))

            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Info!</strong> {{ flashdata('info') }}
            </div>
        @endif
    	{{-- <center><h1>Sistem Informasi Administrasi Keuangan</h1></center> --}}
        <img src="{{ url('asset/image/dash_bg2.png') }}" style="width: 100%">
        <img src="{{ url('asset/image/dash_bg.png') }}" style="width: 100%">
    </div>
@endsection

@section('script')
    {{-- code/script here --}}
@endsection
