@extends('templates.admin')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
    <script src="{{ url('asset/js/phpjs.js') }}"></script>
    <script src="{{ url('asset/js/bootstrap-datepicker.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ url('asset/css/bootstrap-datepicker.min.css') }}" />
    <style type="text/css">
        h3.panel-title{font-size: 14px !important}
        .dx-fileuploader-show-file-list .dx-fileuploader-files-container{padding-top:0px}
        .dx-fileuploader-wrapper{padding:0px;}
    </style>
@endsection 

@section('contentheader')
    <section class="content-header">
        <h1>View Invoice</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Keuangan</li>
            <li class="active">View Invoice</li>
        </ol>
    </section>
@endsection

@section('title','Reject Invoice')

@section('content')
    <form method="POST" action="{{ url('keuangan/invoice/verifikasi/reject_update/'.$id) }}">
        <div class="box-header with-border">
            <h3 class="box-title">Reject Invoice</h3>
        </div>
        <div <div class="box-body">
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {{ flashdata('error') }}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> {{ flashdata('warning') }}
            </div>

        @elseif(flashdata('info'))

            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Info!</strong> {{ flashdata('info') }}
            </div>
        @endif
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {{ flashdata('error') }}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> {{ flashdata('warning') }}
            </div>

        @elseif(flashdata('info'))

            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Info!</strong> {{ flashdata('info') }}
            </div>
        @endif
            <div class="form-group">
                <label>Alasan Reject</label>
                <textarea name="Keterangan" class="form-control" placeholder="Dokumen di revisi karena...." required></textarea>
            </div>
        </div>
        <div class="box-footer">
            <a href="{{ url('keuangan/invoice/verifikasi/open/'.$id) }}" class="btn btn-warning btn-sm" style="color:#fff">
                <i class="fa fa-arrow-left"></i> Kembali
            </a>
            <button class="btn btn-danger btn-sm" name="Submit" value="Approve"><i class="fa fa-close"></i> Reject</button>
        </div>
    </form>
@endsection

@section('script')
   
@endsection
