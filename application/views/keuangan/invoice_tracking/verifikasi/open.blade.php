@extends('templates.admin')

@section('heading')
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
<script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
<script src="{{ url('asset/js/phpjs.js') }}"></script>
<style type="text/css">
h3.panel-title{font-size: 14px !important}
.dx-fileuploader-show-file-list .dx-fileuploader-files-container{padding-top:0px}
.dx-fileuploader-wrapper{padding:0px;}
.form-horizontal .control-label{text-align: left !important;padding-left:20px;}
</style>
@endsection

@section('title','Verifikasi Invoice')

@section('contentheader')
<section class="content-header">
    <h1>Keuangan</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Keuangan</li>
        <li>Invoice Trancking</li>
        <li class="active">Verifikasi Invoice</li>
    </ol>
</section>
@endsection 

@section('content')
<div class="box-header with-border">
    <h3 class="box-title">Verifikasi Invoice</h3>
    <span class="pull-right">Area: {{Auth::unitKerja()->Area}}</span>
</div>
<div <div class="box-body">
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {{ flashdata('error') }}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> {{ flashdata('warning') }}
            </div>

        @elseif(flashdata('info'))

            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Info!</strong> {{ flashdata('info') }}
            </div>
        @endif
    <div role="tabpanel">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a>
            </li>
            <li role="presentation">
                <a href="#history" aria-controls="history" role="tab" data-toggle="tab">History</a>
            </li>
        </ul>
    </div>
    <div class="tab-content" style="padding-top:10px">
       <div role="tabpanel" class="tab-pane active" id="details">
        
        <form>

            @if(isset($_GET['update']) && $_GET['update'] == 'success')
            <div class="alert alert-success">
                <p>Dokumen Berhasil diubah</p>
            </div>
            @elseif(isset($_GET['update']) && $_GET['update'] == 'reject')
            <div class="alert alert-warning">
                <strong>Reject</strong> Dokumen Berhasil di Reject
            </div>
            @elseif(isset($_GET['update']) && $_GET['update'] == 'fail')
            <div class="alert alert-danger">
                <strong>Error</strong> You did't allow to update this document
            </div>
            @endif

            <div class="panel panel-default form-horizontal">
                <div class="panel-heading">
                    <h3 class="panel-title">Detail Invoice</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Vendor</label>
                        <div class="col-sm-4">
                            <input type="text" value="{{ $data->IDVendor }} - {{ $data->NamaVendor }}" class="form-control" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Jenis Invoice</label>
                        <div class="col-sm-4">
                            <input type="text" value="{{ $list_transaksi[$data->JenisTransaksi] }}" class="form-control" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">No. Invoice</label>
                        <div class="col-sm-4">
                            <input type="text" value="{{ $data->NoInvoice }}" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tgl. Invoice</label>
                        <div class="col-sm-2">
                            <div class="input-group date">
                                <input type="text" class="form-control pull-right datepicker" value="{{ date('d-m-Y', strtotime($data->TglInvoice)) }}" readonly>
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nilai Invoice</label>
                        <div class="col-sm-4">
                            <input type="text" value="{{ number_format($data->NilaiInvoice,2,',','.') }}" class="form-control"  readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nilai Verifikasi</label>
                        <div class="col-sm-4">
                            <input type="text" value="{{ number_format($data->NilaiVerifikasi,2,',','.') }}" class="form-control"  readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Kurs</label>
                        <div class="col-sm-1">
                            <input type="text" value="{{ $list_kurs[$data->KursInvoice] }}" class="form-control" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">No. Faktur Pajak</label>
                        <div class="col-sm-4">
                            <input type="text" value="{{ $data->NoFakturPajak }}" class="form-control" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal Faktur Pajak</label>
                        <div class="col-sm-2">
                            <div class="input-group date">
                                <input type="text" class="form-control pull-right datepicker" value="{{ date('d-m-Y',strtotime($data->TglFakturPajak)) }}" readonly>
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                                    <label class="col-sm-2 control-label">No Dokumen SAP</label>
                                    <div class="col-sm-2">
                                        <input type="text"maxlength="10" name="NoDokumenSAP" value="{{ $data->NoDokumenSAP }}" class="form-control" readonly>
                                    </div>

                                    <label class="col-sm-1 control-label">Tahun</label>
                                    <div class="col-sm-1">
                                        <input type="text" name="Tahun" value="{{ $data->Tahun }}" id="tahun" class="form-control" readonly>
                                    </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Payment Term</label>
                        <div class="col-sm-4">
                            <input type="text" value="{{ $data->PT }}" class="form-control" readonly>
                        </div>
                    </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Unit Kerja</label>
                                    <div class="col-sm-4">
                                        <input type="text"  class="form-control" value="{{ $data->CostCenter }} - {{ Auth::getUnitKerja($data->CostCenter) }}" readonly>
                                    </div>
                                </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-4">
                            <textarea name="Keterangan" class="form-control" readonly>{{ $data->Keterangan }}</textarea>
                        </div>
                    </div>

                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Attachments</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered table-striped table-condensed">
                        <tr>
                            <th>File</th>
                            <th>Action</th>
                        </tr>
                        @foreach ($attachments as $attachment)
                        <tr>
                            <td>{{ $attachment->FilePath }}</td>
                            <td>
                                <a href="{{ url('storage/invoice/'.$attachment->FilePath) }}" class="btn btn-xs btn-success" style="color:#fff">View</a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>

            <div class="panel panel-default form-horizontal">
                <div class="panel-heading">
                    <h3 class="panel-title">Detail Purchase Order</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div id="gridContainer"></div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Jaminan</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div id="gridContainerJaminan"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                                <div class="panel panel-default form-horizontal" id="potongan">
                            <div class="panel-heading">
                                <h3 class="panel-title">Potongan</h3>
                            </div>
                            <div class="panel-body">
                                <div class="panel-body">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <div id="gridPotongan"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            
                        </div>
            <div class="panel panel-info form-horizontal">
                <div class="panel-heading">
                    <span class="clickable panel-collapsed">
                    <h3 class="panel-title">Kelengkapan Dokumen</h3>
                    <span class="pull-right"><i class="glyphicon glyphicon-chevron-down"></i></span>
                    </span>
                </div>
                                    
                        <div class="panel-body" style="display: none">
                            <div class="col-sm-6">
                                @foreach($documents as $document)
                                <div class="form-group">
                                    <div class=" col-sm-10">
                                        <div class="checkbox">
                                            <label>
                                                <?php 
                                                $name = "matriks[".$document['ID']."]";
                                                $extra = $document['Required'] ? ['required' => 'required'] : [];
                                                ?>
                                                {!! form_checkbox($name, $document['Dokumen'], $document['Value'], $extra) !!} 
                                                {{ $document['Dokumen'] }}
                                                @if(!empty($extra)) 
                                                *
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            
                            </div>
                            <div class="col-sm-6">
                                <div class="checkbox pull-right">
                                    <label>
                                        {!! form_checkbox('IsPriority', 'IsPriority', $data->IsPriority) !!} Priority
                                    </label>
                                </div>
                            </div>

                        </div>
            </div>
        <style>
            .clickable{
                cursor: pointer;   
            }

            .panel-heading span {
                margin-top: -20px;
                font-size: 15px;
            }

        </style>
        <script>
                
                $(document).on('click', '.panel-heading span.clickable', function(e){
                var $this = $(this);
                if(!$this.hasClass('panel-collapsed')) {
                    $this.parents('.panel').find('.panel-body').slideUp();
                    $this.addClass('panel-collapsed');
                    $this.find('i').removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
                } else {
                    $this.parents('.panel').find('.panel-body').slideDown();
                    $this.removeClass('panel-collapsed');
                    $this.find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
                }
            })
        </script>
        </form>
        <form action="{{ url('keuangan/invoice/verifikasi/update/'.$id) }}" class="form-horizontal invoice_form" method="POST" id="related_invoice">
            <input type="hidden" name="Kurs" value="" id="Kurs">
            <div class="panel panel-info form-horizontal">
                <div class="panel-heading">
                    <h3 class="panel-title">Apply Piutang</h3>
                </div>
                <div class="panel-body">

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Dokumen Piutang</label>
                        <div class="col-sm-4">
                            <select class="form-control js-ARDocNo" id="ARDocNo" name="ARDocNo"></select>
                        </div>
                        <div class="col-sm-4 control-label">
                            <div class="help-text" id="help-ARDocNo"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Customer</label>
                        <div class="col-sm-4">
                            <input type="text" name="IDCustomer" class="form-control IDCustomer" readonly="">
                        </div>

                        <div class="col-sm-4 control-label">
                            <div class="help-text" id="help-Customer"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nilai Piutang</label>
                        <div class="col-sm-4">
                            <input type="text" name="ARAmount" value="" class="form-control ARAmount" id="money" readonly>
                        </div>

                        <div class="col-sm-4 control-label">
                            <div class="help-text" id="help-ARAmount"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Jumlah Potongan</label>
                        <div class="col-sm-4">
                            <input type="text" name="AppliedAmount" value="" class="form-control AppliedAmount" id="money">
                        </div>

                        <div class="col-sm-4 control-label">
                            <div class="help-text" id="help-AppliedAmount"></div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label"></label>
                        <div class="col-sm-4">
                            <button class="btn btn-sm btn-primary" id="btn_add">Add</button> <!-- 
                            <button class="btn btn-sm btn-success" id="btn_edit" disabled>Edit</button>  -->
                            <button class="btn btn-sm btn-danger" id="btn_delete" disabled>Delete</button> 
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <div id="gridContainer2"></div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Sisa Tagihan yang Akan Dibayarkan</label>
                        <div class="col-sm-4">
                            <input type="text" value="{{ $data->NilaiVerifikasi - $count_applied_amount }}" class="form-control SisaTagihan" id="money" readonly>
                        </div>
                    </div>

                </div>
            </div>
            <div class="panel panel-info form-horizontal">
                <div class="panel-heading">
                    <h3 class="panel-title">Catatan Pemeriksaan</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-10 control-label">
                            <textarea name="note" type="text" class="form-control" rows="4" cols="30"></textarea>
                        </label>
                    </div>
                </div>
            </div>

            <center>
                <button class="btn btn-success btn-sm submit" name="submit" value="applied" >
                    <i class="fa fa-check"></i> Process</button>


                        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#m-revisi">
                            <i class="fa fa-close"></i> Reject
                        </button>
                </center>
            </form>
        </div>

        <div role="tabpanel" class="tab-pane" id="history">
            <table class="table table-condensed table-hover table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Action</th>
                        <th>User</th>
                        <th>Keterangan</th>
                        <th>Timestamp</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($histories as $key => $history)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td>{{ $history->Action }}</td>
                        <td>{{ $history->Nama }}</td>
                        <td>{{ $history->Keterangan }}</td>
                        <td>{{ date('d-m-Y H:i:s', strtotime($history->Timestamp)) }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


<div id="m-revisi" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <form id="revisi_form" method="POST" action="{{ url('keuangan/invoice/verifikasi/reject_update/'.$id) }}">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Alasan Reject</h4>
                </div>
                <div class="modal-body">
                    <textarea name="Keterangan" class="form-control" placeholder="Dokumen di revisi karena...."" required="required"></textarea>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger btn-sm reject">Reject</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('script')

    <script type="text/javascript">
        $(function() {

            var dataSourceJaminan = new DevExpress.data.CustomStore({
                key: "ID",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};
            
                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }
            
                    if (loadOptions.filter != 'undefined') {
                        args.filter = JSON.stringify(loadOptions.filter);
                    }
            
                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 12;
            
                    $.ajax({
                        url: "{{ url("invoice/input/getJaminan/".$id) }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });
            
                    return deferred.promise();
                }
            });
            
            var dataGridJaminan = $("#gridContainerJaminan").dxDataGrid({
                dataSource : dataSourceJaminan,
                onSelectionChanged: function(data){
                    $('#btn_delete').prop('disabled', !(data.selectedRowsData.length));/*
                    $('#btn_edit').prop('disabled', !(data.selectedRowsData.length == 1));*/
                },
                selection:{mode:'multiple',showCheckBoxesMode:'always',selectAllMode:'page'},
                remoteOperations: {sorting: true,paging: true,filtering:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: false,visible: true},
                columns : [
                    {caption:'No',cellTemplate:function(cellElement, cellInfo){
                    cellElement.text(cellInfo.row.rowIndex+1)
                    }},
                    {dataField:'NO_PO',caption:'No. PO'},
                    {dataField:'ID_JENIS_JAMINAN',caption:'Jenis Jaminan'},
                    {dataField:'NOMOR_JAMINAN',caption:'No. Jaminan'},
                    {dataField:'NILAI_JAMINAN',caption:'Nilai Jaminan'},
                    {dataField:'MASA_BERLAKU',caption:'Masa Berlaku'},
                    {dataField:'ID_BANK_PENERBIT',caption:'Bank Penerbit'},
                    {dataField:'KETERANGAN_JAMINAN',caption:'Keterangan'},
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
                rowAlternationEnabled: true,
                rowAlternationEnabled: true,
            }).dxDataGrid("instance");
        })

    </script>
<script type="text/javascript">
    $(function() {
            $('form.invoice_form').submit(function(e){
                
                e.preventDefault();
                var form = this;
                var result = DevExpress.ui.dialog.confirm("Apakah anda yakin?", "Confirm");
                    result.done(function (dialogResult) {
                        console.log(dialogResult);
                        if(dialogResult) {   
                            $(form).unbind('submit');  
                            $(form).find("button[type='submit']").prop('disabled',false); 
                            $('.submit').click(); 
                        }else{
                            $(form).find("button[type='submit']").prop('disabled',false); 
                        }
                });
                
            });

            $('form#revisi_form').submit(function(e){
                
                e.preventDefault();
                var form = this;
                var result = DevExpress.ui.dialog.confirm("Apakah anda yakin?", "Confirm");
                    result.done(function (dialogResult) {
                        console.log(dialogResult);
                        if(dialogResult) {   
                            $(form).unbind('submit');  
                            $(form).find("button[type='submit']").prop('disabled',false); 
                            $('.reject').click(); 
                        }else{
                            $(form).find("button[type='submit']").prop('disabled',false); 
                        }
                });
                
            });

        $('#ARDocNo').change(function(){
            var id = $('#ARDocNo').val();
            if(!!id){
                $.post("{{ url('keuangan/invoice/verifikasi/ajax') }}",{id:id})
                .done(function(data){
                    console.log(typeof data);
                    if(typeof data == 'object'){
                        console.log(data);
                        $('#help-ARDocNo').html(data.Deskripsi);
                        $('#help-Customer').html(data.Customer);
                        $('#help-AppliedAmount').html(data.KursLC);
                        $('#help-ARAmount').html(data.KursLC);
                        $('#Kurs').val(data.KursLC);
                        $('.IDCustomer').val(data.IDCustomer);
                        $('.ARAmount').val(number_format(data.AmountLC,2,',','.'));
                    }
                    

                }).fail(function(){
                    alert('Something Wrong');
                })
            }

        })

            //$('input[name=NilaiInvoice]').mask('000.000.000.000.000',{reverse:true});
            //$('.datepicker').datepicker({autoclose: true, format:'dd-mm-yyyy'});
            
        });
    </script>
    <script type="text/javascript">
        
        $('input[type=checkbox]').attr('disabled','true');
        $(function() {
            var dataSource = new DevExpress.data.CustomStore({
                key: "ID",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                    args = {};
                    
                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }
                    
                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 12;
                    
                    $.ajax({
                        url: "{{ url("invoice/input/get/".$data->ID) }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });
                    
                    return deferred.promise();
                }
            });

            var dataGrid = $("#gridContainer").dxDataGrid({
                dataSource : dataSource,
                // columnHidingEnabled: false,
                remoteOperations: {sorting: true,paging: true,filtering:true},
                "export" : {enable:true, fileName : "{{ $table }}"},
                editing : {mode:'form', allowAdding : false,allowUpdating : false,allowDeleting : false},
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : true},
                filterRow : true,
                groupPanel : {visible:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: false,visible: true},
                columns : [
                    // {caption:'No',cellTemplate:function(cellElement, cellInfo){
                    //     cellElement.text(cellInfo.row.rowIndex+1)
                    // }},
                    {dataField:'NoPO',caption:'No. PO'},
                    {dataField:'NilaiPO',caption:'Nilai PO',calculateCellValue:function(data){
                        return number_format(data.NilaiPO,2,',','.')
                    }},
                    {dataField:'NomorKontrak',caption:'No Kontrak'},
                    {dataField:'NilaiKontrak',caption:'Nilai Kontrak'},
                    {dataField:'NoSAGR',caption:'No. SA/GRS'},
                    {dataField:'TglSAGR',caption:'Tgl SA/GRS',dataType: 'date',format: 'dd/MM/yyyy'},
                    {dataField:'Nama',caption:'Created By'},
                    /*
                    {dataField:'DeskripsiPO',caption:'Deskripsi Pekerjaan'},*/
                    {dataField:'NomorBA',caption:'No. BAPP'},
                    {dataField:'TanggalBA',caption:'Tgl BAPP',dataType: 'date',format: 'dd/MM/yyyy'},
                    {dataField:'Nama',caption:'Created By'},
                    {dataField:'Keterangan',caption:'Keterangan'},
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
            }).dxDataGrid("instance");


        });
    </script>    
    <!--  -->
    <!--  -->
    <!--  -->
    <!--  -->
    <!--  -->
    <script type="text/javascript">
        $('.select2').select2();
        $(function() {
            var dataSource2 = new DevExpress.data.CustomStore({
                key: "ARDocNo",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                    args = {};
                    
                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }
                    
                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 999;
                    
                    $.ajax({
                        url: "{{ url("keuangan/invoice/verifikasi/getAppliedDocument/".$id) }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });
                    
                    return deferred.promise();
                }
            });

            var dataGrid2 = $("#gridContainer2").dxDataGrid({
                dataSource : dataSource2,
                onSelectionChanged: function(data){
                    $('#btn_delete').prop('disabled', !(data.selectedRowsData.length));
                    $('#btn_edit').prop('disabled', !(data.selectedRowsData.length == 1));
                },
                selection:{mode:'multiple',showCheckBoxesMode:'always',selectAllMode:'page'},
                remoteOperations: {sorting: true,paging: true,filtering:true},
                "export" : {enable:true, fileName : "{{ $table }}"},
                editing : {mode:'form', allowAdding : false,allowUpdating : false,allowDeleting : false},
                allowColumnReordering : false,
                allowColumnResizing : true,
                searchPanel : {visible : false},
                headerFilter:{visible:false},
                filterRow:{visible:false,applyFilter:'auto'},
                groupPanel : {visible:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                columns : [
                {caption:'No',cellTemplate:function(cellElement, cellInfo){
                    cellElement.text(cellInfo.row.rowIndex+1)
                }},
                {dataField:'ARDocNo',caption:'No. Dokumen Piutang'},
                {caption:'Nilai Piutang',calculateCellValue:function(data){
                return number_format(data.ARAmount,2,',','.')
                }},
                {caption:'Nilai Apply',calculateCellValue:function(data){
                return number_format(data.AppliedAmount,2,',','.')
                }},
                {caption:'Kurs',cellTemplate:function(cellElement, cellInfo){
                    cellElement.text('{{ $list_kurs[$data->KursInvoice] }}')
                }},
                {dataField:'AppliedBy',caption:'Created By'},
                {dataField:'AppliedOn',caption:'Created On'},
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: false},
                columnFixing: {enabled: false},
            }).dxDataGrid("instance");
            $('#btn_add').click(function(e){
                
                e.preventDefault();
                var result = DevExpress.ui.dialog.confirm("Apakah anda yakin?", "Confirm");
                    result.done(function (dialogResult) {
                        console.log(dialogResult);
                        if(dialogResult) {
                var data = $('#related_invoice').serialize();
                $.post('{{ url('keuangan/invoice/verifikasi/add_applied_document/'.$id) }}', data)
                .done(function(results){
                    if(results != '') {
                        $( '#related_invoice' ).each(function(){
                            this.reset();
                        });
                        var SisaTagihan = '{{$data->NilaiVerifikasi}}';
                        $('.SisaTagihan').val(number_format(SisaTagihan-results,2,',','.'));

                        $("#ARDocNo").empty().trigger("change");
                        $('#help-ARDocNo').html('');
                        $('#help-Customer').html('');
                        $('#help-AppliedAmount').html('');
                        $('#help-ARAmount').html('');
                        //$("input[name=ARDocNo]").val('').trigger('change')
                        dataGrid2.refresh();
                    }
                }).fail(function(){

                });
            }
        });
            });
            $('#btn_edit').click(function(e){
                e.preventDefault();
                $.each(dataGrid.getSelectedRowsData(), function() {
                    console.log(this.ARDocNo);
                });

                dataGrid.refresh();
            });

            $('#btn_delete').click(function(e){

                e.preventDefault();
                var result = DevExpress.ui.dialog.confirm("Apakah anda yakin?", "Confirm");
                    result.done(function (dialogResult) {
                        console.log(dialogResult);
                        if(dialogResult) {
                $.each(dataGrid2.getSelectedRowsData(), function() {
                    var ID = this.ARDocNo;
                    $.post("{{ url('keuangan/invoice/verifikasi/delete_applied_document/').$id }}", {ids:ID})
                    .done(function(results){
                        if(results != '') {
                        var SisaTagihan = '{{$data->NilaiVerifikasi}}';
                        $('.SisaTagihan').val(number_format(SisaTagihan-results,2,',','.'));
                        dataGrid2.refresh();
                    }
                    });
                });
            }
        });

                
            });

            $('#ARDocNo').select2({
                ajax: {
                    type:'GET',
                    dataType: 'json',
                    url: '{{ url("keuangan/invoice/verifikasi/get_ARDocNo/{$data->IDVendor}") }}',
                    data: function (params) {
                        var query = {q: params.term}
                        return query;
                    },
                    processResults: function (data) {
                        return {results: data}
                    },
                    cache:true
                },
                placeholder:'Pilih Nomor Dokumen...',
            });

        });
 var dataSourcePotongan = new DevExpress.data.CustomStore({
                key: "ID",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                    args = {};
                    
                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }
                    
                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 999;
                    
                    $.ajax({
                        url: "{{ url("akuntansi/invoice/lists/getPotongan/".$id) }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });
                    
                    return deferred.promise();
                }
            });

             var dataGridPotongan = $("#gridPotongan").dxDataGrid({
                dataSource : dataSourcePotongan,
                // columnHidingEnabled: false,
                remoteOperations: {sorting: true,paging: false},
                "export" : {enable:true, fileName : "{{ $table }}"},
                editing : {mode:'form', allowAdding : false,allowUpdating : false,allowDeleting : false},
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : false},
                headerFilter:{visible:false},
                filterRow:{visible:false,applyFilter:'auto'},
                groupPanel : {visible:false},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                columns : [
                {caption:'No',cellTemplate:function(cellElement, cellInfo){
                    cellElement.text(cellInfo.row.rowIndex+1)
                }},
                {dataField:'Potongan',caption:'Potongan'},
                {caption:'Nominal',calculateCellValue:function(data){
                return number_format(data.Nominal,2,',','.')
            }},
                {caption:'Kurs',cellTemplate:function(cellElement, cellInfo){
                    cellElement.text('{{ $list_kurs[$data->KursInvoice] }}')
                }},
                {dataField:'Keterangan',caption:'Keterangan'},
                {dataField:'Nama',caption:'Created By'},
                {dataField:'CreatedOn',caption:'Created On',dataType: 'date',format: 'dd/MM/yyyy'},
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: false},
                columnFixing: {enabled: false},
            }).dxDataGrid("instance");

$('input[id=money]').inputmask('numeric', {
            groupSeparator: '.',
            radixPoint : ',',
            autoGroup: true,
            digits: 2,
            digitsOptional: false,
            placeholder: '0,00'
        });
    </script>
    @endsection
