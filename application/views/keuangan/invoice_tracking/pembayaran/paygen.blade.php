@extends('templates.admin')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
@endsection

@section('contentheader')
    <section class="content-header">
        <h1>Upload Paygen</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Keuangan</li>
            <li>Invoice Trancking</li>
            <li>Pembayaran</li>
            <li class="active">Upload Paygen</li>
        </ol>
    </section>
@endsection

@section('title','Upload Paygen')

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">Upload Paygen</h3>
    </div>
<div <div class="box-body">
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {!! flashdata('success') !!}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {!! flashdata('error') !!}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                {!! flashdata('warning') !!}
            </div>

        @elseif(flashdata('info'))

            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Info!</strong> {!! flashdata('info') !!}
            </div>
        @endif
            <div class="panel panel-default">
            <div class="panel-body">
                <form action="{{ url('keuangan/invoice/pembayaran/paygenstore') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2">Upload Paygen</label>
                        <div class="col-sm-10">
                            <input name="fileexcel" type="file">
                            <p class="help-block">Upload file excel Paygen</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <button class="btn btn-primary">Upload</button>

                            <a class="btn btn-warning" href="{{ url('storage/template/template-paygen.xlsx') }}" role="button" style="color:#fff">Download Template</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div id="dxPost"></div>
    <div id="gridContainer"></div>

</div>
@endsection

@section('script')
<script type="text/javascript">
    $(function(){

        var postButton = $("#dxPost").dxButton({
            text: "Post",
            height: 34,
            width: 100,
            disabled: true,
            onClick: function () {
                    $.each(dataGrid.getSelectedRowsData(), function() {

                        var NoDokumen = this.NoDokumen;
                        var url = "{{ url('keuangan/invoice/pembayaran/paygenpost') }}";
                        var forms = "NoDokumen="+NoDokumen+"";
                        postButton.option('text', 'Loading...');
                        $.ajax({
                               type: "POST",
                               url: url,
                               data: forms ,

                               success: function(data)
                               {
                                    alert(data)
                               }
                             });
                        postButton.option('text', 'Post');
                        dataGrid.refresh();
                        console.log('refresh');
                    });
            }
        }).dxButton("instance");
        var dataSource = new DevExpress.data.CustomStore({
            key: "NoDokumen",
            load: function (loadOptions) {
                var deferred = $.Deferred(),
                args = {};

                if (loadOptions.sort) {
                    args.orderby = loadOptions.sort[0].selector;
                    if (loadOptions.sort[0].desc)
                        args.orderby += " desc";
                }

                args.skip = loadOptions.skip || 0;
                args.take = loadOptions.take || 12;

                $.ajax({
                    url: "{{ url("keuangan/invoice/pembayaran/getpaygen") }}",
                    data: args,
                    success: function(response) {
                        console.log(response);
                        deferred.resolve(response.data, { totalCount: response.totalCount });
                    },
                    error: function() {
                        deferred.reject("Data Loading Error");
                    },
                    timeout: 5000
                });

                return deferred.promise();
            }
        });

        var dataGrid = $("#gridContainer").dxDataGrid({
            dataSource : dataSource,
                dataSource : dataSource,
                // columnHidingEnabled: true,
                remoteOperations: {sorting: true,paging: true,filtering:true},
                "export" : {enable:true, fileName : "{{ $table }}"},
                editing : {mode:'form', allowAdding : false,allowUpdating : false,allowDeleting : false},
                selection:{mode:'multiple',showCheckBoxesMode:'always',selectAllMode:'page'},
                onSelectionChanged:function(data){
                    postButton.option('disabled', !data.selectedRowsData.length)
                },
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : false},
                headerFilter:{visible:true},
                filterRow:{visible:true,applyFilter:'auto'},
                groupPanel : {visible:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: true,visible: true},
                columns : [
            {dataField:'NoDokumen',caption:'No Dokumen',allowEditing:false/*,allowEditing:false*/},
            {dataField:'Tahun',caption:'Tahun',allowEditing:false/*,allowEditing:false*/},
            {dataField:'TglClearing',caption:'Tgl Clearing',validationRules:[{type:"required"}]},
            {dataField:'NoClearing',caption:'No Clearing',validationRules:[{type:"required"}]},
            {dataField:'IDVendor',caption:'ID Rekanan',validationRules:[{type:"required"}]},
            {dataField:'Amount',caption:'Nominal',validationRules:[{type:"required"}]},
            {dataField:'RekeningTujuan',validationRules:[{type:"required"}]},
            {dataField:'NomorRekening',validationRules:[{type:"required"}]},
            ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
        }).dxDataGrid("instance");
    });

</script>
<style type="text/css">
    .icon-background1 {
    color: red;
    font-size: 24px;
}
.icon-background2 {
    color: green;
    font-size: 24px;
}
</style>
@endsection
