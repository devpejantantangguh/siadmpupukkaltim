@extends('templates.admin')

@section('heading')
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/18/css/dx.spa.css" />
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/18/css/dx.common.css" />
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/18/css/dx.light.css" />
<script src="{{url('/asset/')}}devextreme/18/js/dx.all.js"></script>
<script src="{{ url('asset/js/phpjs.js') }}"></script>
@endsection

@section('title','Pembayaran Invoice')

@section('contentheader')
<section class="content-header">
    <h1>Pembayaran</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Keuangan</li>
        <li>Invoice Tracking</li>
        <li class="active">Pembayaran Invoice</li>
    </ol>
</section>
@endsection

@section('content')
<div class="box-header with-border">
    <h3 class="box-title">Pembayaran Invoice</h3>
    <span class="pull-right">Area: {{Auth::unitKerja()->Area}}</span>
</div>
<div <div class="box-body">
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {{ flashdata('error') }}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> {{ flashdata('warning') }}
            </div>

        @elseif(flashdata('info'))

            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Info!</strong> {{ flashdata('info') }}
            </div>
        @endif
    <div id="btn-open"></div>
    <div id="btn-process"></div>
    <div id="btn-receive"></div>
    <div id="btn-paygen"></div>
    <div id="gridContainer"></div>

</div>
@endsection

@section('script')
<script type="text/javascript">
    $(function(){

        var Status = [{"id":5,"name":"Menunggu Pembayaran"},{"id":6,"name":"Selesai"}];
        var dataSource = new DevExpress.data.CustomStore({
            key: "ID",
            load: function (loadOptions) {
                var deferred = $.Deferred(),
                args = {};

                if (loadOptions.sort) {
                    args.orderby = loadOptions.sort[0].selector;
                    if (loadOptions.sort[0].desc)
                        args.orderby += " desc";
                }
                if (loadOptions.filter != 'undefined') {
                    args.filter = JSON.stringify(loadOptions.filter);
                }

                args.skip = loadOptions.skip || 0;
                args.take = loadOptions.take || 12;

                $.ajax({
                    url: "{{ url("keuangan/invoice/pembayaran/get") }}",
                    data: args,
                    success: function(response) {
                        console.log(response);
                        deferred.resolve(response.data, { totalCount: response.totalCount });
                    },
                    error: function() {
                        deferred.reject("Data Loading Error");
                    },
                    timeout: 5000
                });

                return deferred.promise();
            }
        });

        var openButton = $("#btn-open").dxButton({
                text: "Open",
                height: 34,
                width: 100,
                disabled: true,
                onClick: function () {
                    $.each(dataGrid.getSelectedRowsData(), function() {
                        location.href = "{{ url('keuangan/invoice/pembayaran/open/') }}" + this.ID;
                    });
                    dataGrid.refresh();
                }
            }).dxButton("instance");

        var receiveButton = $("#btn-receive").dxButton({
                text: "Receive",
                height: 34,
                width: 140,
                disabled: true,
                onClick: function () {
                    var result = DevExpress.ui.dialog.confirm("Apakah anda yakin?", "Receive");
                    result.done(function (dialogResult) {
                        console.log(dialogResult);
                        if(dialogResult) {

                            var lists = [];
                            $.each(dataGrid.getSelectedRowsData(), function() {
                                lists.push(this.ID);
                            });
                            $.post("{{ url('keuangan/invoice/pembayaran/receive') }}", {ids:lists})
                                .done(function(data){
                                    dataGrid.refresh();
                                    var obj = JSON.parse(data);
                                    console.log(obj);
                                    if(obj.status == 200){
                                        var result = DevExpress.ui.dialog.alert(obj.msg, "Success");

                                        dataGrid.refresh();
                                    }else{
                                        DevExpress.ui.notify({
                                        message: obj.msg,
                                        position: {
                                            my: "center top",
                                            at: "center top"
                                        }
                                        }, "error", 3000);
                                    }
                                });
                          }
                    });
                }
            }).dxButton("instance");
        var processButton = $("#btn-process").dxButton({
                text: "Process",
                height: 34,
                width: 140,
                disabled: true,
                onClick: function () {
                    var result = DevExpress.ui.dialog.confirm("Apakah anda yakin?", "Confirm");
                    result.done(function (dialogResult) {
                        console.log(dialogResult);
                        if(dialogResult) {

                            var lists = [];
                            $.each(dataGrid.getSelectedRowsData(), function() {
                                lists.push(this.ID);
                            });
                            $.post("{{ url('keuangan/invoice/pembayaran/process') }}", {ids:lists})
                                .done(function(data){
                                    dataGrid.refresh();
                                    var obj = JSON.parse(data);
                                    console.log(obj);
                                    if(obj.status == 200){
                                        var result = DevExpress.ui.dialog.alert(obj.msg, "Success");

                                        dataGrid.refresh();
                                    }else{
                                        DevExpress.ui.notify({
                                        message: obj.msg,
                                        position: {
                                            my: "center top",
                                            at: "center top"
                                        }
                                        }, "error", 3000);
                                    }
                                });
                          }
                    });
                }
            }).dxButton("instance");
        var paygenButton = $("#btn-paygen").dxButton({
                text: "Upload Paygen",
                height: 34,
                width: 140,
                onClick: function () {
                        location.href = "{{ url('keuangan/invoice/pembayaran/paygen') }}";
                }
            }).dxButton("instance");

        var list_status = {!! json_encode($listStatus) !!}

        var dataGrid = $("#gridContainer").dxDataGrid({
            filterRow: { visible: true },
            filterPanel: { visible: true },
            headerFilter: { visible: true },
            dataSource : dataSource,
                // columnHidingEnabled: false,
                paging: {
                    pageSize: 10
                },
                remoteOperations: {sorting: true,paging: true,filtering:true},
                "export" : {enable:true, fileName : "{{ $table }}"},
                editing : {mode:'popup', allowUpdating : false,allowDeleting : false},
                selection:{mode:'multiple'},
                onSelectionChanged: function(data){
                    openButton.option('disabled', !(data.selectedRowsData.length == 1))
                    processButton.option('disabled', !(data.selectedRowsData.length))
                    receiveButton.option('disabled', !(data.selectedRowsData.length))
                },
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : false},
                headerFilter:{visible:true},
                filterRow:{visible:true,applyFilter:'auto'},
                groupPanel : {visible:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: true,visible: true},
                columns:
                [
            {dataField:'No',caption:'No',allowEditing:false,allowFiltering:false},
            {dataField:'NoDokumenSAP',caption:'No Dokumen'},
            {dataField:'NoInvoice',caption:'No Invoice',validationRules:[{type:"required"}]},
            {dataField:'IDVendor',caption:'Kode Vendor',validationRules:[{type:"required"}]},
            {dataField:'NamaVendor',caption:'Nama Vendor',validationRules:[{type:"required"}]},

            {dataField:'NilaiInvoice',caption:'Nilai Invoice',
                dataType: "number",calculateCellValue:function(data){
                return data.KursInvoice + ' ' + number_format(data.NilaiInvoice,2,',','.')
            }},
            {dataField:'NilaiVerifikasi',
                dataType: "number",caption:'Nilai Verifikasi',calculateCellValue:function(data){
                return data.KursInvoice + ' ' + number_format(data.NilaiVerifikasi,2,',','.')
            }},
            {dataField:'Status',caption:'Status',dataType:'integer',lookup:{dataSource: list_status, displayExpr: "name", valueExpr: "id"}},
                    {dataField:'IDArea',caption:'ID Area'},
            {dataField:'Tahun',caption:'Tahun'},
            {dataField:'Keterangan',caption:'Keterangan',width:150},
            {dataField:'NoTransmittal',caption:'No. Transmittal', groupIndex: 0},
            {dataField:'TglTransmittal',caption:'Tgl. Transmittal'},
            {dataField:'ReceivedBy',caption:'Received By'},
            {dataField:'ReceivedOn',caption:'Received On',dataType: 'date',format: 'dd/MM/yyyy hh:mm:ss'}
            ],
            columnAutoWidth: true,
            columnChooser: {enabled: true},
            columnFixing: {enabled: true},

            onRowPrepared: function(e) {
                 console.log(e.rowType);
                if (e.rowType === 'data') {
                    console.log(e.data);
                    if(e.data.IsPriority == 1){
                        e.rowElement.css({ "background-color": "#fcd691" });
                    }
                }

            },
            onEditorPreparing: function(e) {
                if (e.dataField == "Keterangan") {
                    e.editorName = "dxTextArea"; // Changes the editor's type
                    e.editorOptions.onValueChanged = function (args) {
                        // Implement your logic here

                        e.setValue(args.value); // Updates the cell value
                    }
                }
            }
        }).dxDataGrid("instance");
    });
    $(function() {


        $("#login").dxTextBox({
            name: "Login"
        }).dxValidator({
            validationRules: [
            { type: "required" }
            ]
        });

        $("#password").dxTextBox({
            name: "Password",
            mode: "password"
        }).dxValidator({
            validationRules: [
            { type: "required" }
            ]
        });

        $("#validateAndSubmit").dxButton({
            text: "Submit",
            type: "success",
            useSubmitBehavior: true
        });
    });
</script>
<style type="text/css">
    .icon-background1 {
    color: red;
    font-size: 24px;
}
.icon-background2 {
    color: green;
    font-size: 24px;
}
</style>
@endsection
