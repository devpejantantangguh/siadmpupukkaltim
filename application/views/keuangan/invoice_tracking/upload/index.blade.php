@extends('templates.admin')

@section('heading')
     <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
    <script src="{{ url('asset/js/phpjs.js') }}"></script>
@endsection

@section('title','Upload List Piutang')

@section('contentheader')
    <section class="content-header">
        <h1>Upload List Piutang</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Keuangan</li>
            <li>Invoice Tracking</li>
            <li class="active">Upload List Piutang</li>
        </ol>
    </section>
@endsection

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">Upload List Piutang</h3>
    </div>
    <div <div class="box-body">
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {{ flashdata('error') }}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> {{ flashdata('warning') }}
            </div>

        @elseif(flashdata('info'))

            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Info!</strong> {{ flashdata('info') }}
            </div>
        @endif
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {{ flashdata('error') }}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> {{ flashdata('warning') }}
            </div>

        @elseif(flashdata('info'))

            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Info!</strong> {{ flashdata('info') }}
            </div>
        @endif
        <div class="panel panel-default">
            <div class="panel-body">
                <form action="{{ url('keuangan/invoice/upload/store') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2">Upload List Piutang</label>
                        <div class="col-sm-10">
                            <input name="fileexcel" type="file">
                            <p class="help-block">Upload file excel List Piutang</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <button class="btn btn-primary">Upload</button>
                            <a class="btn btn-warning" href="{{ url('storage/template/template-list-piutang.xlsx') }}" role="button" style="color:#fff">Download Template</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div id="dxPost"></div>
        <div id="dxPostAll"></div>
        <div id="gridContainer"></div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){
            var postButton = $("#dxPost").dxButton({
                text: "Post",
                height: 34,
                width: 100,
                disabled: true,
                onClick: function () {
                    var lists = [];
                    $.each(dataGrid.getSelectedRowsData(), function() {
                        lists.push(this.ARDocNo);
                    });
                    console.log(lists);
                    $.post("{{ url('keuangan/invoice/upload/post') }}", {ids:lists})
                        .done(function(data){
                            dataGrid.refresh();
                            var obj = JSON.parse(data);
                            console.log(obj);
                            if(obj.status == 200){
                                var result = DevExpress.ui.dialog.alert(obj.msg, "Success");

                                dataGrid.refresh();
                            }else{
                                DevExpress.ui.notify({
                                message: obj.msg,
                                position: {
                                    my: "center top",
                                    at: "center top"
                                }
                                }, "error", 3000);
                            }
                        });
                }
            }).dxButton("instance");


            var postAllButton = $("#dxPostAll").dxButton({
                text: "Post All",
                height: 34,
                width: 100,
                disabled: false,
                onClick: function () {
                    $.post("{{ url('keuangan/invoice/upload/postall') }}")
                        .done(function(data){
                            dataGrid.refresh();
                            var obj = JSON.parse(data);
                            console.log(obj);
                            if(obj.status == 200){
                                var result = DevExpress.ui.dialog.alert(obj.msg, "Success");

                                dataGrid.refresh();
                            }else{
                                DevExpress.ui.notify({
                                message: obj.msg,
                                position: {
                                    my: "center top",
                                    at: "center top"
                                }
                                }, "error", 3000);
                            }
                        });
                }
            }).dxButton("instance");

            var dataSource = new DevExpress.data.CustomStore({
                // key: "IDBillDocHeader",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};

                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }

                    if (loadOptions.filter != 'undefined') {
                        args.filter = JSON.stringify(loadOptions.filter);
                    }

                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 12;

                    $.ajax({
                        url: "{{ url("keuangan/invoice/upload/get_temp") }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });

                    return deferred.promise();
                }
            });

            var dataGrid = $("#gridContainer").dxDataGrid({
                dataSource : dataSource,
                // columnHidingEnabled: true,
                remoteOperations: {sorting: true,paging: true,filtering:true},
                "export" : {enable:true, fileName : "{{ $table }}"},
                editing : {mode:'form', allowAdding : false,allowUpdating : false,allowDeleting : false},
                selection:{mode:'multiple',showCheckBoxesMode:'always',selectAllMode:'page'},
                onSelectionChanged:function(data){
                    postButton.option('disabled', !data.selectedRowsData.length)
                },
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : false},
                headerFilter:{visible:true},
                filterRow:{visible:true,applyFilter:'auto'},
                groupPanel : {visible:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [10, 50, 100, 200],showInfo: true,showNavigationButtons: true,showPageSizeSelector: true,visible: true},
				paging: {pageSize:50},
                columns : [
                    {dataField:'IDCustomer', caption: 'ID Customer'},
                    {dataField:'PairedVendorName', caption: 'Name'},
                    {dataField:'ARDocNo', caption: 'No. AR Doc'},
                    {dataField:'NoBillingDoc', caption: 'Bill Doc'},
                    {dataField:'NoSO', caption: 'No. SO'},
                    {dataField:'DocumentDate', caption: 'Document Date'},
                    {dataField:'PaymentMethod', caption: 'Payment Method'},
                    {dataField:'Deskripsi', caption: 'Deskripsi'},
                    {dataField:'AmountDC', caption: 'Amount DC'},
                    {dataField:'KursDC', caption: 'Kurs DC'},
                    {dataField:'AmountLC', caption: 'Amount LC'},
                    {dataField:'KursLC', caption: 'Kurs LC'},
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
            }).dxDataGrid("instance");
        });


    </script>
@endsection
