@extends('templates.admin')

@section('heading')
     <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
    <script src="{{ url('asset/js/phpjs.js') }}"></script>
@endsection

@section('title','List Piutang')

@section('contentheader')
    <section class="content-header">
        <h1>List Piutang</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Keuangan</li>
            <li>Invoice Tracking</li>
            <li class="active">List Piutang</li>
        </ol>
    </section>
@endsection

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">List Piutang</h3>
    </div>
    <div <div class="box-body">
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {{ flashdata('error') }}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> {{ flashdata('warning') }}
            </div>

        @elseif(flashdata('info'))

            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Info!</strong> {{ flashdata('info') }}
            </div>
        @endif
        <div id="gridContainer"></div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){
            var dataSource = new DevExpress.data.CustomStore({
                // key: "IDBillDocHeader",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};

                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }

                    if (loadOptions.filter != 'undefined') {
                        args.filter = JSON.stringify(loadOptions.filter);
                    }

                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 12;

                    $.ajax({
                        url: "{{ url("keuangan/invoice/upload/get") }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });

                    return deferred.promise();
                }
            });

            var dataGrid = $("#gridContainer").dxDataGrid({
                dataSource : dataSource,
                // columnHidingEnabled: true,
                remoteOperations: {sorting: true,paging: true,filtering:true},
                "export" : {enable:true, fileName : "{{ $table }}"},
                editing : {mode:'form', allowAdding : false,allowUpdating : false,allowDeleting : false},
                // selection:{mode:'multiple'},
                onSelectionChanged:function(data){
                    deleteButton.option('disabled', !data.selectedRowsData.length)
                    postButton.option('disabled', !data.selectedRowsData.length)
                    editButton.option('disabled', !(data.selectedRowsData.length == 1))
                },
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : false},
                filterRow:{visible:true,applyFilter:'auto'},
                groupPanel : {visible:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [10, 50, 100, 200],showInfo: true,showNavigationButtons: true,showPageSizeSelector: true,visible: true},
				paging: {pageSize:50},
                columns : [
                    {dataField:'IDCustomer', caption: 'ID Customer'},
                    {dataField:'PairedVendorName', caption: 'Name'},
                    {dataField:'IDVendor', caption: 'ID Vendor'},
                    {dataField:'ARDocNo', caption: 'AR Doc'},
                    {dataField:'Deskripsi', caption: 'Deskripsi'},
                    {caption: 'AR Amount', calculateCellValue:function(data){
                        if(data.Kurs == 'IDR') {
                            return 'Rp. ' + number_format(data.ARAmount);
                        }else if(data.Kurs == 'USD') {
                            return '$' + number_format(data.ARAmount)
                        }else{
                            return number_format(data.ARAmount);
                        }
                    }},

                    {dataField:'AmountDC', caption: 'AmountDC'},
                    {dataField:'AmountLC', caption: 'AmountLC'},
                    {dataField:'DocumentDate', caption: 'Document Date'},
                    {dataField:'Kurs', caption: 'Kurs'},
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
            }).dxDataGrid("instance");
        });


    </script>
@endsection
