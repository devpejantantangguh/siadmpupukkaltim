@extends('templates.admin')

@section('contentheader')
    <section class="content-header">
        <h1>Edit Cancel Billing Document</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Keuangan</li>
            <li>Collecting</li>
            <li class="active">Edit Cancel Billing Document</li>
        </ol>
    </section>
@endsection

@section('content')
    <form  action="{{ url('keuangan/collecting/cancel/update/'.$id) }}" method="POST">
        <div class="box-header with-border">
            <h3 class="box-title">Edit Cancel Billing Document</h3>
        </div>
        <div class="box-body">
            <div class="form-group">
                <label>No Billing Document</label>
                <input type="text" name="NoBillingDoc" placeholder="No Billing Document" value="{{ $nobillingdoc }}" class="form-control">
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-success btn-sm">Save</button>
        </div>
    </form>
@endsection
