@extends('templates.admin')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{ url('/asset/devextreme/17/js/dx.all.js') }}"></script>
    <script src="{{ url('asset/js/phpjs.js') }}"></script>
    <style type="text/css">
        .kotak{width:50px;height:10px;display:block;float:left;border-radius:3px;margin-top:4px;margin-right:5px;}
        .g{background:#01bf0178;}
        .o{background:#ff98009e;}
        .b{height:100%;width:100%;display:block;border-radius:3px;text-decoration:none!important;text-align: center;}
        .u{background:#ff98009e}
        .a{background:#01bf0178}
    </style>
@endsection

@section('title','Approval Billing Document')

@section('contentheader')
    <section class="content-header">
        <h1>Approval Billing Document</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Keuangan</li>
            <li>Collecting</li>
            <li class="active">Approval Billing Document</li>
        </ol>
    </section>
@endsection

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">Approval Billing Document</h3>
    </div>
    <div class="box-body">
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {{ flashdata('error') }}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> {{ flashdata('warning') }}
            </div>

        @elseif(flashdata('info'))
            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                {!! flashdata('info') !!}
            </div>
        @endif

		@if(flashdata('results'))
			@foreach(flashdata('results') as $result)
				@if(strpos($result, 'Warning') !== false)
					<div class="alert alert-warning">
						{{ $result }}
					</div>
				@elseif(strpos($result, 'Error') !== false)
					<div class="alert alert-error">
						{{ $result }}
					</div>
				@else
					<div class="alert alert-success">
						{{ $result }}
					</div>
				@endif
			@endforeach
		@endif

        @if($message)
            <div class="alert alert-danger">
                <i class="fa fa-exclamation-triangle"></i> {{ $message }}
            </div>
        @endif

        {{-- <div id="btn-edit"></div> --}}
        <div id="btn-approve"></div>
        <div id="btn-approve-all"></div>
        <div id="btn-upload"></div>
        <div id="gridContainer"></div>
    </div>

    <div class="modal fade" tabindex="-1" role="dialog" id="bs-modal">
    	<div class="modal-dialog" role="document">
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
    				<h4 class="modal-title" id="bs-title">Materials</h4>
    			</div>
    			<div class="modal-body" id="md-content">
    				<p>Loading...</p>
    			</div>
    		</div><!-- /.modal-content -->
    	</div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){
            $('#bs-modal').on('show.bs.modal', function(e){
                var id = $(e.relatedTarget).data('billdoc');

                {{--var url = "{{ url('/keuangan/collecting/approval/list_material')  }}?id="+id;--}}
                var url = "{{ url('/keuangan/collecting/lists/materials') }}?id="+id;

                $('#md-content').html('Loading...');
                $.get(url, function(response){
                    $('#md-content').html(response);
                });
                console.log('okey');
            });

            var dataSource = new DevExpress.data.CustomStore({
                key: "NoBillingDoc",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};

                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }

                    if (loadOptions.filter != 'undefined') {
                        args.filter = JSON.stringify(loadOptions.filter);
                    }

                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 20;

                    $.ajax({
                        url: "{{ url('keuangan/collecting/approval/get/') }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        // timeout: 30 * 60
                    });

                    return deferred.promise();
                }
            });

            var approvalButton = $("#btn-approve").dxButton({
                text: "Approve",
                height: 34,
                width: 100,
                disabled: true,
                onClick: function () {
                    var result = DevExpress.ui.dialog.confirm("Are you sure to Approve Billing Document ?", "Approval");
                    result.done(function (dialogResult) {
                        if (dialogResult) {
                            var lists = [];
                            $.each(dataGrid.getSelectedRowsData(), function() {
                                lists.push(this.NoBillingDoc);
                            });
                            $.post("{{ url('keuangan/collecting/approval/approve') }}", {ids:lists})
                                .done(function(data){
                                    DevExpress.ui.notify({
                                        message: 'Mohon tunggu...',
                                        position: {
                                            my: "center top",
                                            at: "center top"
                                        }
                                    }, "warning", 1000 * 60);
                                    window.open("{{ url('keuangan/collecting/approval/') }}","_self");
                                });
                        }
                    });
                }
            }).dxButton("instance");

            var uploadButton = $("#btn-upload").dxButton({
                text: "Update Doc",
                height: 34,
                width: 120,
                disabled: true,
                onClick: function () {
                    $.each(dataGrid.getSelectedRowsData(), function() {
                        location.href = "{{ url('keuangan/collecting/approval') }}/upload/" + this.NoBillingDoc;
                    });
                }
            }).dxButton("instance");

			var approvalButtonAll = $("#btn-approve-all").dxButton({
                text: "Approve All",
                height: 34,
                width: 120,
                disabled: false,
                onClick: function () {
                    var result = DevExpress.ui.dialog.confirm("Apakah anda akan meng-approve semua dokumen?", "Approval");
                    result.done(function (dialogResult) {
                        if (dialogResult) {
							DevExpress.ui.notify({
								message: 'Mohon tunggu...',
								position: {
									my: "center top",
									at: "center top"
								}
							}, "warning", 1000
								* 60);
                            $.post("{{ url('keuangan/collecting/approval/approve_all') }}")
                                .always(function(){
                                    window.open("{{ url('keuangan/collecting/approval/') }}","_self");
                                });
                        }
                    });
                }
            }).dxButton("instance");

            // var editButton = $("#btn-edit").dxButton({
            //     text: "Edit",
            //     height: 34,
            //     width: 100,
            //     disabled: true,
            //     onClick: function () {
            //         $.each(dataGrid.getSelectedRowsData(), function() {
            //             location.href = "{{ url('keuangan/collecting/approval/edit/') }}" + this.ID;
            //         });
            //         dataGrid.refresh();
            //     }
            // }).dxButton("instance");


            var dataGrid = $("#gridContainer").dxDataGrid({
                dataSource : dataSource,
                // columnHidingEnabled: true,
                remoteOperations: {sorting: true,paging: true,filtering:true},
                "export" : {enabled:true, fileName:'{{ $table }}',allowExportSelectedData:true},
                editing : {mode:'form', allowAdding : false,allowUpdating : false,allowDeleting : false},
                selection:{mode:'multiple',showCheckBoxesMode:'always',selectAllMode:'page'},
                onSelectionChanged: function(data){
                    approvalButton.option('disabled', !data.selectedRowsData.length);

                    uploadButton.option('disabled', true);
                    if((data.selectedRowsData.length == 1)) {
                        uploadButton.option('disabled', false);
					}
                },
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : false},
                groupPanel : {visible:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
				headerFilter:{visible:true},
                filterRow:{visible:true,applyFilter:'auto'},
                pager: {allowedPageSizes: [20, 50, 100, 200],showInfo: true,showNavigationButtons: true,showPageSizeSelector: true,visible: true},
				paging: {pageSize:20},
				onRowPrepared : function (e) {
					if (e.rowType === 'data') {
						if(e.data.PaymentMethod == 'M' || e.data.PaymentMethod == 'P'){
						    if(e.data.UploadedFile == true) {
						    	e.rowElement.css({ "background-color": "#adff2f" });
							} else {
						        e.rowElement.css({ "background-color": "#fcd691" });
							}
						}
					}
				},
                columns : [
                    {dataField:'NoBillingDoc',caption:'Bill Doc'},
                    {dataField:'BillingDate',caption:'Billing Date',dataType:'date',format:'dd-MM-yyyy'},
                    {dataField:'TglFakturPajak',caption:'Tgl. Faktur',dataType:'date', format:'dd-MM-yyyy'},
                    {dataField:'NoFakturPajak',caption:'No Faktur Pajak'},
                    {dataField:'NoSO',caption:'Sales Doc'},
                    {dataField:'IDCustomer',caption:'Kode Customer'},
                    {dataField:'Customer',caption:'Customer'},
                    // {dataField:'IDMaterial',caption:'Kode Material'},
                    // {dataField:'Material',caption:'Material'},
                    {
                        dataField:'ItemCount',caption:'Jumlah Material',allowSorting:false, allowFiltering:false,
                        cellTemplate:function(container, options) {
                            $('<a/>')
                                .addClass('text-center')
                                .attr('data-toggle', 'modal')
                                .attr('data-target', '#bs-modal')
                                .attr('data-billdoc', options.data.NoBillingDoc)
                                .css('cursor','pointer')
                                .text(options.data.ItemCount + ' Material')
                                .appendTo(container)
                        }
                    },
                    {dataField:'Approval1',caption:'Approved 1 By',allowSorting:false, allowFiltering:false},
                    {dataField:'Approval2',caption:'Approved 2 By',allowSorting:false, allowFiltering:false},
                    {dataField:'Approval3',caption:'Approved 3 By',allowSorting:false, allowFiltering:false},
					{
                    	caption: 'Dokumen',
                    	width: 100,
						allowSorting:false, allowFiltering:false,
                    	cellTemplate: function (container, options) {
                    	    if(options.data.UploadedFile == true) {
                    	    	$('<a/>').addClass('View')
                    				.text('Download')
									.attr('target','_blank')
									.attr('href', "{{ url('keuangan/collecting/approval/document') }}/"+options.data.NoBillingDoc)
                    				.appendTo(container);
							} else  {
                    	        $('<a/>').addClass('View')
                    				.text('Upload')
									.attr('href', "{{ url('keuangan/collecting/approval/upload') }}/"+options.data.NoBillingDoc)
                    				.appendTo(container);
							}

                    	}
                    },
                    {
                    	caption: 'SSP',
                    	width: 100,
						allowSorting:false, allowFiltering:false,
                    	cellTemplate: function (container, options) {
                    		if (options.data.SPP_Path) {
                    			if (options.data.SPP) {
                    				htmlClass = 'dx-link b a';
                                    statusText = 'Approved';
                    			} else {
                    				htmlClass = 'dx-link b u';
                                    statusText = 'Pending';
                    			}
                    			$('<a/>')
                    				.addClass(htmlClass)
                    				.text(statusText)
                    				.appendTo(container);
                    		}
                    	}
                    },
					{
                    	caption: 'Bukti Potong',
                    	width: 100,
						allowSorting:false, allowFiltering:false,
                    	cellTemplate: function (container, options) {
                    		if (options.data.Bukpot_Path) {
                    			if (options.data.Bukpot) {
                    				htmlClass = 'dx-link b a';
                                    statusText = 'Approved';
                    			} else {
                    				htmlClass = 'dx-link b u';
                                    statusText = 'Pending';
                    			}
                    			$('<a/>').addClass(htmlClass)
                    				.text(statusText)
                    				.appendTo(container);
                    		}
                    	}
                    },
                    {
                    	dataField: 'Status',
                    	caption: 'Status',
						allowSorting:false, allowFiltering:false,
                    	fixed: true,
                    	fixedPosition: "right"
                    }, {
                    	dataField: 'Total Nilai',
                    	caption: 'Total Nilai',
						allowSorting:false, allowFiltering:false,
						dataType:"number",
                    	calculateCellValue: function (data) {
                    		total = data.TotalNilai;
                    		if (data.Kurs == 'IDR') {
                    			return 'Rp. ' + number_format(total, 0, '.', ',');
                    		} else if (data.Kurs == 'USD') {
                    			return '$' + number_format(total, 0, ',', '.');
                    		} else {
                    			return number_format(total);
                    		}
                    	},
                    	fixed: true,
                    	fixedPosition: "right"
                    }
                ],
                columnFixing: {
                    enabled: true
                },
                columnAutoWidth: true,
                columnChooser: {enabled: true},
            }).dxDataGrid("instance");
        });

    </script>
@endsection
