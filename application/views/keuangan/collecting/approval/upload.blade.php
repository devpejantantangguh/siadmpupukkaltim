@extends('templates.admin')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
@endsection

@section('title', 'Upload Dokumen')

@section('contentheader')
    <section class="content-header">
        <h1>Edit Billing Document</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Keuangan</li>
            <li>Collecting</li>
            <li class="active">Upload Dokumen</li>
        </ol>
    </section>
@endsection

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">Upload Dokumen untuk Billing {{ $billing_id }}</h3>
    </div>
    <div class="box-body">
		<form action="{{ url('keuangan/collecting/approval/upload_file/'.$billing_id) }}" method="POST" enctype="multipart/form-data">
			<div class="form-group">
				<label for="upload">Upload File</label>
				<input type="file" name="document" id="document">
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary btn-sm">Upload File</button>
			</div>
		</form>
    </div>
@endsection

@section('script')
@endsection
