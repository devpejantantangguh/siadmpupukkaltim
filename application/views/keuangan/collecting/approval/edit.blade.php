@extends('templates.admin')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
@endsection 

@section('contentheader')
    <section class="content-header">
        <h1>Edit Billing Document</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Keuangan</li>
            <li>Collecting</li>
            <li class="active">Edit Billing Document</li>
        </ol>
    </section>
@endsection 

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">Edit Billing Document</h3>
    </div>
    <div class="box-body">
        <div role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#edit" aria-controls="edit" role="tab" data-toggle="tab">Edit</a>
                </li>
                <li role="presentation">
                    <a href="#history" aria-controls="history" role="tab" data-toggle="tab">History</a>
                </li>
            </ul>
        
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="edit">
                    <form id="form" method="POST" action="">
                        <div class="dx-fieldset">
                            <div class="dx-field">
                                <div class="dx-field-label">No. Billing Document</div>
                                <div class="dx-field-value">
                                    <div id="nobilldoc-validation"></div>
                                </div>
                            </div>

                            <div class="dx-field">
                                <div class="dx-field-label">Jenis Transaksi</div>
                                <div class="dx-field-value">
                                    <div id="jt-validation"></div>
                                </div>
                            </div>

                            <div class="dx-field">
                                <div class="dx-field-label">Sektor</div>
                                <div class="dx-field-value">
                                    <div id="sektor-validation"></div>
                                </div>
                            </div>

                            <div class="dx-field">
                                <div class="dx-field-label">Billing Date</div>
                                <div class="dx-field-value">
                                    <div id="billdate-validation"></div>
                                </div>
                            </div>
                            
                            <div class="dx-field">
                                <div class="dx-field-label">No. SO</div>
                                <div class="dx-field-value">
                                    <div id="noso-validation"></div>
                                </div>
                            </div>

                            <div class="dx-field">
                                <div class="dx-field-label">No. GI</div>
                                <div class="dx-field-value">
                                    <div id="nogi-validation"></div>
                                </div>
                            </div>

                            <div class="dx-field">
                                <div class="dx-field-label">No. Faktur Pajak</div>
                                <div class="dx-field-value">
                                    <div id="nofaktur-validation"></div>
                                </div>
                            </div>

                            <div class="dx-field">
                                <div class="dx-field-label">Customer</div>
                                <div class="dx-field-value">
                                    <div id="customer-validation"></div>
                                </div>
                            </div>

                            <div class="dx-field">
                                <div class="dx-field-label">Status</div>
                                <div class="dx-field-value">
                                    <div id="status-validation"></div>
                                </div>
                            </div>

                            <div class="dx-field">
                                <div class="dx-field-label">No. Material</div>
                                <div class="dx-field-value">
                                    <div id="idmaterial-validation"></div>
                                </div>
                            </div>

                            <div class="dx-field">
                                <div class="dx-field-label">Deskripsi Material</div>
                                <div class="dx-field-value">
                                    <div id="materialdesc-validation"></div>
                                </div>
                            </div>

                            <div class="dx-field">
                                <div class="dx-field-label">Kuantum</div>
                                <div class="dx-field-value">
                                    <div id="kuantum-validation"></div>
                                </div>
                            </div>

                            <div class="dx-field">
                                <div class="dx-field-label">Unit of Meansure</div>
                                <div class="dx-field-value">
                                    <div id="uom-validation"></div>
                                </div>
                            </div>

                            <div class="dx-field">
                                <div class="dx-field-label">Kurs</div>
                                <div class="dx-field-value">
                                    <div id="kurs-validation"></div>
                                </div>
                            </div>

                            <div class="dx-field">
                                <div class="dx-field-label">Harga Satuan</div>
                                <div class="dx-field-value">
                                    <div id="hargasatuan-validation"></div>
                                </div>
                            </div>

                            <div class="dx-field">
                                <div class="dx-field-label">Jumlah</div>
                                <div class="dx-field-value">
                                    <div id="jumlah-validation"></div>
                                </div>
                            </div>

                            <div class="dx-field">
                                <div class="dx-field-label">DPP</div>
                                <div class="dx-field-value">
                                    <div id="dpp-validation"></div>
                                </div>
                            </div>

                            <div class="dx-field">
                                <div class="dx-field-label">Tax Amount</div>
                                <div class="dx-field-value">
                                    <div id="tax-validation"></div>
                                </div>
                            </div>

                            <div class="dx-field">
                                <div class="dx-field-label">Total</div>
                                <div class="dx-field-value">
                                    <div id="total-validation"></div>
                                </div>
                            </div>

                            <div class="dx-field">
                                <div class="dx-field-label">Keterangan</div>
                                <div class="dx-field-value">
                                    <div id="keterangan-validation"></div>
                                </div>
                            </div>

                            <div class="dx-field">
                                <div class="dx-field-label">Payer</div>
                                <div class="dx-field-value">
                                    <div id="payer-validation"></div>
                                </div>
                            </div>

                            <div class="dx-field">
                                <div class="dx-field-label">Sales Org</div>
                                <div class="dx-field-value">
                                    <div id="salesorg-validation"></div>
                                </div>
                            </div>

                            <div id="button"></div>

                        </div>
                    </form>
                </div>
                <div role="tabpanel" class="tab-pane" id="history">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No. </th>
                                <th>Action</th>
                                <th>User</th>
                                <th>Time</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($histories as $key => $history)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $history->Action }}</td>
                                <td>{{ $history->UserID }}</td>
                                <td>{{ $history->Timestamp }}</td>
                                <td>{{ $history->Keterangan }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function() {
            var listJenisTransaksi = {!! json_encode($listJenisTransaksi) !!};
            var listCustomer = {!! json_encode($customers) !!};
            var listMaterial = {!! json_encode($materials) !!};
            var listStatus = {!! json_encode($listStatus) !!};
            var listSalesOrder = {!! json_encode($listSalesOrder) !!};

            $('#nobilldoc-validation').dxTextBox({
                name:'NoBillingDoc', value:"{{ $data->NoBillingDoc }}"
            }).dxValidator({validationRules:[{type:'required',message:'No. Billing Document is required'}]});

            $("#jt-validation").dxSelectBox({
                name: "JenisTransaksi", value:"{{ $data->JenisTransaksi }}", dataSource: listJenisTransaksi, displayExpr: "name", valueExpr: "id"
            }).dxValidator({validationRules: [{type: "required",message: "Jenis Transaksi is required"}]});

            $('#sektor-validation').dxTextBox({
                name:'Sektor',value:"{{ $data->Sektor }}"
            }).dxValidator({validationRules:[{type:'required',message:'Sektor is required'}]});

            $('#billdate-validation').dxDateBox({
                name:'BillingDate',value:"{{ $data->BillingDate }}"
            }).dxValidator({validationRules:[{type:'required',message:'Billing Date is required'}]});

            $('#noso-validation').dxSelectBox({
                name:'NoSO',value:"{{ $data->NoSO }}", dataSource : listSalesOrder, displayExpr: "name", valueExpr: "id"
            }).dxValidator({validationRules:[{type:'required',message:'No. SO is required'}]});

            $('#nogi-validation').dxTextBox({
                name:'NoGI',value:"{{ $data->NoGI }}"
            }).dxValidator({validationRules:[{type:'required',message:'No. GI is required'}]});

            $('#nofaktur-validation').dxTextBox({
                name:'NoFakturPajak',value:"{{ $data->NoFakturPajak }}"
            }).dxValidator({validationRules:[{type:'required',message:'No. Faktur Pajak is required'}]});

            $('#customer-validation').dxSelectBox({
                name:'IDCustomer',value:"{{ $data->IDCustomer }}", dataSource: listCustomer, displayExpr: "name", valueExpr: "id", searchEnabled:true, showClearButton:true
            }).dxValidator({validationRules:[{type:'required',message:'Customer is required'}]});

            $('#status-validation').dxSelectBox({
                name:'Status',value:"{{ $data->Status }}", dataSource : listStatus, displayExpr: "name", valueExpr: "id"
            }).dxValidator({validationRules:[{type:'required',message:'Status is required'}]});

            $('#idmaterial-validation').dxSelectBox({
                name:'IDMaterial',value:"{{ $data->IDMaterial }}", dataSource: listMaterial, displayExpr: "name", valueExpr: "id"
            }).dxValidator({validationRules:[{type:'required',message:'No. Material is required'}]});

            $('#materialdesc-validation').dxTextBox({
                name:'Material',value:"{{ $data->Material }}"
            }).dxValidator({validationRules:[{type:'required',message:'Deskripsi Material is required'}]});

            $('#kuantum-validation').dxNumberBox({
                name:'Kuantum',value:"{{ $data->Kuantum }}"
            }).dxValidator({validationRules:[{type:'required',message:'Kuantum is required'}]});

            $('#uom-validation').dxTextBox({
                name:'UoM',value:"{{ $data->UoM }}"
            }).dxValidator({validationRules:[{type:'required',message:'Unit of Meansure is required'}]});

            $('#kurs-validation').dxTextBox({
                name:'Kurs',value:"{{ $data->Kurs }}"
            }).dxValidator({validationRules:[{type:'required',message:'Kurs is required'}]});

            $('#hargasatuan-validation').dxNumberBox({
                name:'HargaSatuan',value:"{{ $data->HargaSatuan }}"//, format:"$ #,##0.##"
            }).dxValidator({validationRules:[{type:'required',message:'Harga Satuan is required'}]});

            $('#jumlah-validation').dxNumberBox({
                name:'Jumlah',value:"{{ $data->Jumlah }}"
            }).dxValidator({validationRules:[{type:'required',message:'Jumlah is required'}]});

            $('#dpp-validation').dxNumberBox({
                name:'DPP',value:"{{ $data->DPP }}"
            }).dxValidator({validationRules:[{type:'required',message:'DPP is required'}]});

            $('#tax-validation').dxNumberBox({
                name:'TaxAmount',value:"{{ $data->TaxAmount }}"
            }).dxValidator({validationRules:[{type:'required',message:'Tax Amount is required'}]});

            $('#total-validation').dxTextBox({
                name:'Total',value:"{{ $data->Total }}"
            }).dxValidator({validationRules:[{type:'required',message:'Total is required'}]});

            $('#keterangan-validation').dxTextBox({
                name:'Keterangan',value:"{{ $data->Keterangan }}"
            }); // .dxValidator({validationRules:[{type:'required',message:'Keterangan is required'}]});

            $('#payer-validation').dxSelectBox({
                name:'IDPayer',value:"{{ $data->IDPayer }}", dataSource : listCustomer, displayExpr: "name", valueExpr: "id"
            }).dxValidator({validationRules:[{type:'required',message:'Payer is required'}]});

            $('#salesorg-validation').dxSelectBox({
                name:'IDSalesOrg',value:"{{ $data->IDSalesOrg }}",dataSource:listSalesOrder, displayExpr: "name", valueExpr: "id"
            }).dxValidator({validationRules:[{type:'required',message:'IDSalesOrg is required'}]});

            $("#button").dxButton({text: "Update",type: "success",useSubmitBehavior: true});

            function notify(message, type = 'success') {
                DevExpress.ui.notify({
                    message : message,
                    position : {my: 'center top',at: 'center top'}
                },type,3000);
            }

            $("#form").on("submit", function(e){
                e.preventDefault();

                var urlUpdate = "{{ url('keuangan/collecting/approval/update/'.$id) }}";
                var urlList = "{{ url('keuangan/collecting/approval') }}";
                var form = $('#form').serialize();

                $.ajax({
                    type:"POST",url:urlUpdate,data:form,dataType:'json'
                }).done(function(response){
                    notify('Update Success');
                    window.location = urlList;
                }).fail(function(){
                    notify('Failed to update content','warning');
                });
            });


        });        
    </script>
@endsection
