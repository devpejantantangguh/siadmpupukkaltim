@extends('templates.admin')

@section('title','Edit Pre-Post Billing Document')

@section('contentheader')
    <section class="content-header">
        <h1>Edit Billing Document</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Keuangan</li>
            <li>Collecting</li>
            <li class="active">Edit Billing Document</li>
        </ol>
    </section>

    
@endsection 

@section('content')
    @if( ! empty(validation_errors())) 
        <div class="callout callout-danger">
            <h4><i class="fa fa-info-circle"></i> Validation Error</h4>
            {!! validation_errors() !!}
        </div>
    @endif

    @if(isset($success)) 
        <div class="callout callout-success">
            <h4><i class="fa fa-info-circle"></i> Success</h4>
            <p>Data has been updated</p>
        </div>
    @endif
    {!! form_open('keuangan/collecting/upload/edit/'. $row->ID) !!}
    <div class="box-header with-border">
        <h3 class="box-title">Edit Billing Document</h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>No Billing Doc</label>
                    {!! form_input('NoBillingDoc', $row->NoBillingDoc, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    <label>Sektor</label>
                    {!! form_input('Sektor', $row->Sektor, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    <label>Billing Date</label>
                    {!! form_input('BillingDate', $row->BillingDate, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    <label>No SO</label>
                    {!! form_input('NoSO', $row->NoSO, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    <label>ID Customer</label>
                    {!! form_input('IDCustomer', $row->IDCustomer, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    <label>Customer</label>
                    {!! form_input('Customer', $row->Customer, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    <label>ID Material</label>
                    {!! form_input('IDMaterial', $row->IDMaterial, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    <label>Material</label>
                    {!! form_input('Material', $row->Material, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    <label>Kuantum</label>
                    {!! form_input('Kuantum', $row->Kuantum, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    <label>UoM</label>
                    {!! form_input('UoM', $row->UoM, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    <label>Kurs</label>
                    {!! form_input('Kurs', $row->Kurs, ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <a href="{{ url('keuangan/collecting/upload') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left"></i> Back</a>
        <button type="submit" class="btn btn-primary btn-sm">Update</button>
    </div>
    {!! form_close() !!}
@endsection
