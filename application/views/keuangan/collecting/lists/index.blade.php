@extends('templates.admin')

@section('title', 'List Billing Document')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
    <script src="{{ url('asset/js/phpjs.js') }}"></script>
    <style type="text/css">
        .kotak{width:50px;height:10px;display:block;float:left;border-radius:3px;margin-top:4px;margin-right:5px;}
        .g{background:#01bf0178;}
        .o{background:#ff98009e;}
        .b{height:100%;width:100%;display:block;border-radius:3px;text-decoration:none!important;text-align: center;}
        .u{background:#ff98009e}
        .a{background:#01bf0178}
    </style>
@endsection

@section('contentheader')
    <section class="content-header">
        <h1>List Billing Document</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Keuangan</li>
            <li>Collecting</li>
            <li class="active">List Billing Document</li>
        </ol>
    </section>
@endsection

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">List Billing Document</h3>
    </div>
    <div class="box-body">
        <div id="btn-uploadBillDoc"></div>
        <div id="btn-uploadFakturPajak"></div>
{{--        <div id="btn-edit"></div>--}}
        <div id="btn-upload"></div>
        {{-- <div id="btn-approve"></div> --}}
        {{-- <div id="btn-approveall"></div> --}}
        <div id="btn-cancel"></div>
        <div id="btn-print-faktur"></div>
        <div id="gridContainer"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="kotak o"></div> Belum Approve
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="kotak g"></div> Approved
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="bs-modal-materials">
    	<div class="modal-dialog" role="document">
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
    				<h4 class="modal-title" id="bs-title-materials">Materials</h4>
    			</div>
    			<div class="modal-body" id="md-content-materials">
    				<p>Loading...</p>
    			</div>
    		</div><!-- /.modal-content -->
    	</div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="modal fade" tabindex="-1" role="dialog" id="bs-modal-approval">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="bs-title">Approval</h4>
                    </div>
                    <div class="modal-body" id="md-content">
                        <p>Loading...</p>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){

            $('#bs-modal-materials').on('show.bs.modal', function(e){
                var id = $(e.relatedTarget).data('billdoc');

                var url = "{{ url('/keuangan/collecting/lists/materials') }}?id="+id;

                $('#md-content-materials').html('Loading...');
                $.get(url, function(response){
                    $('#md-content-materials').html(response);
                });
                console.log('okey');
            });

            $('#bs-modal-approval').on('show.bs.modal', function(e){
                var type = $(e.relatedTarget).data('type');
                var id = $(e.relatedTarget).data('id');

                if(type == 1) {
                    $('#bs-title').html('Approval SSP');
                } else {
                    $('#bs-title').html('Approval Bukti Potong');
                }

                var url = "{{ url('/keuangan/collecting/lists/document_popup') }}?type="+type+"&id="+id;

                $('#md-content').html('Loading...');
                $.get(url, function(response){
                    $('#md-content').html(response);
                });
                console.log('okey');
            });

            var dataSource = new DevExpress.data.CustomStore({
                key: "NoBillingDoc",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};

                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }

                    if (loadOptions.filter != 'undefined') {
                        args.filter = JSON.stringify(loadOptions.filter);
                    }

                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 10;

                    $.ajax({
                        url: "{{ url("keuangan/collecting/lists/get") }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 50000
                    });

                    return deferred.promise();
                }
            });

            {{--var uploadBillDocButton = $("#btn-uploadBillDoc").dxButton({--}}
            {{--    text: "Upload Bill Doc",--}}
            {{--    height: 34,--}}
            {{--    width: 140,--}}
            {{--    disabled: false,--}}
            {{--    onClick: function () {--}}
            {{--        location.href = "{{ url('keuangan/collecting/upload') }}";--}}
            {{--    }--}}
            {{--}).dxButton("instance");--}}

            var uploadDocButton = $("#btn-upload").dxButton({
                text: "Update Doc",
                height: 34,
                width: 120,
                disabled: true,
                onClick: function () {
                    $.each(dataGrid.getSelectedRowsData(), function() {
                        {{--window.open("{{ url('app/faktur_penjualan') }}/" + this.Hash,'_blank');--}}
                        location.href = "{{ url('keuangan/collecting/approval/upload/') }}" + this.NoBillingDoc;
                    });

                }
            }).dxButton("instance");

            var uploadFakturPajakButton = $("#btn-uploadFakturPajak").dxButton({
                text: "Upload Faktur Pajak",
                height: 34,
                width: 170,
                disabled: false,
                onClick: function () {
                    location.href = "{{ url('keuangan/collecting/fakturpajak') }}";
                }
            }).dxButton("instance");

            function approveall() {
                        var lists = [];
                        $.post("{{ url('keuangan/collecting/lists/approveall') }}", {ids:lists})
                        .done(function(msg){
                            if(msg == 'go'){
                                approveall();
                            }
                            dataGrid.refresh();
                        });
                    }

            var approvalButtonAll = $("#btn-approveall").dxButton({
                text: "Approve All",
                height: 34,
                width: 120,
                disabled: false,
                onClick: function () {
                    var result = DevExpress.ui.dialog.confirm("Are you sure to Approve Billing Document ?", "Approval");
                    result.done(function (dialogResult) {
                        if (dialogResult) {
                            var lists = [];
                            var msg = '';
                            approveall();
                        }
                    });
                }
            }).dxButton("instance");


            var approvalButton = $("#btn-approve").dxButton({
                text: "Approve",
                height: 34,
                width: 100,
                disabled: true,
                onClick: function () {
                    var result = DevExpress.ui.dialog.confirm("Are you sure to Approve Billing Document ?", "Approval");
                    result.done(function (dialogResult) {
                        if (dialogResult) {
                            var lists = [];
                            $.each(dataGrid.getSelectedRowsData(), function() {
                                lists.push(this.NoBillingDoc);
                            });
                            $.post("{{ url('keuangan/collecting/lists/approve') }}", {ids:lists})
                                .done(function(){
                                    dataGrid.refresh();
                                });
                        }
                    });
                }
            }).dxButton("instance");

            var cancelButton = $("#btn-cancel").dxButton({
                text: "Cancel",
                height: 34,
                width: 100,
                disabled: true,
                onClick: function () {
                    Swal.fire({
						title : 'Apa Anda yakin untuk cancel billing?',
						text: 'Silahkan tulis alasan pembatalan',
						input:'text',
						showCancelButton:true,
						confirmButtonText:'Yes'
					}).then((result) => {
					    if(result.value) {
					        var lists = [];
							$.each(dataGrid.getSelectedRowsData(), function(){
							    lists.push(this.NoBillingDoc);
							});
							$.post("{{ url('keuangan/collecting/lists/cancel') }}",{ids:lists,keterangan:result.value})
								.done(function(){
								    swal.fire({title:'Billing Berhasil di Cancel',type:'success'});
								    dataGrid.refresh();
								});
						}
					});
                    {{--var result = DevExpress.ui.dialog.confirm("Are you sure to Cancel Billing Document ?", "Approval");--}}
                    {{--result.done(function (dialogResult) {--}}
                    {{--    if (dialogResult) {--}}
                    {{--        var lists = [];--}}
                    {{--        $.each(dataGrid.getSelectedRowsData(), function() {--}}
                    {{--            lists.push(this.NoBillingDoc);--}}
                    {{--        });--}}
                    {{--        $.post("{{ url('keuangan/collecting/lists/cancel') }}", {ids:lists})--}}
                    {{--            .done(function(){--}}
                    {{--                dataGrid.refresh();--}}
                    {{--            });--}}
                    {{--    }--}}
                    {{--});--}}
                }
            }).dxButton("instance");

            {{--var editButton = $("#btn-edit").dxButton({--}}
            {{--    text: "Edit",--}}
            {{--    height: 34,--}}
            {{--    width: 100,--}}
            {{--    disabled: true,--}}
            {{--    onClick: function () {--}}
            {{--        $.each(dataGrid.getSelectedRowsData(), function() {--}}
            {{--            location.href = "{{ url('keuangan/collecting/lists/edit/') }}" + this.ID;--}}
            {{--        });--}}
            {{--        dataGrid.refresh();--}}
            {{--    }--}}
            {{--}).dxButton("instance");--}}

            var printButton = $("#btn-print-faktur").dxButton({
                text: "Cetak Faktur",
                height: 34,
                width: 120,
                disabled: true,
                onClick: function () {
                    $.each(dataGrid.getSelectedRowsData(), function() {
                        {{--location.href = "{{ url('app/faktur_penjualan') }}/" + this.NoBillingDoc;--}}
                        window.open("{{ url('app/faktur_penjualan') }}/" + this.Hash,'_blank');
                    });
                }
            }).dxButton("instance");


            var dataGrid = $("#gridContainer").dxDataGrid({
                dataSource : dataSource,
                // columnHidingEnabled: true,
                remoteOperations: {sorting: true,paging: true, filtering:true},
                "export" : {enabled:true, fileName:'List Billing Document',allowExportSelectedData:true},
                editing : {mode:'form', allowAdding : false,allowUpdating : false,allowDeleting : false},
                selection:{mode:'multiple',showCheckBoxesMode:'always',selectAllMode:'page'},
                onSelectionChanged: function(data){
                    // approvalButton.option('disabled', !data.selectedRowsData.length)
                    cancelButton.option('disabled', !data.selectedRowsData.length);
                    printButton.option('disabled', !(data.selectedRowsData.length == 1));
                    uploadDocButton.option('disabled', !(data.selectedRowsData.length == 1));

                    // btn_approve_bukpot.option('disabled', !data.selectedRowsData.length);
                    // btn_approve_spp.option('disabled', !data.selectedRowsData.length);
                    // uploadFakturPajakButton.option('disabled', !(data.selectedRowsData.length == 1))
                },
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : true},
                groupPanel : {visible:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                headerFilter:{visible:true},
                filterRow:{visible:true,applyFilter:'auto'},
                pager: {allowedPageSizes: [20, 50, 100, 200, 500, 1000],showInfo: true,showNavigationButtons: true,showPageSizeSelector: true,visible: true},
                paging: {pageSize: 20},
				onRowPrepared : function (e) {
					if (e.rowType === 'data') {
						if(e.data.PaymentMethod == 'M' || e.data.PaymentMethod == 'P'){
						    if(e.data.UploadedFile == true) {
						    	e.rowElement.css({ "background-color": "#adff2f" });
							} else {
						        e.rowElement.css({ "background-color": "#fcd691" });
							}
						}
					}
				},
                columns : [
                    {dataField:'NoBillingDoc',caption:'No Billing'},
                    {dataField:'BillingDate',caption:'Billing Date', dataType:'date',format:'dd-MM-yyyy',selectedFilterOperation:'between'},
                    {dataField:'TglFakturPajak',caption:'Tgl FP',dataType:'date', format:'dd-MM-yyyy',selectedFilterOperation:'between'},
                    {dataField:'NoFakturPajak',caption:'Faktur Pajak Number'},
                    {dataField:'NoSO',caption:'Sales Doc.'},
                    {dataField:'IDCustomer',caption:'Payer'},
                    {dataField:'Customer',caption:'Payer'},
                    {dataField:'PaymentMethod',caption:'Payment Method'},
                    {dataField:'IsWAPU', caption:'WAPU', calculateCellValue:function(data){
                        if(data.IsWAPU == null){
                            return '??'
						} else if(data.IsWAPU == 1) {
                            return 'Ya';
						} else if( data.IsWAPU == 0) {
                        	return 'Tidak'
						}
					}},
                    {
                        dataField:'ItemCount',caption:'Jumlah Material',allowSorting:false, allowFiltering:false,
                        cellTemplate:function(container, options) {
                            $('<a/>')
                                .addClass('text-center')
                                .attr('data-toggle', 'modal')
                                .attr('data-target', '#bs-modal-materials')
                                .attr('data-billdoc', options.data.NoBillingDoc)
                                .css('cursor','pointer')
                                .text(options.data.ItemCount + ' Material')
                                .appendTo(container)
                        }
                    },
                    {dataField:'HargaSatuan',caption:'Harga Jual (exc.PPN)', alignment:'right', calculateCellValue:function(data){
                        return 'Rp. ' + number_format(data.HargaSatuan,0,'.',',');
                    }},
                    {dataField:'Kurs',caption:'Curr.'},
                    {dataField:'TaxAmount',caption:'PPN in Local Currency.',alignment:'right', calculateCellValue:function(data){
                        // total = data.TaxAmount;
                        // if(data.Kurs == 'IDR') {
                        //     return 'Rp. ' + number_format(total,0,'.',',');
                        // } else if(data.Kurs == 'USD') {
                        //     return '$' + number_format(total,0,',','.');
                        // } else {
                        //     return number_format(total);
                        // }
						return 'Rp. ' + number_format(data.TaxAmount,0,'.',',')
                    }},
                    {dataField:'Approval1',caption:'Approved 1 By',allowSorting:false, allowFiltering:false},
                    {dataField:'Approval2',caption:'Approved 2 By',allowSorting:false, allowFiltering:false},
                    {dataField:'Approval3',caption:'Approved 3 By',allowSorting:false, allowFiltering:false},
					{
                    	caption: 'Dokumen',
                    	width: 100,
                    	cellTemplate: function (container, options) {
							if(options.data.UploadedFile == true) {
								$('<a/>').addClass('View')
									.text('Download')
									.attr('target','_blank')
									.attr('href', "{{ url('keuangan/collecting/approval/document') }}/"+options.data.NoBillingDoc)
									.appendTo(container);
							} else {
								$('<a/>').addClass('View')
								.text('Upload')
								.attr('href', "{{ url('keuangan/collecting/approval/upload') }}/"+options.data.NoBillingDoc)
								.appendTo(container);
							}
                    	}
                    },
                    {caption: 'SSP', width:100, cellTemplate:function(container,options){
                        if(options.data.SPP_Path) {
                            if(options.data.SPP) {
                                htmlClass = 'dx-link b a';
                            }else{
                                htmlClass = 'dx-link b u';
                            }
                            $('<a/>')
                                .addClass(htmlClass)
                                .attr('data-toggle','modal')
                                .attr('data-target','#bs-modal-approval')
                                .attr('data-type',1)
                                .attr('data-id', options.data.NoBillingDoc)
                                .text('Open')
                                .appendTo(container);
                        }
                    }},
                    {caption: 'Bukti Potong', width:100, cellTemplate:function(container,options){
                        if(options.data.Bukpot_Path) {
                            if(options.data.Bukpot) {
                                htmlClass = 'dx-link b a';
                            }else{
                                htmlClass = 'dx-link b u';
                            }
                            $('<a/>').addClass(htmlClass)
                                .attr('data-toggle','modal')
                                .attr('data-target','#bs-modal-approval')
                                .attr('data-type',2)
                                .attr('data-id', options.data.NoBillingDoc)
                                .text('Open')
                                .appendTo(container);
                        }
                    }},
                    {dataField: 'Status', caption:'Status',allowSorting:false, allowFiltering:false,
                        fixed: true,
                        fixedPosition: "right"
                    },
                    {dataField: 'TotalNilai',dataType:"number", allowSorting:false, allowFiltering:false, caption:'Total Nilai', calculateCellValue:function(data){
                        total = data.TotalNilai;
                        if(data.Kurs == 'IDR') {
                            return 'Rp. ' + number_format(total,0,',','.');
                        } else if(data.Kurs == 'USD') {
                            return '$' + number_format(total,2,'.',',');
                        } else {
                            return number_format(total,2);
                        }
                        },
                        fixed: true,
                        fixedPosition: "right"
                    }
                    // {dataField:'StatusText',caption:'Status Apps',
                    //     fixed: true,
                    //     fixedPosition: "right"}
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
            }).dxDataGrid("instance");
        });

    </script>
@endsection
