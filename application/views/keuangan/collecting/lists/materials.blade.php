<p><b>No Billing : {{ $no_billing }}</b></p>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>No.</th>
            <th>Customer</th>
            <th>ID Material</th>
            <th>Material</th>
            <th>Kuantum</th>
			<th>Satuan</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
        @foreach($materials as $key => $material)
        <tr>
            <td>{{ $key+1 }}</td>
            <td>{{ $material->Customer }}</td>
            <td>{{ $material->IDMaterial }}</td>
            <td>{{ $material->Material }}</td>
            <td>{{ $material->Kuantum }}</td>
			<td>{{ $material->UoM }}</td>
            <td>{{ $material->NetValue }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
