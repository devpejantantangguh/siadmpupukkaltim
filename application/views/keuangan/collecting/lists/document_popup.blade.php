<form action="{{ url('keuangan/collecting/lists/document_update/'.$data->IDParent.'/'.$data->Tipe) }}" method="POST">
    <div class="form-group">
        <label>Judul</label>
        <input type="text" name="Judul" value="{{ $data->Judul }}" class="form-control">
    </div>
    <iframe src="{{ url($data->FilePath) }}" style="width: 550px;min-height: 400px"></iframe>
    <hr>
    <div class="form-group">
        <button type="submit" name="status" value="reject" class="btn btn-danger">Reject</button>
        <button type="submit" name="status" value="approve" class="btn btn-primary">Approve</button>
    </div>
</form>
