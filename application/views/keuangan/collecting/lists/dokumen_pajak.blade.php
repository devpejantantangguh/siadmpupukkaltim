@extends('templates.admin')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
@endsection 

@section('contentheader')
    <section class="content-header">
        <h1>Edit Dokumen Pajak [{{ $status }}]</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Keuangan</li>
            <li>Collecting</li>
            <li class="active">Edit Dokumen Pajak [{{ $status }}]</li>
        </ol>
    </section>
@endsection 

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">Edit Dokumen Pajak [{{ $status }}]</h3>
    </div>
    <div class="box-body">
        @if(isset($_GET['submit']) && $_GET['submit'] == 'success')
            <div class="alert alert-success">
                <p>Dokumen {{ $status }} berhasil Approve</p>
            </div>
        @endif

        <form action="{{ url('keuangan/collecting/lists/dp_store/'.$data->IDParent.'/'.$data->Tipe) }}" method="POST">
            <div class="form-group">
                <label>Judul</label>
                <input type="text" name="Judul" value="{{ $data->Judul }}" class="form-control">
            </div>

            <iframe src="{{ url($data->FilePath) }}" style="width: 100%;min-height: 400px"></iframe>

            <div class="form-group">
                <hr>
                <center>
                    <button type="submit" class="btn btn-primary">Approve</button>
                    <a href="{{ url('keuangan/collecting/lists') }}" class="btn btn-default">Cancel</a>
                </center>
            </div>
        </form>
    </div>
@endsection

@section('script')
   
@endsection
