@extends('templates.admin')

@section('contentheader')
    <section class="content-header">
        <h1>Edit Billing Document</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Keuangan</li>
            <li>Collecting</li>
            <li class="active">Edit Billing Document</li>
        </ol>
    </section>
@endsection 

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">Edit Billing Document</h3>
    </div>
    <div class="box-body">
        <div role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#edit" aria-controls="edit" role="tab" data-toggle="tab">Edit</a>
                </li>
                <li role="presentation">
                    <a href="#history" aria-controls="history" role="tab" data-toggle="tab">History</a>
                </li>
            </ul>
        
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="edit">
                    <div class="row">
                        <div class="col-md-12">
                            <br>
                            @if( ! empty(validation_errors())) 
                                <div class="callout callout-danger">
                                    <h4><i class="fa fa-info-circle"></i> Validation Error</h4>
                                    {!! validation_errors() !!}
                                </div>
                            @endif

                            @if(isset($success)) 
                                <div class="callout callout-success">
                                    <h4><i class="fa fa-info-circle"></i> Success</h4>
                                    <p>Data has been updated</p>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            {!! form_open('keuangan/collecting/lists/edit/'.$data->ID) !!}
                                <div class="form-group">
                                    <label>NoBillingDoc</label>
                                    {!! form_input('NoBillingDoc', $data->NoBillingDoc, ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label>JenisTransaksi</label>
                                    {!! form_dropdown('JenisTransaksi', $jenisTransaksi, $data->JenisTransaksi, ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label>Sektor</label>
                                    {!! form_input('Sektor', $data->Sektor, ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label>BillingDate</label>
                                    {!! form_input('BillingDate', $data->BillingDate, ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label>NoSO</label>
                                    {!! form_input('NoSO', $data->NoSO, ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label>NoGI</label>
                                    {!! form_input('NoGI', $data->NoGI, ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label>IDCustomer</label>
                                    {!! form_input('IDCustomer', $data->IDCustomer, ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label>Customer</label>
                                    {!! form_input('Customer', $data->Customer, ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label>IDMaterial</label>
                                    {!! form_input('IDMaterial', $data->IDMaterial, ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label>Material</label>
                                    {!! form_input('Material', $data->Material, ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label>Kuantum</label>
                                    {!! form_input('Kuantum', $data->Kuantum, ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label>UoM</label>
                                    {!! form_input('UoM', $data->UoM, ['class' => 'form-control']) !!}
                                </div>
                                <div class="form-group">
                                    <label>Kurs</label>
                                    {!! form_input('Kurs', $data->Kurs, ['class' => 'form-control']) !!}
                                </div>
                                <hr>
                                <div class="form-group">
                                    <a href="{{ url('keuangan/collecting/lists') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left"></i> Back</a>
                                    <button class="btn btn-primary btn-sm" type="submit">Update</button>
                                </div>
                            {!! form_close() !!}
                        </div>
                    </div>
                    
                </div>
                <div role="tabpanel" class="tab-pane" id="history">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No. </th>
                                <th>Action</th>
                                <th>User</th>
                                <th>Time</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($histories as $key => $history)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $history->Action }}</td>
                                <td>{{ $history->UserID }}</td>
                                <td>{{ date('d M Y H:i:s',strtotime($history->Timestamp)) }}</td>
                                <td>{{ $history->Keterangan }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function() {
            var listJenisTransaksi = {!! json_encode($listJenisTransaksi) !!};
            var listCustomer = {!! json_encode($customers) !!};
            var listMaterial = {!! json_encode($materials) !!};
            var listStatus = {!! json_encode($listStatus) !!};
            var listSalesOrder = {!! json_encode($listSalesOrder) !!};

            $('#nobilldoc-validation').dxTextBox({
                name:'NoBillingDoc', value:"{{ $data->NoBillingDoc }}"
            }).dxValidator({validationRules:[{type:'required',message:'No. Billing Document is required'}]});

            $("#jt-validation").dxSelectBox({
                name: "JenisTransaksi", value:"{{ $data->JenisTransaksi }}", dataSource: listJenisTransaksi, displayExpr: "name", valueExpr: "id"
            }).dxValidator({validationRules: [{type: "required",message: "Jenis Transaksi is required"}]});

            $('#sektor-validation').dxTextBox({
                name:'Sektor',value:"{{ $data->Sektor }}"
            }).dxValidator({validationRules:[{type:'required',message:'Sektor is required'}]});

            $('#billdate-validation').dxDateBox({
                name:'BillingDate',value:"{{ $data->BillingDate }}"
            }).dxValidator({validationRules:[{type:'required',message:'Billing Date is required'}]});

            $('#noso-validation').dxSelectBox({
                name:'NoSO',value:"{{ $data->NoSO }}", dataSource : listSalesOrder, displayExpr: "name", valueExpr: "id"
            }).dxValidator({validationRules:[{type:'required',message:'No. SO is required'}]});

            $('#nogi-validation').dxTextBox({
                name:'NoGI',value:"{{ $data->NoGI }}"
            }).dxValidator({validationRules:[{type:'required',message:'No. GI is required'}]});

            $('#nofaktur-validation').dxTextBox({
                name:'NoFakturPajak',value:"{{ $data->NoFakturPajak }}"
            }).dxValidator({validationRules:[{type:'required',message:'No. Faktur Pajak is required'}]});

            $('#customer-validation').dxSelectBox({
                name:'IDCustomer',value:"{{ $data->IDCustomer }}", dataSource: listCustomer, displayExpr: "name", valueExpr: "id", searchEnabled:true, showClearButton:true
            }).dxValidator({validationRules:[{type:'required',message:'Customer is required'}]});

            $('#status-validation').dxSelectBox({
                name:'Status',value:"{{ $data->Status }}", dataSource : listStatus, displayExpr: "name", valueExpr: "id"
            }).dxValidator({validationRules:[{type:'required',message:'Status is required'}]});

            $('#idmaterial-validation').dxSelectBox({
                name:'IDMaterial',value:"{{ $data->IDMaterial }}", dataSource: listMaterial, displayExpr: "name", valueExpr: "id"
            }).dxValidator({validationRules:[{type:'required',message:'No. Material is required'}]});

            $('#materialdesc-validation').dxTextBox({
                name:'Material',value:"{{ $data->Material }}"
            }).dxValidator({validationRules:[{type:'required',message:'Deskripsi Material is required'}]});

            $('#kuantum-validation').dxNumberBox({
                name:'Kuantum',value:"{{ $data->Kuantum }}"
            }).dxValidator({validationRules:[{type:'required',message:'Kuantum is required'}]});

            $('#uom-validation').dxTextBox({
                name:'UoM',value:"{{ $data->UoM }}"
            }).dxValidator({validationRules:[{type:'required',message:'Unit of Meansure is required'}]});

            $('#kurs-validation').dxTextBox({
                name:'Kurs',value:"{{ $data->Kurs }}"
            }).dxValidator({validationRules:[{type:'required',message:'Kurs is required'}]});

            $('#hargasatuan-validation').dxNumberBox({
                name:'HargaSatuan',value:"{{ $data->HargaSatuan }}"//, format:"$ #,##0.##"
            }).dxValidator({validationRules:[{type:'required',message:'Harga Satuan is required'}]});

            $('#jumlah-validation').dxNumberBox({
                name:'Jumlah',value:"{{ $data->Jumlah }}"
            }).dxValidator({validationRules:[{type:'required',message:'Jumlah is required'}]});

            $('#dpp-validation').dxNumberBox({
                name:'DPP',value:"{{ $data->DPP }}"
            }).dxValidator({validationRules:[{type:'required',message:'DPP is required'}]});

            $('#tax-validation').dxNumberBox({
                name:'TaxAmount',value:"{{ $data->TaxAmount }}"
            }).dxValidator({validationRules:[{type:'required',message:'Tax Amount is required'}]});

            $('#total-validation').dxTextBox({
                name:'Total',value:"{{ $data->Total }}"
            }).dxValidator({validationRules:[{type:'required',message:'Total is required'}]});

            $('#keterangan-validation').dxTextBox({
                name:'Keterangan',value:"{{ $data->Keterangan }}"
            }); // .dxValidator({validationRules:[{type:'required',message:'Keterangan is required'}]});

            $('#payer-validation').dxSelectBox({
                name:'IDPayer',value:"{{ $data->IDPayer }}", dataSource : listCustomer, displayExpr: "name", valueExpr: "id"
            }).dxValidator({validationRules:[{type:'required',message:'Payer is required'}]});

            $('#salesorg-validation').dxSelectBox({
                name:'IDSalesOrg',value:"{{ $data->IDSalesOrg }}",dataSource:listSalesOrder, displayExpr: "name", valueExpr: "id"
            }).dxValidator({validationRules:[{type:'required',message:'IDSalesOrg is required'}]});

            $("#button").dxButton({text: "Update",type: "success",useSubmitBehavior: true});

            function notify(message, type = 'success') {
                DevExpress.ui.notify({
                    message : message,
                    position : {my: 'center top',at: 'center top'}
                },type,3000);
            }

            $("#form").on("submit", function(e){
                e.preventDefault();

                var urlUpdate = "{{ url('keuangan/collecting/lists/update/'.$id) }}";
                var urlList = "{{ url('keuangan/collecting/lists/index') }}";
                var form = $('#form').serialize();

                $.ajax({
                    type:"POST",url:urlUpdate,data:form,dataType:'json'
                }).done(function(response){
                    notify('Update Success');
                    window.location = urlList;
                }).fail(function(){
                    notify('Failed to update content','warning');
                });
            });


        });        
    </script>
@endsection
