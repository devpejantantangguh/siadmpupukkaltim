@extends('templates.admin')

@section('title','Edit Pre-Post Sales Document')

@section('contentheader')
    <section class="content-header">
        <h1>Edit Sales Doc</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Keuangan</li>
            <li>Collecting</li>
            <li class="active">Edit Sales Doc</li>
        </ol>
    </section>
@endsection 

@section('content')

    @if( ! empty(validation_errors())) 
        <div class="callout callout-danger">
            <h4><i class="fa fa-info-circle"></i> Validation Error</h4>
            {!! validation_errors() !!}
        </div>
    @endif

    @if(isset($success)) 
        <div class="callout callout-success">
            <h4><i class="fa fa-info-circle"></i> Success</h4>
            <p>Data has been updated</p>
        </div>
    @endif

    <form action="{{ url('keuangan/collecting/salesdoc/edit/'. $row->ID) }}" method="POST">
        <div class="box-header with-border">
            <h3 class="box-title">Edit Sales Doc</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label>No. SO</label>
                        {!! form_input('NoSO', $row->NoSO, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <label>No. Billing Doc</label>
                        {!! form_input('NoBillingDoc', $row->NoBillingDoc, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <label>Incoterm</label>
                        {!! form_input('Incoterm', $row->Incoterm, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <label>Incoterm 2</label>
                        {!! form_input('Incoterm2', $row->Incoterm2, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <label>Alat Angkut</label>
                        {!! form_input('AlatAngkut', $row->AlatAngkut, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <label>Tanggal Pelayaran</label>
                        {!! form_input('TglPelayaran', $row->TglPelayaran, ['class' => 'form-control datepicker']) !!}
                    </div>
                    <div class="form-group">
                        <label>Tujuan </label>
                        {!! form_input('Tujuan', $row->Tujuan, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <label>Tempat Muat</label>
                        {!! form_input('TempatMuat', $row->TempatMuat, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <label>Harga Satuan</label>
                        {!! form_input('HargaSatuan', $row->HargaSatuan, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <label>Payment Method</label>
                        {!! form_input('PaymentMethod', $row->PaymentMethod, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <label>No. PEB</label>
                        {!! form_input('NoPEB', $row->NoPEB, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <label>Tanggal. PEB</label>
                        {!! form_input('TglPEB', $row->TglPEB, ['class' => 'form-control datepicker']) !!}
                    </div>
                    <div class="form-group">
                        <label>Faktur Pajak</label>
                        {!! form_input('NoFakturPajak', $row->NoFakturPajak, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <label>Tgl. Faktur Pajak</label>
                        {!! form_input('TglFakturPajak', $row->TglFakturPajak, ['class' => 'form-control datepicker']) !!}
                    </div>
                    <div class="form-group">
                        <label>PPN (Local Currency)</label>
                        {!! form_input('TaxAmount', $row->TaxAmount, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group">
                        <label>DPP</label>
                        {!! form_input('DPP', $row->DPP, ['class' => 'form-control']) !!}
                    </div>
                </div>
            </div>
        </div>    
        <div class="box-footer">
            <a href="{{ url('keuangan/collecting/salesdoc') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left"></i> Back</a>
            <button type="submit" class="btn btn-primary btn-sm">Update</button>
        </div>
    </form>
@endsection

@section('script')
	<script>
		 $('.datepicker').datepicker({
			autoclose: true, format:'yyyy-mm-dd',
		});

	</script>
@endsection

