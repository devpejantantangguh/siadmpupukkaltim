@extends('templates.admin')

@section('contentheader')
    <section class="content-header">
        <h1>Edit Cancel Sales Order</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Keuangan</li>
            <li>Collecting</li>
            <li class="active">Edit Cancel Sales Order</li>
        </ol>
    </section>
@endsection

@section('content')
    <form  action="{{ url('keuangan/collecting/cancelso/update/'.$id) }}" method="POST">
        <div class="box-header with-border">
            <h3 class="box-title">Edit Cancel Sales Order</h3>
        </div>
        <div class="box-body">
            <div class="form-group">
                <label>No Sales Order</label>
                <input type="text" name="NoSO" placeholder="No Billing Document" value="{{ $NoSO }}" class="form-control">
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-success btn-sm">Save</button>
        </div>
    </form>
@endsection
