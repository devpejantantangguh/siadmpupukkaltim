@extends('templates.admin')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
    <style type="text/css">
        .bulet {height: 15px;width: 15px;display: block;border-radius: 30px;margin: 0 auto;}
        .b-green {background: green;}
        .b-red {background: red;}
    </style>
    <script src="{{ url('asset/js/phpjs.js') }}"></script>
@endsection

@section('contentheader')
    <section class="content-header">
        <h1>Upload Cancel Sales Order</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Keuangan</li>
            <li>Collecting</li>
            <li class="active">Upload Cancel Sales Order</li>
        </ol>
    </section>
@endsection

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">Upload Cancel Sales Order</h3>
    </div>
    <div class="box-body">
        <div class="panel panel-default">
            <div class="panel-body">
                <form action="{{ url('keuangan/collecting/cancelso/store') }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-2">Upload Cancel Sales Order</label>
                        <div class="col-sm-10">
                            <input name="fileexcel" type="file" required>
                            <p class="help-block">Upload file excel Billing Document</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2"></label>
                        <div class="col-sm-10">
                            <label><input type="checkbox" name="remove_last" value="1" checked> Hapus Temporary Sebelumnya</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-10 col-sm-offset-2">
                            <button class="btn btn-primary btn-sm" id="btn-upload"><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div style="padding-top:20px">
            <div id="gridDeleteSelected"></div>
            <div id="gridEditSelected"></div>
            <div id="gridPostSelected"></div>
            <div id="gridPostAllSelected"></div>
            <div id="btnDownloadTempalte"></div>
            <div id="gridContainer"></div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){

            var dataSource = new DevExpress.data.CustomStore({
                key: "ID",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};

                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }

                    if (loadOptions.filter != 'undefined') {
                        args.filter = JSON.stringify(loadOptions.filter);
                    }

                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 20;

                    $.ajax({
                        url: "{{ url("keuangan/collecting/cancelso/get") }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });

                    return deferred.promise();
                }
            });

            var deleteButton = $("#gridDeleteSelected").dxButton({
                text: "Delete",
                height: 34,
                width: 100,
                disabled: true,
                onClick: function () {
                    var lists = [];
                    $.each(dataGrid.getSelectedRowsData(), function() {
                        lists.push(this.ID);
                    });
                    $.post("{{ url('keuangan/collecting/cancelso/remove') }}", {ids:lists})
                        .done(function(){
                            dataGrid.refresh();
                        });
                }
            }).dxButton("instance");

            var editButton = $("#gridEditSelected").dxButton({
                text: "Edit",
                height: 34,
                width: 100,
                disabled: true,
                onClick: function () {
                    $.each(dataGrid.getSelectedRowsData(), function() {
                        location.href = "{{ url('keuangan/collecting/cancelso/edit/') }}" + this.ID;
                    });
                    dataGrid.refresh();
                }
            }).dxButton("instance");

            var postAllButton = $("#gridPostAllSelected").dxButton({
                text: "Post All",
                height: 34,
                width: 100,
                disabled: false,
                onClick: function () {
                    var lists = [];
                    $.each(dataGrid.getSelectedRowsData(), function() {
                        if(this.IsValid > 0) {
                            lists.push(this.ID);
                        }
                        console.log(this);
                    });
                    $.post("{{ url('keuangan/collecting/cancelso/postall') }}")
                        .done(function(){
                            dataGrid.refresh();
                        });
                }
            }).dxButton("instance");

            var btnDownloadTemplate = $("#btnDownloadTempalte").dxButton({
                text: "Download Template",
                height: 34,
                width: 170,
                disabled: false,
                onClick: function () {
                    location.href = "{{ url('asset/template/download/cancel_so.xlsx') }}";
                }
            }).dxButton("instance");

            var postButton = $("#gridPostSelected").dxButton({
                text: "Post",
                height: 34,
                width: 100,
                disabled: true,
                onClick: function () {
                    var lists = [];
                    $.each(dataGrid.getSelectedRowsData(), function() {
                        lists.push(this.ID);
                        console.log(this);
                    });
                    console.log(lists);
                    $.post("{{ url('keuangan/collecting/cancelso/post') }}", {ids:lists})
                        .done(function(){
                            dataGrid.refresh();
                        });
                }
            }).dxButton("instance");

            var dataGrid = $("#gridContainer").dxDataGrid({
                dataSource : dataSource,
                remoteOperations: {sorting: true,paging: true, filtering:true},
                "export" : {enable:true, fileName : "Cancel Sales Order List",allowExportSelectedData:true},
                editing : {mode:'form', allowAdding : false,allowUpdating : false,allowDeleting : false},
                selection:{mode:'multiple',showCheckBoxesMode:'always',selectAllMode:'page'},
                onSelectionChanged:function(data){
                    deleteButton.option('disabled', !data.selectedRowsData.length)
                    postButton.option('disabled', !data.selectedRowsData.length)
                    editButton.option('disabled', !(data.selectedRowsData.length == 1))
                },
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : true},
                headerFilter:{visible:true},
                filterRow:{visible:true,applyFilter:'auto'},
                groupPanel : {visible:true},
                popup : {
                    title:"Cancel Sales Order List",showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [20, 100, 200, 500, 1000],showInfo: true,showNavigationButtons: true,showPageSizeSelector: true,visible: true},
                paging: {pageSize:20},
                columns : [
                    {dataField:'NoSO',caption:'No. Sales Order'}
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
            }).dxDataGrid("instance");
        });
    </script>
@endsection
