@extends('templates.admin')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
@endsection 

@section('contentheader')
    <section class="content-header">
        <h1>Edit Faktur Pajak</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Keuangan</li>
            <li>Collecting</li>
            <li class="active">Edit Faktur Pajak</li>
        </ol>
    </section>
@endsection 

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">Edit Faktur Pajak</h3>
    </div>
    <div class="box-body">
        @if(isset($_GET['submit']) && $_GET['submit'] == 'success')
            <div class="alert alert-success">
                <p>Dokumen Faktur Pajak berhasil di Update</p>
            </div>
        @endif

        <form action="{{ url('keuangan/collecting/fakturpajak/update/'.$data->ID) }}" method="POST">
            <input type="hidden" name="OldName" value="{{ $data->FileName }}">
            <div class="form-group">
                <label>File Name</label>
                <input type="text" name="FileName" value="{{ $data->FileName }}" class="form-control">
            </div>

            <iframe src="{{ url('storage/faktur_pajak/'.$data->FileName) }}" style="width:100%;min-height:300px"></iframe>

            <div class="form-group">
                <hr>
                <center>
                    <button type="submit" class="btn btn-primary">Update</button>
                    <a href="{{ url('keuangan/collecting/fakturpajak') }}" class="btn btn-default">Cancel</a>
                </center>
            </div>
        </form>
    </div>
@endsection

@section('script')
   
@endsection
