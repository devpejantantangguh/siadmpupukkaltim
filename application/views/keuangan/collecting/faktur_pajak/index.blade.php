@extends('templates.admin')

@section('heading')
    <link rel="stylesheet" href="{{url('/asset/')}}js/dropzone.min.css" />
    <link rel="stylesheet" href="{{ url('asset/css/dropzone.css') }}" />    
    <script src="{{url('/asset/')}}js/dropzone.js"></script>
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
	<script src="{{ url('asset/js/phpjs.js') }}"></script>
    <style type="text/css">
        #dzx{margin-bottom: 30px}
        .bulet {height: 15px;width: 15px;display: block;border-radius: 30px;margin: 0 auto;}
        .b-green {background: green;}
        .b-red {background: red;}
    </style>
@endsection

@section('title','Upload Faktur Pajak')

@section('contentheader')
    <section class="content-header">
        <h1>Upload Faktur Pajak</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Keuangan</li>
            <li>Collecting</li>
            <li class="active">Upload Faktur Pajak</li>
        </ol>
    </section>
@endsection 

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">Upload Faktur Pajak</h3>
    </div>
    <div class="box-body">
        <div class="dzx">
            <form id="upload" action="{{ url('keuangan/collecting/fakturpajak/upload') }}" class="dropzone needsclick">
                <div class="dz-message needsclick">
                    Drop files here or click to upload.<br />
                    <span class="note needsclick"></span>
                </div>
            </form>
        </div>
        <div id="gridContainer"></div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){
            Dropzone.options.upload = {
                addRemoveLinks:true,
                url : "{{ url('keuangan/collecting/fakturpajak/upload') }}"
            };

            var dataSource = new DevExpress.data.CustomStore({
                key: "ID",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};
            
                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }
            
                    if (loadOptions.filter != 'undefined') {
                        args.filter = JSON.stringify(loadOptions.filter);
                    }
            
                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 12;
            
                    $.ajax({
                        url: "{{ url("keuangan/collecting/fakturpajak/get") }}",
                        data: args,
                        success: function(response) {
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });
            
                    return deferred.promise();
                }
            });

            var dxView = $("#dxView").dxButton({
                text: "View",
                height: 34,
                width: 100,
                disabled: true,
                onClick: function () {
                    // var lists = [];
                    // $.each(dataGrid.getSelectedRowsData(), function() {
                    //     if(this.IsValid > 0) {
                    //         lists.push(this.IDBillDocHeader);
                    //     }
                    //     console.log(this);
                    // });
                    // console.log(lists);
                    // $.post("{{ url('keuangan/collecting/upload/post') }}", {ids:lists})
                    //     .done(function(){
                    //         dataGrid.refresh();
                    //     });
                }
            }).dxButton("instance");

            var dxEdit = $("#dxEdit").dxButton({
                text: "Edit",
                height: 34,
                width: 100,
                disabled: true,
                onClick: function () {
                    // var lists = [];
                    // $.each(dataGrid.getSelectedRowsData(), function() {
                    //     if(this.IsValid > 0) {
                    //         lists.push(this.IDBillDocHeader);
                    //     }
                    //     console.log(this);
                    // });
                    // console.log(lists);
                    // $.post("{{ url('keuangan/collecting/upload/post') }}", {ids:lists})
                    //     .done(function(){
                    //         dataGrid.refresh();
                    //     });
                }
            }).dxButton("instance");

            var dxPost = $("#dxPost").dxButton({
                text: "Post",
                height: 34,
                width: 100,
                disabled: true,
                onClick: function () {
                    // var lists = [];
                    // $.each(dataGrid.getSelectedRowsData(), function() {
                    //     if(this.IsValid > 0) {
                    //         lists.push(this.IDBillDocHeader);
                    //     }
                    //     console.log(this);
                    // });
                    // console.log(lists);
                    // $.post("{{ url('keuangan/collecting/upload/post') }}", {ids:lists})
                    //     .done(function(){
                    //         dataGrid.refresh();
                    //     });
                }
            }).dxButton("instance");

            var dataGrid = $("#gridContainer").dxDataGrid({
                onSelectionChanged:function(data){
                    dxView.option('disabled', !(data.selectedRowsData.length == 1));
                    dxEdit.option('disabled', !(data.selectedRowsData.length == 1));
                    dxPost.option('disabled', !(data.selectedRowsData.length == 1));
                },
                dataSource : dataSource,
                // columnHidingEnabled: true,
                remoteOperations: {sorting: false,paging: false,filtering:false},
				"export" : {enabled:true, fileName:'{{ $table }}',allowExportSelectedData:true},
                editing : {mode:'form', allowAdding : false,allowUpdating : false,allowDeleting : false},
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : true},
                headerFilter:{visible:true},
                filterRow:{visible:true,applyFilter:'auto'},
                groupPanel : {visible:true},
                selection:{mode:'multiple',showCheckBoxesMode:'always',selectAllMode:'page'},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [20, 50, 100, 200],showInfo: true,showNavigationButtons: true,showPageSizeSelector: true,visible: true},
				paging: {pageSize: 20},
                columns : [
                    {dataField:'IsValid',caption:'Valid', allowFiltering:false, allowSorting:true, width:50, cellTemplate:function(container,options){
                        if(options.data.NPWP_PKT == '010000727051000'){
                            color = 'bulet b-green';
                        }else{
                            color = 'bulet b-red';
                        }
                        $("<div>").append($("<div>",{"class" : color})).appendTo(container);
                    }},
                    // {dataField:'No',caption:'No'},
                    {dataField:'NoFakturPajak',caption:'Faktur Pajak'},
                    {dataField:'Timestamp',caption:'Tgl. Faktur Pajak', dataType:'date',format:'dd-MM-yyyy',selectedFilterOperation:'between' },
                    {dataField:'IDRekanan',caption:'ID Rekanan', allowEditing:false},
                    {dataField:'Customer',caption:'Nama Rekanan',allowEditing:false},
                    {dataField:'Total',caption:'Nominal',allowEditing:false, alignment:'right',calculateCellValue:function(data){
                        return 'Rp. ' + number_format(data.Total,0,'.',',');
					}},
                    {dataField:'Material',caption:'Material',allowEditing:false},
                    // {dataField:'CreatedOn',caption:'Created On', dataType:'date', format:'dd MMM yyyy H:mm:ss',allowEditing:false},
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
            }).dxDataGrid("instance");
        });
    </script>
@endsection
