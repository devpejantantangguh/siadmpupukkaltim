@extends('templates.admin')

@section('title','Sales Order')

@section('heading')
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
    <link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
    <script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
    <script src="{{ url('asset/js/phpjs.js') }}"></script>
    <style type="text/css">
        .kotak{width:50px;height:10px;display:block;float:left;border-radius:3px;margin-top:4px;margin-right:5px;}
        .g{background:#01bf0178;}
        .o{background:#ff98009e;}
        .b{height:100%;width:100%;display:block;border-radius:3px;text-decoration:none!important;text-align: center;}
        .u{background:#ff98009e}
        .a{background:#01bf0178}
    </style>
@endsection 

@section('contentheader')
    <section class="content-header">
        <h1>List Sales Order</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Keuangan</li>
            <li>Collecting</li>
            <li class="active">List Sales Order</li>
        </ol>
    </section>
@endsection 

@section('content')
    <div class="box-header with-border">
        <h3 class="box-title">List Sales Order</h3>
    </div>
    <div class="box-body">        
        <div id="gridContainer"></div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        $(function(){
            var dataSource = new DevExpress.data.CustomStore({
                key: "ID",
                load: function (loadOptions) {
                    var deferred = $.Deferred(),
                        args = {};
            
                    if (loadOptions.sort) {
                        args.orderby = loadOptions.sort[0].selector;
                        if (loadOptions.sort[0].desc)
                            args.orderby += " desc";
                    }

                   
                if (loadOptions.filter != 'undefined') {
                    args.filter = JSON.stringify(loadOptions.filter);
                }
            
                    args.skip = loadOptions.skip || 0;
                    args.take = loadOptions.take || 12;
            
                    $.ajax({
                        url: "{{ url("keuangan/collecting/salesorder/get") }}",
                        data: args,
                        success: function(response) {
                            console.log(response);
                            deferred.resolve(response.data, { totalCount: response.totalCount });
                        },
                        error: function() {
                            deferred.reject("Data Loading Error");
                        },
                        timeout: 5000
                    });
            
                    return deferred.promise();
                }
            });


            var dataGrid = $("#gridContainer").dxDataGrid({
                dataSource : dataSource,
                // columnHidingEnabled: true,

                paging: {
                    pageSize: 20
                },
                // columnHidingEnabled: false,
                
                remoteOperations: {sorting: true,paging: true,filtering:true},
                "export" : {enabled:true, fileName:'{{ $table }}',allowExportSelectedData:true},
                editing : {mode:'form', allowAdding : false,allowUpdating : false,allowDeleting : false},
                selection:{mode:'multiple',showCheckBoxesMode:'always',selectAllMode:'page'},
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : true},
                groupPanel : {visible:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                headerFilter:{visible:true},
                filterRow:{visible:true,applyFilter:'auto'},
                pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: true,visible: true},
                columns : [
                    {dataField:'NoSO',caption:'No SO'},
                    {dataField:'NoBillingDoc',caption:'No Billing Doc'},
                    {dataField:'IDMaterial',caption:'ID Material'},
                    {dataField:'Incoterm',caption:'Incoterm'},
                    {dataField:'AlatAngkut',caption:'AlatAngkut'},
                    {dataField:'TglPelayaran',caption:'TglPelayaran',dataType:'date', format:'dd-MM-yyyy'},
                    {dataField:'NoFakturPajak'},
                    {dataField:'TglFakturPajak',dataType:'date', format:'dd-MM-yyyy'},
                    {dataField:'Incoterm2'},
                    {dataField:'HargaSatuan', calculateCellValue:function(data){
                        total = data.HargaSatuan;
                        return number_format(total);
                        
                        }
                    },
                    {dataField:'DPP', calculateCellValue:function(data){
                        total = data.DPP;
                        return number_format(total);
                        
                        }
                    },
                    {dataField:'TaxAmount',caption:'PPN', calculateCellValue:function(data){
                        total = data.TaxAmount;
                        return number_format(total);
                        
                        }
                    },
                    {dataField:'NetValueBeforePPH2', calculateCellValue:function(data){
                        total = data.NetValueBeforePPH2;
                        return number_format(total);
                        
                        }
                    },
                    {dataField:'PaymentMethod'},
                    {dataField:'BillT'},
                    {dataField:'FPOption'},
                    {dataField:'Tujuan',caption:'Tujuan'},
                    {dataField:'TempatMuat',caption:'TempatMuat'},
                    {dataField:'NoBAST',caption:'No BAST'},
                    {dataField:'NoRegSurveyor',caption:'No Reg Surveyor'},
                    {dataField:'NoSKBDN',caption:'No SKBDN'},
                    {dataField:'ProductDetail',caption:'Product Detail'},
                    {dataField:'Bank',caption:'Bank'},
                    {dataField:'Status',caption:'Status'},
                    {dataField:'TglSKBDN',caption:'Tgl SKBDN',dataType:'date', format:'dd-MM-yyyy'}
                ],
                columnAutoWidth: true,
                columnChooser: {enabled: true},
                columnFixing: {enabled: true},
            }).dxDataGrid("instance");
        });

    </script>
@endsection
