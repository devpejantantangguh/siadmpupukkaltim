@extends('templates.admin')

@section('heading')
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
<script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
<style type="text/css">
    h3.panel-title{font-size: 14px !important}
    .dx-fileuploader-show-file-list .dx-fileuploader-files-container{padding-top:0px}
    .dx-fileuploader-wrapper{padding:0px;}
    .form-horizontal .control-label{text-align: left !important;padding-left:20px;}
</style>
@endsection 

@section('contentheader')
<section class="content-header">
    <h1>Input Peminjaman Dokumen</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Keuangan</li>
        <li>Kas Besar</li>
        <li class="active">Input Peminjaman Dokumen</li>
    </ol>
</section>
@endsection 

@section('content')
<div class="box-header with-border">
    <h3 class="box-title">Input Peminjaman Dokumen</h3>
</div>

    <div class="box-body">
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {{ flashdata('error') }}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> {{ flashdata('warning') }}
            </div>

        @elseif(flashdata('info'))

            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Info!</strong> {{ flashdata('info') }}
            </div>
        @endif
        <form action="{{ url('keuangan/kas_besar/dokumen/submit') }}" method="POST">

            @if(isset($_GET['update']) && $_GET['update'] == 'checked')
                <div class="alert alert-success">
                    <p>Dokumen Berhasil di Checked</p>
                </div>
            @elseif(isset($_GET['update']) && $_GET['update'] == 'reject')
                <div class="alert alert-warning">
                    <strong>Reject</strong> Dokumen Berhasil di Reject
                </div>
            @elseif(isset($_GET['update']) && $_GET['update'] == 'fail')
                <div class="alert alert-danger">
                    <strong>Error</strong> You did't allow to update this document
                </div>
            @endif

            <div class="panel panel-info form-horizontal">
                <div class="panel-heading">
                    <h3 class="panel-title">Detail</h3>
                </div>
                <div class="panel-body">

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Jenis Transaksi</label>
                        <div class="col-sm-3">
                            {!! form_dropdown('JenisTransaksi', ['' => ''] + $listJenisTransaksi,null, ['class' => 'form-control s2-jenis-transaksi','required' => 'required']) !!}
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">No Peminjaman</label>
                        <div class="col-sm-3">
                            <input  value="" type="text" name="NoPeminjaman" class="form-control" id="NoPeminjaman" disabled required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">NPK Requester</label>
                        <div class="col-sm-3">
                            {!! form_dropdown('IDVendor', ['' => ''] + $workers,null, ['class' => 'form-control s2-npk-requester','id' => 'IDVendor', 'disabled']) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Unit Kerja</label>
                        <div class="col-sm-3">
                            <input type="text" name="CostCenter" value="" class="form-control" id="cost-center" readonly>
                        </div>

                        <div class="col-sm-4">
                            <span class="help-block" id="cost-center-help"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">NPK Kepala Unit Kerja</label>
                        <div class="col-sm-3">
                            <input type="text" name="NPKAtasan" value="" class="form-control" id="NPKAtasan" readonly>
                        </div>

                        <div class="col-sm-4">
                            <span class="help-block" id="NPKAtasan-help"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal Transaksi</label>
                        <div class="col-sm-2">
                            <div class="input-group date">
                                <input type="text" name="TanggalTransaksi" id="TanggalTransaksi" class="form-control pull-right datepicker">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keperluan</label>
                        <div class="col-sm-3">
                            <textarea name="Deskripsi" class="form-control" id="Keperluan" rows="5" disabled></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal Jatuh Tempo</label>
                        <div class="col-sm-2">
                            <div class="input-group date">
                                    <input type="text" name="DueDate" id="DueDate" class="form-control pull-right datepicker2" disabled required=""> 
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-3">
                            <textarea name="Keterangan" class="form-control" rows="5"></textarea>
                        </div>
                    </div>

                </div>
            </div>
            <center>
                <button type="submit" class="btn btn-success btn-sm">
                    <i class="fa fa-check"></i> Next
                </button>
            </center>
        </form>
    </div>
@endsection

@section('script')

    <script type="text/javascript">
        $(function(){

            $('.datepicker').datepicker({autoclose: true, format:'dd-mm-yyyy'}).datepicker("setDate",'now');
            $('.datepicker2').datepicker({autoclose: true, format:'dd-mm-yyyy'});
            $('.s2-jenis-transaksi').select2({placeholder:'Pilih Jenis Transaksi'});
            $('.s2-npk-requester').select2({placeholder:'Pilih NPK Requester'});
            $('input[name=Nominal]').mask('000.000.000.000.000.000',{reverse:true})

            $('.s2-jenis-transaksi').change(function(){
                var text = $('.s2-jenis-transaksi').text();
                var val = $('.s2-jenis-transaksi').val();
                console.log(val);
                console.log(text);
                    document.getElementById('NoPeminjaman').disabled = false;
                    document.getElementById('IDVendor').disabled = false;
                    document.getElementById('Keperluan').disabled = false;
                    document.getElementById('DueDate').disabled = false;
                
            })
            $('#IDVendor').change(function(){
                var id = $('#IDVendor').val();
                console.log(id);
                $.post("{{ url('keuangan/kas_besar/dokumen/ajax') }}",{id:id})
                    .done(function(data){
                        
                        $('#NPKAtasan').val(data.KepalaUnitKerja);
                        $('#NPKAtasan-help').html(data.NamaKepalaUnitKerja);
                        $('#cost-center').val(data.CostCenter);
                        $('#cost-center-help').html(data.UnitKerja);
                    }).fail(function(){
                        alert('Something Wrong');
                    })
            })

        })
    </script>
    @endsection
