@extends('templates.admin')

@section('heading')
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
<script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
<style type="text/css">
    h3.panel-title{font-size: 14px !important}
    .dx-fileuploader-show-file-list .dx-fileuploader-files-container{padding-top:0px}
    .dx-fileuploader-wrapper{padding:0px;}
    .form-horizontal .control-label{text-align: left !important;padding-left:20px;}
</style>
@endsection 

@section('contentheader')
<section class="content-header">
    <h1>Input Peminjaman Dokumen</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Keuangan</li>
        <li>Kas Besar</li>
        <li class="active">Input Peminjaman Dokumen</li>
    </ol>
</section>
@endsection 

@section('content')
<div class="box-header with-border">
    <h3 class="box-title">Input Peminjaman Dokumen</h3>
</div>
<div class="box-body">
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {{ flashdata('error') }}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> {{ flashdata('warning') }}
            </div>

        @elseif(flashdata('info'))

            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Info!</strong> {{ flashdata('info') }}
            </div>
        @endif
            <div class="panel panel-info form-horizontal">
                <div class="panel-heading">
                    <h3 class="panel-title">Detail</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Jenis Transaksi</label>
                        <div class="col-sm-3">
                             <input type="text" name="JenisTransaksi" value="{{ $data->JK }}" class="form-control" id="JenisTransaksi" disabled>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">No Peminjaman</label>
                        <div class="col-sm-3">
                            <input type="text" value="{{ $data->NoPeminjaman }}" class="form-control" id="NoPeminjaman" disabled >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">NPK Requester</label>
                        <div class="col-sm-3"> <input type="text" value="{{ $data->NPKRequester }} - {{ $data->Nama }}" class="form-control" id="NPKRequester" disabled>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Unit Kerja</label>
                        <div class="col-sm-3">
                            <input type="text" name="CostCenter" value="{{ $data->CostCenter }} - {{ $data->UnitKerja }}" class="form-control" id="cost-center" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">NPK Kepala Unit Kerja</label>
                        <div class="col-sm-3">
                            <input type="text" name="NPKAtasan" value="{{ $data->NPKAtasan }} - {{ $data->NamaAtasan }}" class="form-control" id="NPKAtasan" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal Transaksi</label>
                        <div class="col-sm-3">
                                <input type="text" value="{{ date('d-m-Y', strtotime($data->CreatedOn)) }}" name="TanggalTransaksi" id="TanggalTransaksi" class="form-control pull-right" readonly>
                        </div>
                        
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keperluan</label>
                        <div class="col-sm-3">
                            <textarea name="Deskripsi" class="form-control" id="Keperluan" rows="5" disabled>{{ $data->Deskripsi }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal Jatuh Tempo</label>
                        <div class="col-sm-3">
                                    <input type="text" value="{{ date('d-m-Y', strtotime($data->DueDate)) }}" name="DueDate" id="DueDate" class="form-control pull-right datepicker2" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-3">
                            <textarea name="keterangan" class="form-control" rows="5" readonly="">{{ $data->Keterangan }}</textarea>
                        </div>
                    </div>
                </div>
            </div>

            <form action="{{ url('keuangan/kas_besar/dokumen/upload/'.$IDKasBesar) }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">Attachment Dokumen</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Attachment</label>
                                    <div class="col-sm-4">
                                        <input type="file" name="file">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-4">
                                        <button type="submit" class="btn btn-success btn-sm">Upload</button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-10">
                                        <table class="table table-bordered table-striped table-condensed">
                                            <tr>
                                                <th>Filename</th>
                                                <th>Action</th>
                                            </tr>
                                            @foreach($attachments as $attachment)
                                            <tr>
                                                <td>{{ $attachment->FilePath }}</td>
                                                <td >
                                                    <a href="{{ url('storage/dokumen/'.$attachment->FilePath) }}" class="btn btn-primary btn-xs" style="color:#fff !important">View</a> 
                                                    <a href="{{ url('keuangan/kas_besar/dokumen/remove_attachment/'.$attachment->ID) }}" class="btn btn-danger btn-xs" style="color:#fff !important">Delete</a> 
                                                </td>
                                            </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
    <form id="form" method="POST" action="">
        <div class="panel panel-default form-horizontal">
            <div class="panel-heading">
            <h3 class="panel-title">List Dokumen</h3>
            </div>
            <div class="panel-body">
                <div class="panel panel-default form-horizontal">
                    <div class="panel-body">
                        
                <div class="form-group">
                    <label class="col-sm-2 control-label">No Dokumen</label>
                    <div class="col-sm-3">
                        <input id="NoDokumen" name="NoDokumen" type="text" value="" class="form-control">
                    </div>
                    <label class="col-sm-1 control-label">Tahun</label>
                    <div class="col-sm-2">
                        <input type="number" name="Tahun" value="{{ date('Y')}}" id="tahun" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Deskripsi</label>
                    <div class="col-sm-6">
                        <textarea id="Deskripsi" name="Deskripsi" class="form-control"></textarea>
                        </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Keterangan</label>
                    <div class="col-sm-6">
                        <textarea id="Keterangan" name="Keterangan" class="form-control"></textarea>
                       </div>
                </div>
                        <div class="dx-fieldset">

                            <div class="dx-field">
<!--  if not SUBMITTED -->
@if ($data->Status !=3)
                                <div id="add"></div>
@endif
                                <div id="pengembalian"></div>
@if ($data->Status !=3)
                                <div id="remove"></div>
@endif
                            </div>
                <div class="panel panel-default form-horizontal">
                    <div class="panel-body">
                        <div id="widget"></div>
                    </div>
                </div>

                <center>
                    <div class="dx-field">
<!--  if not SUBMITTED -->
@if ($data->Status !=3)
                        <div id="save"></div>
@endif
                        <div id="cancel"></div>
                    </div>
                </center>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(function(){

        $("#add").dxButton({
                text: "Add",
                type: "success",
                useSubmitBehavior: false,
                onClick: function(e){
                    var status = "{{ $data->Status }}";
                    if($('#NoDokumen').val() == '') {
                        alert("Masukan no dokumen.");
                    }else{
                        var url = '{{ url("keuangan/kas_besar/dokumen/submitstep2/$IDKasBesar") }}'; 
                        var forms = $("#form").serialize()+ "&action=add";
                        $.ajax({
                               type: "POST",
                               url: url,
                               data: forms , 

                               success: function(data)
                               {
                                    var obj = JSON.parse(data);
                                    console.log(obj);
                                    if(obj.status == 200){
                                        DevExpress.ui.notify({
                                        message: obj.data,
                                        position: {
                                            my: "center top",
                                            at: "center top"
                                        }
                                        }, "success", 3000);
                                    }else{
                                        DevExpress.ui.notify({
                                        message: obj.data,
                                        position: {
                                            my: "center top",
                                            at: "center top"
                                        }
                                        }, "warning", 3000);
                                    }
                                    
                               }
                             });


                        dataGrid.refresh();
                        $('#NoDokumen').val('');
                        $('#Deskripsi').val('');
                        $('#Keterangan').val('');
                    }
                }
            });
        var pengembalianButton = $("#pengembalian").dxButton({
                text: "Pengembalian",
                type: "default",
                height: 34,
                width: 140,
                disabled: true,
                onClick: function () {
                    $.each(dataGrid.getSelectedRowsData(), function() {
                        
                        var ID = this.ID;
                        var url = "{{ url('keuangan/kas_besar/dokumen/pengembalian') }}"; 
                        var forms = "ID="+ID+"&action=pengembalian";
                        pengembalianButton.option('text', 'Loading...');
                        $.ajax({
                               type: "POST",
                               url: url,
                               data: forms , 

                               success: function(data)
                               {
                                    DevExpress.ui.notify({
                                        message: "Berhasil mengubah dokumen.",
                                        position: {
                                            my: "center top",
                                            at: "center top"
                                        }
                                    }, "success", 3000);
                               }
                             });
                        pengembalianButton.option('text', 'Pengembalian');
                        dataGrid.refresh();
                        console.log('refresh');
                    });

                }
            }).dxButton("instance"); 
        var deleteButton = $("#remove").dxButton({
                text: "Remove",
                type: "danger",
                height: 34,
                width: 140,
                disabled: true,
                onClick: function () {
                    $.each(dataGrid.getSelectedRowsData(), function() {
                        
                        var ID = this.ID;
                        var url = "{{ url('keuangan/kas_besar/dokumen/remove') }}"; 
                        var forms = "ID="+ID+"&action=remove";
                        deleteButton.option('text', 'Loading...');
                        $.ajax({
                               type: "POST",
                               url: url,
                               data: forms , 

                               success: function(data)
                               {
                               }
                             });
                        deleteButton.option('text', 'Remove');
                        dataGrid.refresh();
                        console.log('refresh');
                    });

                }
            }).dxButton("instance"); 

        var saveButton = $("#save").dxButton({
                text: "Submit",
                type: "success",
                height: 34,
                width: 140,
                onClick: function () {
                    location.href = '{{ url("keuangan/kas_besar/dokumen/save/$IDKasBesar") }}';
                }
            }).dxButton("instance"); 
        var cancelButton = $("#cancel").dxButton({
                text: "Close",
                type: "normal",
                height: 34,
                width: 140,
                onClick: function () {
                        
                        location.href = "{{ url('keuangan/kas_besar/kasbesar/?tab=dokumen') }}";

                }
            }).dxButton("instance");  

        var dataSource = new DevExpress.data.CustomStore({
        key: "ID",
        load: function (loadOptions) {
            var deferred = $.Deferred(),
            args = {};
            
            if (loadOptions.sort) {
                args.orderby = loadOptions.sort[0].selector;
                if (loadOptions.sort[0].desc)
                    args.orderby += " desc";
            }
            
            args.skip = loadOptions.skip || 0;
            args.take = loadOptions.take || 12;
            
            $.ajax({
                url: "{{ url("keuangan/kas_besar/dokumen/getdokumen/$IDKasBesar") }}",
                data: args,
                success: function(response) {
                    console.log(response);
                    deferred.resolve(response.data, { totalCount: response.totalCount });
                },
                error: function() {
                    deferred.reject("Data Loading Error");
                },
                timeout: 5000
            });
            
            return deferred.promise();
        }
    });

        var dataGrid = $('#widget').dxDataGrid({
            dataSource: dataSource,
            onSelectionChanged: function(data){

                @if ($data->Status !=3)
                deleteButton.option('disabled', !(data.selectedRowsData.length))
                @endif
                @if ($data->Status !=2)
                pengembalianButton.option('disabled', !(data.selectedRowsData.length))
                @endif
            },
            selection:{mode:'multiple'},
            columns: [
            {dataField:'NoDokumen',caption:'No Dokumen'},
            {dataField:'TahunDokumen',caption:'Tahun'},
            {dataField:'Deskripsi',caption:'Deskripsi'},
            {dataField:'Keterangan',caption:'Keterangan'},
            {dataField:'CreatedBy',caption:'CreatedBy'},
            {dataField:'CreatedOn',caption:'CreatedOn',dataType: 'date',format: 'dd/MM/yyyy'},
            {dataField:'ReturnedBy',caption:'ReturnedBy'},
            {dataField:'ReturnedOn',caption:'ReturnedOn',dataType: 'date',format: 'dd/MM/yyyy'},
            ]
        }).dxDataGrid('instance');




    });
</script>

@endsection
