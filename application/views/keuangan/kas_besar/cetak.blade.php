<!DOCTYPE html>
<html>
<head>
    <title>Form Peminjaman Kas Besar</title>
    <style type="text/css">
    body {font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;font-size: medium;}
        #fp {
            
            border-collapse: collapse;
            width: 100%;
        }

        #fp td, #fp th {
            border: 1px solid #777;
            padding: 8px;
        }
    </style>
</head>
<body>
    <script type="text/javascript"></script>
    <div class="container" style="width:700px;margin:auto;padding:10px">
        <div class="row" style="text-align: center">
            <h2>FORM PEMINJAMAN KAS BESAR</h2>
        </div>
        <p>Pada hari {{ hari_indo(date('D', strtotime($data->CreatedOn))) }} tanggal {{ tanggal_indo(date('Y-m-d', strtotime($data->CreatedOn))) }} saya yang bertanda tangan dibawah ini :</p>
                <br>
        <table>
            <tr>
                <td style="width:150px">Nama</td>
                <td style="width:250px">: {{ $data->Nama }}</td>
            </tr>
            <tr>
                <td>NPK</td>
                <td>: {{ $data->NPKRequester }}</td>
            </tr>
            <tr>
                <td>Unit Kerja</td>
                <td>: {{ @$data->UnitKerja }}</td>
            </tr>
        </table>

        <br>
        <p>Telah meminjam Kas besar senilai Rp. {{ number_format($data->Nominal,0,',','.') }} (<span style="font-style: italic;">{{terbilang($data->Nominal)}}</span>) untuk keperluan {{ @$data->Deskripsi }} dan saya berkewajiban untuk menyetor kembali pada tanggal {{ tanggal_indo(date('Y-m-d', strtotime($data->DueDate)))}}</p>
        <p>Demikian disampaikan, apabila dalam waktu 30 hari setelah tanggal peminjaman, saya belum menyetor kembali, maka saya bersedia untuk dilakukan pemotongan atas gaji saya di bulan berjalan.
        </p>


        <table>
            <tr>
                <td rowspan="3" style="width: 500px">
                </td>
                <td style="width: 200px">
                    Bontang, {{ tanggalSekarang() }}
                    <br>
                </td>
            </tr>
            <tr>
                <td style="height:100px"></td>
            </tr>
            <tr>
                <td style="text-align: center;"><span style="text-decoration: underline;">{{ $data->Nama }}</span> <br> <span>{{ $data->NPKRequester }}</span></td>
            </tr>
        </table>

    </div>
    <script type="text/javascript">window.print();window.close();</script>
</body>
</html>
