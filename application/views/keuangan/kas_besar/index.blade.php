@extends('templates.admin')

@section('heading')
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
<link rel="stylesheet" type="text/css"  href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
<script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
<script src="{{ url('asset/js/phpjs.js') }}"></script>
@endsection 

@section('contentheader')
<section class="content-header">
    <h1>List Penggunaan Kas Besar</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Keuangan</li>
        <li>Kas Besar</li>
        <li class="active">List Penggunaan Kas Besar</li>
    </ol>
</section>
@endsection 

@section('content')
<div class="box-header with-border">
    <h3 class="box-title">List Penggunaan Kas Besar</h3>
</div>

<div class="box-body">
    <?php if(flashdata('tab')){ $tab = true; }else{ $tab = false;} ?>

    @if(flashdata('success'))
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error!</strong> {{ flashdata('error') }}
        </div>

        @elseif(flashdata('warning'))

        <div class="alert alert-warning">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Warning!</strong> {{ flashdata('warning') }}
        </div>

        @elseif(flashdata('info'))

        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Info!</strong> {{ flashdata('info') }}
        </div>
        @endif
        @if(isset($_GET['status']) && $_GET['status'] == 'success')
        <div class="alert alert-success">
            <p>Dokumen Berhasil di Update</p>
        </div>
        @elseif(isset($_GET['status']) && $_GET['status'] == 'fail')
        <div class="alert alert-danger">
            <strong>Error</strong> Dokumen Gagal di Update
        </div>
        @endif
        <div role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#kasbesar" aria-controls="home" role="tab" data-toggle="tab">Kas Besar</a>
                </li>
                <li role="presentation">
                    <a href="#peminjamandokumen" aria-controls="tab" role="tab" data-toggle="tab">Peminjaman Dokumen</a>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="kasbesar" style="margin-top:15px">
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                          <!-- small box -->

                      </div>        
                      <div class="col-lg-6 col-xs-6">
                        <div class="box box-primary">
                            <div class="box-header with-border">
                              <center><h3 class="box-title">Saldo Kas Besar</h3></center>


                              <!-- /.box-tools -->
                          </div>
                          <!-- /.box-header -->
                          <div class="box-body">
                              <center><h1>Rp.{{ number_format($saldo) }}</h1></center>
                          </div>
                          <!-- /.box-body -->
                      </div>
                  </div>        
                  <div class="col-lg-3 col-xs-6">

                  </div>

                  <!-- ./col -->
              </div>
            <div id="btn-create"></div>
            <div id="showButton"></div>
            <div id="btn-view"></div>
            <div id="btn-cetak"></div>
            <div id="btn-send-reminder"></div>

            <div id="gridContainer"></div>
        </div>
        <div role="tabpanel" class="tab-pane" id="peminjamandokumen" style="margin-top:15px">
        <div id="btn-create-doc"></div>
        <div id="btn-view-doc"></div>
        <div id="btn-cancel-doc"></div>
        <div id="btn-cetak-doc"></div>
        <div id="gridContainer-doc"></div>
    </div>
</div>
</div>


</div>

<div id="popup">
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(function(){

        var date = new Date();
        date.setDate(date.getDate() - 1);
        $('.datepicker').datepicker({autoclose: true, format:'dd-mm-yyyy'});

        var pengembalianButton = $("#showButton").dxButton({
            text: "Pengembalian", 
            disabled: true,
            onClick: function () {
                $.each(dataGrid.getSelectedRowsData(), function() {


                    var ID = this.ID;
                    var Nominal = this.Nominal;
                    var LifeCycle = this.LifeCycle;
                    var url = "{{ url('keuangan/kas_besar/kasbesar/pengembalian') }}"; 
                    $("#popup").dxPopup({
                        title: "Pengembalian",
                        visible: true,
                        width: 300,
                        height: 250,
                        contentTemplate: function(container) {
            

                            $('<form action="'+url+'" class="form-horizontal" method="POST"><div class="modal-content"><label class="control-label">Tanggal Kembali</label></div><div id="inputan"></div></form>').appendTo(container);

                            $('<input name="tanggal_kembali" class="form-control" placeholder="" required="required">')
                            .appendTo("#inputan").datepicker({
                                format: 'dd/mm/yyyy'
                            }).datepicker("setDate",'now');
                            $('<input name="ID" value="'+ID+'" type="hidden">')
                            .appendTo("#inputan");
                            $('<input name="nominal" value="'+Nominal+'" type="hidden">')
                            .appendTo("#inputan");
                            $('<input name="LifeCycle" value="'+LifeCycle+'" type="hidden">')
                            .appendTo("#inputan");

                            $('<div class="modal-footer"><button type="submit" class="btn btn-danger btn-sm">Submit</button></div>').appendTo("#inputan");
                            
                            $('form').submit(function() {
                              $(this).find("button[type='submit']").prop('disabled',true);
                            });

                        }
                    });

                }); 
            }
        }).dxButton("instance");


        var Status = [{
            "ID": 0,
            "Name": "Draft"
        }, {
            "ID": 1,
            "Name": "Approved"
        }];
        var LifeCycle = [{
            "ID": 0,
            "Name": "Closed"
        }, {
            "ID": 1,
            "Name": "Open"
        }];
        var UnitKerja = new DevExpress.data.CustomStore({
            loadMode: "raw",
            load: function() {
                return $.getJSON("{{ url("keuangan/kas_besar/kasbesar/select/Ms_UnitKerja/UnitKerja") }}");
            }
        });
        var dataSource = new DevExpress.data.CustomStore({
            key: "ID",
            load: function (loadOptions) {
                var deferred = $.Deferred(),
                args = {};
                
                if (loadOptions.sort) {
                    args.orderby = loadOptions.sort[0].selector;
                    if (loadOptions.sort[0].desc)
                        args.orderby += " desc";
                }
                
                if (loadOptions.filter != 'undefined') {
                    args.filter = JSON.stringify(loadOptions.filter);
                }
                args.skip = loadOptions.skip || 0;
                args.take = loadOptions.take || 12;
                
                $.ajax({
                    url: "{{ url("keuangan/kas_besar/kasbesar/get") }}",
                    data: args,
                    success: function(response) {
                        console.log(response);
                        deferred.resolve(response.data, { totalCount: response.totalCount });
                    },
                    error: function() {
                        deferred.reject("Data Loading Error");
                    },
                    timeout: 5000
                });
                
                return deferred.promise();
            }
        });
        $("#btn-create").dxButton({
            text: "Create",
            height: 34,
            width: 140,
            onClick: function () {
                location.href = "{{ url('keuangan/kas_besar/kasbesar/input') }}";
            }
        }).dxButton("instance");
        var approveButton = $("#btn-approve").dxButton({
            text: "Approve",
            height: 34,
            width: 140,
            disabled: true,
            onClick: function () {
                $.each(dataGrid.getSelectedRowsData(), function() {

                    var ID = this.ID;
                    var url = "{{ url('keuangan/kas_besar/kasbesar/approve') }}"; 
                    var forms = "ID="+ID+"&action=approve";
                    approveButton.option('text', 'Loading...');
                    $.ajax({
                       type: "POST",
                       url: url,
                       data: forms , 

                       success: function(data)
                       {
                        DevExpress.ui.notify({
                            message: "Sukses, Dokumen telah di Approve",
                            position: {
                                my: "center top",
                                at: "center top"
                            }
                        }, "success", 3000);
                    }
                });
                    approveButton.option('text', 'Approve');
                    dataGrid.refresh();
                    console.log('refresh');
                });

            }
        }).dxButton("instance");

        var viewButton = $("#btn-view").dxButton({
            text: "View",
            height: 34,
            width: 140,
            disabled: true,
            onClick: function () {
                $.each(dataGrid.getSelectedRowsData(), function() {

                    var ID = this.ID;
                    location.href = "{{ url('keuangan/kas_besar/kasbesar/view/') }}"+ID;
                });

            }
        }).dxButton("instance");
        var cetakButton = $("#btn-cetak").dxButton({
            text: "Cetak",
            height: 34,
            width: 140,
            disabled: true,
            onClick: function () {
                $.each(dataGrid.getSelectedRowsData(), function() {

                    var ID = this.ID;

                    window.open("{{ url('keuangan/kas_besar/kasbesar/cetak/') }}"+ID,"_blank")
                });

            }
        }).dxButton("instance");
        var sendreminderButton = $("#btn-send-reminder").dxButton({
            text: "Send Reminder",
            height: 34,
            width: 140,
            disabled: true
        }).dxButton("instance");

        var dataGrid = $("#gridContainer").dxDataGrid({
            dataSource : dataSource,

            paging: {
                pageSize: 8
            },
                // columnHidingEnabled: false,
                
                remoteOperations: {sorting: true,paging: true,filtering:true},
                "export" : {enable:true, fileName : "{{ $table }}"},
                selection:{mode:'multiple'},
                onSelectionChanged: function(data){
                    pengembalianButton.option('disabled', true);
                    $.each(data.selectedRowsData, function() {
                        console.log(data.selectedRowsData[0].TanggalPengembalian);
                        if((data.selectedRowsData.length != 1)){
                            pengembalianButton.option('disabled', true)
                        }else{

                            if(data.selectedRowsData[0].JenisTransaksi != 'Peminjaman'){
                                pengembalianButton.option('disabled', true)
                            }else if(data.selectedRowsData[0].TanggalPengembalian != null){
                                pengembalianButton.option('disabled', true)
                            }else{
                                pengembalianButton.option('disabled', false)
                            }
                        }
                    });
                    viewButton.option('disabled', !(data.selectedRowsData.length == 1))
                    cetakButton.option('disabled', !(data.selectedRowsData.length == 1))

                },
                allowColumnReordering : true,
                allowColumnResizing : true,
                searchPanel : {visible : true},
                headerFilter:{visible:true},
                filterRow:{visible:true,applyFilter:'auto'},
                groupPanel : {visible:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: false,visible: true},
                columns : [          
                
            {dataField:'No',caption:'No',allowEditing:false/*,allowEditing:false*/}, 
            {dataField:'JenisTransaksi',caption:'Jenis Transaksi'},

        {dataField:'NoPeminjaman',caption:'No Peminjaman',allowEditing:false/*,allowEditing:false*/},
        {dataField:'CreatedOn',caption:'CreatedOn',dataType: 'date',format: 'dd/MM/yyyy'},

        {dataField:'DueDate',caption:'Last Reminder',name: "DueDate",dataType: "date"},

        {caption:'Nominal', calculateCellValue:function(data){
            return 'Rp. ' + number_format(data.Nominal);
        }},
        {dataField:'TanggalPengembalian',caption:'Tanggal Pengembalian',dataType: 'date',format: 'dd/MM/yyyy'},
        {dataField:'NPKRequester',caption:'NPK Requester'},
        {dataField:'Nama',caption:'Nama'},
        {dataField:'Deskripsi',caption:'Keperluan'},

        {dataField:'LifeCycle',caption:'Life Cycle',
        lookup: {
            dataSource: LifeCycle,
            displayExpr: "Name",
            valueExpr: "ID",
            name : "Status"
        }},

        {dataField:'CostCenter',caption:'Unit Kerja',
        lookup: {
            dataSource: UnitKerja,
            displayExpr: "UnitKerja",
            valueExpr: "CostCenter",
            name: "CostCenter",
            searchEnabled: true
        }},
        {dataField:'Status',caption:'Status',
        lookup: {
            dataSource: Status,
            displayExpr: "Name",
            valueExpr: "ID",
            name : "Status"
        }},

        ],
        columnAutoWidth: true,
        columnChooser: {enabled: true},
        columnFixing: {enabled: true},
        rowAlternationEnabled: true,
        onEditorPreparing: function(e) {
            if (e.dataField == "Keterangan") {
                    e.editorName = "dxTextArea"; // Changes the editor's type
                    e.editorOptions.onValueChanged = function (args) {
                        // Implement your logic here

                        e.setValue(args.value); // Updates the cell value
                    }
                }
            }
        }).dxDataGrid("instance");
});
    //nextgrid
</script>
<script type="text/javascript">
   $(function(){
    $('a[href="#peminjamandokumen"]').on('shown.bs.tab', function (e) {
        var Status = [{
            "ID": 0,
            "Name": "DRAFT"
        }, {
            "ID": 1,
            "Name": "APPROVED"
        }, {
            "ID": 2,
            "Name": "CANCELED"
        }, {
            "ID": 3,
            "Name": "SUBMITTED"
        }];
        var LifeCycle = [{
            "ID": 0,
            "Name": "FALSE"
        }, {
            "ID": 1,
            "Name": "TRUE"
        }];

        var UnitKerja = new DevExpress.data.CustomStore({
            loadMode: "raw",
            load: function() {
                return $.getJSON("{{ url("keuangan/kas_besar/dokumen/select/Ms_UnitKerja/UnitKerja") }}");
            }
        });

        var dataSourceDoc = new DevExpress.data.CustomStore({
            key: "ID",
            load: function (loadOptions) {
                var deferred = $.Deferred(),
                args = {};

                if (loadOptions.sort) {
                    args.orderby = loadOptions.sort[0].selector;
                    if (loadOptions.sort[0].desc)
                        args.orderby += " desc";
                }

                if (loadOptions.filter != 'undefined') {
                    args.filter = JSON.stringify(loadOptions.filter);
                }
                args.skip = loadOptions.skip || 0;
                args.take = loadOptions.take || 12;

                $.ajax({
                    url: "{{ url("keuangan/kas_besar/dokumen/get") }}",
                    data: args,
                    success: function(response) {
                        console.log(response);
                        deferred.resolve(response.data, { totalCount: response.totalCount });
                    },
                    error: function() {
                        deferred.reject("Data Loading Error");
                    },
                    timeout: 5000
                });

                return deferred.promise();
            }
        });
        $("#btn-create-doc").dxButton({
            text: "Create",
            height: 34,
            width: 140,
            onClick: function () {
                location.href = "{{ url('keuangan/kas_besar/dokumen/input') }}";
            }
        }).dxButton("instance");
        var approveButtonDoc = $("#btn-approve-doc").dxButton({
            text: "Approve",
            height: 34,
            width: 140,
            disabled: true,
            onClick: function () {
                $.each(dataGridDoc.getSelectedRowsData(), function() {

                    var ID = this.ID;
                    var url = "{{ url('keuangan/kas_besar/dokumen/approve') }}"; 
                    var forms = "ID="+ID+"&action=approve";
                    approveButtonDoc.option('text', 'Loading...');
                    $.ajax({
                       type: "POST",
                       url: url,
                       data: forms , 

                       success: function(data)
                       {
                        DevExpress.ui.notify({
                            message: "Sukses, Dokumen telah di Approve",
                            position: {
                                my: "center top",
                                at: "center top"
                            }
                        }, "success", 3000);
                    }
                });
                    approveButtonDoc.option('text', 'Approve');
                    dataGridDoc.refresh();
                    console.log('refresh');
                });

            }
        }).dxButton("instance");
        var viewButtonDoc = $("#btn-view-doc").dxButton({
            text: "View",
            height: 34,
            width: 140,
            disabled: true,
            onClick: function () {
                $.each(dataGridDoc.getSelectedRowsData(), function() {

                    var ID = this.ID;
                    location.href = "{{ url('keuangan/kas_besar/dokumen/step2/') }}"+ID;
                });

            }
        }).dxButton("instance");
        var cancelButtonDoc = $("#btn-cancel-doc").dxButton({
            text: "Cancel",
            height: 34,
            width: 140,
            disabled: true,
            onClick: function () {
                $.each(dataGridDoc.getSelectedRowsData(), function() {

                    var ID = this.ID;
                    location.href = "{{ url('keuangan/kas_besar/dokumen/cancel_dokumen/') }}"+ID;
                });

            }
        }).dxButton("instance");

        var dataGridDoc = $("#gridContainer-doc").dxDataGrid({
            dataSource : dataSourceDoc,

            paging: {
                pageSize: 8
            },
                // columnHidingEnabled: false,

                remoteOperations: {sorting: true,paging: true,filtering:true},
                "export" : {enable:true, fileName : "{{ $table }}"},
                selection:{mode:'multiple'},
                onSelectionChanged: function(data){
                    
                    cancelButtonDoc.option('disabled', true);
                    $.each(data.selectedRowsData, function() {
                        console.log(data.selectedRowsData[0].TanggalPengembalian);
                        if((data.selectedRowsData.length != 1)){
                            cancelButtonDoc.option('disabled', true)
                        }else{
                            console.log(data.selectedRowsData[0].Status);
                            if(data.selectedRowsData[0].Status == 2){
                                cancelButtonDoc.option('disabled', true)
                            }else{
                                cancelButtonDoc.option('disabled', false)
                            }
                        }
                    });
                    viewButtonDoc.option('disabled', !(data.selectedRowsData.length == 1))
                },                
                searchPanel : {
                    visible : true,
                    placeholder: 'Search No Dokumen',
                    width: 300,
                    highlightSearchText: false
                },
                headerFilter:{visible:true},
                groupPanel : {visible:true},
                popup : {
                    title:'{{ $table }}',showTitle : true, width : 700, height : 345, position :{my:'top',at:'top',of:window}
                },
                pager: {allowedPageSizes: [5, 10, 15, 30],showInfo: true,showNavigationButtons: true,showPageSizeSelector: false,visible: true},
                columns : [
            {dataField:'No',caption:'No',allowEditing:false/*,allowEditing:false*/},            
            {dataField:'JenisTransaksi',caption:'Jenis Transaksi'},

        {dataField:'NoPeminjaman',caption:'No Peminjaman',allowEditing:false/*,allowEditing:false*/},
        {dataField:'CreatedOn',caption:'CreatedOn',dataType: 'date',format: 'dd/MM/yyyy'},
        {dataField:'NPKRequester',caption:'NPK Requester'},
        {dataField:'Nama',caption:'Nama'},

        {dataField:'Status',caption:'Status',
        lookup: {
            dataSource: Status,
            displayExpr: "Name",
            valueExpr: "ID",
            name : "Status"
        }},
        {dataField:'Deskripsi',caption:'Keperluan'},



        {dataField:'LifeCycle',caption:'Life Cycle',
        lookup: {
            dataSource: LifeCycle,
            displayExpr: "Name",
            valueExpr: "ID",
            name : "Status"
        }},
        {dataField:'CostCenter',caption:'Unit Kerja',
        lookup: {
            dataSource: UnitKerja,
            displayExpr: "UnitKerja",
            valueExpr: "CostCenter",
            name: "CostCenter",
            searchEnabled: true
        }},

        {dataField:'DueDate',caption:'Last Reminder',name: "DueDate",dataType: "date"}

        ],

        columnAutoWidth: true,
        columnChooser: {enabled: false},
        columnFixing: {enabled: true},
        rowAlternationEnabled: true,
        masterDetail: {
            enabled: true,
            template: function(container, options) { 
                var details = new DevExpress.data.CustomStore({
                    key: "ID",
                    load: function (loadOptions) {
                        var deferred = $.Deferred(),
                        args = {};

                        if (loadOptions.sort) {
                            args.orderby = loadOptions.sort[0].selector;
                            if (loadOptions.sort[0].desc)
                                args.orderby += " desc";
                        }

                        if (loadOptions.filter != 'undefined') {
                            args.filter = JSON.stringify(loadOptions.filter);
                        }
                        args.skip = loadOptions.skip || 0;
                        args.take = loadOptions.take || 10;

                        $.ajax({
                                url: "<?php echo base_url('keuangan/kas_besar/dokumen/get_doc/'); ?>" + options.data.ID, // cukup mengubah ini
                                data: args,
                                success: function(response) {
                                    console.log(response);
                                    deferred.resolve(response.data, { totalCount: response.totalCount });
                                },
                                error: function() {
                                    deferred.reject("Data Loading Error");
                                },
                                timeout: 5000
                            });

                        return deferred.promise();
                    }
                });
                $("<div>")
                .dxDataGrid({
                    dataSource: details,
                    columns : [ 
                    { 
                        dataField: "NoDokumen",
                        caption: "No Dokumen",
                    },
            {dataField:'TahunDokumen',caption:'Tahun'},
            {dataField:'Deskripsi',caption:'Deskripsi'},
            {dataField:'Keterangan',caption:'Keterangan'},
            {dataField:'CreatedOn',caption:'CreatedOn',dataType: 'date',format: 'dd/MM/yyyy'},
            {dataField:'ReturnedOn',caption:'ReturnedOn',dataType: 'date',format: 'dd/MM/yyyy'},
                    ]
                }).appendTo(container);
            }
        },
    }).dxDataGrid("instance");
});
@if($tab)
    $('a[href="#peminjamandokumen"]').tab('show');
@endif
});
/**/
</script>
@endsection