@extends('templates.admin')

@section('heading')
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.spa.css" />
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.common.css" />
<link rel="stylesheet" type="text/css" href="{{url('/asset/')}}devextreme/17/css/dx.light.css" />
<script src="{{url('/asset/')}}devextreme/17/js/dx.all.js"></script>
<style type="text/css">
    h3.panel-title{font-size: 14px !important}
    .dx-fileuploader-show-file-list .dx-fileuploader-files-container{padding-top:0px}
    .dx-fileuploader-wrapper{padding:0px;}
    .form-horizontal .control-label{text-align: left !important;padding-left:20px;}
</style>
@endsection 

@section('contentheader')
<section class="content-header">
    <h1>Input Penggunaan Kas Besar</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Keuangan</li>
        <li>Kas Besar</li>
        <li class="active">Input Penggunaan Kas Besar</li>
    </ol>
</section>
@endsection 

@section('content')
<div class="box-header with-border">
    <h3 class="box-title">Input Penggunaan Kas Besar</h3>
</div>
<div class="box-body">
        @if(flashdata('success'))
            <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <i class="icon fa fa-check"></i> {{ flashdata('success') }}</div>

        @elseif(flashdata('error'))

            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Error!</strong> {{ flashdata('error') }}
            </div>

        @elseif(flashdata('warning'))

            <div class="alert alert-warning">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Warning!</strong> {{ flashdata('warning') }}
            </div>

        @elseif(flashdata('info'))

            <div class="alert alert-info">
                <a href="#" class="close" data-dismiss="alert">&times;</a>
                <strong>Info!</strong> {{ flashdata('info') }}
            </div>
        @endif
            <div class="panel panel-info form-horizontal">
                <div class="panel-heading">
                    <h3 class="panel-title">Detail</h3>
                </div>
                <div class="panel-body">

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Jenis Transaksi</label>
                        <div class="col-sm-3">
                             <input type="text" name="JenisTransaksi" value="{{ $data->JK }}" class="form-control" id="JenisTransaksi" disabled>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">No Peminjaman</label>
                        <div class="col-sm-3">
                            <input type="text" value="{{ $data->NoPeminjaman }}" class="form-control" id="NoPeminjaman" disabled>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">NPK Requester</label>
                        <div class="col-sm-3"> <input type="text" value="{{ $data->NPKRequester }}" class="form-control" id="NPKRequester" disabled>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Unit Kerja</label>
                        <div class="col-sm-3">
                            <input type="text" name="CostCenter" value="{{ $data->CostCenter }}" class="form-control" id="cost-center" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">NPK Kepala Unit Kerja</label>
                        <div class="col-sm-3">
                            <input type="text" name="NPKAtasan" value="{{ $data->NPKAtasan }}" class="form-control" id="NPKAtasan" readonly>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Nominal</label>
                        <div class="col-sm-3">
                            <input type="text" name="NPKAtasan" value="{{ $data->Nominal }}" class="form-control" id="NPKAtasan" readonly>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal Transaksi</label>
                        <div class="col-sm-3">
                                <input type="text" value="{{ date('d-m-Y', strtotime($data->CreatedOn) ) }}" name="TanggalTransaksi" id="TanggalTransaksi" class="form-control pull-right" readonly>
                        </div>
                        
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keperluan</label>
                        <div class="col-sm-3">
                            <textarea name="Deskripsi" value="{{ $data->Deskripsi }}" class="form-control" id="Keperluan" rows="5" disabled></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tanggal Jatuh Tempo</label>
                        <div class="col-sm-3">
                                    <input type="text" value="{{ date('d-m-Y', strtotime($data->DueDate) ) }}" name="DueDate" id="DueDate" class="form-control pull-right datepicker2" disabled>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-3">
                            <textarea name="keterangan" class="form-control" rows="5" readonly="">{{ $data->Keterangan }}</textarea>
                        </div>
                    </div>

            <center>
                <a href="{{ url('keuangan/kas_besar/kasbesar/index') }}" class="btn btn-success btn-sm" style="color:#fff">
                    <i class="fa fa-list"></i> Back to lists
                </a>

            </center>
                </div>
            </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(function(){

        var dxNoDokumen = $("#NoDokumen-validation").dxTextBox({
            name: "NoDokumen",
            disabled: false,
        }).dxTextBox("instance");;
        var dxDeskripsi = $("#Deskripsi-validation").dxTextArea({
            name: "Deskripsi",
            disabled: false
        }).dxTextArea("instance");;    
        var dxKeterangan = $("#Keterangan-validation").dxTextArea({
            name: "Keterangan",
            disabled: false
        }).dxTextArea("instance");;

        $("#add").dxButton({
                text: "Add",
                type: "success",
                useSubmitBehavior: false,
                onClick: function(e){
                    var url = '{{ url("keuangan/kas_besar/dokumen/submitstep2/$IDKasBesar") }}'; 
                    var forms = $("#form").serialize()+ "&action=add";
                    $.ajax({
                           type: "POST",
                           url: url,
                           data: forms , 

                           success: function(data)
                           {
                                DevExpress.ui.notify({
                                    message: "Dokumen berhasil di tambahkan.",
                                    position: {
                                        my: "center top",
                                        at: "center top"
                                    }
                                }, "success", 3000);
                           }
                         });


                    dataGrid.refresh();
                    dxNoDokumen.option('value','');
                    dxDeskripsi.option('value','');
                    dxKeterangan.option('value','');
                }
            });
        var deleteButton = $("#remove").dxButton({
                text: "Remove",
                height: 34,
                width: 140,
                disabled: true,
                onClick: function () {
                    $.each(dataGrid.getSelectedRowsData(), function() {
                        
                        var ID = this.ID;
                        var url = "{{ url('keuangan/kas_besar/dokumen/remove') }}"; 
                        var forms = "ID="+ID+"&action=remove";
                        deleteButton.option('text', 'Loading...');
                        $.ajax({
                               type: "POST",
                               url: url,
                               data: forms , 

                               success: function(data)
                               {
                                    DevExpress.ui.notify({
                                        message: "Berhasil menghapus dokumen.",
                                        position: {
                                            my: "center top",
                                            at: "center top"
                                        }
                                    }, "success", 3000);
                               }
                             });
                        deleteButton.option('text', 'Remove');
                        dataGrid.refresh();
                        console.log('refresh');
                    });

                }
            }).dxButton("instance"); 
        var saveButton = $("#save").dxButton({
                text: "Save",
                type: "success",
                height: 34,
                width: 140,
                onClick: function () {
                        DevExpress.ui.notify({
                            message: "Berhasil menyimpan.",
                            position: {
                                my: "center top",
                                at: "center top"
                            }
                        }, "success", 3000);
                        location.href = "{{ url('keuangan/kas_besar/kasbesar/') }}";

                }
            }).dxButton("instance"); 
        var cancelButton = $("#cancel").dxButton({
                text: "Cancel",
                type: "danger",
                height: 34,
                width: 140,
                onClick: function () {
                        var url = "{{ url('keuangan/kas_besar/dokumen/cancel') }}"; 
                        var forms = "ID="+{{ $IDKasBesar }}+"&action=cancel";
                        cancelButton.option('text', 'Loading...');
                        $.ajax({
                               type: "POST",
                               url: url,
                               data: forms , 

                               success: function(data)
                               {
                                    DevExpress.ui.notify({
                                        message: "Berhasil menghapus dokumen.",
                                        position: {
                                            my: "center top",
                                            at: "center top"
                                        }
                                    }, "success", 3000);
                               }
                             });
                        location.href = "{{ url('keuangan/kas_besar/dokumen/input') }}";

                }
            }).dxButton("instance");  
        var dataSource = new DevExpress.data.CustomStore({
        key: "ID",
        load: function (loadOptions) {
            var deferred = $.Deferred(),
            args = {};
            
            if (loadOptions.sort) {
                args.orderby = loadOptions.sort[0].selector;
                if (loadOptions.sort[0].desc)
                    args.orderby += " desc";
            }
            
            args.skip = loadOptions.skip || 0;
            args.take = loadOptions.take || 12;
            
            $.ajax({
                url: "{{ url("keuangan/kas_besar/dokumen/getdokumen/$IDKasBesar") }}",
                data: args,
                success: function(response) {
                    console.log(response);
                    deferred.resolve(response.data, { totalCount: response.totalCount });
                },
                error: function() {
                    deferred.reject("Data Loading Error");
                },
                timeout: 5000
            });
            
            return deferred.promise();
        }
    });

        var dataGrid = $('#widget').dxDataGrid({
            dataSource: dataSource,
            onSelectionChanged: function(data){
                deleteButton.option('disabled', !(data.selectedRowsData.length))
            },
            selection:{mode:'multiple'},
            columns: [
            {dataField:'NoDokumen',caption:'No Dokumen'},
            {dataField:'Deskripsi',caption:'Deskripsi'},
            {dataField:'Keterangan',caption:'Keterangan'},
            ]
        }).dxDataGrid('instance');




    });
</script>
@endsection
