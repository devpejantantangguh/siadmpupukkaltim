<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>MitraKu | Log in</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link id="favicon" rel="shortcut icon" href="{{ url('asset/image/favicon.ico') }}" type="image/png" />
    <script src="{{url('/asset/')}}js/jquery.min.js"></script>
    <script type="text/javascript" src="{{url('/asset/')}}js/vue.js"></script>
    <script type="text/javascript" src="{{url('/asset/')}}js/axios.min.js"></script>
    <link rel="stylesheet" href="{{url('/asset/')}}css/bootstrap.min.css">
    <link rel="stylesheet" href="{{url('/asset/')}}css/animate.css">
    <link rel="stylesheet" href="{{url('/asset/')}}font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{url('/asset/')}}ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="{{url('/asset/')}}css/AdminLTE.css">
    <link rel="stylesheet" href="{{url('/asset/')}}css/_all-skins.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style type="text/css">
        .login-box, .register-box {
            width: 500px;
            margin: 7% auto;
            background: rgb(32, 103, 168,0.5);
            padding: 50px;
            border-radius: 10px;
        }
        .login-page {
            background: url('asset/image/bg.jpg');
            background-repeat: no-repeat;
            background-size: cover;
        }
    </style>
</head>

<body class="hold-transition login-page">
    <div class="login-box" id="app">
        <div class="login-logo">
            <img src="{{url('/asset/')}}image/logo_dash.png" style="width: 350px;">
        </div>
        <!-- /.login-logo -->
        <div :class="loginClass">
            <div class="login-logo" style="margin: 0;">
                <img src="{{url('/')}}asset/image/logo.png" style="width: 200px;">
            </div>
            <p class="login-box-msg">Sign in to start your session</p>

            <form action="{{ url('authorization/store') }}" method="post" @submit.prevent="submitLogin">
                <div :class="alertClass" v-show="showAlert">
                    <i class="icon fa fa-close"></i>
                    <span v-text="alert.message"></span>
                </div>

                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Username" v-model="form.email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="Password" v-model="form.password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">

                    </div>
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat" v-html="btnSign"></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript">
        var app = new Vue({
            el : '#app',
            data : {
                alert : {
                    status : 'success',
                    message : ''
                },
                errorAlert : {
                    status : 'danger',
                    message : 'Something wrong happend!'
                },
                form : {
                    email : '',
                    password : ''
                },
                btnSign : 'Sign In'
            },
            methods : {
                submitLogin(){
                    this.clearAlert();
                    this.alert.message = '';
                    this.btnSign = '<i class="fa fa-spin fa-spinner"></i> Signing...';
                    axios.post('{{url("/authorization/store")}}', this.form)
                        .then(function(response){
                            if(response.data.status == 'success') {
                                app.alert.message = '';
                                location.href = response.data.redirect;
                            } else {
                                app.alert = response.data;
                            }
                        })
                        .catch(() => this.alert = this.errorAlert)
                        .then(() => this.btnSign = 'Sign In')
                },
                clearAlert(){
                    this.alert.message = '';
                }
            },
            computed : {
                alertClass() {
                    if(this.alert.status == 'success') {
                        return 'alert alert-success';
                    }
                    return 'alert alert-danger';
                },
                showAlert(){
                    return this.alert.message.length;
                },
                loginClass() {
                    if(this.alert.message.length == 0) {
                        return 'login-box-body'
                    } else {
                        return 'login-box-body animated shake'
                    }
                }
            }
        })
    </script>
</body>

</html>
