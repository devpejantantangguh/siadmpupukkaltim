
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<HTML>
<HEAD>
    <TITLE>INVOICE</TITLE>
<META http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META http-equiv="X-UA-Compatible" content="IE=8">
<STYLE type="text/css">
body {margin-top: 0px;margin-left: 0px;}
#page_1 {position:relative; ;margin-left: 25px ;margin-right: 25px;padding: 0px;border: none;width: 550px;}
.dclr {clear:both;float:none;height:1px;margin:0px;padding:0px;overflow:hidden;}

.ft0{font: 1px 'Calibri';line-height: 1px;}
.ft1{font: 18px 'Calibri';text-decoration: underline;line-height: 22px;}
.ft2{font: 1px 'Calibri';line-height: 17px;}
.ft3{font: 13px 'Calibri';line-height: 15px;}
.ft4{font: 15px 'Calibri';line-height: 18px;}
.ft5{font: 14px 'Calibri';text-decoration: underline;line-height: 17px;}
.ft6{font: 15px 'Calibri';color: #ff0000;line-height: 18px;}
.ft7{font: italic 15px 'Calibri';line-height: 18px;}
.ft8{font: bold 16px 'Calibri';line-height: 19px;}
.ft9{font: italic bold 15px 'Calibri';line-height: 18px;}

.p0{text-align: left;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p1{text-align: center;padding-right: 15px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p2{text-align: right;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p3{text-align: left;padding-left: 144px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p4{text-align: right;padding-right: 4px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p5{text-align: left;padding-left: 34px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p6{text-align: left;padding-left: 33px;margin-top: 0px;margin-bottom: 0px;white-space: nowrap;}
.p7{text-align: left;padding-left: 129px;margin-top: 17px;margin-bottom: 0px;}

.td0{padding: 0px;margin: 0px;width: 165px;vertical-align: bottom;}
.td1{padding: 0px;margin: 0px;width: 184px;vertical-align: bottom;}
.td2{padding: 0px;margin: 0px;width: 148px;vertical-align: bottom;}
.td3{padding: 0px;margin: 0px;width: 349px;vertical-align: bottom;}
.td4{padding: 0px;margin: 0px;width: 309px;vertical-align: bottom;}
.td5{padding: 0px;margin: 0px;width: 62px;vertical-align: bottom;}
.td6{padding: 0px;margin: 0px;width: 121px;vertical-align: bottom;}

.tr0{height: 29px;}
.tr1{height: 17px;}
.tr2{height: 40px;}
.tr3{height: 31px;}
.tr4{height: 58px;}
.tr5{height: 38px;}
.tr6{height: 39px;}
.tr7{height: 19px;}
.tr8{height: 23px;}

.t0{width: 497px;margin-left: 6px;margin-top: 20px;font: 15px 'Calibri';}
.t1{width: 492px;margin-left: 6px;margin-top: 52px;font: bold 16px 'Calibri';}
.kanan {
    float:right;
}
</STYLE>
</HEAD>

<BODY>
<DIV id="page_1">
<TABLE cellpadding=0 cellspacing=0 class="t0" style="width: 550px;">

</TABLE>

<DIV class="dclr"></DIV>
<TABLE cellpadding=0 cellspacing=0 class="t0">
<TR>
    <TD colspan=3 style="text-align: right;"><img width="180" src="{{ $logo }}" alt=""></TD>
</TR>

<TR>
    <TD class="tr0 td0" colspan=3 style="text-align: center;"><P><span style="font-weight: bold;font-size: 19px;text-decoration:underline;">{{ $data->NoInvoice }}</span><br>{{ $data->NamaVendor }}</P></TD>
</TR>
<TR class="dot" >
    <TD class="tr2 td0" colspan=3 style="border-bottom-style: dotted;border-bottom-color:#789fd2;border-bottom-width: 2px;"><P class="p0 ft4" ><span>Tanggal Tagihan</span><span class="kanan">{{ date('d-m-Y', strtotime($data->TglInvoice)) }}</span></P></TD>
</TR>
<TR>
    <TD class="tr3 td0" colspan=3 style="border-bottom-style: dotted;border-bottom-color:#789fd2;border-bottom-width: 2px;"><P class="p0 ft4"><span>Jenis Invoice</span></span><span class="kanan">{{ $data->JT }}</span></P></TD>
</TR>
<TR>
    <TD class="tr3 td0" colspan=3 style="border-bottom-style: dotted;border-bottom-color:#789fd2;border-bottom-width: 2px;"><P class="p0 ft4"><span>No. Seri Faktur Pajak</span><span class="kanan">{{ $data->NoFakturPajak }}</span></P></TD>
</TR>
<TR>
    <TD class="tr3 td0" colspan=3 style="border-bottom-style: dotted;border-bottom-color:#789fd2;border-bottom-width: 2px;"><P class="p0 ft4"><span>Tanggal Faktur Pajak</span><span class="kanan">{{ date('d-m-Y', strtotime($data->TglFakturPajak)) }}</span></P></TD>
</TR>
<TR>
    <TD class="tr3 td0" colspan=3 style="border-bottom-style: dotted;border-bottom-color:#789fd2;border-bottom-width: 2px;"><P class="p0 ft4"><span>No. Dokumen SAP</span><span class="kanan">{{ $data->NoDokumenSAP }}</span></P></TD>
</TR>
<TR>
    <TD class="tr3 td0"><P class="p0 ft4">Tanggal Pembayaran</P></TD>
    <TD class="tr3 td1"><P class="p0 ft0">&nbsp;</P></TD>
    <TD class="tr3 td2"><P class="p2 ft4">{{ ($data->TglEstimasiBayar == '') ? '' : date('d-m-Y', strtotime($data->TglEstimasiBayar)) }}</P></TD>
</TR>
<TR>
    <TD class="tr4 td0" colspan=3 style="text-align: center;text-decoration: underline;font-size: 15px"><P>DETIL INVOICE</TD>
</TR>
<TR>
    <TD class="tr5 td0"><P class="p0 ft4">Nilai Tagihan</P></TD>
    <TD class="tr5 td1"><P class="p3 ft4">{{ $data->KursInvoice }}</P></TD>
    <TD class="tr5 td2"><P class="p4 ft4">{{ number_format($data->NilaiInvoice,0,',','.') }}</P></TD>
</TR>
<TR>
    <TD class="tr6 td0"><P class="p0 ft4">PPN</P></TD>
    <TD class="tr6 td1"><P class="p3 ft4">{{ $data->KursInvoice }}</P></TD>
    <TD class="tr6 td2"><P class="p4 ft4">{{ (int) $data->PPN }}</P></TD>
</TR>
<TR>
    <TD class="tr6 td0"><P class="p0 ft4">Potongan:</P></TD>
    <TD class="tr6 td1"><P class="p0 ft0">&nbsp;</P></TD>
    <TD class="tr6 td2"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<?php 
$potongan = 0;
foreach ($list_potongan as $key => $value) { 
    $potongan=$potongan+$value->Nominal;
    ?>
<TR>
    <TD class="tr3 td0"><P class="p5 ft4">- {{ $value->Potongan}}</P></TD>
    <TD class="tr3 td1"><P class="p3 ft4">{{ $data->KursInvoice}}</P></TD>
    <TD class="tr3 td2"><P class="p2 ft6">({{ number_format($value->Nominal,0,',','.')}})</P></TD>
</TR>
<TR>
    <TD class="tr7 td0"><P class="p6 ft7">{{ $value->Keterangan}}</P></TD>
    <TD class="tr7 td1"><P class="p0 ft0">&nbsp;</P></TD>
    <TD class="tr7 td2"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<?php } ?>
<TR>
    <TD colspan=2 class="tr3 td3"><P class="p0 ft4">Net Off (No. Dokumen Piutang)</P></TD>
    <TD class="tr3 td2"><P class="p0 ft0">&nbsp;</P></TD>
</TR>
<?php 
$sum=0;
foreach ($getApplied as $applied => $doc) { 
    $sum = $sum + $doc->AppliedAmount;
    ?>
<TR>
    <TD class="tr3 td0"><P class="p6 ft4">- {{$doc->ARDocNo}}</P></TD>
    <TD class="tr3 td1"><P class="p3 ft4">{{ $data->KursInvoice}}</P></TD>
    <TD class="tr3 td2"><P class="p2 ft6">({{number_format($doc->AppliedAmount,0,',','.')}})</P></TD>
</TR>
<?php } ?>
<TR>
    <TD class="tr8 td4"><P class="p0 ft4"></TD>
</TR>
<TR>
    <TD class="tr8 td4"class="tr3 td0" colspan=3 style="border-bottom-style: dotted;border-bottom-color:#789fd2;border-bottom-width: 2px;"></TD>
</TR>

<TR>
    <TD class="tr5 td0"><P class="p0 ft4">TOTAL TAGIHAN DIBAYARKAN</P></TD>
    <TD class="tr3 td1"><P class="p3 ft4" style="font-weight: bold">{{ $data->KursInvoice}}</P></TD>
    <TD class="tr3 td2" style="font-weight: bold:text-color:black"><P class="p2 ft8" >{{ number_format( (int) $data->NilaiInvoice + (int) $data->PPN - (int) $potongan - (int) $sum,0,',','.')}}</P></TD>
</TR>
</TABLE>
</DIV></BODY>
</HTML>
