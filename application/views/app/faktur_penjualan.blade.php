<!DOCTYPE html>
<html>
<head>
    <title>Faktur Penjualan</title>
    <style type="text/css">
        body {font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;}
        #fp {border-collapse: collapse;width: 100%;}
        #fp td, #fp th {border: 1px solid #777;padding: 8px;}
    </style>
</head>
<body style="font-size: 12px">
    <div class="container" style="width:700px;margin:auto;padding:10px">
        <div class="row" style="text-align: center">
            <h2>FAKTUR PENJUALAN</h2>
        </div>
        <table style="font-size:13px">
            <tr>
                <td rowspan="4" style="width:100px;vertical-align: top"><strong>Kepada</strong></td>
                <td width="250px"><strong>{{ $data->Customer }}</strong></td>
                <td width="200px"><strong>Tanggal</strong></td>
                <td width="200px">: {{ tanggal_indo($data->TglFakturPajak) }}</td>
            </tr>
            <tr>
                <td rowspan="3" style="vertical-align: top"><strong>{{ $data->Alamat1 ?? ''}} <br> {{ $data->Alamat2 ?? '' }}</strong></td>
                <td><strong>Nomor Tagihan</strong></td>
                <td>: {{ $data->NoBillingDoc }}</td>
            </tr>
            <tr>
                <td><strong>Nomor Faktur Pajak</strong></td>
                <td>: {{ $data->NoFakturPajak }}</td>
            </tr>
            <tr>
                <td><strong>Nomor Sales Order</strong></td>
                <td>: {{ $data->NoSO }}</td>
            </tr>
        </table>
        <br>
        <table style="font-size:13px">
            <tr>
                <td style="width:150px">Incoterm</td>
                <td style="width:200px">:
					{{ $data->Incoterm }}
					@if(!empty($data->Incoterm2))
, {{ $data->Incoterm2 }}
					@endif
				</td>
                <td style="width:200px">Nomor Billing of Landing / BAST</td>
                <td style="width:200px">: {{ $data->NoBAST }}</td>
            </tr>
            <tr>
                <td>Alat Angkut</td>
                <td>: {!! empty($data->AlatAngkut) ? '' : $data->AlatAngkut !!}</td>
                <td>Nomor Registrasi Surveyor</td>
                <td>: {{ $data->NoRegSurveyor }}</td>
            </tr>
            <tr>
                <td>Tanggal Pelayaran</td>
                <td>: {{ $data->TglPelayaran == '1900-01-01' || empty($data->TglPelayaran) ? '' : tanggal_indo($data->TglPelayaran) }}</td>
                <td>No. SKBDN</td>
                <td>: {{ empty($data->NoSKBDN) ? '' : $data->NoSKBDN }}</td>
            </tr>
            <tr>
                <td>Tujuan</td>
                <td>: {{ empty($data->Tujuan) ?  '' : $data->Tujuan }}</td>
                <td>Tanggal SKBDN</td>
                <td>: {{ $data->TglSKBDN == '1900-01-01' ? '' : tanggal_indo($data->TglSKBDN) }}</td>
            </tr>
            <tr>
                <td>Tempat Muat</td>
                <td>: {{ empty($data->TempatMuat) ? '' : $data->TempatMuat }}</td>
                <td>Bank Penerbit</td>
                <td>: {{ $data->Bank }}</td>
            </tr>
        </table>

        <br>

        <table border="1" style="width: 100%" id="fp">
            <tr>
                <td>No</td>
                <td>Keterangan</td>
                <td style="text-align: center;">Kuantum</td>
                <td style="text-align: center;">Satuan</td>
                <td style="text-align: center;">Harga</td>
                <td style="text-align: center;">Jumlah</td>
            </tr>
            @if($data->FPOption == 'UM')
                <tr style="vertical-align: top">
                    <td style="height:300px">1</td>
                    <td>{{ str_replace("\n","<br/>",$items[0]->Material) }}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
						<div style="float:left;width:50%">
							{{ $data->Kurs }}
						</div>
						<div style="float:right;width:50%;text-align:right">
							@if($data->Kurs == 'USD')
                        		{{ number_format(($data->NetValueBeforePPH2),2,'.',',') }}
							@else
								{{ number_format(($data->NetValueBeforePPH2),0,null,'.') }}
							@endif
						</div>
					</td>
                </tr>
            @else
				@foreach($items as $key => $item)
                <tr style="vertical-align: top">
					@if($key == count($items)-1)
						<td style="height:200px">{{ $key+1 }}</td>
					@else
						<td style="">{{ $key+1 }}</td>
					@endif
                    <td>{!! str_replace("\n","<br/>",$item
                    ->Material) !!}</td>
                    <td style="text-align: center">{{ $item->Kuantum }}</td>
                    <td style="text-align: center">{{ $item->UoM }}</td>
                    <td>
                        <div style="float:left;width:20%">
                            {{ $item->Kurs }}
                        </div>
                        <div style="float:right;width:1%;text-align:right">
                            {{ number_format($item->HargaSatuan,0,null,'.') }}
                        </div>
                    </td>
					<td>
						<div style="float:left;width:50%">
							{{ $item->Kurs }}
						</div>
						<div style="float:right;width:50%;text-align:right">
							@if($data->Kurs == 'USD')
                        		{{ number_format(($item->NetValueBeforePPH2),2,'.',',') }}
							@else
								{{ number_format(($item->NetValueBeforePPH2),0,null,'.') }}
							@endif
						</div>
					</td>
                </tr>
				@endforeach
            @endif
            <tr>
                @if($data->FPOption == 'D')
                    <td rowspan="5"></td>
                @else
                    <td rowspan="4"></td>
                @endif
                <td colspan="4">Subtotal</td>
                <td>
                    <div style="float:left;width:50%">
                        {{ $data->Kurs }}
                    </div>
                    <div style="float:right;width:50%;text-align:right">
						@if($data->Kurs == 'USD')
                        	{{ number_format(($sub_total),2,'.',',') }}
						@else
							{{ number_format(($sub_total),0,null,'.') }}
						@endif
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="4">Dasar Pengenaan Pajak (DPP)</td>
                <td>
                    <div style="float:left;width:50%">
                        {{ $data->Kurs }}
                    </div>
                    <div style="float:right;width:50%;text-align:right">
                        {{ number_format($data->DPP,0,null,'.') }}
                    </div>
                </td>
            </tr>
            @if($data->FPOption == 'D')
                <tr>
                    <td colspan="4">Uang Muka</td>
                    <td>
                        <div style="float:left;width:50%">
                            {{ $data->Kurs }}
                        </div>
                        <div style="float:right;width:50%;text-align:right">
                            {{ number_format($down_payment,0,null,'.') }}
                        </div>
                    </td>
                </tr>
            @endif
            <tr>
				@if($tax_head == 8 || $tax_head == 7)
					<td colspan="4">PPN Dibebaskan</td>
				@else
					<td colspan="4">PPN {{ $sub_total == $total ? 'WAPU' : '' }}</td>
				@endif

                <td>
                    <div style="float:left;width:50%">
                        {{ $data->Kurs }}
                    </div>
                    <div style="float:right;width:50%;text-align:right">
                        {{ number_format($data->TaxAmount,0,null,'.') }}
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="4">Total</td>
                <td>
                    <div style="float:left;width:50%">
                        {{ $data->Kurs }}
                    </div>
                    <div style="float:right;width:50%;text-align:right">
						@if($data->Kurs == 'USD')
                        	{{ number_format(($total),2,'.',',') }}
						@else
							{{ number_format(($total),0,null,'.') }}
						@endif
                    </div>
                </td>
            </tr>
        </table>

		@if($data->Kurs == 'USD')
        	<p>Terbilang # <strong>{{ ucwords($totalTerbilang) }}</strong> #</p>
		@else
			<p>Terbilang # <strong>{{ ucwords($totalTerbilang) }} {{ $kurs[$data->Kurs] }}</strong> #</p>
		@endif

        <table>
            <tr>
                <td rowspan="3" style="width: 500px">
                    <div style="margin-left:50px">
                        <img src="{{ $qrCode }}">
                    </div>
					<p>Please remit into our account number <br/>
					for payment in USD currency, please transfer into below account<br/>
					{{ $gs['Rekening1'] }}<br>
					{{ $gs['Rekening2'] }}</p>
                </td>
                <td style="width: 200px">
                    Bontang, {{ $final_approve_date }}
                    <br>
                    PT Pupuk Kalimantan Timur
                </td>
            </tr>
            <tr>
                <td style="height:80px"></td>
            </tr>
            <tr>
                <td><span style="text-decoration: underline;">{{ $ttd }}</span> <br> {{ $pangkat }}</td>
            </tr>
        </table>

    </div>
</body>
</html>
