@extends('templates.admin')

@section('title','Reset Password')

@section('heading')
    {{-- code/link styleshet here --}}
@endsection

@section('contentheader')
    <section class="content-header">
        <h1>Home</h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
    </section>
@endsection

@section('content')
    <form @submit.prevent="updatePassword">
        <div class="box-header with-border">
            <h3 class="box-title">Reset Password</h3>
        </div>
        <div class="box-body">
            <div :class="alertCss" v-show="showAlert">
                <i class="icon fa fa-check" v-if="alert.status === 'success'"></i>
                <i class="icon fa fa-close" v-if="alert.status === 'danger'"></i>
                <span v-text="alert.message"></span>
            </div>

            <div class="row" v-show="!useLDAP">
                <div class="col-md-5">
                    <div class="form-group">
                        <label>Old Password</label>
                        <input type="password" name="old_password" placeholder="Old Password" class="form-control" v-model="form.old_password" @keyUp="clearMessage">
                    </div>
                    <div class="form-group">
                        <label>New Password</label>
                        <input type="password" name="new_password" placeholder="New Password" class="form-control" v-model="form.new_password" @keyUp="clearMessage">
                    </div>
                    <div class="form-group">
                        <label>Retype Password</label>
                        <input type="password" name="new_password" placeholder="Retypr Password" class="form-control" v-model="form.re_password" @keyUp="validatePassword();clearMessage()">
                        <span class="help-block" style="color:red;font-size: 10px" v-if="differentPassword">Password didn't match, keep typing...</span>
                    </div>
                </div>
            </div>
            <div class="row" v-show="useLDAP">
                <div class="col-sm-12">
                    <p>Silakan gunakan Username & Password email PKT Anda</p>
                </div>
            </div>

        </div>
        <div class="box-footer">
            <button class="submit btn btn-primary btn-sm" type="submit" v-show="!useLDAP">Update</button>
            <a href="/dashboard" class="submit btn btn-primary btn-sm" v-show="useLDAP">OK</a>
        </div>
    </form>
@endsection

@section('script')
    <script type="text/javascript">
        app = new Vue({
            el:'#app',
            data:{
                form:{
                    old_password : '',
                    new_password : '',
                    re_password : '',
                },
                alert:{
                    message : '',
                    status : 'success'
                },
                errorAlert:{
                    message : 'Something wrong when updating password',
                    status : 'danger'
                },
                differentPassword : false,
                useLDAP : {{ Auth::user()->IsLDAP }}
            },
            methods : {
                validatePassword () {
                    console.log(this.form.new_password);
                    if(this.form.re_password.length != 0) {
                        if(this.form.new_password != this.form.re_password) {
                            this.differentPassword = 1;
                            return;
                        }
                    }
                    this.differentPassword = 0;
                },
                updatePassword(){
                    console.log(this.form.new_password);
                    axios.post('{{url('')}}profile/update_password',{
                      old_password: this.form.old_password,
                      new_password: this.form.new_password,
                      re_password: this.form.re_password
                    })
                        .then(response => {this.alert = response.data})
                        .catch(() => this.alert = this.errorAlert);
                    this.clearForm();
                },
                clearForm(){
                    this.form.old_password = '';
                    this.form.re_password = '';
                    this.form.new_password = '';
                    this.differentPassword = 0;
                },
                clearMessage(){
                    this.alert.message = '';
                }
            },
            computed:{
                showAlert(){
                    return this.alert.message.length;
                },
                alertCss(){
                    if(this.alert.status == 'danger') {
                        return 'alert alert-danger';
                    }
                    return 'alert alert-success'
                }
            }
        });
    </script>
@endsection
