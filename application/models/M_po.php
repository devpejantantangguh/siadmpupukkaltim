<?php 

class M_po extends CI_Model
{
    public function get_invoice_po($id_vendor)
    {
        $limit = isset($_GET['take']) ? $_GET['take'] : 100;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;

        if(isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        } else {
            $this->db->order_by('ID', 'DESC');
        }

        $this->db->where('IDVendor', $id_vendor);
        $query = $this->db->select('i.ID,i.NoPO,i.KursInvoice,i.NoInvoice,i.NomorBA,i.NoSAGR,i.NilaiInvoice,s.Status')
                    ->join('Status s','i.Status = s.ID ')
                    // ->join('HistoryInvoice h','i.Status = s.ID ')
                    ->where_in('JenisTransaksi', [1,2,3])
                    ->get('Invoice i', $limit, $offset);

        $items = $query->result();
        $count = $query->num_rows();

        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $items
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }

     public function get_last_note($id)
    {
        $query = $this->db->where('Action',3)
                ->where('IDInvoice', $id)
                ->order_by('Timestamp','desc')
                ->get('HistoryInvoice');

        if($query->num_rows() > 0) {
            return $query->row('Keterangan');
        } else {
            return '';
        }
    }

    public function get_current_note($id)
    {
        $query = $this->db
                ->where('IDInvoice', $id)
                ->order_by('Timestamp','desc')
                ->get('HistoryInvoice');

        if($query->num_rows() > 0) {
            return $query->row('Keterangan');
        } else {
            return '';
        }
    }

    public function updateMatriks($id, $matriks)
    {
        $lists = [];
        foreach($matriks as $matrik_id => $matrik) {
            $lists = $lists + [$matrik_id => true];
        }

        if( ! empty($lists)) {
            $serialize = serialize($lists);    
            $data['Matriks'] = $serialize;
            $this->db->where('ID', $id)->update('Invoice', $data);
        }
    }

    public function createHistory($id)
    {
        $status = $this->input->post('submit');

        if($status == 'checked') {
            $update_id = 4;
        } elseif($status == 'reject') {
            $update_id = 9;
        }

        $data = [
            'IDInvoice' => $id,
            'Action'    => $update_id,
            'UserID'    => Auth::user()->ID,
            'Timestamp' => date('Y-m-d H:i:s'), 
            'Keterangan'    => $this->input->post('note'),
        ];
        $this->db->insert('HistoryInvoice', $data);
    }

    public function get_detail_invoice($id)
    {
        $this->db->select('Invoice.*,VL_PaymentTerm.PaymentTerm as PT');
        $this->db->join('VL_PaymentTerm','VL_PaymentTerm.ID = Invoice.PaymentTerm','LEFT');
        $query = $this->db->where('Invoice.ID', $id)->get('Invoice');
        return $query->num_rows() > 0 ? $query->row() : [];
    }

    public function slug($string)
    {
        return preg_replace("/[^a-z]/",'_',strtolower($string));
    }

    public function get_kelengkapan_dokumen($id_invoice)
    {
        $query = $this->db->query("SELECT m.ID, m.Dokumen, m.IsRequired, m.Keterangan, i.Matriks FROM Invoice as i
                        JOIN VL_JenisTransaksi as j on i.JenisTransaksi = j.ID
                        JOIN MatriksDokumen as m on m.JenisTransaksi = j.ID
                        WHERE i.ID = ?", $id_invoice);
        $lists = [];
        $matriks = null;
        if($query->num_rows() > 0){
            foreach($query->result() as $row) {
                
                if($data = unserialize($row->Matriks)) {
                    $checked = isset($data[$row->ID]) && $data[$row->ID] == true ? true : false;
                } else {
                    $checked = false;
                }

                $lists[] = [
                    'ID'            => $row->ID,
                    'Dokumen'       => $row->Dokumen,
                    'Slug'          => $this->slug($row->Dokumen),
                    'Required'      => $row->IsRequired,
                    'Value'         => $checked,
                    'Keterangan'    => $row->Keterangan
                ];
            }
        }

        return $lists;
    }

    public function update_status($id)
    {
        $status = $this->input->post('submit');

        if($status == 'checked') {
            $update_id = 4;
        } elseif($status == 'reject') {
            $update_id = 1;
        }

        $this->db->where('ID',$id)
                //->where('Status',3)
                ->update('Invoice',array('Status' => $update_id));
    }

    public function update_priority($id)
    {
        $update_id = isset($_POST['IsPriority']) ? true : false;

        $this->db->where('ID',$id)
                 ->update('Invoice',array('IsPriority' => $update_id));
    }

    public function transmittal($ids)
    {
        $this->db->trans_start();

        $number = $this->get_last_trans_number();
        $next   = $number+1;
        $number = date('y').str_pad($number, 4, "0", STR_PAD_LEFT);

        $this->db->where_in('ID', $ids)->update('Invoice',[
            'NoTransmittal' => $number,
            'TglTransmittal'    => date('Y-m-d')
        ]);

        $this->db->where('Setting','TransmittalAKT')
                 ->update('GlobalSettings', ['Value' => $next]);

        $this->db->trans_complete();

        return $number;
    }

    public function get_last_trans_number()
    {
        $query = $this->db->where('Setting','TransmittalAKT')->get('GlobalSettings');

        if($query->num_rows() > 0) {
            return $query->row('Value');
        } else {
            return 0;
        }
    }
}
