<?php 

class M_fakturpajak extends CI_Model
{
    private $npwp_pkt = '010000727051000';
    public $error_validation = '';

    public function is_npwp_customer_exist($npwp_customer)
    {
        $row = $this->db->select('IDCustomer,Customer,NPWP')->where('NPWP', $npwp_customer)->get('Ms_Customer')->row();
        if($row != null) {
            return ['IDCustomer' => $row->IDCustomer,'Customer' => $row->Customer,'NPWP' => $row->NPWP];
        }
        return false;
    }

    public function detail_fakturpajak($document_file)
    {
        $document_file = str_replace('.pdf','', strtolower($document_file));
        $x_document_file = explode('-', $document_file);

        $data = ['File' => $document_file];

        if(count($x_document_file) == 4) {
            list($npwp_pkt, $no_faktur_pajak, $npwp_customer, $timestamp) = $x_document_file;

            $data = $data + [
                'NoFakturPajak'     => $no_faktur_pajak,
                'NPWP_Customer'     => $npwp_customer,
                'TglFakturPajak'    => $this->decode_date($timestamp),
                'NPWP_PKT'          => $npwp_pkt
            ];

            if($npwp_pkt = $this->npwp_pkt) {
                if($row = $this->is_npwp_customer_exist($npwp_customer)) {
                    return $data + ['Valid' => 1,'ErrorMessage' => 'None'] + $row;
                } else {
                    return $data + ['Valid' => 0, 'ErrorMessage' => 'Invalid Customer NPWP'];
                }
            } else {
                return $data + ['Valid' => 0, 'ErrorMessage' => 'Invalid PKT NPWP'];
            }
        } else {
            return $data + ['Valid' => 0, 'ErrorMessage' => 'Invalid format file name'];
        }
    }

    public function decode_date($timestamp)
    {
        try {
            // $date = \DateTime::createFromFormat('YmdHis',$timestamp)->format('d M Y H:i:s');    
            // 2018 02 02 06 46 38
            // 0123 45 67 89 01 23
            $year = substr($timestamp,0,4);
            $month = substr($timestamp,4,2);
            $day = substr($timestamp,6,2);
            $hour = substr($timestamp,8,2);
            $minute = substr($timestamp, 10,2);
            $second = substr($timestamp, 12,2);

            $d = mktime($hour,$minute,$second,$month,$day,$year);
            return date('Y-m-d H:i:s', $d);
            // return "{$year}-{$month}-{$day} {$hour}:{$minute}:{$second}";
        } catch (Exception $e) {
            return null;
        }
    }

    public function get_list_faktur_pajak($paginate, $page = 1)
    {
        $files = glob(base_path('storage/faktur_pajak/*.pdf'));
        $files = array_chunk($files, $paginate);
        $files = $files[$page-1];

        $data = [];
        foreach($files as $file) {
            $file = basename($file);

            $data[] = $this->detail_fakturpajak($file);
        }

        return $data;
    }

    public function get_total_file()
    {
        $files = glob(base_path('storage/faktur_pajak/*.pdf'));
        return count($files);
    }
}
