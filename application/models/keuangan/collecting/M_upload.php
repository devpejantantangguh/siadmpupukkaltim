<?php 

/**
* 
*/
class M_upload extends CI_Model
{
    public function check_and_insert($id_billdoc_header, $row, $user_id, $rowNumber = 0)
    {
        try {
            
            $data = [
                'NoBillingDoc'  => $row[0],
                'Sektor'        => $row[1],
                'BillingDate'   => $row[2],
                'IDSalesOrg'    => $row[3],
                'IDPayer'       => $row[4],
                'Payer'         => $row[5],
                'IDMaterial'    => $row[6],
                'MaterialDesc'  => $row[7],
                // 8,9
                'OriginalCurr'  => $row[10]
                // 'Billed Quantity    SU  Curr.'
            ];

            // $data = array(
            //     'IDBillDocHeader' => $id_billdoc_header,
            //     'IsValid'         => $valid,
            //     'NoBillingDoc'    => max_str($row[1],10), // Bill.Doc.
            //     'JenisTransaksi'  => $this->get_jenis_transaksi($row[15]),
            //     'Sektor'          => max_str($row[7],5), // Sektor
            //     'BillingDate'     => $date, // Bill.Doc.
            //     'NoSO'            => max_str($row[0],10), // Sales Doc.
            //     'NoFakturPajak'   => max_str($row[69],25), // Nomor kontrak
            //     'IDCustomer'      => $this->has_customer($customer) ? $customer : '', // Payer
            //     'Customer'        => max_str($row[18],30), // Payer
            //     'Status'          => 8, // <default system>
            //     'IDMaterial'      => $this->has_material($material) ? $material : '', // Nomor Material
            //     'MaterialDesc'    => max_str($row[30],30), // Deskripsi Material
            //     'Kuantum'         => null,
            //     'UoM'             => max_str($row[32],7), // Unit of Meansure
            //     'Kurs'            => max_str($row[34],3), //Mata Uang
            //     'HargaSatuan'     => 0,
            //     'Jumlah'          => $row[31], // PGI qty?
            //     'DPP'             => null,
            //     'TaxAmount'       => $row[11], // Tax Amount in LC
            //     'Total'           => $nominal, // Total
            //     'Keterangan'      => null,
            //     'IDPayer'         => $this->has_customer($customer) ? $customer : '', // Payer
            //     'Payer'           => max_str($row[18],30), // Payer
            //     'IDSalesOrg'      => max_str($row[75],10), // SOrg
            //     'SalesOrg'        => max_str($row[19],30), // Sales Organization
            //     'LifeCycle'       => 0,
            //     'OriginalCurr'    => $row[34], // curr (ok)
            //     'AmountOriginalCurr' => $row[8],  // Net Value LC sblm PPH22 (ok)
            //     'LC'              => $row[12], // Crcy (ok) 
            //     'AmountLC'        => $row[10], // Net Value LC stlh PPH22
            //     'FakturPajakDate' => is_string($row[70]) ? $row[70] : $row[70]->format('Y-m-d'), // Faktur Pajak Date
            //     'PaymentMethod'   => $row[89], // Faktur Pajak Date
            // );

            $this->db->insert('Temp_BillDocDetail', $data);


            return array('status' => 'ok');
        } catch (Exception $e) {
            Log::error('Error Processing Row ' . $rowNumber);
            return array('status' => 'error');
        }
    }

    public function get_jenis_transaksi($string)
    {
        $type = strtolower($string);
        switch ($type) {
            case 'invoice':
                return 1;
            case 'billing':
                return 2;
            case 'pembatalan invoice':
                return 3;
            default:
                return 4;
        }
    }

    public function has_material($id_material)
    {
        $row = $this->db->where('IDMaterial', $id_material)->get('Ms_Material',1)->row();
        return !empty($row);
    }

    public function has_customer($id_customer)
    {
        $row = $this->db->where('IDCustomer', $id_customer)->get('Ms_Customer',1)->row();
        return !empty($row);
    }

    public function validate_date($date, $format = 'Y-m-d')
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) === $date;
    }

    public function validate_nominal($price)
    {
        return $price > 0;
    }

    public function move_temp_to_billdoc($id)
    {
        $this->db->trans_start();

        $header_id = $this->db->where('ID',$id)->get('Temp_BillDocDetail',1)->row('IDBillDocHeader');
        // Update Header
        $this->db->where('ID',$header_id)->update('Temp_BillDocHeader', array(
            'PostedBy' => Auth::user()->ID, 
            'PostedOn' => date('Y-m-d H:i:s')
        ));

        $this->db->where('IsValid',1);
        $this->db->where('ID',$id);
        $row = $this->db->get('Temp_BillDocDetail',1)->row_array();

        if($row) {
            $row['ID'] = $row['ID'];
            $row['Material'] = $row['MaterialDesc'];
            unset($row['IDBillDocHeader']);
            unset($row['IsValid']);
            unset($row['MaterialDesc']);

            $this->db->insert('BillingDocument', $row);

            $this->db->where('ID',$id)->delete('Temp_BillDocDetail');
        }

        $this->db->trans_complete();
    }

    public function remove($id)
    {
        $this->db->trans_start();
        $header_id = $this->db->where('ID',$id)->get('Temp_BillDocDetail',1)->row('IDBillDocHeader');
        $this->db->where(array('ID' => $header_id))->update('Temp_BillDocHeader', array(
            'CanceledBy' => Auth::user()->ID,
            'CanceledOn' => date('Y-m-d H:i:s')
        ));
        $this->db->delete('Temp_BillDocDetail', array('ID' => $id));
        $this->db->trans_complete();
    }

    public function remove_temp_billdoc_detail()
    {
        $this->db->where('1=1',null, false)->delete('Temp_BillDocDetail');
    }
}
