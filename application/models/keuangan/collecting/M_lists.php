<?php

class M_lists extends CI_Model
{
	private $statuses = null;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('keuangan/collecting/m_billdoc', 'billdoc');

		ini_set('memory_limit', '256M'); // This also needs to be increased in some cases. Can be changed to a higher value as per need)
		ini_set('sqlsrv.ClientBufferMaxKBSize', '524288'); // Setting to 512M
		ini_set('pdo_sqlsrv.client_buffer_max_kb_size', '524288'); // Setting to 512M - for pdo_sqlsrv
	}

	public function dxDataGridFilters($filters)
	{
		if (strpos($filters, '"and"')) {
			$filter = json_decode($filters);
			if (!empty($filter)) {
				foreach ($filter as $f) {
					if (is_array($f)) {
						$this->doFilter($f);
					}
				}
			}
		} else {
			$filter = json_decode($filters);
			if (is_array($filter)) {
				$this->doFilter($filter);
			}
		}
	}

	public function doFilter($filter)
	{
		if (is_array($filter[0])) {
			if($filter[0][0] == 'BillingDate' || $filter[0][0] == 'TglFakturPajak') {
				$condition = $filter[0][0];
				$minValue = $filter[0][2];
				$maxValue = $filter[2][2];
				$this->db->where("$condition BETWEEN '$minValue' AND '$maxValue'");
			}
		} else {
			if ($filter[1] == 'contains') {
				$this->db->like($filter[0], $filter[2]);
			} elseif ($filter[1] == 'notcontains') {
				$this->db->not_like($filter[0], $filter[2]);
			} elseif ($filter[1] == 'startswith') {
				$this->db->like($filter[0], $filter[2], 'after');
			} elseif ($filter[1] == 'endswith') {
				$this->db->like($filter[0], $filter[2], 'before');
			} else {
				$this->db->where("{$filter[0]} {$filter[1]}", $filter[2]);
			}
		}
	}

	public function collection($status = null)
	{
		$user_id = Auth::id();

		$limit = isset($_GET['take']) ? $_GET['take'] : 100;
		$offset = isset($_GET['skip']) ? $_GET['skip'] : 0;

		if ($status == 'approval') {
			$global_setting = [];
			$settings = $this->db->get('globalsettings')->result();
			foreach ($settings as $setting) {
				$global_setting[$setting->Setting] = $setting->Value;
			}

			if ($user_id == $global_setting['Approval1FP']) {
				// $this->db->where('ApproveCount', '0');
				$this->db->where('Status', 21);
			} elseif ($user_id == $global_setting['Approval2FP']) {
				// $this->db->where('ApproveCount', '1');
				$this->db->where('Status', 22);
			} elseif ($user_id = $global_setting['Approval3FP']) {
				// $this->db->where('ApproveCount', '2');
				$this->db->where('Status', 23);
			}
		}
		$filter = $this->input->get('filter');
		$filter = json_decode($filter);
		if ($filter != null) {
			$this->doFilter($filter);
		}

		$count_db = clone $this->db;
		$count = $count_db->get('vw_billdoc_v3')->num_rows();

		isset($_GET['orderby']) ? $this->db->order_by($_GET['orderby']) : $this->db->order_by('NoBillingDoc', 'DESC');

		$items = $this->db->get('vw_billdoc_v3', $limit, $offset)->result();
		Log::info($this->db->last_query());

		foreach ($items as $key => $item) {
			$items[$key]->Hash = base64_encode($item->NoBillingDoc-986543);
			$items[$key]->Approval1 = $this->findApprovalById($item->NoBillingDoc, 0);
			$items[$key]->Approval2 = $this->findApprovalById($item->NoBillingDoc, 1);
			$items[$key]->Approval3 = $this->findApprovalById($item->NoBillingDoc, 2);
			$items[$key]->Status = $this->getStatusApproval($item);

			$orders = $this->db->where('NoBillingDoc', $item->NoBillingDoc)->get('SalesOrder')->result();
			$items[$key]->ItemCount = count($orders);
			$items[$key]->TotalNilai = $this->getTotalValue($orders);

			if ($item->PaymentMethod == 'M' || $item->PaymentMethod == 'P') {
				$items[$key]->UploadedFile = $this->db->where('Kategori', 2)
						->where('IDParent', $item->NoBillingDoc)
						->get('Attachment')->row() != null;
			}
		}

		$data = [
			'totalCount' => $count,
			'data' => $items,
		];

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function getStatusApproval($billingDocument)
	{
		$this->db->reset_query();

		if ($this->statuses == null) {
			$this->statuses = $this->db->where_in('ID', [21, 22, 23, 24, 26])->get('Status')->result();

			$temp = [];
			foreach ($this->statuses as $status) {
				$temp[$status->ID] = $status->Keterangan;
			}
			$this->statuses = $temp;
		}
		return $this->statuses[$billingDocument->Status];
	}

	public function findApprovalByUser($noBillingDoc, $user_id = 0, $status)
	{
		$items = $this->db
			->where('HistoryBillDoc.UserID', $user_id)
			->where('HistoryBillDoc.NoBillDoc', $noBillingDoc)
			->join('BillingDocument', 'BillingDocument.NoBillingDoc = HistoryBillDoc.NoBillDoc')
			->get('HistoryBillDoc');
		$count = $items->num_rows();
		if ($count > 0 && $status != 26) {
			return 'Approved';
		} else {
			if ($status == 26) {
				return 'Canceled';
			}

			return 'Belum Approve';
		}
	}

	public function findApprovalById($noBillingDoc, $step = 0)
	{
		$items = $this->db
			->select('Ms_Karyawan.Nama')
			->join('Ms_User', 'Ms_User.ID = HistoryBillDoc.UserID')
			->join('Ms_Karyawan', 'Ms_User.IDKaryawan = Ms_Karyawan.ID')
			->where_in('Action', [21, 22, 23, 24]) // action approval
			->where('NoBillDoc', $noBillingDoc)
			->order_by('HistoryBillDoc.Timestamp', 'ASC')
			->get('HistoryBillDoc');

		if (!$items->num_rows()) {
			return null;
		}

		$steps = [];
		foreach ($items->result() as $approval) {
			$steps[] = $approval->Nama;
		}

		return isset($steps[$step]) ? $steps[$step] : null;
	}

	public function get_user_email($id)
	{
		$row = $this->db->where('ID', $id)->select('Email')->get('Ms_User', 1)->row();
		return isset($row->Email) ? $row->Email : '???';
	}

	private function getTotalValue($orders)
	{
		$value = 0;
		foreach ($orders as $order) {
			$value += $order->NetValueBeforePPH2;
		}
		return $value;
	}
}
