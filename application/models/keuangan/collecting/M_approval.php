<?php 

/**
* 
*/
class M_approval extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('keuangan/collecting/m_billdoc','billdoc');
    }
    
    public function approve_document($bill_doc_id)
    {
        $user_id = Auth::user()->ID;
        $action_id = 21;

        $this->db->trans_start();

        $billingDocument = $this->db->where('Status !=',26)->where('NoBillingDoc',$bill_doc_id)->get('vw_billdoc')->row();

        // 1 user cant approve doble
        $hasUpdate = $this->db->where('Action', $action_id)
                            ->where('UserID', $user_id)
                            ->where('NoBillDoc', $bill_doc_id)
                            ->count_all_results('HistoryBillDoc');
        
        // sudah berapa orang yang approve?
        $approval_count = $this->db->where('NoBillDoc', $bill_doc_id)
                                ->where('Action', $action_id)
                                ->count_all_results('HistoryBillDoc');

        if( ! $hasUpdate && $approval_count < 3) 
        {

            if($this->billdoc->isFinalApproval($billingDocument->TotalNilai, $approval_count+1)) {
                $approval_status = 11;
            } else {
                $approval_status = $approval_count+9;
            }

            $this->db->where('NoBillingDoc', $bill_doc_id)
                    ->update('BillingDocument', array(
                        'Status'    => $approval_status
                    ));
                
            $this->db->insert('HistoryBillDoc', array(
                'NoBillDoc' => $bill_doc_id, 
                'Action'    => $action_id, // action approval,
                'UserID'    => $user_id, 
                'Timestamp' => date('Y-m-d H:i:s'),
                'Keterangan' => 'Approval'
            ));
        }

        $this->db->trans_complete();
    }
}
