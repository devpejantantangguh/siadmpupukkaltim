<?php 

class M_billdoc extends CI_Model
{
    public function getBillingStatus($billing)
    {
		if($billing['Status'] == 24) { // 24 final approve

			// jika user wapu, maka dia harus sudah di approva bukti potongnya
			if($billing['IsWAPU'] == 1 && empty($billing['Bukpot'])) {
				return 'ON PROGRESS';
			}
			return 'COMPLETED';
		}

    	return 'ON PROGRESS';
    }

    public function isFinalApproval($nominal, $approval_count = 0)
    {

        /*$limits = $this->db->query("SELECT g1.Value [LimitFP1], g2.Value [LimitFP2], g3.Value [LimitFP3]
            from GlobalSettings g1
            left join GlobalSettings g2 on g2.Setting = 'LimitFP2'
            left join GlobalSettings g3 on g3.Setting = 'LimitFP3'
            WHERE g1.Setting = 'LimitFP1'")->row();*/
        $limits = $this->db->query("SELECT g1.Value LimitFP1, g2.Value LimitFP2, g3.Value LimitFP3
            from GlobalSettings g1
            left join GlobalSettings g2 on g2.Setting = 'LimitFP2'
            left join GlobalSettings g3 on g3.Setting = 'LimitFP3'
            WHERE g1.Setting = 'LimitFP1'")->row();

        if($approval_count == 3)
            return true;

        $nilai[0] = 0;
        $nilai[1] = $limits->LimitFP1;
        $nilai[2] = $limits->LimitFP2;
        $nilai[3] = $limits->LimitFP3;

        if($nominal > $nilai[$approval_count]) {
            return false;
        } else {
            return true;
        }
    }

    public function getDokumenPajakStatus($approvedBy, $filePath)
    {
        if(empty($approvedBy)) {
            if(empty($filePath)) {
                return 'Unsubmitted';
            }else{
                return 'Submitted';
            }
        } else {
            return 'Approved';
        }
    }

    public function getMinimalYear($id_customer)
    {
        $query = $this->db->query("SELECT MIN(BillingDate) FROM BillingDocument WHERE IDCustomer = ? LIMIT 1", $id_customer);
        if($query->num_rows() > 0) {
            $row = $query->result();
            return $row[0]->BillingDate;
        } else {
            return date('Y');
        }
    }

}



/*

select
  b.ID,b.NoBillingDoc,b.Status, b.PaymentMethod,c.IsWAPU, dp1.ApprovedBy as [SPP], dp2.ApprovedBy as [BuktiPotong],sum(l.AmountLC) as [JumlahPiutang]
from BillingDocument b
left join Ms_Customer c on b.IDCustomer = c.IDCustomer
left join DokumenPajak dp1 on b.ID = dp1.IDParent and dp1.Tipe = 1
left join DokumenPajak dp2 on b.ID = dp2.IDParent and dp2.Tipe = 2
left join ListPiutang l on b.IDCustomer = l.IDCustomer
  -- where dp1.ApprovedBy is not null and dp2.ApprovedBy is not null
group by b.ID, b.NoBillingDoc,b.Status, b.PaymentMethod,c.IsWAPU, dp1.ApprovedBy,dp2.ApprovedBy
-- having sum(l.AmountLC) = 0 or sum(l.AmountLC) is null


*/
