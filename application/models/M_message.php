<?php 

class M_message extends CI_Model
{
    public function searchVendor($params)
    {
        $simona = $this->load->database('simona', TRUE);  

        if(is_numeric($params)) {
            $simona->like('KODE_VENDOR_SAP', $params);
        } else {
            $simona->like('NAMA', $params);
        }

        $simona->select('KODE_VENDOR_SAP,NAMA');
        $query = $simona->get('ms_vendor', 30);

        $lists = [];
        if($query->num_rows() > 0) {
            foreach($query->result() as $row) {
                $lists[] = [
                    'id' => $row->KODE_VENDOR_SAP, 
                    'text' => "{$row->KODE_VENDOR_SAP} - {$row->NAMA}"
                ];
            }
        } else {
            $lists = [];
        }

        return $lists;
    }

    public function vendorName($id_rekanan)
    {
        $simona = $this->load->database('simona', TRUE);  
        $row = $simona->where('KODE_VENDOR_SAP',$id_rekanan)->get('ms_vendor')->row();
        return "{$row->KODE_VENDOR_SAP} - {$row->NAMA}";    
    }

    public function customerName($id_rekanan)
    {
        $row = $this->db->where('IDCustomer', $id_rekanan)->get('Ms_Customer')->row();
        return "$row->IDCustomer - $row->Customer";
    }

    public function searchCustomer($params)
    {
        if(is_numeric($params)) {
            $this->db->like('IDCustomer', $params);
        } else {
            $this->db->like('Customer', $params);
        }

        $this->db->select('IDCustomer, Customer');
        $query = $this->db->get('Ms_Customer',20);
        
        $lists = [];
        if($query->num_rows() > 0) {
            foreach($query->result() as $row) {
                $lists[] = [
                    'id' => $row->IDCustomer, 
                    'text' => "{$row->IDCustomer} - {$row->Customer}"
                ];
            }
        } else {
            $lists = [];
        }

        return $lists;
    }
}
