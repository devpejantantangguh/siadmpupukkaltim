<?php 

class M_invoice extends CI_Model
{

    public function getEmailManager($id_invoice)
    {
        /*return $results = $this->db->select('Ms_User.Email,Ms_UnitKerja.CostCenter,Ms_Role.RoleName')
                            ->where('Invoice.ID',$id_invoice)
                            ->like('Ms_Role.RoleName','Manager')
                            ->join('Ms_Role','Ms_Role.ID = Ms_User.RoleID','left')
                            ->join('Ms_Karyawan','Ms_Karyawan.ID = Ms_User.IDKaryawan','left')
                            ->join('Ms_UnitKerja','Ms_UnitKerja.CostCenter = Ms_Karyawan.CostCenter','left')
                            ->join('Invoice','Invoice.CostCenter = Ms_UnitKerja.CostCenter','left')
                            ->get('Ms_User')->result();*/
        return $results = $this->db->select('Ms_UnitKerja.KepalaUnitKerja,Ms_User.Email,Ms_UnitKerja.CostCenter')
                            ->where('Invoice.ID',$id_invoice)
                            ->join('Invoice','Invoice.CostCenter = Ms_UnitKerja.CostCenter')
                            ->join('Ms_User','Ms_User.IDKaryawan = Ms_UnitKerja.KepalaUnitKerja')
                            ->get('Ms_UnitKerja')->result();
    }

    public function findStatusID($key)
    {
        return $this->db->limit('1')
                ->where('Status',$key)
                ->get('Status')
                ->row('ID');
    } 

    public function findActionID($key)
    {
        return $this->db->limit('1')
                ->where('Action',$key)
                ->get('VL_Action')
                ->row('ID');
    }   
    public function getLastKeterangan($invoice_id)
    {
        return $this->db->order_by('Timestamp','desc')
                ->where('IDInvoice',$invoice_id)
                ->where('Keterangan !=','')
                ->where('Keterangan !=',null)
                ->get('HistoryInvoice')
                ->row('Keterangan');
    }  
    public function getEmailCreator($invoice_id)
    {
        return $this->db->where('Invoice.ID',$invoice_id)
                ->join('Ms_User','Ms_User.ID = Invoice.CreatedBy')
                ->get('Invoice')
                ->row('Email');
    }
    public function isKirimEmail($id_invoice = false) {
        $status = statusID('WAITING APPROVAL');
        $CostCenter = $this->getDetail($id_invoice)->CostCenter;
        $this->db->where('CostCenter',$CostCenter);
        $this->db->where('SubmittedOn >=',date('Y-m-d'));
        $query = $this->db->where('Status',$status)
                ->get('Invoice');
        if($query->num_rows() > 1) {
            return false;
        }
        return true;

    }
    public function filter_ids_bystatus($ids = [], $status = null) {
        foreach ($ids as $key => $id) {
            if(!is_numeric($id)){
                $ids[$key] = decode_url($id);
            }
            
        }
        if(strpos($status,'-') !== false) {
            $xstatus = explode('-', $status);
            $this->db->where_in('Status', $xstatus);
        }elseif(empty($status)) {

        }else {
            $this->db->where('Status', $status);
        }
        $query = $this->db->where_in('ID',$ids)
                ->get('Invoice');

        $new_ids = [];
        foreach($query->result() as $row)
        {
            $new_ids[] = encode_url($row->ID); 
        }
        return $new_ids;

    }
    public function newHistory($id_invoice, $id_action, $id_status, $keterangan)
    {
        $data = [
            'IDInvoice'     => $id_invoice,
            'Action'        => $id_action,
            'IDStatus'        => $id_status,
            'UserID'        => Auth::user()->ID,
            'Timestamp'     => date('Y-m-d H:i:s'),
            'Keterangan'    => $keterangan
        ];
        $this->db->insert('HistoryInvoice', $data);

        return $this->db->affected_rows() > 0 ? true : false;
    }
    public function createHistory($id_invoice, $id_action, $keterangan)
    {
        $data = [
            'IDInvoice'     => $id_invoice,
            'Action'        => $id_action,
            'UserID'        => Auth::user()->ID,
            'Timestamp'     => date('Y-m-d H:i:s'),
            'Keterangan'    => $keterangan
        ];
        $this->db->insert('HistoryInvoice', $data);

        return $this->db->affected_rows() > 0 ? true : false;
    }

    public function getHistory($id_invoice)
    {
        return $this->db->select('HistoryInvoice.Keterangan, HistoryInvoice.Timestamp, VL_Action.Action, Ms_User.Email,Ms_Karyawan.Nama')
                        ->join('VL_Action','VL_Action.ID = HistoryInvoice.Action','left')
                        ->join('Ms_User','Ms_User.ID = HistoryInvoice.UserID','left')
                        ->join('Ms_Karyawan','Ms_User.IDKaryawan = Ms_Karyawan.ID','left')
                        ->where('IDInvoice', $id_invoice)
                        ->order_by('Timestamp', 'asc')
                        ->get('HistoryInvoice')->result();
    }

    public function getDetail($id_invoice)
    {
        return $this->db->where('ID', $id_invoice)
                        ->get('Invoice')->row();
    }
    public function setIDArea($id_invoice)
    {
        $area = Auth::unitKerja()->Area;
        $idarea = 3; //Pusat default
        if($area == 'Dept. PSO 1'){
                $idarea = 10;            
        }elseif($area == 'Dept. PSO 2'){
                $idarea = 20;       
        }

        $this->db->where('ID', $id_invoice)
                ->update('Invoice', array(
                    'IDArea'    => $idarea
                ));
    }
    public function updateIDArea($id_invoice,$action)
    {
/*
Jika terdapat invoice dengan nilai 25juta, maka invoice dari PSO1 & PSO2 akan diproses pembayarannya di Bontang/Pusat. Maka setelah invoice selesai diverifikasi oleh verifikator di daerah, maka akan merubah area menjadi area Pusat 
⁃   IDArea 10 menjadi 1
⁃   IDArea 20 menjadi 2
⁃   Jika pada keuangan terdapat action Reject dan kembali ke verifikator daerah, maka IDArea diubah lagi ke asalnya
- IDArea 1 menjadi 10
- IDArea 2 menjadi 20

*/

        $currentArea = Auth::unitKerja()->Area;
        $verified = statusID('VERIFIED');
        if($action == 'update'){

            if($currentArea == 'Dept. PSO 1' || $currentArea == 'Dept. PSO 2'){
                //jika >25 juta gantike 1 atau 2
                $IDArea = ($currentArea == 'Dept. PSO 1') ? 1 : 2;
                    $this->db->where('Status', $verified)->where('ID', $id_invoice)->where('NilaiInvoice >', 25000000)
                            ->update('Invoice', array(
                                'IDArea'    => $IDArea
                            ));

            }
        }
        if($action == 'reject'){
                //jika >25 juta && verified gantike 10
                $this->db->where('IDArea', 1)->where('ID', $id_invoice)->where('NilaiInvoice >', 25000000)
                            ->update('Invoice', array(
                                'IDArea'    => 10
                            ));
                //jika >25 juta gantike 20

                $this->db->where('IDArea', 2)->where('ID', $id_invoice)->where('NilaiInvoice >', 25000000)
                            ->update('Invoice', array(
                                'IDArea'    => 20
                            ));
            
        }
    }
}
