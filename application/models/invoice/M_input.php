<?php

class M_input extends CI_Model
{

    public function searchVendor($params)
    {
        $simona = $this->load->database('simona', TRUE);

        if (is_numeric($params)) {
            $simona->like('KODE_VENDOR_SAP', $params);
        } else {
            $simona->like('LOWER(NAMA)', strtolower($params));
        }

        $simona->select('KODE_VENDOR_SAP,NAMA,CITY');
        $query = $simona->get('ms_vendor', 30);

        $lists = [];
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $lists[] = [
                    'id' => $row->KODE_VENDOR_SAP,
                    'text' => "{$row->KODE_VENDOR_SAP} - {$row->NAMA} - {$row->CITY}"
                ];
            }
        } else {
            $lists = [];
        }

        return $lists;
    }

    public function getNamaVendor($id_vendor)
    {
        $simona = $this->load->database('simona', TRUE);

        $query = $simona->select('NAMA')->where('KODE_VENDOR_SAP', $id_vendor)->get('ms_vendor')->row();
        return $query->NAMA;
    }

    public function createInvoice($data)
    {
        $this->db->insert('Invoice', $data);

        return $this->db->insert_id();
    }

    // public function createInvoiceHistory($id_invoice, $keterangan) {
    //     $data = [
    //         'IDInvoice' => $id_invoice,
    //         'Action'    => '',
    //         'UserID'    => Auth::user()->ID,
    //         'Timestamp' => date('Y-m-d H:i:s'),
    //         'Keterangan' => $keterangan
    //     ];

    //     $this->db->insert('HistoryInvoice', $data);
    // }

    public function uploadAttachment($id, $path = './storage/invoice/')
    {
        $config['upload_path']          = $path;
        $config['allowed_types']        = '*';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file')) {
            $this->session->set_flashdata('message', $this->upload->display_errors());
            return false;
        } else {
            return $this->upload->data();
        }
    }

    public function createAttachment($id, $upload)
    {
        $data = [
            'IDParent'      => $id,
            'Kategori'      => 1,
            'Judul'         => substr($upload['raw_name'], 0, 50),
            'FilePath'      => $upload['file_name'],
            'UploadedBy'    => Auth::user()->ID,
            'UploadedOn'    => date('Y-m-d H:i:s')
        ];
        $this->db->insert('Attachment', $data);
    }

    public function get_detail_po_barang($nomor_po)
    {
        header('Content-Type: application/json');
        $simona = $this->load->database('simona', TRUE);

        $data = [
            'DeskripsiPO'   => '',
            'NilaiPO'       => 0,
            'KursPO'        => ''
        ];

        $sql = "SELECT PO_NO as NoPO,PO_DESKRIPSI as DeskripsiPO,SUM(PO_TOTAL) as NilaiPO, PO_CURR as KursPO from po_goods
                LEFT JOIN po_goods_item on po_goods.PO_GOODS_ID = po_goods_item.PO_GOODS_ID
                WHERE PO_NO = $nomor_po
                GROUP BY PO_NO";

        $query = $simona->query($sql);

        if ($query->num_rows() > 0) {
            $row = $query->row();

            $data['DeskripsiPO'] = $row->DeskripsiPO;
            $data['NilaiPO'] = number_format($row->NilaiPO, 0, ',', '.');
            $data['KursPO'] = $row->KursPO;
        }

        echo json_encode($data);
    }

    public function get_detail_po_jasa($nomor_po)
    {
        header('Content-Type: application/json');
        $simona = $this->load->database('simona', TRUE);

        $data = [
            'DeskripsiPO'   => '',
            'NilaiPO'       => 0,
            'KursPO'        => '',
            'NilaiKontrak'  => 0
        ];

        $sql = "SELECT
                a.PO_SVC_ID, PO_NO as NoPO, PO_DESKRIPSI as DeskripsiPO,PO_CURR as KursPO,SUM(PO_VALUE) as NilaiPO, NILAI_KONTRAK as NilaiKontrak
                FROM po_svc a
                LEFT JOIN po_svc_item b on a.PO_SVC_ID = b.PO_SVC_ID
                LEFT JOIN receiving_svc c on a.PO_SVC_ID = c.PO_SVC_ID
                WHERE a.PO_NO = $nomor_po
                GROUP BY PO_NO";

        $query = $simona->query($sql);

        if ($query->num_rows() > 0) {
            $row = $query->row();

            $data['DeskripsiPO'] = $row->DeskripsiPO;
            $data['NilaiPO'] = number_format($row->NilaiPO, 0, ',', '.');
            $data['KursPO'] = $row->KursPO;
            $data['NilaiKontrak'] = $row->NilaiKontrak;
        }

        echo json_encode($data);
    }

    public function get_sagr_barang($id_po)
    {
        $sql = "SELECT  PO_NO as NoPO, NO_MAT_DOC as NoSAGR, TGL_GR as TanggalSAGR from po_goods
                left join po_goods_item on po_goods.PO_GOODS_ID = po_goods_item.PO_GOODS_ID
                left join receiving_goods_item on receiving_goods_item.PO_GOODS_ITEM_ID = po_goods_item.PO_GOODS_ITEM_ID
                WHERE JENIS_GR = 105 AND PO_NO = $id_po
                GROUP BY NO_MAT_DOC;";

        $simona = $this->load->database('simona', TRUE);
        $query = $simona->query($sql);

        $lists = [];
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $lists[] = [
                    'id'    => $row->NoSAGR,
                    'text'  => $row->NoSAGR
                ];
            }
        }

        echo json_encode($lists);
    }

    public function get_sagr_jasa($id_po)
    {
        $sql = "SELECT a.PO_NO,c.NO_SA as NoSAGR, c.TGL_SA, c.NO_BAPP FROM po_svc a
                    JOIN po_svc_item b ON a.PO_SVC_ID = b.PO_SVC_ID
                    JOIN receiving_svc_item c ON c.PO_SVC_ITEM_ID = b.PO_SVC_ITEM_ID
                    WHERE a.PO_NO = ?";

        $simona = $this->load->database('simona', TRUE);
        $query = $simona->query($sql, $id_po);

        $lists = [];
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $lists[] = [
                    'id'    => $row->NoSAGR,
                    'text'  => $row->NoSAGR
                ];
            }
        }

        echo json_encode($lists);
    }

    public function get_detail_sagr_barang($no_sagr)
    {
        header('Content-Type: application/json');
        $simona = $this->load->database('simona', TRUE);

        $sql = "SELECT NO_MAT_DOC as NoSAGR, TGL_GR as TanggalSAGR FROM receiving_goods_item where JENIS_GR = 105 AND NO_MAT_DOC = $no_sagr";

        $query = $simona->query($sql);

        if ($query->num_rows() > 0) {
            $data = ['TanggalSAGR' => date('d-m-Y', strtotime($query->row('TanggalSAGR')))];
        } else {
            $data = ['TanggalSAGR' => ''];
        }

        echo json_encode($data);
    }

    public function get_detail_sagr_jasa($no_sagr)
    {
        header('Content-Type: application/json');
        $simona = $this->load->database('simona', TRUE);

        $sql = "SELECT NO_SA as NoSAGR, TGL_SA as TanggalSAGR, NO_BAPP as NomorBA FROM receiving_svc_item WHERE NO_SA = $no_sagr";

        $query = $simona->query($sql);

        if ($query->num_rows() > 0) {
            $data = [
                'TanggalSAGR'   => date('d-m-Y', strtotime($query->row('TanggalSAGR'))),
                'NomorBA'       => $query->row('NomorBA')
            ];
        } else {
            $data = ['TanggalSAGR' => '', 'NomorBA' => ''];
        }

        echo json_encode($data);
    }
}
