<?php 

class M_wum extends CI_Model
{
    public function get_list_karyawan()
    {
        $query = $this->db->get('Ms_Karyawan');

        if($query->num_rows() > 0) {
            $lists = [];
            foreach($query->result() as $row) {
                $lists[$row->ID] = "$row->ID - $row->Nama";
            }
            return $lists;
        }
        return [];
    }

    public function get_payment_term()
    {
        $result = [];
        $query = $this->db->get('VL_PaymentTerm');

        if($query->num_rows() > 0) {
            foreach($query->result_array() as $row) {
                $result[$row['ID']] = $row['PaymentTerm'];
            }
        }
        return $result;
    }

    public function get_payment_term_time($id)
    {
        $addDay = $this->db->where('PaymentTerm', $id)->get('VL_PaymentTerm')->row('JumlahHari');
        $date = date('d M Y', strtotime("+{$addDay} day"));
        return "Estimasi Pembayaran $date";
    }

    public function get_detail($id)
    {
        $query = $this->db->where('ID', $id)->get('Invoice');

        if($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function decode_matriks($string)
    {
        if(empty($string)) {
            return [];
        } 

        $matriks = unserialize($string);
        $lists = [];
        foreach($matriks as $key => $matrik) {
            $lists[$key] = true;
        }
        return $lists;
    }
}
