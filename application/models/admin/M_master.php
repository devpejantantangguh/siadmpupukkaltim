<?php 

class M_master extends CI_Model
{
    public function getUserRole()
    {
        $rows = $this->db->select('ID,RoleName')->get('Ms_Role')->result();

        $lists = [];
        foreach($rows as $row) {
            $lists[] = ['ID' => $row->ID,'Name' => $row->RoleName];
        }

        return $lists;
    }

    public function getUserType()
    {
        return [
            ["ID" => 1,"Name" => 'Website'],
            ["ID" => 2,"Name" => 'App']
        ];
    }

    public function getListCustomer()
    {
        $rows = $this->db->select('IDCustomer,Customer')->get('Ms_Customer')->result();

        $lists = [];
        foreach($rows as $row) {
            $lists[] = ['ID' => $row->IDCustomer,'Name' => $row->Customer];
        }

        return $lists;
    }

    public function getKaryawan()
    {
        $rows = $this->db->select('ID,Nama')->get('Ms_Karyawan')->result();

        $lists = [];
        foreach($rows as $row) {
            $lists[] = ['ID' => $row->ID,'Name' => "$row->ID - $row->Nama"];
        }

        return $lists;
    }

    public function getCostCenter()
    {
        $rows = $this->db->select('CostCenter,UnitKerja')->get('Ms_UnitKerja')->result();

        $lists = [];
        foreach($rows as $row) {
            $lists[] = ['ID' => $row->CostCenter,'Name' => $row->UnitKerja];
        }

        return $lists;
    }

    public function getCostCenter2()
    {
        $rows = $this->db->select('CostCenter,UnitKerja')->get('Ms_UnitKerja')->result();

        $lists = [];
        foreach($rows as $row) {
            $lists[] = ['ID' => $row->CostCenter,'Name' => "$row->CostCenter - $row->UnitKerja"];
        }

        return $lists;
    }

    public function getListVendor()
    {
        $simona = $this->load->database('simona', TRUE);  
        $rows = $simona->select('KODE_VENDOR_SAP,NAMA')->get('ms_vendor')->result();

        $lists = [];
        foreach($rows as $row) {
            $lists[] = ['ID' => $row->KODE_VENDOR_SAP,'Name' => "$row->KODE_VENDOR_SAP $row->NAMA"];
        }

        return $lists;
    }

    public function getPropinsi()
    {
        $rows = $this->db->select('ID,Propinsi')->get('Ms_Propinsi')->result();
        $lists = [];
        foreach($rows as $row) {
            $lists[] = ['ID' => $row->ID,'Name' => $row->Propinsi];
        }

        return $lists;
    }

    public function getCountry()
    {
        $rows = $this->db->select('ID,Country')->get('Ms_Country')->result();
        $lists = [];
        foreach($rows as $row) {
            $lists[] = ['ID' => $row->ID,'Name' => $row->Country];
        }

        return $lists;
    }

    public function getKota()
    {
        $rows = $this->db->select('ID,Kota')->get('Ms_Kota')->result();
        $lists = [];
        foreach($rows as $row) {
            $lists[] = ['ID' => $row->ID,'Name' => $row->Kota];
        }

        return $lists;
    }

    
}
