<?php 

use Faker\Factory;

class M_panduan extends CI_Model
{
    public function ajax_list()
    {
        $limit = isset($_GET['take']) ? $_GET['take'] : 100;
        $offset = isset($_GET['skip']) ? $_GET['skip'] : 0;

        if(isset($_GET['orderby'])) {
            $this->db->order_by($_GET['orderby']);
        } else {
            $this->db->order_by('ID', 'DESC');
        }

        $items = $this->db->get('Panduan', $limit, $offset)->result();
        $count = $this->db->count_all('Panduan');

        $data = json_decode(json_encode(array(
            'totalCount'    => $count,
            'data'          => $items
        )));

        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function dummy()
    {
        $faker = Factory::create();

        foreach(range(0,10) as $i) {
            $data= [];
            $data['Judul'] = $faker->sentence(1);
            $data['Deskripsi'] = $faker->sentence(1);
            $data['FilePath'] = '/var/www/htdocs/index.html';
            $data['Keterangan'] = $faker->sentence(1);
            $data['AKtif'] = $faker->randomElement([1,0]);
            $data['CreatedBy'] = $faker->randomElement([1,2,3,4]);
            $data['CreatedOn'] = $faker->date();
            $data['UpdatedBy'] = $faker->randomElement([1,2,3,4]);
            $data['UpdatedOn'] = $faker->date();

            $this->db->insert('Panduan', $data);
        }
    }

    
}
