<?php 

class M_vl extends CI_Model
{
    
    public function get_PaymentTerm()
    {
        $result = [];
        $query = $this->db->get('VL_PaymentTerm');
        if( $query->num_rows() > 0 ) {
            foreach($query->result_array() as $row) {
                $result[] = [
                    'ID'    => (int) trim($row['ID']),
                    'PaymentTerm'  => $row['PaymentTerm'],
                    'JumlahHari'  => $row['JumlahHari'],
                ];    
            }
        } 

        return $result;
    }    
    public function get_status($kategori = null)
    {
        $result = [];

        if($kategori != null) {
            $this->db->where('Kategori', $kategori);
        }

        $query = $this->db->get('Status');
        if( $query->num_rows() > 0 ) {
            foreach($query->result_array() as $row) {
                $result[] = [
                    'id'    => trim($row['ID']),
                    'name'  => $row['Status']
                ];    
            }
        } 

        return $result;
    }

    public function get_jenis_transaksi($type = 'all')
    {
        $result = [];

        if($type == 'po') {
            $this->db->where('Kategori',1)->where('Keterangan',null);
        } elseif($type = 'wum') {
            $this->db->where('Kategori',1)->where('Keterangan','WUM');
        }

        $query = $this->db->get('VL_JenisTransaksi');

        if($query->num_rows() > 0) {
            foreach($query->result_array() as $row) {
                $result[$row['ID']] = $row['JenisTransaksi'];
            }
        }

        return $result;
    }

    public function get_payment_term()
    {
        $result = [];
        $query = $this->db->get('VL_PaymentTerm');

        if($query->num_rows() > 0) {
            foreach($query->result_array() as $row) {
                $result[$row['PaymentTerm']] = $row['PaymentTerm'];
            }
        }
        return $result;
    }
    public function get_potongan()
    {
        $result = [];
        $query = $this->db->get('VL_PotonganInvoice');
        $result[''] ='';
        if($query->num_rows() > 0) {
            foreach($query->result_array() as $row) {
                $result[$row['ID']] = $row['Potongan'];
            }
        }

        return $result;
    }

    public function get_kurs()
    {
        $kurs = $this->db->select('Kurs')->get('VL_Kurs')->result();

        $lists = [];
        foreach($kurs as $bil) {
            $lists[$bil->Kurs] = $bil->Kurs;
        }

        return $lists;
    }


}
