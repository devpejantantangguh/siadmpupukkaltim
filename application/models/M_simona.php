<?php 

class M_simona extends CI_Model
{
    var $simona;

    public function __construct()
    {
        $this->simona = $this->load->database('simona', TRUE);  
    }

    public function get_array_po()
    {
        $rows = $this->get_purcashe_order();

        $lists = [];
        foreach ($rows as $row) {
            $lists[$row['NoPO']] = $row['NoPO'];
        }

        return $lists;
    }

    public function get_purcashe_order()
    {
        return array(
            0 => array(
                'NoPO' => '5000003421',
                'NilaiPO' => '50000000',
                'KursPO' => 'IDR',
                'DeskripsiPO' => 'Bearing 50 inch',
                'IDVendor' => '1000000232',
                'NamaVendor' => 'PT Sentra Mekanika',
                'NomorKontrak' => '5100002324',
                'NilaiKontrak' => '75000000',
                'KursKontrak' => 'IDR',
                'NoSAGR' => '3100000442',
                'TanggalSAGR' => '13/2/2018'
            ),
            1 => array(
                'NoPO' => '5000003450',
                'NilaiPO' => '25000000',
                'KursPO' => 'IDR',
                'DeskripsiPO' => 'Bearing 50 inch',
                'IDVendor' => '1000000232',
                'NamaVendor' => 'PT Sentra Mekanika',
                'NomorKontrak' => '5100002324',
                'NilaiKontrak' => '75000000',
                'KursKontrak' => 'IDR',
                'NoSAGR' => '3100000520',
                'TanggalSAGR' => '20/3/2018'
            ),
            2 => array(
                'NoPO' => '5000001001',
                'NilaiPO' => '100000000',
                'KursPO' => 'IDR',
                'DeskripsiPO' => 'Small Vessel',
                'IDVendor' => '1000000442',
                'NamaVendor' => 'PT Indomatt',
                'NomorKontrak' => '5100002001',
                'NilaiKontrak' => '100000000',
                'KursKontrak' => 'IDR',
                'NoSAGR' => '3100000222',
                'TanggalSAGR' => '30/3/2018'
            ),
            3 => array(
                'NoPO' => '5000002299',
                'NilaiPO' => '350000000',
                'KursPO' => 'IDR',
                'DeskripsiPO' => 'Pengaspalan Jalan',
                'IDVendor' => '1000000665',
                'NamaVendor' => 'Bontang Teknik',
                'NomorKontrak' => '5100001444',
                'NilaiKontrak' => '350000000',
                'KursKontrak' => 'IDR',
                'NoSAGR' => '3100000632',
                'TanggalSAGR' => '31/3/2018'
            ),
            4 => array(
                'NoPO' => '5000003133',
                'NilaiPO' => '35200',
                'KursPO' => 'USD',
                'DeskripsiPO' => 'Catalyst',
                'IDVendor' => '1000000532',
                'NamaVendor' => 'Kellog Brown',
                'NomorKontrak' => '5100000045',
                'NilaiKontrak' => '35200',
                'KursKontrak' => 'USD',
                'NoSAGR' => '3100001222',
                'TanggalSAGR' => '28/2/2018'
            ),
        );
    }

    public function find_vendor_by_po($nomor_po)
    {
        $rows = $this->get_purcashe_order();

        foreach($rows as $row) {
            if($row['NoPO'] == $nomor_po) {
                return ['IDVendor' => $row['IDVendor'], 'NamaVendor' => $row['NamaVendor']];
            }
        }

    }

    public function get_jaminan()
    {
        return array(
            0 => array(
                'IDJaminan' => '1',
                'IDInvoice' => 'INV/01/SM-PKT/2018',
                'IDVendor' => '1000000232',
                'NomorPO' => '5000003421',
                'JenisJaminan' => 'Setoran Tunai',
                'NomorJaminan' => '123-MAN-SM/2017',
                'NilaiJaminan' => '35000000',
                'KursJaminan' => 'IDR',
                'BankPenerbit' => 'MANDIRI',
                'TanggalJaminan' => '30/12/2017',
                'Keterangan' => ''
            ),
            1 => array(
                'IDJaminan' => '2',
                'IDInvoice' => 'INV/03/SM-PKT/2018',
                'IDVendor' => '1000000232',
                'NomorPO' => '5000003450',
                'JenisJaminan' => 'Setoran Tunai',
                'NomorJaminan' => '129-MAN-SM/2017',
                'NilaiJaminan' => '15000000',
                'KursJaminan' => 'IDR',
                'BankPenerbit' => 'MANDIRI',
                'TanggalJaminan' => '31/12/2017',
                'Keterangan' => ''
            ),
            2 => array(
                'IDJaminan' => '3',
                'IDInvoice' => 'IM/201/III/2018',
                'IDVendor' => '1000000442',
                'NomorPO' => '5000001001',
                'JenisJaminan' => 'Bank Garansi',
                'NomorJaminan' => '3121/BNI/11.2017',
                'NilaiJaminan' => '75000000',
                'KursJaminan' => 'IDR',
                'BankPenerbit' => 'BNI 46',
                'TanggalJaminan' => '23/11/2017',
                'Keterangan' => ''
            ),
            3 => array(
                'IDJaminan' => '4',
                'IDInvoice' => '321/3.2018/PKT',
                'IDVendor' => '1000000665',
                'NomorPO' => '5000002299',
                'JenisJaminan' => 'Bank Garansi',
                'NomorJaminan' => '4112/BNI/10.2017',
                'NilaiJaminan' => '300000000',
                'KursJaminan' => 'IDR',
                'BankPenerbit' => 'BNI 46',
                'TanggalJaminan' => '14/10/2017',
                'Keterangan' => ''
            ),
            4 => array(
                'IDJaminan' => '5',
                'IDInvoice' => '4320000012',
                'IDVendor' => '1000000532',
                'NomorPO' => '5000003133',
                'JenisJaminan' => 'Bank Garansi',
                'NomorJaminan' => 'OC231-011',
                'NilaiJaminan' => '28000',
                'KursJaminan' => 'USD',
                'BankPenerbit' => 'OCBC',
                'TanggalJaminan' => '29/10/2017',
                'Keterangan' => ''
            ),
        );
    }
}
