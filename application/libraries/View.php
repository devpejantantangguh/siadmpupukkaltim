<?php 

use Philo\Blade\Blade;

class View
{
	
	function __construct()
	{
		# code...
	}

	public function make($path, $vars = array())
	{
		$views = APPPATH.'views';
		$cache = APPPATH.'cache';

		$blade = new Blade($views, $cache);
		echo $blade->view()->make($path)->render();
	}
}
