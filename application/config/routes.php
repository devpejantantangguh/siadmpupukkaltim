<?php
defined('BASEPATH') or exit('No direct script access allowed');

$route['login'] = 'authorization/login';
$route['logout'] = 'authorization/logout';
$route['d3v3l0p3r/login'] = 'authorization/login_playground';
$route['dashboard'] = 'home/dashboard';
$route['admin/master/customer'] = 'admin/customer/index';
$route['admin/master/customer/create'] = 'admin/customer/create';
$route['admin/master/customer/store'] = 'admin/customer/store';
$route['admin/master/customer/edit/(:any)'] = 'admin/customer/edit/$1';
$route['admin/master/customer/update/(:any)'] = 'admin/customer/update/$1';
$route['admin/master/customer/delete'] = 'admin/customer/destroy';

$route['admin/master/users'] = 'admin/users/index';
$route['admin/master/users/create'] = 'admin/users/create';
$route['admin/master/users/create/(:any)'] = 'admin/users/create/$1';
$route['admin/master/users/store'] = 'admin/users/store';
$route['admin/master/users/edit/(:any)'] = 'admin/users/edit/$1';
$route['admin/master/users/edit/(:any)/(:any)'] = 'admin/users/edit/$1/$2';
$route['admin/master/users/update/(:any)'] = 'admin/users/update/$1';
$route['admin/master/users/update/(:any)/(:any)'] = 'admin/users/update/$1/$2';
$route['admin/master/users/delete/(:any)'] = 'admin/users/destroy/$1';

$route['admin/master/list_vendor'] = 'admin/master/list_vendor';
$route['admin/master/(:any)'] = 'admin/master/index/$1';

// $route['admin/master/karyawan'] = 'admin/master/index';


$route['default_controller'] = 'home';
$route['404_override'] = 'errors/404';
$route['translate_uri_dashes'] = FALSE;
