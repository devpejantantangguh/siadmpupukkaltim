CREATE OR REPLACE VIEW vw_billdoc AS
SELECT
    b.ID,
    b.NoBillingDoc,
    b.IDCustomer,
    b.Customer,
    b.BillingDate,
    b.NoSO,
    b.IDPayer,
    b.Kurs,
    b.IDMaterial,
    b.Material,
    b.UoM,
    b.Kuantum,
    s.HargaSatuan,
    s.TglFakturPajak,
    s.NoFakturPajak,
    s.TaxAmount,
    s.NetValueBeforePPH2,
    s.BillT,
    s.FPOption,
    (SELECT COUNT(*) FROM HistoryBilldoc h where b.NoBillingDoc = h.NoBillDoc) as ApproveCount,
    dp1.FilePath as SPP_Path,
    dp1.ApprovedBy as SPP,
    dp2.FilePath as Bukpot_Path,
    dp2.ApprovedBy as Bukpot,
    (SELECT SUM(ss.NetValueBeforePPH2) FROM SalesOrder ss WHERE b.NoSO = s.NoSO AND b.IDMaterial = s.IDMaterial) as TotalNilai,
    (SELECT SUM(ss.TaxAmount) FROM SalesOrder ss WHERE b.NoSO = s.NoSO AND b.IDMaterial = s.IDMaterial) as TotalTax
FROM
BillingDocument b
LEFT JOIN SalesOrder s on b.NoSO = s.NoSO AND b.IDMaterial = s.IDMaterial
LEFT JOIN DokumenPajak dp1 ON b.ID = dp1.IDParent and dp1.Tipe = 1
LEFT JOIN DokumenPajak dp2 ON b.ID = dp2.IDParent and dp2.Tipe = 2