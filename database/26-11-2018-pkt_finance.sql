# ************************************************************
# Sequel Pro SQL dump
# Version 4529
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: localhost (MySQL 5.6.38)
# Database: pkt_finance
# Generation Time: 2018-11-26 11:48:55 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table AppliedDocument
# ------------------------------------------------------------

CREATE TABLE `AppliedDocument` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDInvoice` int(11) DEFAULT NULL,
  `IDCustomer` varchar(10) DEFAULT NULL,
  `ARDocNo` varchar(10) DEFAULT NULL,
  `ARAmount` decimal(9,0) DEFAULT NULL,
  `AppliedBy` varchar(10) DEFAULT NULL,
  `AppliedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(10) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `AppliedAmount` decimal(9,0) DEFAULT NULL,
  `KursApplied` varchar(3) DEFAULT NULL,
  `Kurs` char(3) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Attachment
# ------------------------------------------------------------

CREATE TABLE `Attachment` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDParent` int(11) DEFAULT NULL,
  `Kategori` int(11) DEFAULT NULL,
  `FilePath` varchar(255) DEFAULT NULL,
  `Judul` varchar(50) DEFAULT NULL,
  `UploadedBy` varchar(10) DEFAULT NULL,
  `UploadedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table BillingDocument
# ------------------------------------------------------------

CREATE TABLE `BillingDocument` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `NoBillingDoc` varchar(10) DEFAULT NULL,
  `JenisTransaksi` smallint(6) DEFAULT NULL,
  `Sektor` varchar(5) DEFAULT NULL,
  `BillingDate` date DEFAULT NULL,
  `NoSO` varchar(10) DEFAULT NULL,
  `NoGI` varchar(10) DEFAULT NULL,
  `IDCustomer` varchar(10) DEFAULT NULL,
  `Customer` varchar(30) DEFAULT NULL,
  `Status` char(2) DEFAULT NULL,
  `IDMaterial` varchar(10) DEFAULT NULL,
  `Material` varchar(100) DEFAULT '',
  `Kuantum` decimal(8,0) DEFAULT NULL,
  `UoM` varchar(7) DEFAULT NULL,
  `Kurs` varchar(3) DEFAULT NULL,
  `Keterangan` varchar(255) DEFAULT NULL,
  `IDPayer` varchar(10) DEFAULT NULL,
  `Payer` varchar(30) DEFAULT NULL,
  `IDSalesOrg` varchar(10) DEFAULT NULL,
  `SalesOrg` varchar(30) DEFAULT NULL,
  `LifeCycle` tinyint(4) DEFAULT NULL,
  `LastDownload` datetime DEFAULT NULL,
  `NoBAST` varchar(20) DEFAULT NULL,
  `NoRegSurveyor` varchar(20) DEFAULT NULL,
  `NoSKBDN` varchar(20) DEFAULT NULL,
  `Bank` varchar(20) DEFAULT NULL,
  `TglSKBDN` date DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table DokumenFinansial
# ------------------------------------------------------------

CREATE TABLE `DokumenFinansial` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDKasBesar` int(11) DEFAULT NULL,
  `NoDokumen` varchar(30) DEFAULT NULL,
  `TahunDokumen` varchar(4) NOT NULL,
  `Keterangan` varchar(50) DEFAULT NULL,
  `Deskripsi` varchar(50) DEFAULT NULL,
  `CreatedOn` datetime NOT NULL,
  `CreatedBy` varchar(10) NOT NULL,
  `ReturnedOn` datetime DEFAULT NULL,
  `ReturnedBy` varchar(10) NOT NULL,
  `LifeCycle` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table DokumenPajak
# ------------------------------------------------------------

CREATE TABLE `DokumenPajak` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDParent` int(11) DEFAULT NULL,
  `Kategori` char(1) DEFAULT NULL,
  `Tipe` char(1) DEFAULT NULL,
  `Judul` varchar(50) DEFAULT NULL,
  `FilePath` varchar(255) DEFAULT NULL,
  `CreatedBy` varchar(10) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `ApprovedBy` varchar(10) DEFAULT NULL,
  `ApprovedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table FakturPajak
# ------------------------------------------------------------

CREATE TABLE `FakturPajak` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDParent` int(11) DEFAULT NULL,
  `FileName` varchar(255) DEFAULT NULL,
  `NPWP_PKT` varchar(50) DEFAULT NULL,
  `NoFakturPajak` varchar(50) DEFAULT NULL,
  `NPWP_Customer` varchar(50) DEFAULT NULL,
  `Timestamp` datetime DEFAULT NULL,
  `CreatedBy` varchar(10) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(10) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table GlobalSettings
# ------------------------------------------------------------

CREATE TABLE `GlobalSettings` (
  `Setting` varchar(50) NOT NULL DEFAULT '',
  `Value` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`Setting`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table HistoryBillDoc
# ------------------------------------------------------------

CREATE TABLE `HistoryBillDoc` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `NoBillDoc` varchar(10) DEFAULT NULL,
  `Action` char(2) DEFAULT NULL,
  `UserID` varchar(10) DEFAULT NULL,
  `Timestamp` datetime DEFAULT NULL,
  `Keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table HistoryInvoice
# ------------------------------------------------------------

CREATE TABLE `HistoryInvoice` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDInvoice` varchar(10) DEFAULT NULL,
  `Action` char(2) DEFAULT NULL,
  `UserID` varchar(10) DEFAULT '',
  `Timestamp` datetime DEFAULT NULL,
  `Keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Invoice
# ------------------------------------------------------------

CREATE TABLE `Invoice` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `NoDokumen` varchar(10) DEFAULT NULL,
  `JenisTransaksi` smallint(6) DEFAULT NULL,
  `IDVendor` varchar(10) DEFAULT NULL,
  `NamaVendor` varchar(50) DEFAULT NULL,
  `NoInvoice` varchar(50) DEFAULT NULL,
  `NilaiInvoice` float DEFAULT NULL,
  `PPN` tinyint(4) DEFAULT NULL,
  `KursInvoice` char(3) DEFAULT NULL,
  `NoFakturPajak` varchar(25) DEFAULT NULL,
  `TglFakturPajak` date DEFAULT NULL,
  `AmountPaid` float DEFAULT NULL,
  `NoTransmittal` varchar(20) DEFAULT NULL,
  `TglTransmittal` date DEFAULT NULL,
  `Status` tinyint(4) DEFAULT NULL,
  `LifeCycle` tinyint(4) DEFAULT NULL,
  `Piutang` tinyint(4) DEFAULT NULL,
  `Tahun` varchar(4) DEFAULT NULL,
  `PaymentTerm` varchar(4) DEFAULT NULL,
  `NoDokumenSAP` varchar(10) DEFAULT NULL,
  `TglEstimasiBayar` date DEFAULT NULL,
  `PaidAmount` float DEFAULT NULL,
  `KursPaid` varchar(3) DEFAULT NULL,
  `IsPriority` tinyint(4) DEFAULT NULL,
  `Matriks` varchar(100) DEFAULT NULL,
  `TglInvoice` date DEFAULT NULL,
  `NilaiVerifikasi` float DEFAULT NULL,
  `CostCenter` varchar(20) DEFAULT '',
  `Keterangan` varchar(50) DEFAULT '',
  `StatusTransmittal` tinyint(4) DEFAULT '0',
  `CreatedOn` datetime DEFAULT NULL,
  `CreatedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Jaminan
# ------------------------------------------------------------

CREATE TABLE `Jaminan` (
  `IDJaminan` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDInvoice` int(11) DEFAULT NULL,
  `IDVendor` varchar(10) DEFAULT NULL,
  `NomorPO` varchar(10) DEFAULT NULL,
  `JenisJaminan` varchar(25) DEFAULT NULL,
  `NomorJaminan` varchar(30) DEFAULT NULL,
  `BankPenerbit` varchar(20) DEFAULT NULL,
  `NilaiJaminan` int(11) DEFAULT NULL,
  `KursJaminan` char(3) DEFAULT NULL,
  `TanggalJaminan` date DEFAULT NULL,
  `Keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IDJaminan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table KasBesar
# ------------------------------------------------------------

CREATE TABLE `KasBesar` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `JenisTransaksi` int(11) DEFAULT NULL,
  `NPKRequester` varchar(10) DEFAULT NULL,
  `CostCenter` varchar(10) DEFAULT NULL,
  `NPKAtasan` varchar(10) DEFAULT NULL,
  `Nominal` decimal(18,0) DEFAULT NULL,
  `Deskripsi` varchar(255) DEFAULT NULL,
  `DueDate` date DEFAULT NULL,
  `Keterangan` varchar(255) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `CreatedBy` varchar(10) DEFAULT '',
  `CreatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(10) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `ApprovedBy` varchar(10) DEFAULT NULL,
  `ApprovedOn` datetime DEFAULT NULL,
  `LifeCycle` tinyint(1) DEFAULT '0',
  `NoPeminjaman` varchar(20) DEFAULT NULL,
  `TanggalPengembalian` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table KursValas
# ------------------------------------------------------------

CREATE TABLE `KursValas` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Tanggal` date DEFAULT NULL,
  `Currency` tinyint(4) DEFAULT NULL,
  `CreatedBy` varchar(10) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `Kurs` double DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table ListPiutang
# ------------------------------------------------------------

CREATE TABLE `ListPiutang` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDCustomer` varchar(10) DEFAULT NULL,
  `ARDocNo` varchar(10) DEFAULT NULL,
  `NoBillingDoc` varchar(10) DEFAULT NULL,
  `NoSO` varchar(10) DEFAULT NULL,
  `DocumentDate` date DEFAULT NULL,
  `PaymentMethod` varchar(2) DEFAULT NULL,
  `Deskripsi` varchar(100) DEFAULT NULL,
  `AmountDC` decimal(18,0) DEFAULT NULL,
  `KursDC` varchar(3) DEFAULT NULL,
  `AmountLC` decimal(18,0) DEFAULT NULL,
  `KursLC` varchar(3) DEFAULT '',
  `CreatedBy` varchar(10) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table MatriksDokumen
# ------------------------------------------------------------

CREATE TABLE `MatriksDokumen` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `JenisTransaksi` tinyint(4) DEFAULT NULL,
  `Dokumen` varchar(20) DEFAULT NULL,
  `IsRequired` tinyint(4) DEFAULT NULL,
  `Keterangan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Ms_Country
# ------------------------------------------------------------

CREATE TABLE `Ms_Country` (
  `ID` varchar(3) NOT NULL DEFAULT '',
  `Country` varchar(20) DEFAULT NULL,
  `Keterangan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Ms_Customer
# ------------------------------------------------------------

CREATE TABLE `Ms_Customer` (
  `IDCustomer` varchar(10) NOT NULL DEFAULT '',
  `PairedVendorID` varchar(10) DEFAULT NULL,
  `NPWP` varchar(30) DEFAULT NULL,
  `Title` varchar(10) DEFAULT NULL,
  `Customer` varchar(50) DEFAULT NULL,
  `IsWAPU` tinyint(4) DEFAULT NULL,
  `Alamat1` varchar(200) DEFAULT NULL,
  `Alamat2` varchar(200) DEFAULT NULL,
  `Kota` varchar(20) DEFAULT NULL,
  `Propinsi` varchar(30) DEFAULT NULL,
  `Country` varchar(20) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `Phone` varchar(20) DEFAULT NULL,
  `KodePos` varchar(10) DEFAULT NULL,
  `Keterangan` varchar(50) DEFAULT NULL,
  `CreatedBy` varchar(10) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(10) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`IDCustomer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Ms_Karyawan
# ------------------------------------------------------------

CREATE TABLE `Ms_Karyawan` (
  `ID` varchar(10) NOT NULL DEFAULT '',
  `Nama` varchar(100) DEFAULT NULL,
  `CostCenter` varchar(100) DEFAULT NULL,
  `Keterangan` varchar(50) DEFAULT NULL,
  `NPKLama` varchar(7) DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Ms_Kota
# ------------------------------------------------------------

CREATE TABLE `Ms_Kota` (
  `ID` varchar(10) NOT NULL DEFAULT '',
  `IDPropinsi` varchar(3) DEFAULT NULL,
  `Kota` varchar(100) DEFAULT NULL,
  `Keterangan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Ms_Kurs
# ------------------------------------------------------------

CREATE TABLE `Ms_Kurs` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Currency` varchar(3) DEFAULT NULL,
  `Image` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Ms_Material
# ------------------------------------------------------------

CREATE TABLE `Ms_Material` (
  `IDMaterial` varchar(7) NOT NULL DEFAULT '',
  `Material` varchar(25) DEFAULT NULL,
  `Keterangan` varchar(255) DEFAULT NULL,
  `CreatedBy` varchar(10) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(10) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`IDMaterial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Ms_Propinsi
# ------------------------------------------------------------

CREATE TABLE `Ms_Propinsi` (
  `ID` varchar(3) NOT NULL DEFAULT '',
  `Propinsi` varchar(20) DEFAULT NULL,
  `Keterangan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Ms_Role
# ------------------------------------------------------------

CREATE TABLE `Ms_Role` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(50) DEFAULT NULL,
  `Permission` text,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Ms_Status
# ------------------------------------------------------------

CREATE TABLE `Ms_Status` (
  `Id_status` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(20) DEFAULT NULL,
  `Ket1` varchar(50) DEFAULT NULL,
  `Ket2` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Ms_UnitKerja
# ------------------------------------------------------------

CREATE TABLE `Ms_UnitKerja` (
  `CostCenter` varchar(10) NOT NULL DEFAULT '',
  `UnitKerja` varchar(30) DEFAULT NULL,
  `KepalaUnitKerja` varchar(10) DEFAULT NULL,
  `Keterangan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`CostCenter`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Ms_User
# ------------------------------------------------------------

CREATE TABLE `Ms_User` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `RoleID` varchar(100) DEFAULT '',
  `LastLogin` datetime DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Type` int(11) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `IDCustomer` varchar(10) DEFAULT NULL,
  `IDVendor` varchar(10) DEFAULT NULL,
  `IDKaryawan` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Ms_Vendor
# ------------------------------------------------------------

CREATE TABLE `Ms_Vendor` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDVendor` varchar(10) DEFAULT NULL,
  `Vendor` varchar(50) DEFAULT NULL,
  `Keterangan` varchar(255) DEFAULT NULL,
  `Aktif` tinyint(4) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `CreatedBy` varchar(10) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(10) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `Alamat1` varchar(200) DEFAULT NULL,
  `Alamat2` varchar(200) DEFAULT NULL,
  `Kota` varchar(20) DEFAULT NULL,
  `Propinsi` varchar(30) DEFAULT NULL,
  `Country` varchar(20) DEFAULT NULL,
  `Phone` varchar(20) DEFAULT NULL,
  `KodePos` varchar(10) DEFAULT NULL,
  `IsWAPU` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table News
# ------------------------------------------------------------

CREATE TABLE `News` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Judul` varchar(100) DEFAULT NULL,
  `Berita` text,
  `ImageURL` varchar(255) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `CreatedBy` varchar(10) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Panduan
# ------------------------------------------------------------

CREATE TABLE `Panduan` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Judul` varchar(100) DEFAULT NULL,
  `Deskripsi` varchar(255) DEFAULT NULL,
  `FilePath` varchar(255) DEFAULT NULL,
  `Keterangan` varchar(50) DEFAULT NULL,
  `Aktif` int(11) DEFAULT NULL,
  `CreatedBy` varchar(10) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(10) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table PayGen
# ------------------------------------------------------------

CREATE TABLE `PayGen` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `NoDokumen` varchar(10) DEFAULT NULL,
  `IDVendor` varchar(10) DEFAULT NULL,
  `Tahun` varchar(4) DEFAULT NULL,
  `TglClearing` date DEFAULT NULL,
  `NoClearing` varchar(10) DEFAULT NULL,
  `RekeningTujuan` varchar(20) DEFAULT NULL,
  `NomorRekening` varchar(20) DEFAULT NULL,
  `CreatedBy` varchar(10) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `Amount` float DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Pesan
# ------------------------------------------------------------

CREATE TABLE `Pesan` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDRekanan` varchar(10) DEFAULT NULL,
  `Judul` varchar(80) DEFAULT NULL,
  `Pesan` varchar(255) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Keterangan` varchar(50) DEFAULT NULL,
  `IsBoardcast` tinyint(4) DEFAULT NULL,
  `CreatedBy` varchar(10) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `IsCustomer` tinyint(4) DEFAULT NULL,
  `IsVendor` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table PotonganInvoice
# ------------------------------------------------------------

CREATE TABLE `PotonganInvoice` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDInvoice` int(11) DEFAULT NULL,
  `IDPotongan` tinyint(4) DEFAULT NULL,
  `Nominal` decimal(18,0) DEFAULT NULL,
  `Keterangan` varchar(50) DEFAULT NULL,
  `CreatedBy` varchar(10) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table RelatedInvoice
# ------------------------------------------------------------

CREATE TABLE `RelatedInvoice` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDInvoice` int(11) DEFAULT NULL,
  `NoPO` varchar(10) DEFAULT NULL,
  `NilaiPO` int(11) DEFAULT NULL,
  `KursPO` varchar(5) DEFAULT NULL,
  `DeskripsiPO` varchar(100) DEFAULT NULL,
  `NomorBA` varchar(30) DEFAULT NULL,
  `TanggalBA` date DEFAULT NULL,
  `NoSAGR` varchar(10) DEFAULT NULL,
  `TglSAGR` date DEFAULT NULL,
  `NomorKontrak` varchar(30) DEFAULT NULL,
  `NilaiKontrak` decimal(18,0) DEFAULT NULL,
  `KursKontrak` char(3) DEFAULT NULL,
  `Keterangan` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table SalesOrder
# ------------------------------------------------------------

CREATE TABLE `SalesOrder` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `NoSO` varchar(10) DEFAULT NULL,
  `NoBillingDoc` varchar(10) DEFAULT NULL,
  `Incoterm` varchar(20) DEFAULT NULL,
  `Incoterm2` varchar(50) DEFAULT NULL,
  `AlatAngkut` varchar(50) DEFAULT NULL,
  `TglPelayaran` date DEFAULT NULL,
  `Tujuan` varchar(50) DEFAULT NULL,
  `TempatMuat` varchar(50) DEFAULT NULL,
  `ProductDetail` varchar(50) DEFAULT NULL,
  `Status` char(2) DEFAULT NULL,
  `NoPEB` varchar(20) DEFAULT NULL,
  `TglPEB` date DEFAULT NULL,
  `HargaSatuan` decimal(18,0) DEFAULT NULL,
  `NoFakturPajak` varchar(25) DEFAULT NULL,
  `DPP` decimal(18,0) DEFAULT NULL,
  `TaxAmount` decimal(18,0) DEFAULT NULL,
  `PaymentMethod` varchar(1) DEFAULT NULL,
  `TglFakturPajak` date DEFAULT NULL,
  `IDMaterial` int(11) DEFAULT NULL,
  `NetValueBeforePPH2` int(11) DEFAULT NULL,
  `BillT` varchar(10) DEFAULT NULL,
  `FPOption` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Status
# ------------------------------------------------------------

CREATE TABLE `Status` (
  `ID` char(2) NOT NULL DEFAULT '',
  `Status` varchar(20) DEFAULT NULL,
  `Kategori` tinyint(4) DEFAULT NULL,
  `Sorting` tinyint(4) DEFAULT NULL,
  `Keterangan` varchar(50) DEFAULT NULL,
  `AppText` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Temp_BillDocDetail
# ------------------------------------------------------------

CREATE TABLE `Temp_BillDocDetail` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDBillDocHeader` int(11) DEFAULT NULL,
  `IsValid` tinyint(11) DEFAULT NULL,
  `NoBillingDoc` varchar(10) DEFAULT NULL,
  `JenisTransaksi` smallint(6) DEFAULT NULL,
  `Sektor` varchar(5) DEFAULT NULL,
  `BillingDate` date DEFAULT NULL,
  `NoSO` varchar(10) DEFAULT NULL,
  `NoGI` varchar(10) DEFAULT NULL,
  `IDCustomer` varchar(10) DEFAULT NULL,
  `Customer` varchar(30) DEFAULT NULL,
  `Status` char(2) DEFAULT NULL,
  `IDMaterial` varchar(10) DEFAULT NULL,
  `Material` varchar(100) DEFAULT '',
  `Kuantum` decimal(8,0) DEFAULT NULL,
  `UoM` varchar(7) DEFAULT NULL,
  `Kurs` varchar(3) DEFAULT NULL,
  `Keterangan` varchar(255) DEFAULT NULL,
  `IDPayer` varchar(10) DEFAULT NULL,
  `Payer` varchar(30) DEFAULT NULL,
  `IDSalesOrg` varchar(10) DEFAULT NULL,
  `SalesOrg` varchar(30) DEFAULT NULL,
  `LifeCycle` tinyint(4) DEFAULT NULL,
  `LastDownload` datetime DEFAULT NULL,
  `NoBAST` varchar(20) DEFAULT NULL,
  `NoRegSurveyor` varchar(20) DEFAULT NULL,
  `NoSKBDN` varchar(20) DEFAULT NULL,
  `Bank` varchar(20) DEFAULT NULL,
  `TglSKBDN` date DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Temp_BillDocHeader
# ------------------------------------------------------------

CREATE TABLE `Temp_BillDocHeader` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CreatedBy` varchar(10) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(10) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `PostedBy` varchar(10) DEFAULT '',
  `PostedOn` datetime DEFAULT NULL,
  `CanceledBy` varchar(10) DEFAULT NULL,
  `CanceledOn` datetime DEFAULT NULL,
  `Keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Temp_CancelBillDoc
# ------------------------------------------------------------

CREATE TABLE `Temp_CancelBillDoc` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `NoBillingDoc` varchar(10) DEFAULT '',
  `CreatedBy` varchar(10) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Temp_CancelSalesOrder
# ------------------------------------------------------------

CREATE TABLE `Temp_CancelSalesOrder` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `NoSO` varchar(10) DEFAULT '',
  `CreatedBy` varchar(10) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Temp_ListPiutang
# ------------------------------------------------------------

CREATE TABLE `Temp_ListPiutang` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDCustomer` varchar(10) DEFAULT NULL,
  `ARDocNo` varchar(10) DEFAULT NULL,
  `NoBillingDoc` varchar(10) DEFAULT NULL,
  `NoSO` varchar(10) DEFAULT '',
  `DocumentDate` date DEFAULT NULL,
  `PaymentMethod` varchar(2) DEFAULT '',
  `Deskripsi` varchar(100) DEFAULT NULL,
  `AmountDC` decimal(18,0) DEFAULT NULL,
  `KursDC` varchar(3) DEFAULT NULL,
  `AmountLC` decimal(18,0) DEFAULT NULL,
  `KursLC` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Temp_Paygen
# ------------------------------------------------------------

CREATE TABLE `Temp_Paygen` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `NoDokumen` varchar(10) DEFAULT NULL,
  `IDVendor` varchar(10) DEFAULT NULL,
  `Tahun` varchar(4) DEFAULT NULL,
  `TglClearing` date DEFAULT NULL,
  `NoClearing` varchar(10) DEFAULT NULL,
  `CreatedBy` varchar(10) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `IsMatch` tinyint(4) DEFAULT NULL,
  `RekeningTujuan` varchar(20) DEFAULT NULL,
  `NomorRekening` varchar(20) DEFAULT NULL,
  `Amount` float DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Temp_SalesdocDetail
# ------------------------------------------------------------

CREATE TABLE `Temp_SalesdocDetail` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IsValid` tinyint(4) DEFAULT NULL,
  `NoSO` varchar(10) DEFAULT NULL,
  `NoBillingDoc` varchar(10) DEFAULT NULL,
  `Incoterm` varchar(20) DEFAULT NULL,
  `Incoterm2` varchar(50) DEFAULT NULL,
  `AlatAngkut` varchar(50) DEFAULT NULL,
  `TglPelayaran` date DEFAULT NULL,
  `Tujuan` varchar(50) DEFAULT NULL,
  `TempatMuat` varchar(50) DEFAULT NULL,
  `ProductDetail` varchar(50) DEFAULT NULL,
  `Status` char(2) DEFAULT NULL,
  `NoPEB` varchar(20) DEFAULT NULL,
  `TglPEB` date DEFAULT NULL,
  `HargaSatuan` decimal(18,0) DEFAULT NULL,
  `NoFakturPajak` varchar(25) DEFAULT NULL,
  `DPP` decimal(18,0) DEFAULT NULL,
  `TaxAmount` decimal(18,0) DEFAULT NULL,
  `PaymentMethod` varchar(1) DEFAULT NULL,
  `TglFakturPajak` date DEFAULT NULL,
  `IDMaterial` int(11) DEFAULT NULL,
  `NetValueBeforePPH2` int(11) DEFAULT NULL,
  `BillT` varchar(10) DEFAULT '',
  `FPOption` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Temp_SalesdocHeader
# ------------------------------------------------------------

CREATE TABLE `Temp_SalesdocHeader` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CreatedBy` varchar(10) DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL,
  `UpdatedBy` varchar(10) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `PostedBy` varchar(10) DEFAULT '',
  `PostedOn` datetime DEFAULT NULL,
  `CanceledBy` varchar(10) DEFAULT NULL,
  `CanceledOn` datetime DEFAULT NULL,
  `Keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table Users_Session
# ------------------------------------------------------------

CREATE TABLE `Users_Session` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IDUSER` int(11) DEFAULT NULL,
  `IDDEVICE` varchar(100) DEFAULT NULL,
  `DeviceModel` varchar(100) DEFAULT NULL,
  `LastLoginDate` datetime DEFAULT NULL,
  `LastLogoutDate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table VL_Action
# ------------------------------------------------------------

CREATE TABLE `VL_Action` (
  `ID` char(2) NOT NULL DEFAULT '',
  `Action` varchar(20) DEFAULT NULL,
  `Keterangan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table VL_AlatAngkut
# ------------------------------------------------------------

CREATE TABLE `VL_AlatAngkut` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `AlatAngkut` varchar(20) DEFAULT NULL,
  `Keterangan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table VL_Incoterm
# ------------------------------------------------------------

CREATE TABLE `VL_Incoterm` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Incoterm` varchar(5) DEFAULT NULL,
  `Keterangan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table VL_JenisTransaksi
# ------------------------------------------------------------

CREATE TABLE `VL_JenisTransaksi` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `JenisTransaksi` varchar(20) DEFAULT NULL,
  `Keterangan` varchar(255) DEFAULT NULL,
  `Kategori` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table VL_Kurs
# ------------------------------------------------------------

CREATE TABLE `VL_Kurs` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Kurs` varchar(5) DEFAULT NULL,
  `Nama` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table VL_PaymentTerm
# ------------------------------------------------------------

CREATE TABLE `VL_PaymentTerm` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PaymentTerm` varchar(4) DEFAULT NULL,
  `JumlahHari` decimal(18,0) DEFAULT NULL,
  `Keterangan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table VL_PotonganInvoice
# ------------------------------------------------------------

CREATE TABLE `VL_PotonganInvoice` (
  `ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Potongan` varchar(15) DEFAULT NULL,
  `Keterangan` varchar(50) DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
