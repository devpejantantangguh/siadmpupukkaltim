CREATE view vw_billdoc_v3 as
select
    b.ID  AS ID,
    b.NoBillingDoc AS NoBillingDoc,
    b.IDCustomer AS IDCustomer,
    b.Customer AS Customer,
    c.IsWAPU,
    b.BillingDate AS BillingDate,
    b.NoSO AS NoSO,
    b.IDPayer AS IDPayer,
    b.Kurs AS Kurs,
    b.IDMaterial AS IDMaterial,
    b.Material AS Material,
    b.UoM AS UoM,
    b.Kuantum AS Kuantum,
    b.Status AS "Status",
    s.HargaSatuan AS HargaSatuan,
    s.TglFakturPajak AS TglFakturPajak,
    s.NoFakturPajak AS NoFakturPajak,
    s.TaxAmount AS TaxAmount,
--     (SELECT SUM(nv.NetValueBeforePPH2) FROM salesorder nv where nv.NoBillingDoc = new_billdoc.NoBillingDoc) as TotalNilai,
    s.BillT AS BillT,
    s.FPOption AS FPOption,
    s.PaymentMethod,
--     (SELECT count(*) FROM historybilldoc h WHERE (b.NoBillingDoc = h.NoBillDoc)) AS ApproveCount,
--     (SELECT count(*) from billingdocument where (billingdocument.NoBillingDoc = new_billdoc.NoBillingDoc)) AS ItemCount,
    dp1.FilePath AS SPP_Path,
    dp1.ApprovedBy AS SPP,
    dp2.FilePath AS Bukpot_Path,
    dp2.ApprovedBy AS Bukpot

from (select min(ID) as ID, (NoBillingDoc) from billingdocument group by NoBillingDoc) as new_billdoc
join (select min(ID) as ID, NoSO,NoBillingDoc from salesorder group by NoSO,NoBillingDoc) as new_salesorder on new_billdoc.NoBillingDoc = new_salesorder.NoBillingDoc

join salesorder s on s.ID = new_salesorder.ID
join billingdocument b on b.ID = new_billdoc.ID

left join dokumenpajak dp1 on b.NoBillingDoc = dp1.IDParent and dp1.Tipe = 1
left join dokumenpajak dp2 on b.NoBillingDoc = dp2.IDParent and dp2.Tipe = 2
left join ms_customer c on c.IDCustomer = b.IDCustomer;
