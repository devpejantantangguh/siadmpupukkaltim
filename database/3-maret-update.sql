create table Invoice
(
    ID int identity
        primary key,
    NoDokumen varchar(10),
    JenisTransaksi smallint,
    IDVendor varchar(10),
    NamaVendor varchar(50),
    NoInvoice varchar(30),
    NilaiInvoice decimal(18),
    PPN bit,
    KursInvoice char(3),
    NoFakturPajak varchar(25),
    TglFakturPajak date,
    AmountPaid decimal(18),
    NoTransmittal varchar(20),
    TglTransmittal date,
    Status smallint,
    LifeCycle bit,
    Piutang bit,
    Tahun varchar(4),
    PaymentTerm varchar(4),
    NoDokumenSAP varchar(10),
    TglEstimasiBayar date,
    PaidAmount decimal(18),
    KursPaid varchar(3),
    IsPriority bit,
    Matriks varchar(100),
    TglInvoice date
)
go

create table RelatedInvoice
(
    ID int identity primary key,
    IDInvoice int,
    NoPO varchar(10),
    NilaiPO int,
    KursPO varchar(5),
    DeskripsiPO varchar(100),
    NomorBA varchar(30),
    TanggalBA date,
    NoSAGR varchar(10),
    TglSAGR date,
    NomorKontrak varchar(30),
    NilaiKontrak decimal(18) default 0,
    KursKontrak char(3),
    Keterangan varchar(100)
)
go

create unique index RelatedInvoice_ID_uindex
    on RelatedInvoice (ID)
go

create table VL_Kurs
(
    ID int identity
        primary key,
    Kurs varchar(5) not null,
    Nama varchar(20)
)
go

create table AppliedDocument
(
    IDInvoice int,
    IDCustomer varchar(10),
    ARDocNo varchar(10),
    ARAmount decimal(9),
    AppliedBy varchar(10),
    AppliedOn datetime2,
    UpdatedBy varchar(10),
    UpdatedOn datetime2,
    AppliedAmount decimal(9),
    KursApplied varchar(3),
    Kurs char(3),
    ID int identity
        constraint AppliedDocument_ID_pk
            primary key
)
go

create table Attachment
(
    IDParent int,
    Kategori int,
    FilePath varchar(255),
    Judul varchar(50),
    UploadedBy varchar(10),
    UploadedOn datetime,
    ID int identity
)
go

create table BillingDocument
(
    ID int,
    NoBillingDoc varchar(10),
    JenisTransaksi smallint,
    Sektor varchar(5),
    BillingDate date,
    NoSO varchar(10),
    NoGI varchar(10),
    NoFakturPajak varchar(25),
    IDCustomer varchar(10),
    Customer varchar(30),
    Status char(2),
    IDMaterial varchar(10),
    Material varchar(30),
    Kuantum decimal(18),
    UoM varchar(7),
    Kurs varchar(3),
    HargaSatuan decimal(18),
    Jumlah decimal(18),
    DPP decimal(18),
    TaxAmount decimal(18),
    Total decimal(18),
    Keterangan varchar(255),
    IDPayer varchar(10),
    Payer varchar(30),
    IDSalesOrg varchar(10),
    SalesOrg varchar(30),
    LifeCycle bit,
    LastDownload datetime,
    PaymentMethod varchar(1)
)
go

create table DokumenFinansial
(
    IDKasBesar int,
    NoDokumen varchar(30),
    Keterangan varchar(50),
    Deskripsi varchar(50),
    ID int identity
        constraint DokumenFinansial_ID_pk
            primary key
)
go

create table DokumenPajak
(
    IDParent int,
    Kategori char(1),
    Tipe char(1),
    Judul varchar(50),
    FilePath varchar(255),
    CreatedBy varchar(10),
    CreatedOn datetime,
    ApprovedBy varchar(10),
    ApprovedOn datetime
)
go

create table GlobalSettings
(
    Setting varchar(50) not null,
    Value varchar(30)
)
go

create table HistoryBillDoc
(
    NoBillDoc varchar(10),
    Action char(2),
    UserID varchar(10),
    Timestamp datetime,
    Keterangan varchar(255)
)
go

create table HistoryInvoice
(
    IDInvoice varchar(10),
    Action char(2),
    UserID varchar(10),
    Timestamp datetime,
    Keterangan varchar(255)
)
go

create table Jaminan
(
    IDJaminan int not null
        constraint PK__Jeminan__7B1984E571DD56AD
            primary key,
    IDInvoice int,
    IDVendor varchar(10),
    NomorPO varchar(10),
    JenisJaminan varchar(25),
    NomorJaminan varchar(30),
    BankPenerbit varchar(20),
    NilaiJaminan int,
    KursJaminan char(3),
    TanggalJaminan date,
    Keterangan varchar(255)
)
go

create table MatriksDokumen
(
    JenisTransaksi smallint,
    Dokumen varchar(20),
    IsRequired bit,
    Keterangan varchar(50),
    ID int identity
)
go

create table Ms_Country
(
    ID varchar(3),
    Country varchar(20),
    Keterangan varchar(50)
)
go

create table Ms_Customer
(
    IDCustomer varchar(10) not null
        constraint Ms_Customer_IDCustomer_pk
            primary key,
    PairedVendorID varchar(10),
    NPWP varchar(30),
    Customer varchar(50),
    IsWAPU bit,
    Alamat1 varchar(200),
    Alamat2 varchar(200),
    Kota varchar(20),
    Propinsi varchar(30),
    Country varchar(20),
    Email varchar(20),
    Phone varchar(20),
    KodePos varchar(10),
    Keterangan varchar(50),
    CreatedBy varchar(10),
    CreatedOn datetime,
    UpdatedBy varchar(10),
    UpdatedOn datetime
)
go

create table Ms_Karyawan
(
    ID varchar(10),
    Nama varchar(100),
    CostCenter varchar(100),
    Keterangan varchar(50),
    NPKLama varchar(7)
)
go

create table Ms_Kota
(
    ID varchar(3),
    IDPropinsi varchar(3),
    Kota varchar(20),
    Keterangan varchar(50)
)
go

create table Ms_Material
(
    IDMaterial varchar(7),
    Material varchar(25),
    Keterangan varchar(255),
    CreatedBy varchar(10),
    CreatedOn datetime,
    UpdatedBy varchar(10),
    UpdatedOn datetime
)
go

create table Ms_Propinsi
(
    ID varchar(3),
    Propinsi varchar(20),
    Keterangan varchar(50)
)
go

create table Ms_UnitKerja
(
    CostCenter varchar(10),
    UnitKerja varchar(30),
    KepalaUnitKerja varchar(10),
    Keterangan varchar(50)
)
go

create table Ms_Vendor
(
    IDVendor varchar(10) not null
        constraint PK__Ms_Vendo__8914FFB2D619B355
            primary key,
    Vendor varchar(50),
    Keterangan varchar(255),
    Aktif binary(1),
    Email varchar(50),
    CreatedBy varchar(10),
    CreatedOn datetime2,
    UpdatedBy varchar(10),
    UpdatedOn datetime2,
    Alamat1 varchar(200),
    Alamat2 varchar(200),
    Kota varchar(20),
    Propinsi varchar(30),
    Country varchar(20),
    Phone varchar(20),
    KodePos varchar(10),
    IsWAPU bit
)
go

create table Panduan
(
    ID smallint,
    Judul varchar(100),
    Deskripsi varchar(255),
    FilePath varchar(255),
    Keterangan varchar(50),
    Aktif bit,
    CreatedBy varchar(10),
    CreatedOn datetime,
    UpdatedBy varchar(10),
    UpdatedOn datetime
)
go

create table PayGen
(
    NoDokumen varchar(10) not null
        constraint PK__PayGen__91C471B703E22CF4
            primary key,
    IDVendor varchar(10),
    Tahun varchar(4),
    TglClearing date,
    NoClearing varchar(10),
    RekeningTujuan varchar(20) not null,
    NomorRekening varchar(20) not null,
    CreatedBy varchar(10),
    CreatedOn datetime,
    Amount decimal(18)
)
go

create table Pesan
(
    ID smallint,
    IDRekanan varchar(10),
    Judul varchar(80),
    Pesan varchar(255),
    StartDate date,
    EndDate date,
    Keterangan varchar(50),
    IsBoardcast bit,
    CreatedBy varchar(10),
    CreatedOn datetime,
    IsCustomer bit,
    IsVendor bit
)
go

create table SalesOrder
(
    ID int,
    NoSO varchar(10),
    NoBillingDoc varchar(255),
    Incoterm varchar(5),
    AlatAngkut varchar(15),
    TglPelayaran date,
    Tujuan varchar(20),
    TempatMuat varchar(20),
    NoBAST varchar(20),
    NoRegSurveyor varchar(20),
    NoSKBDN varchar(20),
    ProductDetail varchar(50),
    Bank varchar(20),
    Status char(2),
    TglSKBDN date
)
go

create table Status
(
    ID int,
    Status varchar(20),
    Kategori smallint,
    Sorting smallint,
    Keterangan varchar(50),
    AppText varchar(50)
)
go

create table Temp_BillDocHeader
(
    CreatedBy varchar(10),
    CreatedOn datetime,
    UpdatedBy varchar(10),
    UpdatedOn datetime,
    PostedBy varchar(10),
    PostedOn datetime,
    CanceledBy varchar(10),
    CanceledOn datetime,
    Keterangan varchar(255),
    ID int identity
)
go

create table Temp_Paygen
(
    NoDokumen varchar(10) not null
        constraint PK__Temp_pay__91C471B774AE267E
            primary key,
    IDVendor varchar(10),
    Tahun varchar(4),
    TglClearing date,
    NoClearing varchar(10),
    CreatedBy varchar(10),
    CreatedOn datetime,
    IsMatch bit,
    RekeningTujuan varchar(20),
    NomorRekening varchar(20),
    Amount decimal(18)
)
go

create table VL_Action
(
    [ ID ] char(2),
    [ Action ] varchar(15),
    [ Keterangan ] varchar(50)
)
go

create table VL_AlatAngkut
(
    ID smallint,
    AlatAngkut varchar(20),
    Keterangan varchar(50)
)
go

create table VL_Incoterm
(
    ID smallint,
    Incoterm varchar(5),
    Keterangan varchar(50)
)
go

create table VL_JenisTransaksi
(
    ID smallint,
    JenisTransaksi varchar(20),
    Keterangan varchar(255),
    Kategori int
)
go

create table VL_PaymentTerm
(
    ID smallint,
    PaymentTerm varchar(4),
    JumlahHari decimal(18),
    Keterangan varchar(50)
)
go

create table Ms_User
(
    ID varchar(100) not null,
    RoleID varchar(100),
    LastLogin datetime,
    Email varchar(255),
    Type int,
    Password varchar(255),
    IDCustomer varchar(10),
    IDVendor varchar(10)
)
go

create unique index User_Email_uindex
    on Ms_User (Email)
go

create table KasBesar
(
    ID smallint identity
        primary key,
    JenisTransaksi int,
    NPKRequester varchar(10),
    CostCenter varchar(10),
    NPKAtasan varchar(10),
    Nominal decimal(18),
    Deskripsi varchar(255),
    DueDate date,
    Keterangan varchar(255),
    Status int,
    CreatedBy varchar(10),
    CreatedOn datetime,
    UpdatedBy varchar(10),
    UpdatedOn datetime,
    ApprovedBy varchar(10),
    ApprovedOn datetime,
    LifeCycle bit,
    NoPeminjaman varchar(20),
    TanggalPengembalian datetime
)
go

create table Temp_BillDocDetail
(
    IDBillDocHeader int,
    IsValid bit,
    NoBillingDoc varchar(10),
    JenisTransaksi smallint,
    Sektor varchar(5),
    BillingDate date,
    NoSO varchar(10),
    NoFakturPajak varchar(25),
    IDCustomer varchar(10),
    Customer varchar(30),
    Status char(2),
    IDMaterial varchar(10),
    MaterialDesc varchar(30),
    Kuantum decimal(18),
    UoM varchar(7),
    Kurs varchar(3),
    HargaSatuan decimal(18),
    Jumlah decimal(18),
    DPP decimal(18),
    TaxAmount decimal(18),
    Total decimal(25),
    Keterangan varchar(255),
    IDPayer varchar(10),
    Payer varchar(30),
    IDSalesOrg varchar(10),
    SalesOrg varchar(30),
    LifeCycle bit,
    ID int identity
)
go

create table Temp_ListPiutang
(
    IDCustomer varchar(10),
    ARDocNo varchar(10),
    NoBillingDoc varchar(10),
    NoSO varchar(10),
    DocumentDate date,
    PaymentMethod varchar(2),
    Deskripsi varchar(100),
    AmountDC decimal(18),
    KursDC varchar(3),
    AmountLC decimal(18),
    KursLC varchar(3)
)
go

create table ListPiutang
(
    IDCustomer varchar(10),
    ARDocNo varchar(10),
    NoBillingDoc varchar(10),
    NoSO varchar(10),
    DocumentDate date,
    PaymentMethod varchar(2),
    Deskripsi varchar(100),
    AmountDC decimal(18),
    KursDC varchar(3),
    AmountLC decimal(18),
    KursLC varchar(3),
    CreatedBy varchar(10),
    CreatedOn datetime
)
go

create table News
(
    Judul varchar(100),
    Berita varchar(8000),
    ImageURL varchar(300),
    StartDate date,
    EndDate date,
    CreatedBy varchar(10),
    CreatedOn datetime,
    ID smallint identity
        constraint News_ID_pk
            primary key
)
go

create unique index News_ID_uindex
    on News (ID)
go

create table FakturPajak
(
    ID int identity
        primary key,
    FileName varchar(255),
    NPWP_PKT varchar(30),
    NoFakturPajak varchar(25),
    NPWP_Customer varchar(30),
    Timestamp datetime,
    CreatedBy varchar(10),
    CreatedOn datetime,
    UpdatedBy varchar(10),
    UpdatedOn datetime
)
go

